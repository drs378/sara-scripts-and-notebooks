import os

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"

def condtopath(status, iteration=None):
    '''returns a path for the xrd data to be stored and the Calibration PONI files.
     Formatted based on SARA status object. 
    '''
    #read in the status
    xpos, ypos  = status.pos
    Tmax, tau   = status.cond
    prefix      = status.directory
    #modify xpos, ypos, to be signed integers
    if xpos >= 0.:
        xpos = f'+{str(int(xpos)).zfill(2)}'
    else:
        xpos = f'-{str(int(abs(xpos))).zfill(2)}'
    if ypos >= 0.:
        ypos = f'+{str(int(ypos)).zfill(2)}'
    else:
        ypos = f'-{str(int(abs(ypos))).zfill(2)}'
    tau = f'+{str(int(tau)).zfill(5)}'
    Tmax = f'+{str(int(Tmax)).zfill(4)}'
    if iteration is None:
        scan_data = ['chess',xpos,ypos,tau,Tmax]
    else:
        scan_data = [str(iteration).zfill(3),xpos,ypos,tau,Tmax]
    StripeFolder = '_'.join(scan_data)

    dirpath = os.path.join(prefix,f'Stripes/{StripeFolder}/')
    # calibpath = f'{prefix}/Calibration/PONIs/'
    calibpath = os.path.join(prefix,f'Calibration/PONIs/')
    # print(dirpath, calibpath)
    return dirpath, calibpath

def auxpath(status, iteration=None, auxprefix=None):
    """Returns the auxiliary path for writing processed data out to"""
    #read in the status
    xpos, ypos  = status.pos
    Tmax, tau   = status.cond

    #aux prefix necessary
    if auxprefix == None:
        prefix  = status.directory
    else:
        prefix  = auxprefix


    #modify xpos, ypos, to be signed integers
    if xpos >= 0.:
        xpos = f'+{str(int(xpos)).zfill(2)}'
    else:
        xpos = f'-{str(int(abs(xpos))).zfill(2)}'
    if ypos >= 0.:
        ypos = f'+{str(int(ypos)).zfill(2)}'
    else:
        ypos = f'-{str(int(abs(ypos))).zfill(2)}'
    tau = f'+{str(int(tau)).zfill(5)}'
    Tmax = f'+{str(int(Tmax)).zfill(4)}'
    if iteration is None:
        scan_data = ['chess',xpos,ypos,tau,Tmax]
    else:
        scan_data = [str(iteration).zfill(3),xpos,ypos,tau,Tmax]
    StripeFolder = '_'.join(scan_data)

    aux = os.path.join(prefix,f'Stripes/{StripeFolder}/')
    auxcalib = os.path.join(prefix,f'Calibration/{StripeFolder}/')


    return aux, auxcalib

def StripeKeyAndFolder(status,iteration=None):
    xpos, ypos  = status.pos
    Tmax, tau   = status.cond

    #modify xpos, ypos, to be signed integers
    if xpos >= 0.:
        xpos = f'+{str(int(xpos)).zfill(2)}'
    else:
        xpos = f'-{str(int(abs(xpos))).zfill(2)}'
    if ypos >= 0.:
        ypos = f'+{str(int(ypos)).zfill(2)}'
    else:
        ypos = f'-{str(int(abs(ypos))).zfill(2)}'
    tau = f'+{str(int(tau)).zfill(5)}'
    Tmax = f'+{str(int(Tmax)).zfill(4)}'
    if iteration is None:
        scan_data = ['chess',xpos,ypos,tau,Tmax]
    else:
        scan_data = [str(iteration).zfill(3),xpos,ypos,tau,Tmax]
    cond = '_'.join([xpos,ypos,tau,Tmax])
    StripeFolder = '_'.join(scan_data)
    return cond, StripeFolder   

if __name__ == "__main__":
    status = CurrentState()
    status.directory = '/nfs/chess/daq/2021-1/id3b/vandover-937-3/'
    print(condtopath(status))
    print(auxpath(status, auxprefix='/nfs/chess/auxiliary/cycles/2021-1/id3b/'))
