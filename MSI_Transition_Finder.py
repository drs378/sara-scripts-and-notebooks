import numpy as np
import matplotlib.pyplot as plt
from cookb_signalsmooth import *
from edge_finder import *
from SARA_cornell_funcs import *
import PIL
import PIL.Image as Image
import imutils
from skimage.measure import block_reduce
import imageio
import math
#optimization algorithms require some of this
from scipy import optimize, signal
from scipy import ndimage
import cv2

def MSI_Transition_Finder(filepath,mpath,blur=5,Gpromfilt=200,v_cutoff=0,h_cutoff=200,h_thresh=4.0,pixel_size=1.0298619149407369,SpectWindow=True,plotting=True,norm=True,ImgIO=False,Gradnorm=False):
    """Determines the transitions of the HSI images and outputs a metadata
    
    filepath:  (path) location to file OR Image data, see specifier
    mpath:     (path) location to mirror file
    blur:      (int) the amount of blur applied to smooth the image
    Gpromfilt: (int) the threshold value that determines which peaks are accepted in the gradient of the summed image
    v_cutoff   (int) an index value corresponding to the number of pixels shaved off the top and bottom of the image 
    h_cutoff   (int) an index value corresponding to the number of pixels averaged from the left and right of the image as an "unannealed zone"
    h_thresh:  (float) is the number of standard deviations above the amorphous background for peak selection
    pixel_size (float) the size per pixel in microns (um) of the pixels in the image
    plotting:  (bool) displays plots that contain the image, where the transitions occur, the gradient, and which peaks are involved
    norm:      (bool) applys histogram filling of an image to ACB and divides the image by the mirror     
    ImageIO:   (bool) This allows one to use the "filepath" and "mpath" as array inputs for your data
    
    (future edits)
    Could also use symmetry constraints
    
    Returns: (dict) Output dictionary containing transitions and transition finding paramenters.
    
    
    """
    
    TransitionStats = {}
    outputfile = {}
    arr_output = []

    #this is for labeles on plots and key construction
    if ImgIO == False:
        file = glob.glob(filepath)[0]
        filename = os.path.basename(file)[:-4]

        cond = '_'
        s = filename.split('_')[2:]
        s[0] = str(int(s[0]))
        s[1] = str(int(s[1]))
        s.insert(0,'tau')
        s.insert(2,'T')
        cond = cond.join(s)
    else:
        cond = 'filler'
        filename = 'filler'
        file = 'filler'
        
        
    #This is where the imaeg processing starts
    if 'align' not in filename:
        if ImgIO==True:
            img1 = filepath[v_cutoff:(filepath.shape[0]-v_cutoff),:]
            img = cv2.equalizeHist(img1.astype('uint8'))
            img = Image.fromarray(img.astype('uint8'))
            if norm==True:
                bkgd = mpath[v_cutoff:(filepath.shape[0]-v_cutoff),:]
                img = cv2.equalizeHist(((img1/bkgd)/np.max((img1/bkgd))*255).astype('uint8'))
                img = Image.fromarray(img,mode='L')
            else:
                img = cv2.equalizeHist(img1.astype('uint8'))
                img = Image.fromarray(img,mode='L')
                
                
        else:
            img1 = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
            img1 = img1[v_cutoff:(img1.shape[0]-v_cutoff),:]
            
            if norm==True:
                bkgd = np.array(Image.open(mpath,'r'))
                bkgd = bkgd[v_cutoff:(bkgd.shape[0]-v_cutoff),:]

                img = cv2.equalizeHist(((img1/bkgd)/np.max((img1/bkgd))*255).astype('uint8'))
                img = Image.fromarray(img,mode='L')
            else:
                img = cv2.equalizeHist(img1.astype('uint8'))
                img = Image.fromarray(img,mode='L')
            
            
        xpix = np.array(img).shape[1]
        ypix = np.array(img).shape[0]
        
        fp = blur/100


        HSI_1D = smooth(np.sum(img,axis=0),window_len=np.round(fp*img.size[1]).astype(int))      


        slope = (HSI_1D[-2]-HSI_1D[0])/(len(HSI_1D))
        bs_HSI = HSI_1D-(np.arange(len(HSI_1D))*slope+HSI_1D[0])

        
        Grad = np.gradient(bs_HSI)
    
        Grad = np.sqrt(Grad*Grad)
        if Gradnorm==True:
            Grad = Grad/max(Grad)
        
        #statistics for data gathering
        HSI_mean = np.mean(HSI_1D)
        HSI_std = np.std(HSI_1D)
        HSI_contrast = HSI_std/HSI_mean
        
        #removing DC component
        HSI_1D = (HSI_1D-HSI_mean)/HSI_std

        #center finding through correlation
        #using the gradient sum
        correlation = ndimage.correlate(HSI_1D,HSI_1D[::-1],mode='wrap')
        HSI_center = (len(correlation))*0.5 + (np.argmax(correlation)-(len(correlation))*0.5)*0.5
        
        

        # Grad = Grad+abs(min(Grad))
        a_sigma = abs((np.std(Grad[0:h_cutoff])+np.std(Grad[-h_cutoff:]))*0.5)
        a_mu =  abs((np.mean(Grad[0:h_cutoff])+np.mean(Grad[-h_cutoff:]))*0.5)
        Grad = Grad - a_mu


        h_filt = h_thresh*a_sigma
        # print(h_filt)
        #peak finding in the gradient I used an all encompasing sest so I could get the fit values to output
        #same with the height
        HSI_trans = signal.find_peaks(Grad,prominence=Gpromfilt,width=[0.001*xpix,0.25*xpix],height=h_filt)
        

        
        TransitionStats['center'] = HSI_center
        TransitionStats['mean'] = HSI_mean
        TransitionStats['std'] = HSI_std
        TransitionStats['contrast'] = HSI_contrast
        TransitionStats['S/N'] = HSI_mean/HSI_std
        TransitionStats['Gradient'] = Grad
        if ImgIO == False:
            TransitionStats['filepath'] = filepath
        else:
            TransitionStats['filepath'] = 'none'

        pix_siz = pixel_size
        if HSI_contrast>0.025:
            TransitionStats['TR_idx'] = HSI_trans[0]
            TransitionStats['transitions'] = {}
            TransitionStats['transitions']['widths'] = HSI_trans[1]['widths']
            TransitionStats['transitions']['FWHMs'] = signal.peak_widths(Grad,HSI_trans[0],rel_height=0.5)[0]
            TransitionStats['transitions']['heights'] = HSI_trans[1]['peak_heights']
            TransitionStats['transitions']['S/N'] = HSI_trans[1]['peak_heights']/HSI_std
            TransitionStats['transitions']['prominences'] = HSI_trans[1]['prominences']
            TransitionStats['transitions']['distance_from_center'] = (HSI_trans[0]-HSI_center)*pix_siz 
            TransitionStats['StripeCenterDistFromImageCenter']=(HSI_center-int(0.5*img.size[0]))*pix_siz
            TransitionStats['horizontal FOV']=xpix*pix_siz
            TransitionStats['vertical FOV']=ypix*pix_siz
            TransitionStats['LSA_condition']=cond
        else:
            TransitionStats['transitions']='none'
            
        outputfile=TransitionStats
        
        
        if plotting == True:
            plt.figure()
            plt.title('Anneal conditions '+cond)
            plt.imshow(img,cmap='Greys',aspect='auto')
            plt.colorbar()
            if HSI_contrast>0.025:
                for tr in HSI_trans[0]:
                    plt.plot(tr*np.ones(int(img.size[1]/2)),np.arange(int(img.size[1]/2)),c='gold')
            plt.axvline(x=HSI_center,ymin=0.5,ymax=1,c='darkorchid',linestyle=':')
            plt.show()
            
#             plt.figure()
#             plt.title('sum')
#             plt.plot(np.sum(img1,axis=0),'b')
#             plt.plot(np.sum(bkgd,axis=0),'r')
#             plt.plot(np.sum(img,axis=0),'g')
#             plt.show()
            
            
            plt.figure()
            plt.title('Gradient plot')
            plt.plot(Grad,'r')
            if HSI_contrast>0.025:
                for tr in HSI_trans[0]:
                    plt.axvline(x=tr,ymax=1,ymin=0.5,c='gold')
                plt.axvline(x=HSI_center,c='orchid',linestyle=':')
            plt.show()
            
        return outputfile