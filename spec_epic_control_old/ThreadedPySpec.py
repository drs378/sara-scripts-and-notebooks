import sys
sys.path.insert(1, '/home/duncansutherland/sara-socket-client/CHESS2021')
sys.path.insert(1, '/home/duncansutherland/sara-socket-client/Scripts')
sys.path.insert(1, '/home/duncansutherland/sara-socket-client')
sys.path.insert(1, '/home/duncansutherland/sara-scripts-and-notebooks')
import numpy as np
import pyspec.client.SpecConnectionsManager as SCM
import time
# sys.path.insert(1,'/home/duncan/Documents/')
from CondtoPath import condtopath
from pyspec.css_logger import log
from threading import Thread
import laser as LSA_Laser
import socket_clients as sc

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"


class PySpecAgent():
    """
    This class will interact with the spec server on the hutch IDB3 computer and allow SARA pass the appropriate commands

    ConnectToServer()                 Creates the spec connection from sara to the spec server
    SetFilePaths(path,filename2D)     Creates the directory structure and moves to the proper location. sends newfile cmd to spec
    SetFlyScan()                     Sets up flyscan cmd and arms the detector for XRD collection

    """
    def __init__(self, address='id3b.classe.cornell.edu', port='spec'):
        self.address = address
        self.port = port
        self.connected = False        
        self.output = []
        self.last_output = None
    def _update_output(self, value, channel):
        self.output.append(value)
        self.last_output = self.output[-1]
        print(self.last_output)
        

    def _update_channel(self, value, channel):
        print(f"{channel}: {value}")


    def ConnectToServer(self):
        '''
        Connect to the external spec server at connection name conn.
        This should be an object that can be passed to the other functions that talk to a spec server
        '''
        self.conn = f'{self.address}:{self.port}'
        print('')
        print(self.conn)

        self.Spec = SCM.SpecConnection.SpecConnection(self.conn) #hard coded connection
        while not self.Spec.is_connected():
            pass

        if self.Spec.is_connected():
            print(f'established connection to {self.conn}')
        
        #need to register our desired channels:
        self.Spec.register('status/ready',self._update_channel) #this channel can be read as if spec is busy or not??
        self.Spec.register('output/tty',self._update_output) #this channel returns the output from the cmd line and passes it into the output list
        print('List of registered channels')
        for ch in list(self.Spec.reg_channels):
            print(ch)
        print("")
        return

    def SetFilePaths(self, path, filename2D):
        '''
        SetFilePaths will create the file system and change to the directory where the current raw data willbe stored
        The commands passed into the spec server are:
            mkdir -p path/to/2d/dir/
            cd path/to/2d/dir/
            newfile 2dfilename
        '''
        #make the directory structure
        self.makedirs_cmd = f'u mkdir -p {path}'
        #change into
        self.cd_cmd = f'cd {path}'
        #set newfile
        self.nf_cmd = f'newfile {filename2D}'
        #pass newfile to detector
        self.eig_setdir_cmd = 'eig_setdir'
        
        #execute
        self.Spec.run_cmd(self.makedirs_cmd)
        self.Spec.run_cmd(self.cd_cmd)
        self.Spec.run_cmd(self.nf_cmd)        
        self.Spec.run_cmd(self.eig_setdir_cmd)
        return

    def SetFlyScan(self, nframes=151, det_int_time=0.05):
        '''
        Instantiate the flyscan macro and arm the detector for a XRD measurement
        '''

        self.flyscan_cmd = f'flyscan {nframes} {det_int_time}' 
        #print(nframes*det_int_time*2)
        self.Spec.run_cmd(self.flyscan_cmd, timeout=60)
        print('Detector Armed')
        return

    def GetTimeToFill(self):
        """Returns the time in seconds until beam refill at chess"""
        self.queryTTF_cmd = 'epics_get(cesr_run_left)'
        self.Spec.run_cmd(self.queryTTF_cmd)
        return output[-1]

    def cd_spec(self, path):
        """ A generic mv command. Moves the Spec directory to the specified path
        """

        self.mv_cmd = f'cd {path}'
        self.Spec.run_cmd(self.mv_cmd)
        return

    def mkdir_spec(self, dirpath):
        """A generic mkdir command. makes the specified directory or set of directories """

        self.mkdir_cmd = f'u mkdir {dirpath}'
        self.Spec.run_cmd(self.mkdir_cmd)
        return

    def round_b(self, x, base):
          return base * round(x/base)

    def test_func(self, nframes=151, det_int_time=0.05, path=None, filename2D=None):

        self.makedirs_cmd = f'u mkdir -p {path}'
        self.cd_cmd = f'cd {path}'
        self.nf_cmd = f'newfile {filename2D}'

        self.flyscan_cmd = f'flyscan {nframes} {det_int_time}' 

        #list o commands
        self.Spec.run_cmd(self.makedirs_cmd)
        self.Spec.run_cmd(self.cd_cmd)
        self.Spec.run_cmd(self.nf_cmd)
        self.Spec.run_cmd(self.flyscan_cmd)
        
#        self.Spec.run_cmd(self.makedirs_cmd)
#        self.Spec.run_cmd(self.cd_cmd)
#        self.Spec.run_cmd(self.nf_cmd)
#necessary to send the queue of cmds to the spec server. If this is suppressed, it will not send anything        
        # time.sleep(0.05)


if __name__ == "__main__":
    #Laser stuff
    address =  "CHESS"
    socket_list = ["lasgo"]
    socket_clients = sc.socket_clients()
    clients = socket_clients.get_clients(address, socket_list = socket_list)
    laser = LSA_Laser.laser()
    laser.socket_clients = socket_clients
    
    #This is address info for IDB3 hutch computer which is where it will be running the Spec server
    address = 'id3b.classe.cornell.edu'
    port = 'spec'

    #filepath naming convention for the DAQ and 2021 CHESS run

    DAQfs = '/nfs/chess/daq/2021-1/id3b/vandover-test/'    #working directory
    MatSysName = '01_21F7_Bi2O3'                #base MatSysName
    Stripe_iteration = 114                         #iteration number


    #Read the current state of the lasgo agent 
    status = CurrentState()

    #set the parent DAQ directory
    status.directory = DAQfs+MatSysName

    #test the file scheme generation and test stripes
    test_conds = [
        [(-18.6,39.2),     (900,1400)],
        [(42.,-15.),    (1150,900)],
        [(-5.6,0.),        (10000,650)]
    ]
    psa = PySpecAgent()
    psa.ConnectToServer()
    for idx, conds in enumerate(test_conds):
        Sidx = Stripe_iteration + idx
        status.pos = conds[0]
        status.cond = conds[1]

        dirpath, calibpath = condtopath(status)
        print(dirpath)
        print('')
        # print(calibpath)

    #instantiate the pyspec agent and set the file system and flyscan parameters

    
    print("Is it armed?", "Armed" in psa.Spec.reg_channels['output/tty'].read())
    time.sleep(4)
    psa.SetFilePaths(path=dirpath, filename2D='frame')
    process = Thread(target=psa.SetFlyScan, args=[111, 0.05])
    process.start()
    while "Armed" not in psa.Spec.reg_channels['output/tty'].read():
        print("Calling status", psa.Spec.reg_channels['output/tty'].read())
        print("Not ready yet")
    start = [0.,0.]
    end = [10.,0]
    int_time = 50.
    nframes = 111
    laser.execute_flyscan(start, end, int_time, nframes)
    print("Calling status", psa.Spec.reg_channels['status/ready'].read())
    print("Waiting for my thread to finish")
    process.join()
    print("The PySpec thread has finished")
    
##    psa.SetFlyScan(nframes=11, det_int_time=0.5)
##    ttf = psa.GetTimeToFill()
##    print(ttf)




    # import matplotlib.pyplot as plt
    # print('')
    # frames = []
    # inttime = []
    # for i in range(10):
    #     t1 = time.time()
    #     psa.SetFlyScan(nframes=i, det_int_time=1)
    #     print('')
    #     t2 = time.time()
    #     frames.append(t2-t1)

    # for i in range(10):
    #     t1 = time.time()
    #     psa.SetFlyScan(nframes=1, det_int_time=i)
    #     print('')
    #     t2 = time.time()
    #     inttime.append(t2-t1)
    #     #while not psa.Spec.reg_channels['status/ready']:
    #      #   print('spec is busy')

    # plt.plot(range(10), frames, c='r',label='# of frames' )
    # plt.plot(range(10), inttime, c='b',label='seconds')
    # plt.xlabel('frame number or det int time in s')
    # plt.ylabel('time in seconds')
    # plt.show()
    # file = open('/home/duncansutherland/Desktop/overhead.txt', 'w')
    # file.write(frames)
    # file.write(inttime)
    # file.close()
        #print(psa.output)

    # psa.test_func(nframes=1,det_int_time=5,path=dirpath,filename2D='frame')
