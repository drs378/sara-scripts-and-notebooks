import pyspec.client.SpecConnectionsManager as SCM
import pyspec.client.SpecReply as SR
import time
from pyspec.css_logger import log

sc = SCM.SpecConnection.SpecConnection('id3b.classe.cornell.edu:spec')
while not sc.is_connected():
    pass
log.start()
sed = SCM.SpecEventsDispatcher
scm = SCM.SpecConnectionsManager()

print(sc.is_connected())
#An attempt to register channels. The channel name is something I can add but the receiver slot is unknown to me.
cmd_line = []
cmd_out_ch = sc.registerChannel('output/tty',cmd_line)

status_ch = sc.get_channel('status/ready')
print(f'status channel{status_ch.read()}')

#Can use the SpecConnection object to pass commands and request a return but we need the receiver object
sr = SR.SpecReply()
cmd = 'pwd' # this is a spec innert command that has a return value that we should see


print('sending cmd')
cmd_id = sc.send_msg_cmd_with_return(cmd,sr)
print(f'cmd output from pwd {cmd_out_ch}')

print(cmd_id)
#My attempt at storing the recv data in the SpecReply object and calling on the data
print('')
print('data and msg id of spec reply object where the send_msg_cmd_with_return should have been sent')
print(sr.data)
print(sr.id)

print('')
#sending multiple pwd cmds
for i in range(10):
    retrn = sc.send_msg_cmd_with_return(cmd,sr)
    print(retrn)

