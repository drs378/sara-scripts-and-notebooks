# %matplotlib widget
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import os
import sys
sys.path.insert(1,"../")
import glob
from SARA_cornell_funcs import *
import pandas as pd
import imageio
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
from IPython.display import display, clear_output
import ipywidgets as ipy
from itertools import compress
import json
import seaborn as sns

class DataLoader():
    def __init__(self, InitFile=None):
        self.InitFile = InitFile
        if self.InitFile:
            with open(self.InitFile,'r') as infile:
                self.LIIF = json.load(infile)
            self.MatSysName = self.LIIF['DataPaths']['MatSysName']
            self.ptim =  self.LIIF['DataPaths']['Images Data']
            self.ptim_m = self.LIIF['DataPaths']['Image mirror']
            self.O_pxs = self.LIIF['DataPaths']['Optical Pxs']
            self.S_pxs = self.LIIF['DataPaths']['Spectroscopy Pxs']
            self.X_pxs = self.LIIF['DataPaths']['XRD Pxs']
                    
            self.ptsp_raw = self.LIIF['DataPaths']['Spec raw']
            self.ptsp_b = self.LIIF['DataPaths']['Spec blank']
            self.ptsp_m = self.LIIF['DataPaths']['Spec mirror']

            self.pth5 = self.LIIF['DataPaths']['H5 path']
            self.pth2csvdb = self.LIIF['DataPaths']['CHESS CSVDB']
            self.ptcomp = self.LIIF['DataPaths']['XRF CSV']
            self.pth2rawXRD = self.LIIF['DataPaths']['Raw 2D location']
            self.CIFpaths = self.LIIF['DataPaths']['Path to CIFs']
            self.pth2exPM = self.LIIF['DataPaths']['Phase Info']

        else:
            self.MatSysName = None
            
            self.ptim = None
            self.ptim_m = None
            self.ptsp_raw = None
            self.ptsp_m = None
            self.ptsp_b = None
            
            self.pth5 = None
            self.pth2rawXRD = None
            self.pth2csvdb = None
            
            self.ptcomp = None
            self.CIFpaths = None
            self.pth2exPM = None
            
            self.O_pxs = None
            self.S_pxs = None
            self.X_pxs = None
        
        self.output = ipy.Output()
        #define widgets
        self.style = {'description_width':'initial'}
        self.MatSysName_Box = ipy.Text(
            value = self.MatSysName,
            placeholder = 'La-Mn-Ox',
            description = 'Material System',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptim_Box = ipy.Text( 
            value = self.ptim,
            placeholder = 'path/to/images/',
            description = 'Images Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptim_mirror_Box = ipy.Text(
            value = self.ptim_m,
            placeholder = '/path/to/RGBmirror.bmp',
            description = 'Image Mirror .bmp',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptsp_raw_Box = ipy.Text(
            value = self.ptsp_raw,
            placeholder = '/path/to/raw/spectroscopy/',
            description = 'Raw Spectroscopy Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptsp_b_Box =  ipy.Text(
            value = self.ptsp_b,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Take6/Spectroscopy/Blank/blank_00.csv',
            description = 'Spectroscopy Blank Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptsp_m_Box =  ipy.Text(
            value = self.ptsp_m,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Take6/Spectroscopy/Mirror/mirror_00.csv',
            description = 'Spectroscopy Mirror Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Pth5_Box = ipy.Text(
            value = self.pth5,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/LaMnOx_18CIT49586_rerun_all_oned.h5',
            description = '1D XRD H5',
            continuous_update = False,
            style=self.style
        )
        
        self.Pth2rawXRD_box = ipy.Text(
            value = self.pth2rawXRD,
            placeholder = '/home/vandover/zoo-fs/data/CHESS/2019-12-CHESS-3B-raw/LaMnOx_18CIT49586_rerun/Stripes/',
            description = 'Raw 2D XRD',
            continuous_update = False,
            style=self.style
        )
        
        self.P2CSVDB_Box = ipy.Text(
            value = self.pth2csvdb,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/17_LaMnO_CIT49586_SummaryDB.csv',
            description = 'CSV DataBaseFile',
            continuous_update = False,
            style=self.style
        )
        
        self.PtComp_Box = ipy.Text(
            value = self.ptcomp,
            placeholder = 'N/A',
            description = 'XRF Composition File',
            continuous_update = False,
            style=self.style
        )
            
        self.PtCIFs_Box = ipy.Text(
            value = self.CIFpaths,
            placeholder = '/home/vandover/Documents/GoogleDrive/KnownLaMnOxPhases/SeparatedCIFS/',
            description = 'CIFs Path',
            continuous_update = False,
            style=self.style
        )

        self.ExistingPhaseMap = ipy.Text(
            value = self.pth2exPM,
            placeholder = 'N/A',
            description = 'Existing Phase Map',
            continuous_update = False,
            style = self.style
        )
        
        self.Ops_Box = ipy.FloatText(
            value = self.O_pxs,
            placeholder = 0.808,
            description = 'Image PxSize (\u03BCm)',
            continuous_update = False,
            style=self.style
        ) 
        self.Sps_Box = ipy.FloatText(
            value = self.S_pxs,
            placeholder = 10.,
            description = 'Spectroscopy Pitch (\u03BCm)',
            continuous_update = False,
            style=self.style
        )
        self.Xps_Box = ipy.FloatText(
            value = self.X_pxs,
            placeholder = 10.,
            description = 'XRD Pitch (\u03BCm)',
            continuous_update = False,
            style=self.style
        )
        
        self.group1 = ipy.VBox([
            self.MatSysName_Box,
            self.Ptim_Box,
            self.Ptim_mirror_Box,
            self.Ptsp_raw_Box,
            self.Ptsp_b_Box,
            self.Ptsp_m_Box
        ])

        self.group2 = ipy.VBox([
            self.Pth5_Box,
            self.Pth2rawXRD_box,
            self.PtCIFs_Box,
            self.P2CSVDB_Box,
            self.ExistingPhaseMap
        ])
        
        self.group3 = ipy.VBox([
            self.PtComp_Box,
            self.Ops_Box,
            self.Sps_Box,
            self.Xps_Box
        ])
        
        self.FpBoxs = ipy.VBox([
            ipy.HBox([self.group1,self.group2]),
            self.group3
        ])
        self.LoadButton = ipy.Button(description = 'Load It!')
        self.LoadPorts = ipy.VBox([self.FpBoxs,self.LoadButton])
        display(self.LoadPorts)
        self.LoadButton.on_click(self.LoadIt)
            
    def LoadIt(self,event):
        #Dictionary structure:
        # Dictionary:
        #     condtion key
        #         [tau,T,xpos,ypos,fp]
        self.MatSysName = self.MatSysName_Box.value
        self.O_pxs = self.Ops_Box.value
        self.S_pxs = self.Sps_Box.value
        self.X_pxs = self.Xps_Box.value
        self.ptim = self.Ptim_Box.value
        self.ptim_m = self.Ptim_mirror_Box.value
        self.ptsp_raw = self.Ptsp_raw_Box.value
        self.ptsp_b = self.Ptsp_b_Box.value
        self.ptsp_m = self.Ptsp_m_Box.value
        self.pth5 = self.Pth5_Box.value
        self.pth2rawXRD = self.Pth2rawXRD_box.value
        self.CIFpaths = self.PtCIFs_Box.value
        self.pth2exPM = self.ExistingPhaseMap.value

        
        self.pth2csvdb = self.P2CSVDB_Box.value
        self.ptcomp = self.PtComp_Box.value
        
        self.DictGen()
        
    def DictGen(self):
        # For the a-Si work, T is maximum power for a stripe
        if self.ptim:
            self.OptDict = FpDict(pathtofiles=self.ptim,ftype='.bmp')
        if self.ptsp_raw:
            self.SpecDict = FpDict(self.ptsp_raw,ftype='.csv')
        if self.pth5:
            try:
                self.XrdDict = FpDict(self.pth5,Xrays=True,json_friendly=True)
            except:
                self.XrdDict = FpDict(self.pth5,Xrays=True,json_friendly=True, chess_2019=True)
        if self.pth2csvdb:
            # This DB is Mike's LasGO generated file from the CHESS run
            self.db = pd.read_csv(self.pth2csvdb)
        if self.ptcomp:
            self.comp_raw = np.genfromtxt(self.ptcomp,dtype=float,delimiter=',',skip_header=3)
            #tau,T,x_mm,y_mm,Cation_frac
            for idx in np.arange(len(self.comp_raw)):
                taus = int(self.comp_raw[idx,3])
                Ts = int(self.comp_raw[idx,4])
                Cat1_frac = self.comp_raw[idx,7]
                xpos = self.comp_raw[idx,5]
                ypos = self.comp_raw[idx,6]

                key = f'tau_{taus}_T_{Ts}'
                self.XrdDict[key]['Cation1_frac'] = str(Cat1_frac)
                self.OptDict[key]['Cation1_frac'] = str(Cat1_frac)
                self.SpecDict[key]['Cation1_frac'] = str(Cat1_frac)
        
    def SaveInitFile(self,path2output='/Some/path/to/save/File/'):
        self.LID = {}
        self.LID['DataPaths'] = {}
        self.LID['DataPaths']['MatSysName'] = self.MatSysName
        self.LID['DataPaths']['Optical Pxs'] = self.O_pxs
        self.LID['DataPaths']['Spectroscopy Pxs'] = self.S_pxs
        self.LID['DataPaths']['XRD Pxs'] = self.X_pxs
        self.LID['DataPaths']['Images Data'] = self.ptim
        self.LID['DataPaths']['Image mirror'] =self.ptim_m
        self.LID['DataPaths']['Spec raw'] = self.ptsp_raw
        self.LID['DataPaths']['Spec blank'] = self.ptsp_b
        self.LID['DataPaths']['Spec mirror'] = self.ptsp_m
        self.LID['DataPaths']['H5 path'] = self.pth5
        self.LID['DataPaths']['CHESS CSVDB'] = self.pth2csvdb
        self.LID['DataPaths']['XRF CSV'] = self.ptcomp
        self.LID['DataPaths']['Raw 2D location'] = self.pth2rawXRD
        self.LID['DataPaths']['Path to CIFs'] = self.CIFpaths
        self.LID['DataPaths']['Phase Info'] = self.pth2exPM

        with open(f'{path2output}{self.MatSysName}_init.json','w') as outfile:
            print(outfile)
            outfile.write(json.dumps(self.LID))