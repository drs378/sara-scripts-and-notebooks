
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import os
import sys
sys.path.insert(1,"../")
import glob
from SARA_cornell_funcs import *
import pandas as pd
import imageio
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
from IPython.display import display, clear_output
import ipywidgets as ipy
from itertools import compress
import json
import seaborn as sns

class ManualXRD_GUI():
    def __init__(self, DL, StripeData, CompSpread=False, c2019=False, twoD_IT=300, PMoutputpath=None):
        self.StripeData = StripeData
#         self.WTplot = WTplot
        # self.init_idx = np.int(np.round(self.StripeData.TechDict['xrd']['center']))
        output = ipy.Output()
        self.tau = self.StripeData.tau
        self.Tmax = self.StripeData.Tmax
        self.xpos = self.StripeData.xpos
        self.ypos = self.StripeData.ypos
        self.PMoutputpath = PMoutputpath
        self.DL = DL
        self.c2019 = c2019
        self.CompSpread = CompSpread
        self.current_phases = []

        # pull the relevant info from StripeData for now
        self.xx = self.StripeData.TechDict['xrd']['comextent']
        self.xmap1 = self.StripeData.TechDict['xrd']['commap']
        self.xsym = self.StripeData.TechDict['xrd']['symgrad']
        self.xcenter = self.StripeData.TechDict['xrd']['center']
        self.init_idx = int(np.round(self.xmap1.shape[1]/2))
        self.twoD_IT = twoD_IT


        with output:
            self.XRDfig = plt.figure(figsize=(10,6))
            
        self.gs = self.XRDfig.add_gridspec(4, 5)
        self.OneDMapPlot = self.XRDfig.add_subplot(self.gs[:2,:2])
        self.OneDMap = self.OneDMapPlot.imshow(self.xmap1,aspect='auto',
                            extent=[self.StripeData.minx,self.StripeData.maxx,max(self.StripeData.Q),min(self.StripeData.Q)],
                            cmap='rainbow')
        self.vertline = self.OneDMapPlot.axvline(x=self.xx[self.init_idx],ymin=0,ymax=1,c='orchid',alpha=0.7)
        
        if self.CompSpread:
            catfrac = np.round(float(self.DL.XrdDict[self.StripeData.key]['Cation1_frac']),2)
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key} at comp {catfrac}')
        else:
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key}')

        self.OneDMapPlot.set_xlabel('across stripe \u03BCm')
        self.OneDMapPlot.set_ylabel('Q in nm$^{-1}$')

        self.OneDPlot = self.XRDfig.add_subplot(self.gs[2,2:])
        self.StickPlots = self.XRDfig.add_subplot(self.gs[3,2:])
        self.PhaseRegions = self.XRDfig.add_subplot(self.gs[:2,2:])
        self.StickPlots.set_title(f'Reference patterns')
        self.StickPlots.set_xlabel('Q in nm$^{-1}$')
        self.StickPlots.set_ylabel('Rel. Int. (a.u.)')
        
        self.OneDPlot.set_ylabel('Int. (a.u.)')
        self.PhaseRegions.set_ylabel('Int. (a.u.)')
        self.OneDPlot.set_yticks([])
        self.PhaseRegions.set_yticks([])

        self.oneD_DP = self.OneDPlot.plot(self.StripeData.Q,self.xmap1[:,self.init_idx],c='orchid')[0]
        self.OneDPlot.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        self.oneD_text = self.OneDPlot.set_title(f'1D XRD at {np.int(self.xx[self.init_idx])} \u03BCm')
        
        #Using the Phase Region code to help with signal to noise and averaging over the entire bounded region
        self.xtrs = TransitionFinder(data=self.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        self.DP_bases = PhaseRegions(techmap=self.xmap1,trs=self.xtrs)

        for dpx,dp in enumerate(self.DP_bases[0]):
            self.PhaseRegions.plot(self.StripeData.Q,dp+0.2*dpx)

        for tdx,tr in enumerate(self.xtrs):
            self.OneDMapPlot.axvline(self.xx[tr],ymin=0.5,ymax=1,c='turquoise',linestyle='--')
        

        self.TwoDPlot = self.XRDfig.add_subplot(self.gs[2:,:2])
        self.TwoDPlot.set_xlabel('pixel x')
        self.TwoDPlot.set_ylabel('pixel y')
        # print(len(self.StripeData.raw2DXRD_f))
        if (len(self.StripeData.raw2DXRD_f)>0):
            if self.c2019==True:
                self.twoDDP_img = imageio.imread(self.StripeData.raw2DXRD_f[self.init_idx])
            else:
                self.twoDDP_img = ZingerBGone(h5.File(self.StripeData.raw2DXRD_f[self.init_idx], 'r')['entry']['data']['data'][0],maxi=self.twoD_IT)
            self.twoDDP_img = np.log10(self.twoDDP_img+abs(np.min(self.twoDDP_img))+2)
            self.twoD_DP = self.TwoDPlot.imshow(self.twoDDP_img,cmap='rainbow')
            self.twoD_text = self.TwoDPlot.set_title(f'raw 2D data at {np.int(self.xx[self.init_idx])} \u03BCm')
        
        self.XRDfig.tight_layout()


        #Interactables
        #stripe slider
        self.int_slider = ipy.IntSlider(
            value=self.init_idx,min=0,max=self.xmap1.shape[1],
            continuous_update=False,
        )
        self.int_slider.readout = False
        self.style = {'description_width':'initial'}

        #loaded phase check boxes for comparison
        self.phase_cboxes = []
        for idx, phaseset in enumerate(list(self.StripeData.PhaseSets)):
            self.phase_cboxes.append([])
            for jdx, phase in enumerate(list(self.StripeData.PhaseSets[phaseset])):
                formula = self.StripeData.PhaseSets[phaseset][phase]['Formula']
                sg = self.StripeData.PhaseSets[phaseset][phase]['SpaceGroup']
                self.phase_cboxes[idx].append(ipy.Checkbox(
                    description=formula+'_'+sg,
                    value=False,

                ))

        # restructuring for multiple phase sets in a system
        self.phaseset_cboxes = []
        for pset in self.phase_cboxes:
            self.phaseset_cboxes.append(ipy.VBox([cbox for cbox in pset]))
        self.phase_cboxes = ipy.HBox([pset for pset in self.phaseset_cboxes])
        
        #button controling the stick pattern replot
        self.replot_sticks_button = ipy.Button(description='Stick DP replot')

        #button creating/or passing labels to phsae diagram file
        self.PhaseMapPassingButton = ipy.Button(description='Pass to Phase Map')
        
        #button to pass write in phases
        self.WriteInPhaseButton = ipy.Button(description='Add write in Phase')

        #morphology and unknown toggles
        self.texturetoggle = []
        self.defaultTexTyp = ['equiaxed','textured','amorphous','nanocrystalline','small grain','large grain'] 
        for texturetype in self.defaultTexTyp:
            self.texturetoggle.append(
                ipy.Checkbox(description=texturetype,value=False)
            )

        #last elemen in texture toggle is the write in function. 
        self.texturetoggle.append(
            ipy.Text(description='write in:',placeholder='unknown phase 1')
            )

        #layout of widgets
        self.texturetoggle = ipy.VBox(self.texturetoggle)
        

        
        # supplying the write in checkboxes with phases that one has already assigned (not in the CIFs)
        self.writeins = []
        self.assignedPLs = []
        for vbox in list(self.phaseset_cboxes):
            for cb in vbox.children:
                self.assignedPLs.append(cb.description)
                
        # print('this is what exits ',self.DL.pth2exPM)
        if (self.DL.pth2exPM != None) and (self.DL.pth2exPM != ''):
            self.PMdict = json.load(open(self.DL.pth2exPM,'r'))
            for key in list(self.PMdict):
                for pos in list(self.PMdict[key]['PosDepPLs']):
                    for phase in list(self.PMdict[key]['PosDepPLs'][pos]):
                        self.writeins.append(phase)
            self.assigned = np.unique(self.writeins).tolist()
            self.writeins = []
            for known in self.assigned:
                if all(known not in l for l in self.assignedPLs+self.defaultTexTyp):
                    self.writeins.append(ipy.Checkbox(description=known,value=False))
            # self.cor_wi = [obj.children for obj in self.writeins.children]
            self.writeins = ipy.VBox(self.writeins)
        else:
            self.writeins = ipy.VBox([])

        self.position_control = ipy.HBox([ipy.VBox([ipy.Label('stripe position'),self.int_slider]),self.texturetoggle,self.writeins,ipy.VBox([self.PhaseMapPassingButton,self.WriteInPhaseButton])])
        pc_layout = ipy.Layout(
            border = 'solid 3px gray',
            margin = '10px 10px 10px 10px',
            padding = '5px 5px 5px 5px',
            align_items = 'center'
        )
        self.position_control.layout = pc_layout
        self.phase_control = ipy.VBox([self.phase_cboxes,self.replot_sticks_button])
        self.phase_control.layout = pc_layout
        self.phase_control.layout.align_items = 'center'

        #observe the interactables. link to a function that interactable does
        self.int_slider.observe(self.VerticalLineAndDP_Updates,'value')
        self.replot_sticks_button.on_click(self.StickPatternUpdate)
        self.PhaseMapPassingButton.on_click(self.WritePhasesToFile)
        self.WriteInPhaseButton.on_click(self.AddWriteInPhase)

        controls = ipy.VBox([self.position_control,self.phase_control])

        display(ipy.VBox([controls,output]))
        
    def StripeReInitialize(self):        
        # print(self.StripeData.key)

        self.xx = self.StripeData.TechDict['xrd']['comextent']
        self.xmap1 = self.StripeData.TechDict['xrd']['commap']
        self.xsym = self.StripeData.TechDict['xrd']['symgrad']
        self.xcenter = self.StripeData.TechDict['xrd']['center']

        self.init_idx = int(np.round(self.xmap1.shape[1]/2))
        print('reloaded')
        
        ###manipulables
        #vertical line on XRD map
        self.OneDMapPlot.cla()
        self.OneDMap = self.OneDMapPlot.imshow(self.xmap1,aspect='auto',
                            extent=[self.StripeData.minx,self.StripeData.maxx,max(self.StripeData.Q),min(self.StripeData.Q)],
                            cmap='rainbow')
        self.OneDMap.set_extent([self.StripeData.minx,self.StripeData.maxx,max(self.StripeData.Q),min(self.StripeData.Q)])
        self.OneDMap.set_clim(np.min(self.xmap1),np.max(self.xmap1))
        self.OneDMapText.set_text(f'XRD map {self.StripeData.key}')
        self.vertline = self.OneDMapPlot.axvline(x=self.xx[self.init_idx],ymin=0,ymax=1,c='orchid',alpha=0.7)
        if self.CompSpread:
            catfrac = np.round(float(self.DL.XrdDict[self.StripeData.key]['Cation1_frac']),2)
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key} at comp {catfrac}')
        else:
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key}')
        self.OneDMapPlot.set_xlabel('across stripe \u03BCm')
        self.OneDMapPlot.set_ylabel('Q in nm$^{-1}$')
        

        self.oneD_DP.set_xdata(self.StripeData.Q)
        self.oneD_DP.set_ydata(self.xmap1[:,self.init_idx])
        self.OneDPlot.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        self.OneDPlot.set_ylim(bottom=min(self.oneD_DP.get_ydata())-0.01,
                               top=max(self.oneD_DP.get_ydata())+0.01)
        self.oneD_text.set_text(f'1D XRD at {np.int(self.xx[self.init_idx])} \u03BCm')
        

        self.PhaseRegions.cla()
        self.PhaseRegions.set_yticks([])
        self.PhaseRegions.set_ylabel('Int. (a.u.)')

        self.xtrs = TransitionFinder(data=self.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        self.DP_bases = PhaseRegions(techmap=self.xmap1,trs=self.xtrs)
        self.PhaseRegions.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        for dpx,dp in enumerate(self.DP_bases[0]):
            self.PhaseRegions.plot(self.StripeData.Q,dp+0.2*dpx)
        for tdx,tr in enumerate(self.xtrs):
            self.OneDMapPlot.axvline(self.xx[tr],ymin=0.5,ymax=1,c='turquoise',linestyle='--')

        #2D DP corresponding to the XRD map with location in um
        if len(self.StripeData.raw2DXRD_f)>0:
            if self.c2019==True:
                self.twoDDP_img = imageio.imread(self.StripeData.raw2DXRD_f[self.init_idx])
            else:
                self.twoDDP_img = ZingerBGone(h5.File(self.StripeData.raw2DXRD_f[self.init_idx], 'r')['entry']['data']['data'][0],maxi=self.twoD_IT)

            self.twoDDP_img = np.log10(self.twoDDP_img+abs(np.min(self.twoDDP_img))+2)
            self.twoD_DP.set_array(self.twoDDP_img)
            self.twoD_text.set_text(f'raw 2D data at {np.int(self.xx[self.init_idx])} \u03BCm')
        self.int_slider.max = self.xmap1.shape[1]
        self.XRDfig.canvas.draw_idle()
        

    def AddWriteInPhase(self,change):
        writeinphase = list(self.texturetoggle.children)[-1].value
        wip_cbox = ipy.Checkbox(description=writeinphase,value=False)
        self.position_control.children[-2].children = tuple(list(self.position_control.children[-2].children )+ [wip_cbox])

        # list(self.position_control.children)
    #Callback Functions
    def VerticalLineAndDP_Updates(self,change):
        """ Redraws the vertical line and the 2D DP corresponding to the current position on the XRD map"""
        # print(self.StripeData.key)
        self.vertline.set_xdata(self.xx[change.new])
        # print(StripeData.xx1[change.new])
        if len(self.StripeData.raw2DXRD_f)>0:
            if self.c2019==True:
                self.twoDDP_img = imageio.imread(self.StripeData.raw2DXRD_f[change.new])
            else:
                self.twoDDP_img = ZingerBGone(h5.File(self.StripeData.raw2DXRD_f[change.new], 'r')['entry']['data']['data'][0],maxi=self.twoD_IT)
            self.twoDDP_img = np.log10(self.twoDDP_img+abs(np.min(self.twoDDP_img))+2)
            self.twoD_DP.set_array(self.twoDDP_img)
            self.twoD_text.set_text(f'raw 2D data at {np.int(self.xx[change.new])} \u03BCm')
            self.twoD_DP.set_clim(np.min(self.twoDDP_img),np.max(self.twoDDP_img))
        self.oneD_DP.set_ydata(self.xmap1[:,change.new])
        self.OneDPlot.set_ylim(bottom=min(self.oneD_DP.get_ydata())-0.01,
                               top=max(self.oneD_DP.get_ydata())+0.01)
        self.oneD_text.set_text(f'1D XRD at {np.int(self.xx[change.new])} \u03BCm')
        self.XRDfig.canvas.draw_idle()

    def WritePhasesToFile(self,change):
        # needs to read a file if it exists... write to a file if it exists... create a file if it does not exist.
        # file structure should be readable by new class that plots the "labeled" phase map
        fpath = self.DL.pth2exPM
        key = self.StripeData.key
        xpos = self.StripeData.xpos
        ypos = self.StripeData.ypos
        tau = self.StripeData.tau
        Tmax = self.StripeData.Tmax
        outputpath = self.PMoutputpath
        # set the exiting file path from the init file if it exists.
        if os.path.exists(fpath):
            # print('your file exists')
            with open(fpath,'r') as fp:
                PMdict = json.load(fp)

                # print(PMdict)
        # otherwise create a dictionary and set the fpath to what it should be
        else:
            
            fpath = f'{outputpath}{self.DL.MatSysName}_PhaseMap.json'
            self.DL.pth2exPM = fpath
            self.DL.SaveInitFile(path2output=os.path.dirname(self.DL.InitFile)+'/')
            PMdict = {}
        PMdict[key] = {}
        PMdict[key]['tau'] = tau
        PMdict[key]['Tmax'] = Tmax
        PMdict[key]['xpos'] = xpos
        PMdict[key]['ypos'] = ypos
        if self.CompSpread:
            PMdict[key]['Cation1_frac'] = float(self.DL.XrdDict[key]['Cation1_frac'])
            
        #need to pass sub stripe position, and phases/morphology. create a sub folder for it
        PMdict[key]['PosDepPLs'] = {}
        sub_xpos = self.xx[self.int_slider.value]
        PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'] = []

        #refreshes the current phases to what is currently toggled.
        self.current_phases = []
        for idx, ps_child in enumerate(self.phase_cboxes.children):
            self.current_phases.append([])
            for phase_child in ps_child.children:
                if phase_child.value:
                    self.current_phases[idx].append(phase_child.description)

        for phaseset in self.current_phases:
            for active_phase in phaseset:
                PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(active_phase)
        for child in self.texturetoggle.children:
            if child.value:
                PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(child.description)
        for child in self.writeins.children:
            if child.value:
                PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(child.description)


        
#         #need to updtate WaferandTauTplot:
#         if self.CompSpread:
#             print(np.log10(tau),PMdict[key]['Cation1_frac'],Tmax)
#             self.WTplot.tTax.scatter(xs=np.log10(tau),ys=PMdict[key]['Cation1_frac'],zs=Tmax,c='g')
#         else:
#             self.WTplot.tTax.scatter(xs=tau,ys=Tmax,c='g')
#         self.WTplot.tTfig.canvas.draw_idle()
        
        #write out the file to a .json
        with open(fpath,'w') as fp:
            json.dump(PMdict,fp)
            
    def StickPatternUpdate(self,change):
        self.StickPlots.clear()
        self.current_phases = []
        newLabels, newHandles = [], []
        # print(self.phase_cboxes.children)
        for idx, ps_child in enumerate(self.phase_cboxes.children):
            self.current_phases.append([])
            for phase_child in ps_child.children:
                if phase_child.value:
                    self.current_phases[idx].append(phase_child.description)

        colorcounter = 0
        for idx, pset in enumerate(self.current_phases):
            for jdx, activephase in enumerate(pset):
                
                phaseID = activephase
                formula, sg = phaseID.split('_')
                phaseset = list(self.StripeData.PhaseSets)[idx]
                candidate_phases = list(self.StripeData.PhaseSets[phaseset])
                
                for phase in candidate_phases:
#                     print(phase)
#                     print(list(StripeData.PhaseSets[phaseset][phase]))
#                     print(formula,sg)

                    checkformula = self.StripeData.PhaseSets[phaseset][phase]['Formula']
                    checksg = self.StripeData.PhaseSets[phaseset][phase]['SpaceGroup']
                    if (formula == checkformula) & (sg == checksg):
                        Qs = self.StripeData.PhaseSets[phaseset][phase]['Qs']
                        Is = self.StripeData.PhaseSets[phaseset][phase]['Is']
                        for pdx, (q,i) in enumerate(zip(Qs,Is)):
                            self.StickPlots.plot([q,q],[i+1,1],linewidth=2,c=self.StripeData.colors[colorcounter],label=phaseID)
                        self.StickPlots.set_xlim(self.StripeData.Qmin,self.StripeData.Qmax)

                    handles, labels = self.StickPlots.get_legend_handles_labels()
                    newLabels, newHandles = [], []
                    for handle, label in zip(handles, labels):
                        if label not in newLabels:
                            newLabels.append(label)
                            newHandles.append(handle)
                    
                colorcounter += 1
        self.StickPlots.set_xlabel('Q in nm$^{-1}$')
        self.StickPlots.legend(newHandles,newLabels,loc='upper left',bbox_to_anchor=(0,1.5))
        self.StickPlots.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        self.XRDfig.canvas.draw_idle()
