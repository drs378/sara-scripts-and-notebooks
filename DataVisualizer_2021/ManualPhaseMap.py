import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import os
import sys
sys.path.insert(1,"../")
import glob
from SARA_cornell_funcs import *
import pandas as pd
import imageio
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
from IPython.display import display, clear_output
import ipywidgets as ipy
from itertools import compress
import json
import seaborn as sns

class ManualPhaseMap():
    def __init__(self, DL, StripeData, CompSpread=False):

        self.DL = DL
        self.xpos_all = []
        self.ypos_all = []
        self.taus_all = []
        self.logtaus_all = None
        self.Tmaxs_all = []
        self.Comp_all = []
        self.phaselabels_all = []
        self.CationFrac = None
        self.CompSpread = CompSpread
        self.colors = list(sns.color_palette("tab10",16).as_hex())
        
        output = ipy.Output()
        with output:
            self.PMapFig = plt.figure(constrained_layout=True,figsize=(7,5))
        if self.CompSpread:
            self.logtaus_all = []
            self.PMapax = self.PMapFig.add_subplot(111,projection='3d')
            self.PMapax.set_xlabel('processing time log10(\u03BCs)')
            self.PMapax.set_ylabel('Cation fraction')
            self.PMapax.set_zlabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
        else:
            self.PMapax = self.PMapFig.add_subplot(111)
            self.PMapax.set_xlabel('procesing time (\u03BCs)')
            self.PMapax.set_ylabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
            self.PMapax.set_xscale('symlog')
#         self.PMapax.axis('equal')
        with open(self.DL.pth2exPM) as fp:
            self.PMdict = json.load(fp)
        
#         print(list(self.PMdict))
        for key in list(self.PMdict):
            # print(self.PMdict[key])
            self.taus_all.append(self.PMdict[key]['tau'])
            self.Tmaxs_all.append(self.PMdict[key]['Tmax'])
            self.xpos_all.append(self.PMdict[key]['xpos'])
            self.ypos_all.append(self.PMdict[key]['ypos'])
            for pos in list(self.PMdict[key]['PosDepPLs']):
                self.phaselabels_all.append(self.PMdict[key]['PosDepPLs'][pos])
            if self.CompSpread:
                self.Comp_all.append(float(self.PMdict[key]['Cation1_frac']))
                self.logtaus_all.append(np.log10(self.PMdict[key]['tau']))
                
                
        #get the unique Phase labels and generate checkboxes for them
        self.PhaseLabels = set(x for l in self.phaselabels_all for x in l)
        self.PhaseViewerBoxes = []
        for pl in self.PhaseLabels:
            self.PhaseViewerBoxes.append(
                ipy.Checkbox(value=False,
                             description=pl                    
                )
            )
        rows = int(np.ceil(np.sqrt(len(self.PhaseLabels))))
        cols = int(np.ceil(len(self.PhaseLabels)/rows))
        controls = ipy.HBox([ipy.VBox(self.PhaseViewerBoxes[cols*i : cols*(i+1)]) for i in range(rows)])
        
        self.ReplotButton = ipy.Button(description='Replot Phases')
        self.ReplotButton.on_click(self.ReplotPhases)
        
        controls = ipy.VBox([controls,self.ReplotButton])
        display(ipy.VBox([controls,output]))
        
    def ReplotPhases(self,change):
        self.PMapax.clear()
        current_phases = [act_ph.description for act_ph in self.PhaseViewerBoxes if act_ph.value]
        for idx, phase in enumerate(current_phases):
            voro_hull = []
            color = self.colors[idx]
            for pdx in range(len(self.Tmaxs_all)):
                if phase in list(self.phaselabels_all)[pdx]:
                    if self.CompSpread:
                        tau = self.logtaus_all[pdx]
                        Tmax = self.Tmaxs_all[pdx]
                        comp = self.Comp_all[pdx]
                        self.PMapax.scatter(tau,comp,Tmax,
                                            c=color,alpha=0.3,label=phase)
                        voro_hull.append([tau,Tmax,comp])
                    else:
                        tau = self.taus_all[pdx]
                        Tmax = self.Tmaxs_all[pdx]
                        self.PMapax.scatter(x=tau,y=Tmax,
                                           c=color,alpha=0.3,label=phase)
                        voro_hull.append([tau,Tmax])
                        
#             #calculate the voronoi hull for the labeled data
#             cvhull = scipy.spatial.ConvexHull(voro_hull)
#             cv_surf = []
#             for smplx in cvhull.simplices:
#                 if self.CompSpread:
#                     cv_surf.append([self.logtaus_all[smplx[0]],self.Comp_all[smplx[1]],self.Tmaxs_all[smplx[2]]])
#                 else:
#                     cv_surf.append([self.taus_all[smplx[0]],self.Tmaxs_all[smplx[1]]])
#                     self.PMapax.plot_surface(cv_surf[:,0],cv_surf[:,1],c=color,alpha=0.2)
            
#             cv_surf = np.array(cv_surf)
#             if self.CompSpread:
#                 self.PMapax.plot_surface(cv_surf[:,0],cv_surf[:,1],cv_surf[:,2],c=color,alpha=0.2)
#             else:
#                 self.PMapax.plot_surface(cv_surf[:,0],cv_surf[:,1],c=color,alpha=0.2)
            
        handles, labels = self.PMapax.get_legend_handles_labels()
        newLabels, newHandles = [], []
        for handle, label in zip(handles, labels):
            if label not in newLabels:
                newLabels.append(label)
                newHandles.append(handle)
        self.PMapax.legend(newHandles,newLabels,loc='upper left',bbox_to_anchor=(1,1.04))
        if self.CompSpread:
            self.PMapax.set_xlabel('processing time log10(\u03BCs)')
            self.PMapax.set_ylabel('Cation fraction')
            self.PMapax.set_zlabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
        else:
            self.PMapax.set_xlabel('procesing time (\u03BCs)')
            self.PMapax.set_ylabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
            self.PMapax.set_xscale('symlog')