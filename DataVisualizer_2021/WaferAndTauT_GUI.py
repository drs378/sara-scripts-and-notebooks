import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import os
import sys
sys.path.insert(1,"../")
import glob
from SARA_cornell_funcs import *
import pandas as pd
import imageio
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
from IPython.display import display, clear_output
import ipywidgets as ipy
from itertools import compress
import json
import seaborn as sns


class WaferAndTauT_GUIs():
    def __init__(self, DL, StripeData, XRD_GUI, CompSpread=False, c2019=False, path2pm=None):
        self.StripeData = StripeData
        self.XRD_GUI = XRD_GUI
        self.DL = DL
        self.xpos_all = []
        self.ypos_all = []
        self.taus_all = []
        self.logtaus_all = None
        self.Tmaxs_all = []
        self.Comp_all = []
        self.CationFrac = None
        self.CompSpread = CompSpread
        self.key = StripeData.key
        self.c2019 = c2019

        self.xpos_done = []
        self.ypos_done = []
        self.taus_done = []
        self.logtaus_done = []
        self.Tmaxs_done = []
        self.Comps_done = []


        self.width = 0.75
        self.height = 4.5
        self.wafer_r = 50

        self.ws_start = (self.StripeData.xpos,self.StripeData.ypos)
        self.taut_start = (self.StripeData.tau,self.StripeData.Tmax)

        if self.CompSpread:
            self.logtaus_all = []
            self.tTfig = plt.figure(constrained_layout=True,figsize=(5,5),dpi=150)
            self.tTax = self.tTfig.add_subplot(111,projection='3d')
            self.tTax.set_xlabel('processing time log10(\u03BCs)')
            self.tTax.set_ylabel('Cation fraction')
            self.tTax.set_zlabel('Tmax (\u00b0C)')
            self.tTax.set_title(f'{self.DL.MatSysName} Tau vs T')

        else:
            self.tTfig, self.tTax = plt.subplots(1,1,constrained_layout=True,figsize=(5,5),dpi=150)
            self.tTax.set_xlabel('procesing time (\u03BCs)')
            self.tTax.set_ylabel('Tmax (\u00b0C)')
            self.tTax.set_title(f'{self.DL.MatSysName} Tau vs T')
        
        self.Waffig, self.Wafax = plt.subplots(1,1,constrained_layout=True,figsize=(5,5),dpi=150)
        self.Wafax.set_xlabel('x position (mm)')
        self.Wafax.set_ylabel('y position (mm)')
        self.Wafax.set_title(f'{self.DL.MatSysName} Wafer Map')

        
        self.Waffig.canvas.mpl_connect('pick_event', self.WaferPosAndTauT_picker)
        self.tTfig.canvas.mpl_connect('pick_event', self.WaferPosAndTauT_picker)
        self.XRD_GUI.XRDfig.canvas.mpl_connect('pick_event', self.WaferPosAndTauT_picker)
    
        if path2pm != None:
            with open(path2pm,'r') as fp:
                self.PMdict = json.load(fp)
        else:
            self.PMdict = {}

        # print(list(self.PMdict))


    def Plotter(self,CompSpread=False):

        for cond in dwellsort(list(self.DL.XrdDict)):
            self.xpos_all.append(int(self.DL.XrdDict[cond]['xpos']))
            self.ypos_all.append(int(self.DL.XrdDict[cond]['ypos']))
            self.taus_all.append(int(self.DL.XrdDict[cond]['tau']))
            self.Tmaxs_all.append(int(self.DL.XrdDict[cond]['T']))

            if cond in list(self.PMdict):
                self.xpos_done.append(int(self.DL.XrdDict[cond]['xpos']))
                self.ypos_done.append(int(self.DL.XrdDict[cond]['ypos']))
                self.taus_done.append(int(self.DL.XrdDict[cond]['tau']))
                self.Tmaxs_done.append(int(self.DL.XrdDict[cond]['T']))


            if self.CompSpread:
                self.logtaus_all.append(np.log10(int(self.DL.XrdDict[cond]['tau'])))
                self.Comp_all.append(float(self.DL.XrdDict[cond]['Cation1_frac']))
                if cond in list(self.PMdict):
                    self.logtaus_done.append(np.log10(int(self.DL.XrdDict[cond]['tau'])))
                    self.Comps_done.append(float(self.DL.XrdDict[cond]['Cation1_frac']))

        self.wafer_space = self.Wafax.scatter(self.xpos_all,self.ypos_all,s=1,c='b')
        self.wafer_edge = self.Wafax.add_patch(Circle(xy=(0,0),radius=self.wafer_r,
                                            color='k',fill=False,linewidth=2))
        self.Wafax.set_xlim([-(self.wafer_r+2),self.wafer_r+2])
        self.Wafax.set_ylim([-(self.wafer_r+2),self.wafer_r+2])
        self.Wafax.axis('equal')

        for x,y in zip(self.xpos_all,self.ypos_all):
            self.Wafax.add_patch(Rectangle(xy=(x-self.width/2, y-self.height/2),
                                      width=self.width, height=self.height,
                                      color='b',fill=True,picker=True))

        for x,y in zip(self.xpos_done,self.ypos_done):
            self.Wafax.add_patch(Rectangle(xy=(x-self.width/2, y-self.height/2),
                                      width=self.width, height=self.height,
                                      color='lime',fill=True))

        self.wafer_select = self.Wafax.add_patch(Rectangle(xy=(self.ws_start[0]-self.width/2,self.ws_start[1]-self.height/2),
                                                 width=self.width,height=self.height,
                                                 color='r',fill=True))



        if self.CompSpread:
            self.CationFrac = float(self.DL.XrdDict[self.key]['Cation1_frac'])
            self.tau_t_space = self.tTax.scatter(xs=self.logtaus_all,ys=self.Comp_all,zs=self.Tmaxs_all,
                                                 s=12,color=['b' for i in range(len(self.Tmaxs_all))],picker=True)

            self.tau_t_processed = self.tTax.scatter(xs=self.logtaus_done,ys=self.Comps_done,zs=self.Tmaxs_done,
                                                 s=12,c='lime',edgecolors='face')

            self.taut_select = self.tTax.scatter(xs=np.log10(self.taut_start[0]),ys=self.CationFrac,zs=self.taut_start[1],
                                                 s=20,facecolors='none',
                                                 edgecolors='r',linewidth=2)
                
    
        else:
            self.tau_t_space = self.tTax.scatter(x=self.taus_all,y=self.Tmaxs_all,s=12,facecolors='b',picker=True)

            self.taut_select = self.tTax.scatter(self.taut_start[0],self.taut_start[1],s=20,
                                        facecolors='none',edgecolors='r',linewidth=2)
            
            self.tau_t_processed = self.tTax.scatter(x=self.taus_done,y=self.Tmaxs_done,s=12,c='lime')

            self.tTax.set_xscale('symlog')


        # print(self.tau_t_space.get_facecolors())
        


    def WaferPosAndTauT_picker(self,event):
        # print('you clicked!')
        artist = event.artist
        if artist in self.tau_t_space.findobj():
            print('tau_t space')
            taut_idx = event.ind[0]
            self.tau = self.taus_all[taut_idx]
            self.Tmax = self.Tmaxs_all[taut_idx]
            
            self.key = f'tau_{self.tau}_T_{self.Tmax}'
            self.xpos = int(self.DL.XrdDict[self.key]['xpos'])
            self.ypos = int(self.DL.XrdDict[self.key]['ypos'])
            
            if self.CompSpread:
                self.CationFrac = self.Comp_all[taut_idx]
                self.taut_select._offsets3d = (np.array([self.logtaus_all[taut_idx]]),
                                              np.array([self.CationFrac]),
                                              np.array([self.Tmax]))
                self.tau_t_space._facecolor3d
            else:
                self.taut_select.set_offsets([self.tau,self.Tmax])
                
            self.tTax.figure.canvas.draw()
            self.wafer_select.set_xy((self.xpos-0.5*self.width,self.ypos-0.5*self.height))
            self.Waffig.canvas.draw()
            
        else:
            print('wafer space')
            self.xpos,self.ypos = artist.xy
            #shifted origin to lot position
            self.xpos = self.xpos+0.5*self.width
            self.ypos = self.ypos+0.5*self.height
            _,_,_,self.tau,self.Tmax = CondToPos(self.DL.db,xpos=self.xpos,ypos=self.ypos,cond2pos=False)
            
            self.key = f'tau_{self.tau}_T_{self.Tmax}'
            if self.CompSpread:
                self.CationFrac = float(self.DL.XrdDict[self.key]['Cation1_frac'])
                self.taut_select._offsets3d = (np.array([np.log10(self.tau)]),
                                              np.array([self.CationFrac]),
                                              np.array([self.Tmax]))
            else:
                self.taut_select.set_offsets([self.tau,self.Tmax])
                
            self.tTfig.canvas.draw_idle()  
            self.wafer_select.set_xy((self.xpos-0.5*self.width,self.ypos-0.5*self.height))
            self.Waffig.canvas.draw_idle()

        print('point selected')
        self.StripeData.key = self.key
        self.StripeData.xpos = self.xpos
        self.StripeData.ypos = self.ypos
        self.StripeData.tau = self.tau
        self.StripeData.Tmax = self.Tmax

        if self.c2019:
            self.StripeData.TechMapCreation()
            if len(self.StripeData.raw2DXRD_f)>0:
                self.StripeData.TwoD_DP_Loader(c2019=True)
            self.StripeData.TechExtentMatch(minx=-400,maxx=400)
            self.XRD_GUI.StripeReInitialize()

        else:
            self.StripeData.TechMapCreation()
            if len(self.StripeData.raw2DXRD_f)>0:
                self.StripeData.TwoD_DP_Loader()
            self.StripeData.TechExtentMatch(minx=-400,maxx=400)
            self.XRD_GUI.StripeReInitialize()
        print('reinitialized')
