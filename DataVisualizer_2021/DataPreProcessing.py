import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import os
import sys
sys.path.insert(1,"../")
import glob
from SARA_cornell_funcs import *
import pandas as pd
import imageio
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
from IPython.display import display, clear_output
import ipywidgets as ipy
from itertools import compress
import json
import seaborn as sns

class ConditionDataPreProcessor():
    """
    A class that generates stripe data for all 3 techniques.    
    """
    def __init__(self,DL,CompSpread=False):
        self.DL = DL
        self.key = list(self.DL.XrdDict)[0]
        self.CompSpread = CompSpread
        print(self.key)
        self.tau = self.DL.XrdDict[self.key]['tau']
        self.Tmax = self.DL.XrdDict[self.key]['T']
        self.stripe = TechStripe()
        self.xpos = None
        self.ypos = None
        #the filepaths to the appropriate stripels and technique normalization data are called on here
        self.stripe.fpb = self.DL.ptsp_b
        self.stripe.fpm = self.DL.ptsp_m
        self.stripe.h5file = self.DL.pth5

        #Each technique esesentailly produces a 'map' of data. 
        #The Y and intensity coordinates all vary, but they share a common spatial extent.
        #It is important to make sure that the effective pixel size or technique resolution is recorded
        #  - The pitch of most reflectance spectroscopy data are 10 um, this example uses 5 um
        #  - the image pixel size depends on the magnification and needs the PSC. recent data are 0.808 um/pixel
        self.stripe.O_pxs = self.DL.O_pxs
        self.stripe.S_pxs = self.DL.S_pxs
        self.stripe.X_pxs = self.DL.X_pxs
        self.raw2DXRD = []
        self.stripe_idx = None
        self.colors = list(sns.color_palette('tab10',16).as_hex())
        self.comp = None
        self.TechNames = []
        
    def TechMapCreation(self, wlmin=450, wlmax=800, Qmin=10, Qmax=55):
        self.tau = int(self.DL.XrdDict[self.key]['tau'])
        self.Tmax = int(self.DL.XrdDict[self.key]['T'])
        self.xpos = int(float(self.DL.XrdDict[self.key]['xpos']))
        self.ypos = int(float(self.DL.XrdDict[self.key]['ypos']))
        self.TechNames = []
        self.TechMaps = []
        self.pxs = []
        #create optical and spectroscopy maps with the techstripe fucntions Optmap and Specmap
        if self.DL.ptim:
            self.stripe.fpo = self.DL.OptDict[self.key]['fp']
            self.omap = self.stripe.Optmap(okey=self.key,weights=[0,0,1],grayscale=False,Norm=False,Average=True,crop=(True,[250,750]))
            self.TechNames.append('opt')
            self.TechMaps.append(self.omap)
            self.pxs.append(self.stripe.O_pxs)
        if self.DL.ptsp_raw:
            self.stripe.fprs = self.DL.SpecDict[self.key]['fp']
            self.smap = self.stripe.Specmap(self.key,wlmax=wlmax,wlmin=wlmin,s_param=5)[2]
            self.wl = self.stripe.wl
            self.TechNames.append('spec')
            self.TechMaps.append(self.smap)
            self.pxs.append(self.stripe.S_pxs)
        if self.DL.pth5:
            self.xmap = self.stripe.Xraymap(self.key,dpath=self.stripe.h5file)
            self.Q_raw = self.stripe.Q
            self.Q = self.Q_raw[(self.Q_raw>=Qmin) & (self.Q_raw<=Qmax)]
            self.xmap = sharpen_y(self.xmap,alpha=0.25,bf=[1,1],fbf=[0.0,0.8],plotting=False)[((self.Q_raw>=Qmin) & (self.Q_raw<=Qmax)),:]
            self.TechNames.append('xrd')
            self.TechMaps.append(self.xmap)
            self.pxs.append(self.stripe.X_pxs)
        if self.CompSpread:
            self.comp = self.DL.XrdDict[self.key]['Cation1_frac']

        # print(self.TechNames)
        
    def TwoD_DP_Loader(self,c2019 = False):
        self.raw2DXRD = []
        if self.DL.pth2rawXRD:
            if c2019:
                _,self.stripe_idx,_,_,_ = CondToPos(self.DL.db,tau=self.tau,Tmax=self.Tmax)
                for pth in list(glob.glob(self.DL.pth2rawXRD+f'scan_{self.stripe_idx}/*.tiff')):
        #             print(pth)
                    self.raw2DXRD.append(pth)
            else:
                all_2d = sorted(glob.glob(self.DL.pth2rawXRD+'*/'))
                current_stripe = [s for s in all_2d if (str(self.tau) in s) & (str(self.Tmax) in s)][0]
                all_flyscans = sorted(glob.glob(current_stripe+'*/'))
                if len(all_flyscans) > 1:
                    flyscan = all_flyscans[-1]
                else:
                    flyscan = all_flyscans[0]
                    print(flyscan)
                for pth in sorted(glob.glob(flyscan+'*.h5')):
                    self.raw2DXRD.append(pth)
                # print(self.raw2DXRD)    
            
    def StickPatterns(self):
        self.Qmin = self.Q[0]
        self.Qmax = self.Q[-1]

        self.TTmin = QtoTT(self.Qmin)
        self.TTmax = QtoTT(self.Qmax)
        xrdcalc = pmg.XRDCalculator()
        
#         self.StickStructures = {}
        self.PhaseSets = {}
        for idx, (root, dirs, files) in enumerate(os.walk(self.DL.CIFpaths)):
            if idx == 0:
#                 print('Create Dictionary')
                for jdx in np.arange(len(dirs)):
                    self.PhaseSets[f'Phase Set {jdx+1}'] = {}

            if idx > 0:
                for jdx, file in enumerate(files):
#                     print(root+'/'+file)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}'] = {}
                    smeta = os.path.basename(file).split('_')
                    dp = xrdcalc.get_pattern(cif.CifParser(root+'/'+file).get_structures()[0],two_theta_range=(self.TTmin,self.TTmax))
#                     print(smeta)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Formula'] = smeta[0]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['SpaceGroup'] = smeta[1]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['fp'] = file
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['MP Structures'] = cif.CifParser(root+'/'+file).get_structures()[0]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Qs'] = TTtoQ(dp.x)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Is'] = dp.y
        
    def TechExtentMatch(self,minx=None,maxx=None):
        #output the spatial extent for each technique given
        self.TechDict = {}
        maps = self.TechMaps
        # print(len(maps))
        pxs = self.pxs
        xtents = []
        for tdx, tech in enumerate(self.TechNames):#This will need some more intellegent thought. currently there are 3 possible techniques available
            print(tech)
            self.TechDict[tech] ={}
            tmap = maps[tdx]
            tpxs = pxs[tdx]
            if tech == 'opt':
                self.omap = ndimage.gaussian_filter(self.omap,(5/self.stripe.O_pxs))
                self.g = np.gradient(np.sum(self.omap,axis=0))
                self.g = np.sqrt(self.g*self.g)
                self.og = self.g/max(self.g)
                self.TechDict[tech]['fullmap'] = self.omap
                self.TechDict[tech]['grad'] = self.og
            else:
                self.TechDict[tech]['fullmap'] = tmap
                self.TechDict[tech]['grad'],_ = Grad(tmap,bound_avg=True,norm=True,plotting=False)
            # grad = self.TechDict[tech]['grad']

            if tech == 'xrd':

                self.TechDict[tech]['center'] = get_center(tmap[(self.Q<26),:])[0]
            elif tech == 'opt':
                mid = int(np.round(tmap.shape[0]))
                self.TechDict[tech]['center'] = get_center(tmap[(mid-50):(mid+50)])[0]
            else:
                self.TechDict[tech]['center'] = get_center(tmap)[0]
            center = self.TechDict[tech]['center']

            x = np.linspace(0,tmap.shape[1]*tpxs,tmap.shape[1])
            x = x - center*tpxs
            # print(tech, center)
            # print(tmap.shape,tpxs)
            # print(x)
            xtents.append(x)
            self.TechDict[tech]['fullxtent'] = x

        #common extent
        if minx or maxx != None:
            self.minx = minx
            self.maxx = maxx
        else:
            self.minx = np.max([np.min(x) for x in xtents])
            self.maxx = np.min([np.max(x) for x in xtents])
        # print(self.minx,self.maxx)

        for tdx, tech in enumerate(self.TechNames):
            x = self.TechDict[tech]['fullxtent']
            # print(min(x),max(x))
            grad = self.TechDict[tech]['grad']
            center = self.TechDict[tech]['center']
            tmap = self.TechDict[tech]['fullmap']


            self.TechDict[tech]['comextent'] = x[(x>=self.minx)&(x<=self.maxx)]
            self.TechDict[tech]['symgrad'] = Symmetrize(grad,center,plotting=False)[(x>=self.minx)&(x<=self.maxx)]
            self.TechDict[tech]['commap'] = tmap[:,(x>=self.minx)&(x<=self.maxx)]

            if (self.raw2DXRD != '') and (tech == 'xrd'):
                self.raw2DXRD_f = list(compress(self.raw2DXRD,[(x>=self.minx)&(x<=self.maxx)][0]))
            print(f'finsihed {tech}')
        xx = self.TechDict['xrd']['fullxtent']

        self.raw2DXRD_f = list(compress(self.raw2DXRD,[(xx>=self.minx)&(xx<=self.maxx)][0]))
        #Defining the spatial extent

        # self.ox = np.linspace(0,self.omap.shape[1]*self.stripe.O_pxs,self.omap.shape[1])
        # self.sx = np.linspace(0,self.smap.shape[1]*self.stripe.S_pxs,self.smap.shape[1])
        # self.xx = np.linspace(0,self.xmap.shape[1]*self.stripe.X_pxs,self.xmap.shape[1])

        # #The spectroscopy and diffraction gradient
        # self.sg,_ = Grad(self.smap,bound_avg=True,norm=True,plotting=False)
        # self.xg,_ = Grad(self.xmap,bound_avg=True,norm=True,plotting=False)

        #determining the center of each stripe through the gradient signals and setting the center to 0
        # self.ocenter = np.round(CenterFinding(self.og,sparam=2,plotting=False))
        # self.scenter = np.round(CenterFinding(self.sg,sparam=2,plotting=False))
        # self.xcenter = np.round(CenterFinding(self.xg,sparam=2,plotting=False))

        # self.ocenter = get_center(self.omap)[0]
        # self.scenter = get_center(self.smap)[0]
        # self.xcenter = get_center(self.xmap[:500,:])[0] #this allows for the center finding to avoid the silicon peak

        # self.ox = self.ox - self.ocenter*self.stripe.O_pxs
        # self.sx = self.sx - self.scenter*self.stripe.S_pxs
        # self.xx = self.xx - self.xcenter*self.stripe.X_pxs


        #Matching the spatial extents
        #Identifying the common spatial extent for each technique 
        # self.maxx = np.min([np.max(self.ox),np.max(self.sx),np.max(self.xx)])
        # self.minx = np.max([np.min(self.ox),np.min(self.sx),np.min(self.xx)])

        # #can also hard code it
        # self.minx,self.maxx = [minx,maxx]

        # self.ox1 = self.ox[(self.ox>=self.minx)&(self.ox<=self.maxx)]
        # self.sx1 = self.sx[(self.sx>=self.minx)&(self.sx<=self.maxx)]
        # self.xx1 = self.xx[(self.xx>=self.minx)&(self.xx<=self.maxx)]

        # self.osym = Symmetrize(self.og,self.ocenter,plotting=False)[(self.ox>=self.minx)&(self.ox<=self.maxx)]
        # self.ssym = Symmetrize(self.sg,self.scenter,plotting=False)[(self.sx>=self.minx)&(self.sx<=self.maxx)]
        # self.xsym = Symmetrize(self.xg,self.xcenter,plotting=False)[(self.xx>=self.minx)&(self.xx<=self.maxx)]

        # self.omap1 = self.omap[:,[(self.ox>=self.minx)&(self.ox<=self.maxx)][0]]
        # self.smap1 = self.smap[:,[(self.sx>=self.minx)&(self.sx<=self.maxx)][0]]
        # self.xmap1 = self.xmap[:,[(self.xx>=self.minx)&(self.xx<=self.maxx)][0]]

        
        # self.raw2DXRD_f = list(compress(self.raw2DXRD,[(self.xx>=self.minx)&(self.xx<=self.maxx)][0]))
        
    # def TechPlotter(self):
    #     self.TPfig,self.TPax = plt.subplots(3,1,dpi=150,figsize=(6,6),sharex=True)
    #     if self.CompSpread:
    #         self.TPfig.suptitle(f'{self.DL.MatSysName} {self.key} Cat. 1 frac: {np.round(a=float(self.comp),decimals=2)}')
    #     else:
    #         self.TPfig.suptitle(f'{self.DL.MatSysName} {self.key}')
    #     self.TPax[0].set_title('Optical Micrograph')
    #     self.TPax[1].set_title('Reflectance Map')
    #     self.TPax[2].set_title('XRD Map')
    #     self.TPax[0].imshow(self.omap1,aspect='auto',extent=[self.minx,self.maxx,0,self.omap1.shape[0]*self.stripe.O_pxs])
    #     self.TPax[1].imshow(self.smap1,aspect='auto',extent=[self.minx,self.maxx,self.wl[0],self.wl[-1]])
    #     self.TPax[2].imshow(self.xmap1,aspect='auto',extent=[self.minx,self.maxx,self.Q[-1],self.Q[0]])

    #     # ax[0].set_xlabel('across stripe in \u03BCm')
    #     self.TPax[2].set_xlabel('across stripe in \u03BCm')
    #     self.TPax[0].set_ylabel('along stripe in \u03BCm')
    #     self.TPax[1].set_ylabel('wavelengthin  nm')
    #     self.TPax[2].set_ylabel('Q in nm$^{-1}$')

    #     plt.show(self.TPfig)





