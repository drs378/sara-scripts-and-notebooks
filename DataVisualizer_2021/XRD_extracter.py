import numpy as np
import matplotlib.pyplot as plt
import os
import h5py as h5
import ipywidgets as ipy
import json
from SARA_cornell_funcs import QtoTT, TTtoQ
from scipy.interpolate import interp1d

class XRD_exporter():
    def __init__(self, StripeData, XRD_WL = 0.12782):
        self.output1 = ipy.Output()
        self.StripeData = StripeData
        self.xx = self.StripeData.TechDict['xrd']['comextent']
        self.xmap1 = self.StripeData.TechDict['xrd']['commap'][24:-24,:]
        self.xsym = self.StripeData.TechDict['xrd']['symgrad']
        self.xcenter = self.StripeData.TechDict['xrd']['center']
        self.init_idx = int(np.round(self.xmap1.shape[1]/2))
        self.Q = self.StripeData.Q[24:-24]
        self.TT = None
        self.x_axis = self.Q

        self.in_heatmap = True
        self.in_1D_plot = False
        self.leftbound = False
        self.rightbound = False
        self.ROI = None
        self.bkgd_points = []
        self.bkgd = None
        self.bkgd_scatter = None
        self.cs_bkgd_subt = None
        self.spline = None
        self.wl = XRD_WL

        ## Interactables

        self.avg_window = ipy.IntRangeSlider(
            value=(0, int(np.round(len(self.xx)/2))), 
            min=0, max=len(self.xx), step=1, 
            description='stripe average',
            continuous_update=False,
            style = {'description_width': 'initial'}
            )

        self.save_box = ipy.Text(
            description='Data Save Path:',
            placeholder='/path/to/save/data/',
            style = {'description_width': 'initial'}
            )

        self.save_button = ipy.Button(
            description='Save'
            )
        # self.save_button.on_click(self.save_data_fxn)

        self.xlabels = ['Q (nm\u207B\u00B9)','Q (\u212B\u207B\u00B9)','\u00B02\u03B8 Cu k\u03B1']
        self.x_axis_labels = ipy.RadioButtons(
            value=self.xlabels[0], 
            options=self.xlabels, 
            description='scattering vector',
            style = {'description_width': 'initial'}
        )
        interaction_modes = ['ROI select', 'background select', 'peak select']
        self.interact_mode = ipy.RadioButtons(
            value=interaction_modes[0],
            options=interaction_modes,
            description='interaction mode',
            style={'description_width': 'initial'}
            )
        self.subtract_bkgd_button = ipy.Button(
            description='subtract background',
            style={'description_width': 'initial'}
            )
        self.clr_bkgd_button = ipy.Button(
            description='clear background',
            style={'description_width': 'initial'}
            )
        self.revert_to_raw_button = ipy.Button(
            description='revert to raw',
            style={'description_width': 'initial'}
            )
        #Background Subtraction
        # insert here
        # Reload raw button (clears out background subtraction and any truncation)

        row1 = ipy.HBox([self.save_box, self.save_button])
        top =  ipy.VBox([row1,self.avg_window])


        ## Plotting components
        with self.output1:
            self.XRDfig = plt.figure(figsize=(8,8))

        self.gs = self.XRDfig.add_gridspec(2, 1)
        self.OneD_plot = self.XRDfig.add_subplot(self.gs[0])
        self.current_selection = np.average(self.xmap1[:,self.avg_window.value[0]:self.avg_window.value[1]], axis=1)
        self.OneD_DP = self.OneD_plot.plot(self.x_axis, self.current_selection, c='red', linewidth=2, label='current selection')
        self.OneD_plot.set(
            xlabel = self.x_axis_labels.value,
            ylabel = "Intensity (a.u.)",
            title = "Current Selection",
            xlim = (min(self.x_axis), max(self.x_axis))
            )
        self.bkgd_points.append([self.x_axis[0],self.current_selection[0]])
        self.bkgd_points.append([self.x_axis[-1],self.current_selection[-1]])

        self.OneDMapPlot = self.XRDfig.add_subplot(self.gs[1])

        self.OneDMap = self.OneDMapPlot.imshow(self.xmap1,aspect='auto',
                            extent=[min(self.xx),max(self.xx),max(self.x_axis),min(self.x_axis)],
                            cmap='cividis')
        self.OneDMapPlot.set(
            xlabel = "across stripe in \u03BCm",
            ylabel = self.x_axis_labels.value,
            title = f"XRD heatmap for {StripeData.key}",
            )

        # self.vertline = self.OneDMapPlot.axvline(x=self.xx[self.init_idx],ymin=0,ymax=1,c='orchid',alpha=0.7)

        #connect interaction functions to plots
        self.XRDfig.canvas.mpl_connect('button_press_event', self.redraw_ROI_on_press)
        self.XRDfig.canvas.mpl_connect('axes_enter_event', self.enter_heatmap)
        self.XRDfig.canvas.mpl_connect('button_release_event', self.redraw_ROI_on_release)
        self.XRDfig.canvas.mpl_connect('button_press_event', self.select_background)
        self.clr_bkgd_button.on_click(self.clear_background)
        self.subtract_bkgd_button.on_click(self.subtract_bkgd)
        self.revert_to_raw_button.on_click(self.revert_to_raw)
        self.save_button.on_click(self.save_data_fxn)
        self.x_axis_labels.observe(self.scattering_vector_scaling, names='value')
        self.XRDfig.tight_layout()

        buttons =  ipy.VBox([self.revert_to_raw_button, self.clr_bkgd_button, self.subtract_bkgd_button])
        bottom = ipy.HBox([self.output1,ipy.VBox([self.x_axis_labels, self.interact_mode, buttons])])
        UI = ipy.VBox([top,bottom])
        display(UI)


    def save_data_fxn(self):
        pritn('To be added')

    def enter_heatmap(self, event):
        if event.inaxes == self.OneDMapPlot:
            self.in_heatmap = True
            self.in_1D_plot = False
            self.current_axis = event.inaxes
        elif event.inaxes == self.OneD_plot:
            self.in_heatmap = False
            self.in_1D_plot = True
            self.current_axis = event.inaxes

    def redraw_ROI_on_press(self, event):
        if (self.interact_mode.value == 'ROI select') and (self.in_heatmap):
            if self.leftbound:
                self.OneDMapPlot.lines.remove(self.leftbound)
                self.OneDMapPlot.lines.remove(self.rightbound)
            if self.ROI:
                self.ROI.remove()

            self.ROI_points = np.array([event.xdata, event.ydata])
            #axvspan method
            self.leftbound = self.OneDMapPlot.axvline(self.nearest(event.xdata, self.xx)[0], ymin = 0, ymax = 1, c='r', linewidth = 2, alpha = 0.25)

    def redraw_ROI_on_release(self, event):
        if (self.interact_mode.value == 'ROI select') and (self.in_heatmap):
            self.ROI_points = np.stack((self.ROI_points,[event.xdata, event.ydata]))

            #axvspan method
            self.rightbound = self.OneDMapPlot.axvline(self.nearest(event.xdata, self.xx)[0], ymin = 0, ymax = 1, c='r', linewidth = 2, alpha = 0.25)


            self.ROI_xmap_idx = (self.nearest(self.ROI_points[0,0],self.xx)[1], self.nearest(self.ROI_points[1,0],self.xx)[1])
            self.ROI = self.OneDMapPlot.axvspan(self.nearest(self.ROI_points[0,0],self.xx)[0],
                self.nearest(self.ROI_points[1,0],self.xx)[0], facecolor='r', alpha = 0.1)

            #update the avg_window Value and replot the 1D plot
            self.avg_window.value = sorted(self.ROI_xmap_idx)
            if self.ROI_xmap_idx[0] == self.ROI_xmap_idx[1]:
                self.current_selection = self.xmap1[:,self.ROI_xmap_idx[0]]
            else:
                self.current_selection = np.average(self.xmap1[:,self.avg_window.value[0]:self.avg_window.value[1]], axis=1)
            self.OneD_DP[0].set_ydata(self.current_selection)
            self.OneD_plot.set_ylim((min(self.current_selection) - 0.1), max(self.current_selection) + 0.1)

            #dump the spline, the background selection, the 

    def select_background(self, event):
        if (self.interact_mode.value == 'background select') and (self.in_1D_plot):

            #nearest actual data point
            nx, nidx = self.nearest(event.xdata, self.x_axis)
            ny = self.current_selection[nidx]

            #write the selected point to the background model on left-click or subtract if right-clicked
            if event.button == 1:
                self.bkgd_points.append([nx,ny])
            elif event.button > 1:
                nx, nidx = self.nearest(event.xdata, np.array(self.bkgd_points)[:,0])
                ny = self.bkgd_points[nidx][1]
                self.bkgd_points.remove([nx,ny])

                #There is a bug with the spline such that if the endpoints are removed, the spline is invalid (only interpolates not extrapolates)

            self.bkgd_points = sorted(self.bkgd_points)
            
            if len(self.bkgd_points) >= 3:
                if self.bkgd and (self.bkgd[0] in self.OneD_plot.lines):
                    self.OneD_plot.lines.remove(self.bkgd[0])
                if self.bkgd_scatter:
                    self.bkgd_scatter.remove()
                
                if len(self.bkgd_points) < 4:
                    self.spline = interp1d(np.array(self.bkgd_points)[:,0], np.array(self.bkgd_points)[:,1])
                else:
                    self.spline = interp1d(np.array(self.bkgd_points)[:,0], np.array(self.bkgd_points)[:,1], kind='cubic')
                self.bkgd = self.OneD_plot.plot(self.x_axis, self.spline(self.x_axis),
                 c='dodgerblue', linewidth=1, linestyle='--')
                
                self.bkgd_scatter = self.OneD_plot.scatter(np.array(self.bkgd_points)[:,0], np.array(self.bkgd_points)[:,1], s=40,
                 edgecolors='dodgerblue', facecolors='none')
                self.OneD_plot.set_ylim((min(self.current_selection) - 0.1, max(self.current_selection) + 0.1))
                

    def clear_background(self, event):
        self.bkgd_points = [[self.x_axis[0],self.current_selection[0]],   
            [self.x_axis[-1],self.current_selection[-1]]]
        if self.bkgd[0] in self.OneD_plot.lines:
            self.OneD_plot.lines.remove(self.bkgd[0])
        try:
            self.bkgd_scatter.remove()
        except:
            pass
        self.bkgd_scatter = None

    def subtract_bkgd(self, event):
        self.OneD_DP[0].set_color('lightgray')
        self.subt_dp = (self.current_selection - self.spline(self.x_axis))
        self.subt_dp = self.subt_dp - min(self.subt_dp)

        self.cs_bkgd_subt = self.OneD_plot.plot(self.x_axis, self.subt_dp, c='r', linewidth=2)
        self.OneD_plot.set_ylim(-0.05, max(self.subt_dp) + 0.1)

    def revert_to_raw(self, event):
        self.OneD_DP[0].set_color('r')
        if self.bkgd_scatter:
            self.bkgd_scatter.set_color('lightgray')
        self.bkgd[0].set_color('lightgray')
        if self.cs_bkgd_subt:
            self.OneD_plot.lines.remove(self.cs_bkgd_subt[0])

        self.OneD_plot.set_ylim((min(self.current_selection) - 0.1, max(self.current_selection) + 0.1))

    def save_data_fxn(self, event):
        name = self.save_box.value+'.xy'
        print(f'saved to {name}')
        # name = open(name, 'w')
        if self.cs_bkgd_subt:
            np.savetxt(name , np.stack((self.x_axis,self.subt_dp), axis=1), delimiter=',')
        else:
            np.savetxt(name , np.stack((self.x_axis, self.current_selection), axis=1), delimiter=',')

    def scattering_vector_scaling(self, change):
        if change['new'] == self.xlabels[0]:
            self.x_axis = self.Q
        elif change['new'] == self.xlabels[1]:
            self.x_axis = 0.1* self.Q
        elif change['new'] == self.xlabels[2]:
            self.x_axis = QtoTT(self.Q)

        #handling the selected bkgd points conversion
        self.temp_bkgd_points = np.array(self.bkgd_points)
        if (change['old'] == self.xlabels[0]) and (change['new'] == self.xlabels[1]):
            self.temp_bkgd_points[:,0] = 0.1 * self.temp_bkgd_points[:,0]
        elif (change['old'] == self.xlabels[0]) and (change['new'] == self.xlabels[2]):
            self.temp_bkgd_points[:,0] = QtoTT(self.temp_bkgd_points[:,0])
        elif (change['old'] == self.xlabels[1]) and (change['new'] == self.xlabels[0]):
            self.temp_bkgd_points[:,0] = 10 * self.temp_bkgd_points[:,0]
        elif (change['old'] == self.xlabels[2]) and (change['new'] == self.xlabels[0]):
            self.temp_bkgd_points[:,0] = TTtoQ(self.temp_bkgd_points[:,0])
        elif (change['old'] == self.xlabels[1]) and (change['new'] == self.xlabels[2]):
            self.temp_bkgd_points[:,0] = QtoTT(10 * self.temp_bkgd_points[:,0])
        elif (change['old'] == self.xlabels[2]) and (change['new'] == self.xlabels[1]):
            self.temp_bkgd_points[:,0] = 0.1 * TTtoQ(self.temp_bkgd_points[:,0])

        # update plots
        self.OneD_DP[0].set_xdata(self.x_axis)

        if self.bkgd:
            self.spline = interp1d(np.array(self.temp_bkgd_points)[:,0], np.array(self.temp_bkgd_points)[:,1])

            self.bkgd[0].set_xdata(self.x_axis)
            self.bkgd[0].set_ydata(self.spline(self.x_axis))

        if self.bkgd_scatter:
            self.bkgd_scatter.set_offsets(self.temp_bkgd_points)

        if self.cs_bkgd_subt:
            self.cs_bkgd_subt[0].set_xdata(self.x_axis)
        self.OneD_plot.set_xlim((min(self.x_axis), max(self.x_axis)))
        self.OneD_plot.set_xlabel(change['new'])

        self.OneDMap.set_extent([min(self.xx), max(self.xx), max(self.x_axis), min(self.x_axis)])
        self.OneDMapPlot.set_ylim((max(self.x_axis) - 0.1, min(self.x_axis) + 0.1))
        self.OneDMapPlot.set_ylabel(change['new'])


    def nearest(self, value, array):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx], idx

