import glob
import os
from SARA_cornell_funcs import *
def TauT(imagespath,outputpath,MatSysName,pathtoblank,filetype='.bmp'):
    """This will generate a dwell vs. Temperature map given a directory of images.
    
    imagespath: (path) the directory containing all the .bmp files for a given system
    outputpath: (path) where you want the map to be stored
    MatSysName: (str) appends an identifying label to the output file
    pathtoblank: (path) an image to fill any missing conditions/blank space in a wafer map
    Resolution: (int) should be a typical image size (e.g. 128, 256, 512...)
    
    
    Future Edits: Needs axes on the overall image to show conditions
    
    Returns: a .png image of the tau vs. T map

    """
    #if not 625 stripes, need to find where 8 are missing in condition paths
    keys = []
    fnamepaths = []
    sortedFPs = []
    x=0
    for file in glob.glob(imagespath+'*'+filetype):
        if 'background' not in file:
            filename = os.path.basename(file)[:-4]
#             print(filename)
            key = '_'
            s = filename.split('_')[2:]
            s[0] = str(int(s[0]))
            s[1] = str(int(s[1]))
            s.insert(0,'tau')
            s.insert(2,'T')
            key = key.join(s) 
            keys.append(key)
            fnamepaths.append(file)
#         else:
#             continue
#     print(keys)
#     print(fnamepaths)
    allkeys = dwellsort(keys+MissingConditions(keys))
    coords = []
    
    tau=[]
    T=[]

    for i in allkeys:

        conds = '_'
        s = i.split('_')
        tau.append(s[1])
        T.append(s[3])
#     print(allkeys)
    dimex=len(np.unique(tau))
    dimey=len(np.unique(T))
#     print(dimex,dimey)
    for i in allkeys:
        dw = i.split('_')[1].zfill(5)
        T = i.split('_')[3].zfill(4)
#         print(dw)
#         print(T)
        if i in keys:
#             print([j for j in fnamepaths if (dw in j and T+'.bmp' in j)])
            file = glob.glob([j for j in fnamepaths if (dw in j and T+filetype in j)][0])[0]
    #         print(file)
        else:
            file = glob.glob(pathtoblank)[0]
    #         print(file)
        filename = os.path.basename(file)[:-4]
        sortedFPs.append(file)
    grid = np.reshape(sortedFPs,(dimex,dimey))
    sortedFPs = np.flipud(grid).ravel()
#     print(len(sortedFPs))
    inew = make_contact_sheet(sortedFPs,(dimex,dimey),(128,128),(0,0,0,0),0)
    inew.save(outputpath+MatSysName+'_tauvsT.png')
    return 'Done'