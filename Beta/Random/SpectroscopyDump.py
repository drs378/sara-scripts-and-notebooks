#Python "essentials"
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import glob
import itertools
import json
#The functions need to be on your path. If you are having trouble loading them,
#check to see what is "on path". Add the proper directory containing the files
#to either a folder that is already on the path OR add the directory to the path
#with the following code.

# if  '/path/to/gitlab/functions/' in sys.path:
#     print('you good')
# else:
#     sys.path.append('/path/to/gitlab/functions/')


#These are the librarires that Max and I have built
from cookb_signalsmooth import *
from edge_finder import *
from SARA_cornell_funcs import *


#This is optical reflectance data and analysis
data_filepath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/Spectroscopy/'
Blank_filepath = '/home/vandover/Documents/Data/La-Mn-18CIT49586/Spectroscopy/Blank/blank_01.csv'
Mirror_filepath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/Spectroscopy/Mirror/mirror_00.csv'

ReflDict = {}
ReflDict['Maps'] = {}
Location = {}
FPs = []
keys = []
plotting = False
#extracting the data from the .csv files
for i in glob.glob(data_filepath+'*.csv'):
    #extracting lasGO coordinates and anneal conditions
    filename = os.path.basename(i)
    x = filename.split('_')
    string = '_'.join(['tau',str(int(x[2])),'T',str(int(x[3][:-4]))])
    FPs.append(i)
    keys.append(string)

for idx in np.arange(len(FPs)):#np.arange(5)
    data_fn = FPs[idx]
    string = keys[idx]
    smooth_data = get_spects(data_fn,Mirror_filepath,Blank_filepath)
    wl = smooth_data[0]
    wl_range = wl[(wl>=400) & (wl<= 850)]
    #Dictionary Generation
    print(idx)
    ReflDict['Maps'][string] = smooth_data[2][[(wl>=400) & (wl<= 850)][0],:].tolist()
#     Location[string] = (smooth_data[3]['scan center'][0],smooth_data[3]['scan center'][1])
    
    if plotting == True: 
        
        data = smooth_data[2][[(wl>=400) & (wl<= 800)][0],:]
        plt.imshow(data,aspect='auto',extent=[0,2.00,min(wl_range),max(wl_range)])
        plt.title(string)
        plt.xlabel('Anneal Space in mm')
        plt.ylabel('wavelength in nm')
        plt.show()
        plt.close()
wl = wl_range
ReflDict['Wavelength_Range'] = wl

with open('/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/NormReflectanceArrays.txt', 'w') as dump:
    json.dump(ReflDict, dump)
