import glob
import numpy 
import matplotlib.pyplot as plt
from TauT_Mapper import TauT
from WaferMapper import WaferMap
from SARA_cornell_funcs import *

def DustCollector(im,binsize=20,exc_z=1,filt_crit=3,plotting=True):
    """DustCollector filters the image"""
    nbins = int(np.floor(im.shape[0]/binsize))
    emeans = []
    estds = []
    ROIs = []
    for i in np.linspace(exc_z,nbins-exc_z,nbins-2*exc_z,dtype=int):
    #     print(i)
        ROI_lower = i*binsize
        ROI_upper = (i+1)*binsize
        ROI_avg = np.average(im[ROI_lower:ROI_upper],axis=0)
        ROIs.append(ROI_avg)

        ensemble_mean = 0.5*(np.mean(im[:ROI_lower],axis=0) + np.mean(im[ROI_upper:],axis=0))
        ensemble_std = 0.5*(np.std(im[:ROI_lower],axis=0) + np.std(im[ROI_upper:],axis=0))
        emeans.append(ensemble_mean)
        estds.append(ensemble_std)

    ROIs = np.array(ROIs,dtype=float)
    emeans = np.array(emeans,dtype=float)
    estds = np.array(estds,dtype=float)
    diffs = []
    good = []
    bad = []
    for idx in np.arange(len(ROIs)):
        diff = abs(ROIs[idx]-emeans[idx])
        diffs.append(diff)
        if np.any(diff>filt_crit*estds[idx]):
            bad.append(ROIs[idx])
        else:
            good.append(ROIs[idx])
    good = np.array(good)
    bad = np.array(bad)
    diffs = np.array(diffs)
    if plotting:
        fig,ax = plt.subplots(4,1,figsize = (4,12))
        ax[0].imshow(im)
        ax[1].imshow(diffs,aspect='auto',label='all')
        if len(good)>0:
            ax[2].imshow(good,aspect='auto',label='good')
        if len(bad)>0:
            ax[3].imshow(bad,aspect='auto',label='bad')
        plt.show()
        plt.close(fig)

        fig,ax = plt.subplots(3,1)
        ax[0].plot(np.average(ROIs,axis=0))
        if len(good)>0:
            ax[1].plot(np.average(good,axis=0))
        if len(bad)>0:
            ax[2].plot(np.average(bad,axis=0))
        plt.show()
        plt.close(fig)
        
    return good,bad,diffs

def JitterCorrection(xmap):
    """This will remove some of the jitter in an XRD map due to the detector binning incorrectly
    The mean of each column (1D XRD scan) is collected into an array. the mean of the means is
    determined and a uniform shift is applied based on the differnece between the mean of a column
    and the overall mean so that they are all the same.
    
    Basically, the average intensity of each scan should be the same
    """
    temp = []
    newmap=[]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]
        temp.append(np.mean(col))
    meanofmeans = np.mean(temp)
    diffs = [mean-meanofmeans for mean in temp]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]-diffs[idx]
        newmap.append(col)
        
    return np.array(newmap).T
class TechStripe:
    def __init__(self):
        
        #Technique Yspans
        self.opt_yrange = (0,70)
        self.wl = []
        self.Q = []
        
        #Technique Maps
        self.omap = []
        self.smap = []
        self.xmap = []
        
        self.omapcrop = (200,500)
        
        #Gradients of techniques
        self.ograd = []
        self.sgrad = []
        self.xgrad = []
        
        #Distane Across Stripes
        self.ospan = ()
        self.sspan = ()
        self.xspan = ()
        
        #Pixel Sizes
        self.O_pxs = 0.943
        self.S_pxs = 10
        self.X_pxs = 10
    
        #paths to raw data
        self.fpo = 'path/to/img'
        self.fprs = 'path/to/rawSpec'
        self.fpb = 'path/to/blank'
        self.fpm = 'path/to/mirror'
        self.fpImM = 'path/to/image/mirror'
    h5file = 'path/to/.h5'
        
    def Optmap(self,okey,weights=[0.33,0.33,0.33],grayscale=False,Norm=True,crop=False):
        """Loads the image of the stripe, converts to grayscale if desired with 
        a specified weighting [r,g,b]"""
        omap = cv2.imread(self.fpo)
        
        if Norm:
            mirror = cv2.imread(self.fpImM)
            mirror = np.transpose(np.array([[smooth(row,window_len=25) for row in mirror[:,:,j]] for j in np.arange(mirror.shape[2])]),axes=(1,2,0))
            omap = omap/mirror
        
        if grayscale:
            omap = np.average(omap,axis=2,weights=weights)
            if crop:
                self.omap = omap[self.omapcrop[0]:self.omapcrop[1],:]
        else:
            if crop:
                self.omap = omap[self.omapcrop[0]:self.omapcrop[1],:,:]
            
        self.opt_yrange = self.O_pxs*np.array([0,omap.shape[0]],dtype=float)
        return omap
        
    
#     def plot(Tmap,span):
    def Specmap(self,okey):
        """The data is pretty much only good from 400 nm to 800 nm so that is 
        where the filter is appied ot the self.smap, the complete data can be called 
        upon with the output of the function"""
        wl, unfsmap, smap,meta = get_spects(self.fprs,self.fpm,self.fpb)
        
        self.smap = smap[[(wl>=500) & (wl<= 850)][0],:]
        self.wl = wl[(wl>=500) & (wl<= 850)]
        return wl,unfsmap,smap,meta
        

    def Xraymap(self,xkey,dpath=h5file,logscale=True):
        """This function generates the xrd map from the location in the .h5 file with the 
        appropriate key corresponding to the condition desired.
               
        if logscale is True(False), the data will be scaled appropriately.
        
        """
        AllXRD = h5.File(dpath,'r')
        substripescans = list(AllXRD['exp'][xkey].keys())
        substripescans.sort(key=int)
        data=[]
        for jdx, scan_num in enumerate(substripescans):
            Q,I = AllXRD['exp'][xkey][scan_num]['integrated_1d']

            if scan_num == '0':
                data = np.append(data, I[:-24], axis=0)
            else:
                data = np.vstack((data,I[:-24]))
        
        data = data.T
        if logscale==True:
            data = np.log10(data)
        
        data = JitterCorrection(data)
        self.xmap = data
        self.Q = Q
        return data
    
    def Grad(TechMap,bwind=15,bound_avg=False,norm=False):
        gradTechMap = np.array([GenGrad(signal=row,bwind=bwind,exc_z=3,bound_avg=bound_avg) for row in TechMap])
        grad1D = np.average(gradTechMap,axis=0)
        if norm:
            grad1D = grad1D/max(grad1D) 
        return grad1D,gradTechMap
#end of obj
def Grad(TechMap,bwind=15,bound_avg=False,norm=False):
#     print(bound_avg)
    gradTechMap = np.array([GenGrad(signal=row,bwind=bwind,exc_z=3,bound_avg=bound_avg,plotting=False) for row in TechMap])
    grad1D = np.average(gradTechMap,axis=0)
    if norm:
        grad1D = grad1D/max(grad1D) 
        
    return grad1D,gradTechMap

def GenGrad(signal,bwind=15,exc_z=3,bound_avg=True,norm=False,plotting=True):
    signal = LinearBS(signal)
    grad = np.gradient(signal)
    grad = np.sqrt(grad*grad)
    grad1 = np.copy(grad)
    if bound_avg:
        Lfilt = np.array(grad[exc_z:bwind]) 
        Rfilt = np.array(grad[-bwind:-exc_z])
        mean = np.average(Lfilt)
        std = np.std(Lfilt)
        grad[:exc_z] = np.random.normal(loc=mean,scale=std,size=exc_z)
        
        mean = np.average(Rfilt)
        std = np.std(Rfilt)
        grad[-exc_z:] = np.random.normal(loc=mean,scale=std,size=exc_z)
        
    if norm:
        grad = grad/max(grad)
    
    if plotting:
        fig,ax = plt.subplots(3,1)
        ax[0].plot(signal)
        ax[1].plot(grad1)
        ax[2].plot(grad)
        plt.show()
        plt.close()
    return grad

def LinearBS(s):
    slope = (s[-1]-s[0])/len(s)
    bkg = np.arange(len(s))*slope
    news = s-bkg
    return news

pth5 = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/LaMnOx_18CIT49586_rerun_all_oned.h5'
XrdDict = FpDict(pth5,Xrays=True)
MatSysName = '18CIT49586_LaMnOx_xrd_map'
imagespath = '/home/vandover/Desktop/XRDmapImgs/La-Mn-O/'

# minI = 10
# maxI = 0
# for key in dwellsort(list(XrdDict)):
# #     print(key)
#     s = TechStripe()
#     s.Xraymap(key,dpath=pth5)
#     xmap = s.xmap

#     if np.min(xmap)<minI:
#         minI = np.min(xmap)
    
#     if np.max(xmap)>maxI:
#         maxI = np.max(xmap)


# print(minI,maxI)

for key in dwellsort(list(XrdDict)):
    print(key)
    s = TechStripe()
    s.Xraymap(key,dpath=pth5)
    xmap = s.xmap
    xpos = XrdDict[key]['xpos']
    ypos = XrdDict[key]['ypos']
    tau = XrdDict[key]['tau']
    T = XrdDict[key]['T']
    
    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(xmap,aspect='auto',vmin=0.20757583264003507,vmax=2.5660075960790842)
    ax.set_xticks([])
    ax.set_yticks([])

    plt.subplots_adjust(left=0,bottom=0,right=1,top=1,wspace=0,hspace=0)
    fig.savefig(imagespath+f'x{xpos}_{ypos}_{tau}_{T}.png')
    
#     plt.show()
    plt.close(fig)

#data paths for user to define
imagespath = '/home/vandover/Desktop/XRDmapImgs/La-Mn-O/'
outputpath = '/home/vandover/Desktop/XRDmapImgs/'
pathtoblank = '/home/vandover/Desktop/GrayBullshit.png'
MatSysName = '18CIT49586_LaMnOx_xrd_map'

WaferMap(MatSysName=MatSysName,imagespath=imagespath,outputpath=outputpath,pathtoblank=pathtoblank,filetype='.png')