import numpy as np
import matplotlib.pyplot as plt
from SARA_cornell_funcs import *
from edge_finder import *
from cookb_signalsmooth import *
import os
import glob
import sys
import cv2
import json
from WaferMapper import WaferMap
import matplotlib.backends.backend_pdf as PDF_saver


#all FPs for the 3 techniques
MatSysName = '2020-06-12_19AJA17_Ga2O3'
ptim = '/home/vandover/Documents/Data/19AJA17_Ga2O3/Images/2020-06-15_white/'
ptImgMirror = '/home/vandover/Documents/Data/19AJA17_Ga2O3/PSC/2020-06-12/mirror.bmp'
ptsp_raw = '/home/vandover/Documents/Data/19AJA17_Ga2O3/Spectroscopy/2020-06-12/'
ptsp_m = '/home/vandover/Documents/Data/19AJA17_Ga2O3/Spectroscopy/2020-06-12/Mirror/mirror_00.csv'
ptsp_b = '/home/vandover/Downloads/REAL_Blank.dat'
pth5 = '/home/vandover/Documents/Data/19AJA17_Ga2O3/Ga2O3_19AJA17_all_oned.h5'
OptDict = FpDict(ptim,ftype='.bmp')
SpecDict = FpDict(ptsp_raw,ftype='.csv')
XrdDict = FpDict(pth5,Xrays=True)


#For Ga2O3, one sample was missed during the CHESS run. This supliments the data so that 
# things can run smoothly
XrdDict['tau_10000_T_1160'] = XrdDict['tau_10000_T_1200']

print(XrdDict['tau_10000_T_1160'])
print(XrdDict['tau_10000_T_1200'])

def DustCollector(im,binsize=20,exc_z=1,filt_crit=3,plotting=True):
    """DustCollector filters the image"""
    nbins = int(np.floor(im.shape[0]/binsize))
    emeans = []
    estds = []
    ROIs = []
    for i in np.linspace(exc_z,nbins-exc_z,nbins-2*exc_z,dtype=int):
    #     print(i)
        ROI_lower = i*binsize
        ROI_upper = (i+1)*binsize
        ROI_avg = np.average(im[ROI_lower:ROI_upper],axis=0)
        ROIs.append(ROI_avg)

        ensemble_mean = 0.5*(np.mean(im[:ROI_lower],axis=0) + np.mean(im[ROI_upper:],axis=0))
        ensemble_std = 0.5*(np.std(im[:ROI_lower],axis=0) + np.std(im[ROI_upper:],axis=0))
        emeans.append(ensemble_mean)
        estds.append(ensemble_std)

    ROIs = np.array(ROIs,dtype=float)
    emeans = np.array(emeans,dtype=float)
    estds = np.array(estds,dtype=float)
    diffs = []
    good = []
    bad = []
    for idx in np.arange(len(ROIs)):
        diff = abs(ROIs[idx]-emeans[idx])
        diffs.append(diff)
        if np.any(diff>filt_crit*estds[idx]):
            bad.append(ROIs[idx])
        else:
            good.append(ROIs[idx])
    good = np.array(good)
    bad = np.array(bad)
    diffs = np.array(diffs)
    if plotting:
        fig,ax = plt.subplots(4,1,figsize = (4,12))
        ax[0].imshow(im)
        ax[1].imshow(diffs,aspect='auto',label='all')
        if len(good)>0:
            ax[2].imshow(good,aspect='auto',label='good')
        if len(bad)>0:
            ax[3].imshow(bad,aspect='auto',label='bad')
        plt.show()
        plt.close(fig)

        fig,ax = plt.subplots(3,1)
        ax[0].plot(np.average(ROIs,axis=0))
        if len(good)>0:
            ax[1].plot(np.average(good,axis=0))
        if len(bad)>0:
            ax[2].plot(np.average(bad,axis=0))
        plt.show()
        plt.close(fig)
        
    return good,bad,diffs

def JitterCorrection(xmap):
    """This will remove some of the jitter in an XRD map due to the detector binning incorrectly
    The mean of each column (1D XRD scan) is collected into an array. the mean of the means is
    determined and a uniform shift is applied based on the differnece between the mean of a column
    and the overall mean so that they are all the same.
    
    Basically, the average intensity of each scan should be the same
    """
    temp = []
    newmap=[]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]
        temp.append(np.mean(col))
    meanofmeans = np.mean(temp)
    diffs = [mean-meanofmeans for mean in temp]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]-diffs[idx]
        newmap.append(col)
        
    return np.array(newmap).T
class TechStripe:
    def __init__(self):
        
        #Technique Yspans
        self.opt_yrange = (0,70)
        self.wl = []
        self.Q = []
        
        #Technique Maps
        self.omap = []
        self.smap = []
        self.xmap = []
        
        self.omapcrop = (200,500)
        
        #Gradients of techniques
        self.ograd = []
        self.sgrad = []
        self.xgrad = []
        
        #Distane Across Stripes
        self.ospan = ()
        self.sspan = ()
        self.xspan = ()
        
        #Pixel Sizes
        self.O_pxs = 0.943
        self.S_pxs = 10
        self.X_pxs = 10
    
        #paths to raw data
        self.fpo = 'path/to/img'
        self.fprs = 'path/to/rawSpec'
        self.fpb = 'path/to/blank'
        self.fpm = 'path/to/mirror'
        self.fpImM = 'path/to/image/mirror'
    h5file = 'path/to/.h5'
        
    def Optmap(self,okey,weights=[0.33,0.33,0.33],grayscale=False,Norm=True,crop=False):
        """Loads the image of the stripe, converts to grayscale if desired with 
        a specified weighting [r,g,b]"""
        omap = cv2.imread(self.fpo)
        
        if Norm:
            mirror = cv2.imread(self.fpImM)
            mirror = np.transpose(np.array([[smooth(row,window_len=25) for row in mirror[:,:,j]] for j in np.arange(mirror.shape[2])]),axes=(1,2,0))
            omap = omap/mirror
        
        if grayscale:
            omap = np.average(omap,axis=2,weights=weights)
            if crop:
                self.omap = omap[self.omapcrop[0]:self.omapcrop[1],:]
        else:
            if crop:
                self.omap = omap[self.omapcrop[0]:self.omapcrop[1],:,:]
            
        self.opt_yrange = self.O_pxs*np.array([0,omap.shape[0]],dtype=float)
        return omap
        
    
#     def plot(Tmap,span):
    def Specmap(self,okey):
        """The data is pretty much only good from 400 nm to 800 nm so that is 
        where the filter is appied ot the self.smap, the complete data can be called 
        upon with the output of the function"""
        wl, unfsmap, smap,meta = get_spects(self.fprs,self.fpm,self.fpb)
        
        self.smap = smap[[(wl>=500) & (wl<= 850)][0],:]
        self.wl = wl[(wl>=500) & (wl<= 850)]
        return wl,unfsmap,smap,meta
        

    def Xraymap(self,xkey,dpath=h5file,logscale=True):
        """This function generates the xrd map from the location in the .h5 file with the 
        appropriate key corresponding to the condition desired.
               
        if logscale is True(False), the data will be scaled appropriately.
        
        """
        AllXRD = h5.File(dpath,'r')
        substripescans = list(AllXRD['exp'][xkey].keys())
        substripescans.sort(key=int)
        data=[]
        for jdx, scan_num in enumerate(substripescans):
            Q,I = AllXRD['exp'][xkey][scan_num]['integrated_1d']

            if scan_num == '0':
                data = np.append(data, I[:-24], axis=0)
            else:
                data = np.vstack((data,I[:-24]))
        
        data = data.T
        if logscale==True:
            data = np.log10(data)
        
        data = JitterCorrection(data)
        self.xmap = data
        self.Q = Q
        return data
    
    def Grad(TechMap,bwind=15,bound_avg=False,norm=False):
        gradTechMap = np.array([GenGrad(signal=row,bwind=bwind,exc_z=3,bound_avg=bound_avg) for row in TechMap])
        grad1D = np.average(gradTechMap,axis=0)
        if norm:
            grad1D = grad1D/max(grad1D) 
        return grad1D,gradTechMap
#end of obj
def Grad(TechMap,bwind=15,bound_avg=False,norm=False):
#     print(bound_avg)
    gradTechMap = np.array([GenGrad(signal=row,bwind=bwind,exc_z=3,bound_avg=bound_avg,plotting=False) for row in TechMap])
    grad1D = np.average(gradTechMap,axis=0)
    if norm:
        grad1D = grad1D/max(grad1D) 
        
    return grad1D,gradTechMap

def GenGrad(signal,bwind=15,exc_z=3,bound_avg=True,norm=False,plotting=True):
    signal = LinearBS(signal)
    grad = np.gradient(signal)
    grad = np.sqrt(grad*grad)
    grad1 = np.copy(grad)
    if bound_avg:
        Lfilt = np.array(grad[exc_z:bwind]) 
        Rfilt = np.array(grad[-bwind:-exc_z])
        mean = np.average(Lfilt)
        std = np.std(Lfilt)
        grad[:exc_z] = np.random.normal(loc=mean,scale=std,size=exc_z)
        
        mean = np.average(Rfilt)
        std = np.std(Rfilt)
        grad[-exc_z:] = np.random.normal(loc=mean,scale=std,size=exc_z)
        
    if norm:
        grad = grad/max(grad)
    
    if plotting:
        fig,ax = plt.subplots(3,1)
        ax[0].plot(signal)
        ax[1].plot(grad1)
        ax[2].plot(grad)
        plt.show()
        plt.close()
    return grad

def LinearBS(s):
    slope = (s[-1]-s[0])/len(s)
    bkg = np.arange(len(s))*slope
    news = s-bkg
    return news

PC = {}
SC = {}
Okeys = dwellsort(list(OptDict))
Xkeys = dwellsort(list(XrdDict))
shiftol = 51
basepath = '/home/vandover/Documents/Data/19AJA17_Ga2O3/'

# instantiating the pdf to be made. ALL pdf lines must be enabled. uncomment this line if desired
# pdf = PDF_saver.PdfPages(basepath+MatSysName+'_3-Technique-Anaylsis.pdf')
#yellow,red,green,blue,mint
colors = ['#EEC643','#BF4E30','#44BBA4','#06AED5','#ACFCD9']

# test = [17,42,108,151,200,330,450,600]

# test = [['tau_700_T_900']]
# for idx in test:
# for key in ['tau_700_T_900','tau_400_T_900','tau_1000_T_1075','tau_8000_T_1250']:
#     idx = 0
for idx in np.arange(len(Okeys)):
    key = Okeys[idx]
    s = TechStripe()
    #Have to instantiate the data with the proper paths to data, pixel sizes, etc. 
    PC[key] = {}
    SC[key] = {}
    TechNames = ['Optical','Spectroscopy','XRD']
    #occasionally the XRD keys and Spectroscopy or Optical keys are not QUITE the same
    #but when sorted by tau or T, will be properly paired up. hence the Okey Xkey distinction
    okey = Okeys[idx]
    xkey = Xkeys[idx]
    xkey = XrdDict[xkey]['fp'][1]
    # okey = key
    # xkey = key
    
    xpos = OptDict[okey]['xpos']
    ypos = OptDict[okey]['ypos']
    print(okey)
    print(xkey)
    print('')
    
    #giving the stripe class the appropriate paths to the data
    s.fpo = OptDict[okey]['fp']
    s.fprs = SpecDict[okey]['fp']
    s.fpb = ptsp_b
    s.fpm = ptsp_m
    s.fpImM = ptImgMirror
    s.h5file = pth5
    s.O_pxs = 0.785
    s.omapcrop = (425,575)
    #making the technique maps
    s.Optmap(okey=okey,grayscale=True,weights=[0.3,0.59,0.11],Norm=True,crop=True)
    s.Specmap(okey=okey)
    s.Xraymap(xkey=xkey,dpath=s.h5file)
    
    
    #getting the pixel sizes or defining them here
    ops = s.O_pxs
    sps = s.S_pxs
    xps = s.X_pxs
    
    #converting wavelength to eV
    wlr = s.wl
    hc = 2.998*10**8*4.1357*10**-15
    eVr =  hc/(wlr*10**-9)
    Q = s.Q
    
    #calling on the appodized maps(400-800 nm) and Y-cropped omap
#     omap = ndimage.gaussian_filter(np.array(s.omap[:,:,1],dtype=float),(15,15))
    omap = s.omap
    smap = s.smap
    xmap = s.xmap

    #defining fourier smoothing criteria (needs to be the same SPATIAL distance smoothed over)
    sf_o = int(np.round(10/s.O_pxs))
    sf_s = 1
    sf_x = 1
    #the signals should be comprably smoothed so a fourier fileter of 3% total pixels is applied
    #the XRD gets a second modification which is a sharpen unsharpen filter. see the "sharpen_y"
    #filter in the edge finder code.
    
#     omap = DustCollector(omap,binsize=5,exc_z=2,filt_crit=4,plotting=False)[0]
    omap1 = ndimage.gaussian_filter(omap,(sf_o,sf_o))
    xmap = ndimage.gaussian_filter(xmap,(sf_x,sf_x))
    xmap1 = sharpen_y(xmap,alpha=0.25,bf=[1,1],fbf=[0.0,0.8],plotting=False)
    
    #Gradients
    osig = np.average(omap1,axis=0)
    og = GenGrad(osig,bwind=200,exc_z=75,bound_avg=True,norm=True,plotting=False)
    sg = Grad(smap,bound_avg=True,norm=True)[0]
    xg = Grad(xmap1,bound_avg=True,norm=True)[0]

# #     Gradient test plot
#     fig,ax = plt.subplots(3,2)
#     ax[0,0].plot(og,'r')
#     ax[1,0].plot(sg,'g')
#     ax[2,0].plot(xg,'b')
#     ax[0,1].imshow(omap1,aspect='auto')
#     ax[1,1].imshow(smap,aspect='auto')
#     ax[2,1].imshow(xmap1,aspect='auto')
    
    # plt.show()
    # plt.close(fig)
    

    def h_filt(sig,h_cutoff,filt=3):
        a_sigma = abs((np.std(sig[0:h_cutoff])+np.std(sig[-h_cutoff:]))*0.5)
        a_mean = abs((np.mean(sig[0:h_cutoff])+np.mean(sig[-h_cutoff:]))*0.5)
        h_filt = filt*a_sigma + a_mean
#         print(max(sig))
#         print(h_filt)
        return h_filt
    
    otrs = signal.find_peaks(og,prominence=0.05,width=[0.001*len(og),0.25*len(og)],height=h_filt(og,150,filt=3))[0]
    strs = Spectroscopy_Transiton_Finder(smap,Gpromfilt=.05,h_thresh=3,h_cutoff=10,s_param=0.01,cond=key,plotting=False)['TR_idx']
    xtrs = signal.find_peaks(xg,prominence=0.02,width=[0.001*len(xg),0.25*len(xg)],height=h_filt(xg,15,filt=3))[0]


    
    # each technique spans a different anneal space. Each technique needs to be tailored accordingly.
    # For this sample, the x-ray maps span 1.5 mm, whereas the optical and spectroscopy are ~1.2 mm 
    # and 2.01 mm respectively. This will change depending on the material system, the zoom of the
    # optical camera, the space collected by the spectroscopy and that of the XRD. 
    
    #lateral space of each technique   
    ox = np.linspace(0,ops*(len(og)-1),len(og))
    sx = np.linspace(0,sps*(len(sg)-1),len(sg))
    xx = np.linspace(0,xps*(len(xg)-1),len(xg))    
    
    #centers
    oc = CenterFinding(og,sparam=20,plotting=False)
    sc = CenterFinding(sg,sparam=2,plotting=False)
    xc = CenterFinding(xg,sparam=2,plotting=False)
#     print(oc,sc,xc)
    
    #centering the anneal space around the center of the stripe
    ox = (ox-oc*ops)
    sx = (sx-sc*sps)
    xx = (xx-xc*xps)
    
    #correcting the Transition positions for origin shift and putting it in anneal space
    otrs = [ox[i] for i in otrs]  
    strs = [sx[i] for i in strs] 
    xtrs = [xx[i] for i in xtrs]

    # minmax of each technique
    minx = max(min(ox),min(sx),min(xx))
    maxx = min(max(ox),max(sx),max(xx))
#     print(minx,maxx)

    omap = omap[:,[(ox>=minx)&(ox<=maxx)][0]]
    smap = smap[:,[(sx>=minx)&(sx<=maxx)][0]]    
    xmap = xmap[:,[(xx>=minx)&(xx<=maxx)][0]]
    
    og = og[(ox>=minx)&(ox<=maxx)]
    sg = sg[(sx>=minx)&(sx<=maxx)]
    xg = xg[(xx>=minx)&(xx<=maxx)]
    
    ox = ox[(ox>=minx)&(ox<=maxx)]
    sx = sx[(sx>=minx)&(sx<=maxx)]
    xx = xx[(xx>=minx)&(xx<=maxx)]
    
    otrs = np.array(otrs)[(otrs>=minx) & (otrs<=maxx)] 
    strs = np.array(strs)[(strs>=minx) & (strs<=maxx)]
    xtrs = np.array(xtrs)[(xtrs>=minx) & (xtrs<=maxx)]
    
    
    #Pulling it all together
    TechPSs = [ops,sps,xps]
    TechGrads = [og,sg,xg]
    TechMaps = [omap,smap,xmap]
#     print(f'pixel sizes:{TechPSs}')
    
#     #Centering test plot
#     fig,ax = plt.subplots(3,1,sharex=True)
#     ax[0].plot(ox,og,'r')
#     ax[1].plot(sx,sg,'g')
#     ax[2].plot(xx,xg,'b')
#     for c in [0,1,2]:
#         ax[c].axvline(0,c='goldenrod')
#     ax[0].set_xlim(minx,maxx)
#     plt.show()
#     plt.close(fig)
    
    plotting=True
    if plotting:
        fig,ax = plt.subplots(3,2,figsize=(6.4,6.4),constrained_layout=True,dpi=300)
        fig.suptitle(f'{MatSysName}  cond: {key}  x: {xpos}  y: {ypos}')
        ax[0,0].imshow(omap,aspect='auto',cmap='cividis',extent=[minx,maxx,0,omap.shape[0]*ops])
        ax[1,0].imshow(smap,aspect='auto',cmap='cividis',extent=[minx,maxx,eVr[-1],eVr[0]],)
        ax[2,0].imshow(xmap,aspect='auto',cmap='cividis',extent=[minx,maxx,max(Q),min(Q)])

        ax[0,0].set_xticks([])
        ax[1,0].set_xticks([])
        ax[2,0].set_xlabel('Across stripe (\u03bcm)')
        ax[0,0].set_ylabel('Along stripe (\u03bcm)')
        ax[1,0].set_ylabel('Photon energy (eV)')
        ax[2,0].set_ylabel('Q (nm$^{-1})$')

    #     ax[0,0].set_xlim([0,800])
    #     ax[1,0].set_xlim([0,800])
    #     ax[2,0].set_xlim([-400,400])

        #rightern plot
        ax[0,1].plot(ox,og,c=colors[0])
        ax[1,1].plot(sx,sg,c=colors[3])
        ax[2,1].plot(xx,xg,c=colors[1])
        for tr in otrs:
            ax[0,0].axvline(tr,ymin=0.5,ymax=1,c=colors[2])
        for tr in strs:
            ax[1,0].axvline(tr,ymin=0.5,ymax=1,c=colors[2])
        for tr in xtrs:
            ax[2,0].axvline(tr,ymin=0.5,ymax=1,c=colors[2])
        ax[0,1].set_xticks([])
        ax[1,1].set_xticks([])
        ax[2,1].set_xlabel('Across stripe (\u03bcm)')
        ax[0,1].set_xticks([])
        ax[1,1].set_xticks([])
        ax[0,1].set_xlim([-575,575])
        ax[1,1].set_xlim([-575,575])
        ax[2,1].set_xlim([-575,575])
        for yt in [0,1,2]:
            ax[yt,1].set_yticks([])

        ax[0,0].set_title('Maps')
        ax[0,1].set_title('Gradients (a.u.)')

#         plt.show(fig)
#         plt.savefig(f'/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Take6/pdf_imgs/{MatSysName}_{okey}.pdf')
#         plt.close(fig)
    
    #Correlation code
    for adx,bdx in [(0,1),(0,2),(1,2)]:
    
        #loading in the technique pairs Opt:Spec, Opt:XRD, Spec:XRD
        a = TechGrads[adx]
        b = TechGrads[bdx]
        aps = TechPSs[adx]
        bps = TechPSs[bdx]
        aname = TechNames[adx]
        bname = TechNames[bdx]
    #         print(aname,bname)
    #         print(len(a),len(b))

        #Turning the gradients into  splines for the optimized correlation functions
        afxn,anormgrad,axgrad,acidx = GradSigNormAndCenter(signal=a,pixelsize=aps,CFon=True,plotting=False)
        bfxn,bnormgrad,bxgrad,bcidx = GradSigNormAndCenter(signal=b,pixelsize=bps,CFon=True,plotting=False)


        # finding the shared space for the two techniques to be compared
        maxx = min(max(axgrad),max(bxgrad)) 
        sharedx = np.linspace(0,maxx,501)
        
        # optimizes the Gradients over the shared space within some distance from eachother
        xshift, PC0, newx, nafxn, nbfxn = bestPC(sharedx,bfxn,afxn,shift_tol=shiftol,plotting=False)
        PC[key][f'PC for {aname} and {bname}'] = PC0
        
        xshift, SC0, newx, nafxn, nbfxn = bestSC(sharedx,bfxn,afxn,shift_tol=shiftol,plotting=False)
        SC[key][f'SC for {aname} and {bname}'] = SC0
        
#         print('pearsons')
#         print(scipy.stats.pearsonr(afxn(sharedx),bfxn(sharedx))[0])
#         print('')
        
#         print('spearmans')
#         print(scipy.stats.spearmanr(a=afxn(sharedx),b=bfxn(sharedx))[0])
        
# #         print(PC0)
#         print('')
    textstr = ['PC']
    for compare in list(PC[key]):
        textstr.append(f'{compare[7:]}: {PC[key][compare]}')
    textstr = '\n'.join(textstr)
#     print(textstr)
    ax[2,0].text(0, -0.75, textstr,transform=ax[2,0].transAxes)
    
    textstr = ['SC']
    for compare in list(SC[key]):
        textstr.append(f'{compare[7:]}: {SC[key][compare]}')
    textstr = '\n'.join(textstr)
#     print(textstr)
    ax[2,1].text(0, -0.75, textstr,transform=ax[2,1].transAxes)
    
    plt.savefig(f'/home/vandover/Documents/Data/19AJA17_Ga2O3/pdf_imgs/{MatSysName}_{okey}.pdf')
    plt.close(fig)


PCCfp = f'/home/vandover/Documents/Data/19AJA17_Ga2O3/{MatSysName}_optimized_PC.json'
with open(PCCfp,'w') as json_file:
    json.dump(PC,json_file)

SCCfp = f'/home/vandover/Documents/Data/19AJA17_Ga2O3/{MatSysName}_optimized_SC.json'
with open(SCCfp,'w') as json_file:
    json.dump(SC,json_file)


# #making the pdf from the images
# import PyPDF2
# fp2pdfs = '/home/vandover/Documents/Data/19AJA17_Ga2O3/pdf_imgs/'
# fps = list(glob.glob(fp2pdfs+'*.pdf'))
# merger = PyPDF2.PdfFileMerger()

# for key in Okeys:
#     file = fp2pdfs+f'{MatSysName}_{key}.pdf'
#     merger.append(file)
    
# merger.write(f'/home/vandover/Desktop/{MatSysName}_withoutDustCollector.pdf')