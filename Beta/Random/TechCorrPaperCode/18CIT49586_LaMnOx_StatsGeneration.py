#Python "essentials"
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import glob
import itertools
import cv2
import json

from TauT_Mapper import TauT
from WaferMapper import WaferMap
from MSI_Transition_Finder import MSI_Transition_Finder 
#These are the librarires that Max and I have built
from cookb_signalsmooth import *
from edge_finder import *
from SARA_cornell_funcs import *
#for image processing and map stitching
import PIL
import PIL.Image as Image

import imutils
from skimage.measure import block_reduce
import imageio
import math
from scipy.interpolate import interp1d
#optimization algorithms require some of this
from scipy import optimize, signal
from scipy import ndimage

import matplotlib as mpl
from matplotlib import gridspec
mpl.rcParams['figure.dpi'] = 150
import colorsys
import matplotlib.backends.backend_pdf as PDF_saver

#defined Functions
def CenterFinding(signal,sparam,window_len=0,filt=False,plotting=False):
    ssig = ndimage.gaussian_filter1d(signal,sparam)
    mid = int(0.5*len(ssig))
    if filt == True:
        corr = ndimage.correlate(ssig,ssig[::-1],mode='wrap')[(mid-window_len):(mid+window_len)]
    else:
        corr = ndimage.correlate(ssig,ssig[::-1],mode='wrap')
    center = (len(ssig))*0.5 + (np.argmax(corr)-(len(corr))*0.5)*0.5
    if plotting == True:
        plt.figure()
        plt.plot(signal,c='dodgerblue')
        plt.plot(ssig,c='goldenrod')
        plt.axvline(center,c='darkorchid')
        plt.show()
    return center

def GradSigNormAndCenter(signal,pixelsize,sparam=0.5,CFon=True,plotting=False):
    pxs = pixelsize
    grad = signal
    if CFon==True:
        center_idx = CenterFinding(grad,sparam)
    else:
        center_idx = int(len(signal)*0.5)
    Cgrad = center_idx*pxs
    xgrad = np.linspace(0,pxs*len(grad),len(grad))

    SplinedFunction = interp1d(xgrad,grad,kind='cubic')
    if plotting == True:
        plt.plot(xgrad,grad)
        plt.axvline(Cgrad,c='goldenrod')
        plt.show()
        plt.close()
    return SplinedFunction,grad,xgrad,center_idx

def bestPC(x,fopt,fspec,shift_tol=20,plotting=False):
    #remove DC component
    opt = fopt(x)
    spec = fspec(x)
    optmod = opt-np.mean(opt) 
    specmod = spec-np.mean(spec)
    dx = x[1]-x[0]
    mid = int(np.round(0.5*(len(x))))
    
    PCs=[]
    search_window = np.linspace(-shift_tol,shift_tol,2*shift_tol+1)
    for mc in search_window:
    #correct allowed dimension
        xmod = x-mc*dx
        minx = max(min(x),min(xmod))
        maxx = min(max(x),max(xmod))
    #     print(minx,maxx)
        newx = np.round(np.linspace(minx,maxx,501),decimals=1)
    #     print(newx)

        nfopt = interp1d(newx,fopt(newx),kind='cubic')
        nfspec = interp1d(newx,spec,kind='cubic')

        PC=np.round(scipy.stats.pearsonr(nfopt(newx),nfspec(newx))[0],decimals=3)
        PCs.append(PC)
        
    mc = search_window[PCs==max(PCs)][0]
    
    xmod = x-mc*dx
    minx = max(min(x),min(xmod))
    maxx = min(max(x),max(xmod))
#     print(minx,maxx)
    newx = np.round(np.linspace(minx,maxx,501),decimals=1)
#     print(newx)

    nfopt = interp1d(newx,fopt(newx),kind='cubic')
    nfspec = interp1d(newx,spec,kind='cubic')
    PC = np.round(scipy.stats.pearsonr(nfopt(newx),nfspec(newx))[0],decimals=3)
    xshift=mc*dx
    
    # plotting to troubleshoot
    if plotting == True:
        fig,ax = plt.subplots(3,1,constrained_layout=True)
        ax[0].plot(x,optmod)
        ax[0].plot(x,specmod)
#         ax[1].plot(cor,'dodgerblue')
        ax[1].plot(search_window,PCs)
        ax[2].plot(newx,nfopt(newx))
        ax[2].plot(newx,nfspec(newx))
        
        plt.show()
        plt.close()
    return xshift, PC, newx, nfopt, nfspec

def get_N_HexCol(N=5):
    HSV_tuples = [(x * 1.0 / N, 0.5, 0.5) for x in range(N)]
    hex_out = []
    for rgb in HSV_tuples:
        rgb = map(lambda x: int(x * 255), colorsys.hsv_to_rgb(*rgb))
        hex_out.append('#%02x%02x%02x' % tuple(rgb))
    return hex_out

def symmetrize(x,sig,center):

    xmod1 = x-center
    left = xmod1[xmod1<0]
    right = xmod1[xmod1>0]
    fsignal = interp1d(xmod1,sig,kind='cubic')
    
    minx = min(max(abs(left)),max(abs(right)))
    xmod = np.round(np.linspace(-minx,minx,len(xmod1)),decimals=5)
    left = xmod[xmod<0]
    right = xmod[xmod>0]
    
    lsig = fsignal(left)
    rsig = fsignal(right)
    avg_opt = (lsig+rsig[::-1])*0.5
    sym_opt = np.hstack((avg_opt,fsignal(0),avg_opt[::-1]))
    xmod = np.linspace(-minx,minx,len(sym_opt))
    sym_sig = interp1d(xmod,sym_opt,kind='cubic')
    
    return sym_sig,xmod


def Spectroscopy_Transiton_Finder(spec_map,cond='string',Gpromfilt=0,h_thresh=10,h_cutoff=12,pix_siz=10,s_param=2,plotting=True):
    TransitionStats = {}
    
    im = spec_map
    grad_im = []
    xpix = im.shape[1]
    for idx in np.arange(im.shape[0]):
        x = np.gradient(im[idx,:])
        x = np.sqrt(x*x)
        grad_im.append(x)
        
    grad_im = np.array(grad_im)
    grad = ndimage.gaussian_filter1d(np.sum(grad_im,axis=0),s_param)
    grad = grad/max(grad)
    
    correlation = ndimage.correlate(grad,grad[::-1],mode='wrap')
    spec_center = (len(correlation))*0.5 + (np.argmax(correlation)-(len(correlation))*0.5)*0.5
    
    a_sigma = abs((np.std(grad[0:h_cutoff])+np.std(grad[-h_cutoff:]))*0.5)
    a_mu = abs((np.mean(grad[0:h_cutoff])+np.mean(grad[-h_cutoff:]))*0.5)
    grad = grad-a_mu
    
    h_filt = h_thresh*a_sigma
#     print(a_mu,a_sigma,h_filt)
    
    spec_trans = signal.find_peaks(grad,prominence=Gpromfilt,width=[0.001*xpix,0.25*xpix],height=h_filt)
    
    TransitionStats['center'] = spec_center
    TransitionStats['TR_idx'] = spec_trans[0]
    TransitionStats['transitions'] = {}
    TransitionStats['transitions']['widths'] = spec_trans[1]['widths']
    TransitionStats['transitions']['FWHMs'] = signal.peak_widths(grad,spec_trans[0],rel_height=0.5)[0]
    TransitionStats['transitions']['heights'] = spec_trans[1]['peak_heights']
    TransitionStats['transitions']['prominences'] = spec_trans[1]['prominences']
    TransitionStats['transitions']['distance_from_center'] = (spec_trans[0]-spec_center)*pix_siz 
    TransitionStats['StripeCenterDistFromImageCenter'] = (spec_trans[0]-spec_center)*pix_siz-(int(0.5*im.shape[1])-spec_center)*pix_siz
    TransitionStats['horizontal FOV']= xpix*pix_siz
    TransitionStats['LSA_condition']= cond
    TransitionStats['gradient'] = grad
    if plotting==True:
        fig,ax = plt.subplots(2,1,constrained_layout=True)
        
        fig.suptitle(cond)
        ax[0].imshow(spec_map,aspect='auto')
        ax[0].axvline(spec_center,c='orchid')
        for tr in spec_trans[0]:
            ax[0].axvline(tr,c='goldenrod')
        ax[1].plot(grad)
        ax[1].axvline(spec_center,c='orchid')
        for tr in spec_trans[0]:
            ax[1].axvline(tr,c='goldenrod')
        
        plt.show()
        plt.close(fig)
    return TransitionStats

def xrd_transition_finder(xrd_map,cond='string',Gpromfilt=0,h_thresh=10,h_cutoff=12,pix_siz=10,s_param=2,plotting=True):
    TransitionStats = {}
    
    im = sharpen_y(xrd_map)
    grad_im = []
    xpix = im.shape[1]
    for idx in np.arange(im.shape[0]):
        x = np.gradient(im[idx,:])
        x = np.sqrt(x*x)
        grad_im.append(x)
        
    grad_im = np.array(grad_im)
    grad = ndimage.gaussian_filter1d(np.sum(grad_im,axis=0),s_param)
    grad = grad/max(grad)    
    
    correlation = ndimage.correlate(grad,grad[::-1],mode='wrap')
    xrd_center = (len(correlation))*0.5 + (np.argmax(correlation)-(len(correlation))*0.5)*0.5
    
    a_sigma = abs((np.std(grad[0:h_cutoff])+np.std(grad[-h_cutoff:]))*0.5)
    a_mu = abs((np.mean(grad[0:h_cutoff])+np.mean(grad[-h_cutoff:]))*0.5)
    grad = grad-a_mu
    
    h_filt = h_thresh*a_sigma
    
    
    xrd_trans = signal.find_peaks(grad,prominence=Gpromfilt,width=[0.001*xpix,0.25*xpix],height=h_filt)
    
    TransitionStats['center'] = xrd_center
    TransitionStats['TR_idx'] = xrd_trans[0]+1
    TransitionStats['transitions'] = {}
    TransitionStats['transitions']['widths'] = xrd_trans[1]['widths']
    TransitionStats['transitions']['FWHMs'] = signal.peak_widths(grad,xrd_trans[0],rel_height=0.5)[0]
    TransitionStats['transitions']['heights'] = xrd_trans[1]['peak_heights']
    TransitionStats['transitions']['prominences'] = xrd_trans[1]['prominences']
    TransitionStats['transitions']['distance_from_center'] = (xrd_trans[0]+1-xrd_center)*pix_siz 
    TransitionStats['StripeCenterDistFromImageCenter'] = (xrd_trans[0]+1-xrd_center)*pix_siz-(int(0.5*im.shape[1])-xrd_center)*pix_siz
    TransitionStats['horizontal FOV'] = xpix*pix_siz
    TransitionStats['LSA_condition'] = cond
    TransitionStats['gradient'] = grad
    if plotting==True:
        fig,ax = plt.subplots(2,1,constrained_layout=True)
        
        fig.suptitle(cond)
        ax[0].imshow(xrd_map,aspect='auto')
        ax[0].axvline(xrd_center,c='orchid')
        for tr in xrd_trans[0]:
            ax[0].axvline(tr,c='goldenrod')
        ax[1].plot(grad)
        ax[1].axvline(xrd_center,c='orchid')
        for tr in xrd_trans[0]:
            ax[1].axvline(tr,c='goldenrod')
        
        plt.show()
        plt.close(fig)
    return TransitionStats

print('')
print('')
print('')
print('')
print('Collecting RGB data')



#RGB DATA and Mat Sys name
imagespath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/Images/'
outputpath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/'
pathtoblank = '/home/vandover/Desktop/GrayBullshit.png'
MatSysName='18CIT49586_LaMnOx_color'

#parameters for the user to adjust as the need to
RGB = True
mpath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake1/Mirror_for_Images_85ms.bmp'

#important for setting up your output dictionaries
channel = ['r','g','b','bw']
ImgTrs = {}
ImgDict = {}
xys = {}
fnames = {}
FPs = []
testkeys = []
for file in glob.glob(imagespath+'*.bmp'):
    FPs.append(file)
mirror = cv2.imread(mpath,0)[400:530,:]
# for jdx in np.arange(5):
for file in FPs:
#     file = FPs[jdx]
    fname = os.path.basename(file)[:-4]
    s = fname.split('_')
    tau = str(int(s[2]))
    T = str(int(s[3]))
    key = '_'.join(('tau',tau,'T',T))
    testkeys.append(key)
    xys[key] = (int(s[0][1:]),int(s[1]))
    ImgTrs[key] = {}
    ImgDict[key] = {}
    fnames[key] = fname
    if RGB==True:
        ImgDict[key]['whole'] = np.array(cv2.imread(file)[400:530,:,:],dtype='uint8')
        for ch in [0,1,2]:
            img = np.array(cv2.imread(file)[:,:,ch][400:530,:],dtype='uint8')
            ImgDict[key][channel[ch]]=img
            
#         ImgDict[key]['bw'] = np.array(cv2.imread(file,0)[400:530,:],dtype='uint8')
        ImgDict[key]['gray']=cv2.equalizeHist(cv2.imread(file,0)[400:530])
        img = ImgDict[key]['gray']
        Trs = MSI_Transition_Finder(filepath=img,mpath=mirror,blur=10,h_thresh=10,Gpromfilt=0,v_cutoff=0,pixel_size=0.668,norm=False,ImgIO=True,plotting=False)
#         I = np.sum(img,axis=0)
#         I = I - np.arange(len(I))*(I[-2]-I[0])/len(I)
#         grad = np.gradient(I)
#         grad = np.sqrt(grad*grad)
#         plt.plot(smooth(grad,30))
        
        ImgTrs[key]['center'] = Trs['center']
        if Trs['transitions']!='none':
            ImgTrs[key]['Tr_indicies'] = Trs['TR_idx']
            ImgTrs[key]['heights'] = Trs['transitions']['heights']
            ImgTrs[key]['FWHMs'] = Trs['transitions']['FWHMs']
            ImgTrs[key]['proms'] = Trs['transitions']['prominences']
            ImgTrs[key]['widths'] = Trs['transitions']['widths']
            ImgTrs[key]['distance_from_center'] = Trs['transitions']['distance_from_center']
            ImgTrs[key]['gradient'] = Trs['Gradient']
        else:
            ImgTrs[key] = 'none'

    else:

        img = cv2.imread(file,0)
#         print(img.shape)
        ImgDict[key]=img
        Trs = MSI_Transition_Finder(filepath=img,mpath=mpath,blur=4,h_thresh=10,Gpromfilt=50,norm=True,plotting=False,v_cutoff=0)
#         print(Trs)
        ImgTrs[key]['center'] = Trs['center']
        if Trs['transitions']!='none':
            ImgTrs[key]['Tr_indicies'] = Trs['TR_idx']
            ImgTrs[key]['heights'] = Trs['transitions']['heights']
            ImgTrs[key]['FWHMs'] = Trs['transitions']['FWHMs']
            ImgTrs[key]['proms'] = Trs['transitions']['prominences']
            ImgTrs[key]['widths'] = Trs['transitions']['widths']
            ImgTrs[key]['distance_from_center'] = Trs['transitions']['distance_from_center']
            ImgTrs[key]['gradient'] = Trs['Gradient']
        else:
            ImgTrs[key] = 'none'



print('')
print('')
print('RGB finished')
print('')
print('')
print('Collecting Reflectance Data')







#Reflectance Spectroscopy data import
#This is optical reflectance data and analysis
data_filepath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/Spectroscopy/'
Blank_filepath = '/home/vandover/Documents/Data/La-Mn-18CIT49586/Spectroscopy/Blank/blank_01.csv'
Mirror_filepath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/Spectroscopy/Mirror/mirror_00.csv'


ReflDict = {}
Location = {}
FPs = []
keys = []
plotting = False
#extracting the data from the .csv files
for i in glob.glob(data_filepath+'*.csv'):
    #extracting lasGO coordinates and anneal conditions
    filename = os.path.basename(i)
    x = filename.split('_')
    string = '_'.join(['tau',str(int(x[2])),'T',str(int(x[3][:-4]))])
    FPs.append(i)
    keys.append(string)

for idx in np.arange(len(FPs)):#np.arange(5)
    data_fn = FPs[idx]
    string = keys[idx]
    smooth_data = get_spects(data_fn,Mirror_filepath,Blank_filepath)
    wl = smooth_data[0]
    wl_range = wl[(wl>=400) & (wl<= 850)]
    #Dictionary Generation
    print(idx)
    ReflDict[string] = smooth_data[2][[(wl>=400) & (wl<= 850)][0],:]
#     Location[string] = (smooth_data[3]['scan center'][0],smooth_data[3]['scan center'][1])
    
    if plotting == True: 
        
        data = smooth_data[2][[(wl>=400) & (wl<= 800)][0],:]
        plt.imshow(data,aspect='auto',extent=[0,2.00,min(wl_range),max(wl_range)])
        plt.title(string)
        plt.xlabel('Anneal Space in mm')
        plt.ylabel('wavelength in nm')
        plt.show()
        plt.close()
wl = wl_range

ReflTrs = {}
for key in list(ReflDict):
    print(key)
    spec_map = ReflDict[key]
    ReflTrs[key] =  Spectroscopy_Transiton_Finder(spec_map,h_thresh=5,h_cutoff=15,s_param=1,cond=key,plotting=False)



print('')
print('')
print('Reflectance Spectroscopy finished')
print('')
print('')
print('Collecting XRD data')



OG_path = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/LaMnOx_18CIT49586_rerun_all_oned.h5'

file = h5.File(OG_path,'r')
xrd_exp_files = file['exp']
data = []
xrdDict = {}
xrdQs = {}
xrdAttrs = {}
x = []
for idx, params in enumerate(xrd_exp_files):
    anneal_conditions = xrd_exp_files[params]
    stripe = list(anneal_conditions.keys())
    stripe.sort(key=int)
    name = params
    print(idx)
    xpos,ypos = (anneal_conditions.attrs['xw'],anneal_conditions.attrs['yw'])
    

    for jdx, scan_num in enumerate(stripe):        
        Q,I = anneal_conditions[scan_num]['integrated_1d']
        
        if scan_num == '0':
            data = np.append(data, I[:-24], axis=0)
        else:
            data = np.vstack((data,I[:-24]))


        xrdDict[name] = data.T
        xrdQs[name] = Q[:-24]

    data = []   
    xrdAttrs[name] = xpos,ypos
    
xrdkeys = dwellsort(list(xrdAttrs))




xrdkeys=dwellsort(list(xrdDict),Temp=True)
xrdTrs = {}
for key in xrdkeys: 
    xrd_map = xrdDict[key]
    print(key)
    xrdTrs[key] = xrd_transition_finder(xrd_map,h_thresh=5,h_cutoff=15,s_param=1,cond=key,plotting=False)
xrdkeys = dwellsort(list(xrdDict))







print('')
print('')
print('XRD Done Collecting')
print('')
print('')
print('PCC analysis')


#This is the correlation plots and saving the outputs for further manipulation
#the optimization is over a 3x3x3 mesh of RGB weights 


# where to write the output document to
basepath = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/'

# this needs to be set to True if you want to see the maps up against one another 
plotting = True

# This is more important when comparing the optical to either technique, because of the 
# resolution difference
resample = False
shiftol = 51

# instantiating the pdf to be made. ALL pdf lines must be enabled. uncomment this line if desired
# pdf = PDF_saver.PdfPages(basepath+MatSysName+'_3-Technique-Anaylsis.pdf')

# there is a slight discrepancy with the dwells for the active learned samples vs what was requested
# it is a rounding issue that can be overcome by sorting the same way. 
xkeys = dwellsort(list(xrdDict),Temp=True) 
okeys = dwellsort(list(ImgDict),Temp=True)

demkeys = dwellsort(list(ReflDict),Temp=True)
splitlitst = np.array([key.split('_') for key in demkeys])

# Defining a list of hexcode colors that make a rainbow! This gives the plot another dimension
# it's sorted by increasing temperature
utau = sorted(np.unique(splitlitst[:,1]),key=int)
uT = sorted(np.unique(splitlitst[:,3]),key=int)
ckey_dict = {}
colorsarray = cm.rainbow(np.linspace(0,1,len(uT)))

for idx,T in enumerate(uT):
    for tau in utau:
        c =  colorsarray[idx]
        ckey_dict['tau_'+str(tau)+'_T_'+str(T)] = c
ckey = []
for key in demkeys:
    ckey.append(ckey_dict[key])

# the dictionary that will house the gradient information for easier correlation
graddict = {}

# the correlation statistics are stored here
PC = {}
#for pretty image making
jazz = np.linspace(0,600,11,dtype=int)
# iterating through the conditions and extracting all the information from each of the techniques
for idx in np.arange(len(xkeys)):#jazz:
    #This is a diagnostic parameter important for determining where stripes were annealed
    xpos = xys[okeys[idx]][0]
    ypos = xys[okeys[idx]][1]
    xkey = xkeys[idx]
    key = okeys[idx]
    print(key)

    #inputing the pixel sizes once. Different sets of data were collected at different resolutions
    xps = 10
    sps = 10
    ops = 0.668
    ops1 = 10
    TechPSs = [ops,sps,xps]
    TechNames = ['Optical','Spectroscopy','XRD']
    
    # each technique spans a different anneal space. Each technique needs to be tailored accordingly.
    # For this sample, the x-ray maps span 1 mm, whereas the optical and spectroscopy are ~1.2 mm 
    # and 2.01 mm respectively. This will change depending on the material system, the zoom of the
    # optical camera, the space collected by the spectroscopy and that of the XRD. 
    
    #defining the anneal space span for each technique
    oscale = ImgDict[key]['gray'].shape[1]*ops
    sscale = ReflDict[key].shape[1]*sps
    xscale = xrdDict[xkey].shape[1]*xps    
    
    #locating the smallest one and determining the appropriate crop pixel
    scales = [oscale,sscale,xscale]
    minscale = min(scales)

    oscale = int(np.round(0.5*(oscale-minscale)/ops))
    sscale = int(np.round(0.5*(sscale-minscale)/sps))
    xscale = int(np.round(0.5*(xscale-minscale)/xps))
    scales = [oscale,sscale,xscale]
    
    #loading the technique maps
    smap = ReflDict[key][300:1300,:][:,sscale:-sscale] # the spectrometer is only good for the following wavelength values
    xmap = xrdDict[xkey][:,xscale:-xscale]
    
    #Locating Transitions in Spect and XRD
    sstats = Spectroscopy_Transiton_Finder(smap,Gpromfilt=.05,h_thresh=3,h_cutoff=10,s_param=.5,cond=key,plotting=False)
    xstats = xrd_transition_finder(xmap,h_thresh=3,h_cutoff=10,s_param=0.5,cond=xkey,plotting=False)
    
    
    graddict[key] = {}
    PC[key] = {}

    #Reducing RGB image down to grayscale with artificially optimizing the contrast via mixing RGB components
    #ChannelOptimization
    for rw in np.linspace(0.1,0.3,3):
        for gw in np.linspace(0.6,0.8,3):
            for bw in np.linspace(0.1,0.5,3):
#                 print(rw,gw,bw)
                mix = f'{rw} {gw} {bw}'
                omap = np.array(ImgDict[key]['whole'],dtype=float) # the image needs to be in float value for calculations
                omap = np.average(omap,axis=2,weights=[rw,gw,bw])
    
#     #this is the disection of the image into the RGB channels and to 
#     for jdx,cchannel in enumerate(['r','g','b']):
#         #was having issues with some of the transition finding/gradient calculations. Re-did the gradient
#         #calculations here
#         print(cchannel)
#         omap = np.array(ImgDict[key]['whole'],dtype=float)[:,:,jdx]
#         print(omap.shape)
                print(mix)
                isum = np.sum(omap,axis=0)/np.sum(mirror,axis=0)
                isum = ndimage.gaussian_filter1d(isum,15)
                isum = isum-(isum[-1]-isum[0])/len(isum)*np.arange(len(isum))
                isum = isum/max(isum)
                g = np.gradient(isum)
                g = np.sqrt(g*g)
                g = g/max(g)

                h_cutoff = 150

                a_sigma = abs((np.std(g[0:h_cutoff])+np.std(g[-h_cutoff:]))*0.5)
                a_mean = abs((np.mean(g[0:h_cutoff])+np.mean(g[-h_cutoff:]))*0.5)
                h_filt = 3*a_sigma
                xpix = len(g)
                g = g - a_mean

                # Locating the transitions
                ostats = signal.find_peaks(g,prominence=.05,width=[0.001*xpix,0.25*xpix],height=h_filt)[0]

                # redefined maps so that they span approximately the same anneal space. also applying a smoothing function
                omap = ImgDict[key]['whole']
                smap = ndimage.gaussian_filter(ReflDict[key][300:1300,:][:,sscale:-sscale],0.5)
                xmap = ndimage.gaussian_filter(xrdDict[xkey][:,xscale:-xscale],(3,.25))

                # Taking the gradients along the correct scale
                ograd = g
            #     ograd = ostats['Gradient']
                sgrad = sstats['gradient']
                xgrad = xstats['gradient']

                for grad in [ograd,sgrad,xgrad]:
                    grad[0] = np.mean(grad[:15])
                    grad[-1] = np.mean(grad[-15:])

                TechGrads = [ograd,sgrad,xgrad]

                if resample==True:
                    y = signal.resample(ograd,len(xgrad))
                    ops = 10
                    TechPSs = [ops,sps,xps]
                    TechGrads = [y,sgrad,xgrad]

                #the transitions that were located were for each techniques FULL map. this appropriately shifts
                #them to the proper cropped location
            #     otrs = ostats['TR_idx']
                otrs = ostats
                strs = sstats['TR_idx']
                xtrs = xstats['TR_idx']
            #     print('optical transitions',otrs)
                # anneal space definition
                ox = np.linspace(0,ops*(len(ograd)-1),len(ograd))
                sx = np.linspace(0,sps*(len(sgrad)-1),len(sgrad))
                xx = np.linspace(0,xps*(len(xgrad)-1),len(xgrad))

                #Centerfinding
                oc = CenterFinding(ograd,1,window_len=240,filt=True,plotting=False)
                sc = CenterFinding(sgrad,1,window_len=20,filt=True,plotting=False)
                xc = CenterFinding(xgrad,1,window_len=20,filt=True,plotting=False)
            #     print(oc,sc,xc)

                #Shifting the centers to be the zeropoint
                ox = (ox-oc*ops)
                sx = (sx-sc*sps)
                xx = (xx-xc*xps)

                #test plotting
            #     fig,ax = plt.subplots(3,1,constrained_layout=True)
            #     ax[0].plot(ox,ograd,c='r')
            #     ax[1].plot(sx,sgrad,c='g')
            #     ax[2].plot(xx,xgrad,c='b')
            #     ax[0].axvline(0,c='r')
            #     ax[1].axvline(0,c='g')
            #     ax[2].axvline(0,c='b')
            #     plt.show()
            #     plt.close(fig)

                # minmax so everything spans the same distance
                minx = max(min(ox),min(sx),min(xx))
                maxx = min(max(ox),max(sx),max(xx))

                #rewriting the maps to be over the correct distances
                omap = omap[:,[(ox>=minx)&(ox<=maxx)][0],:]
                smap = smap[:,[(sx>=minx)&(sx<=maxx)][0]]    
                xmap = xmap[:,[(xx>=minx)&(xx<=maxx)][0]]

                ocrop = np.where((ox>=minx)&(ox<=maxx))[0]
                scrop = np.where((sx>=minx)&(sx<=maxx))[0]
                xcrop = np.where((xx>=minx)&(xx<=maxx))[0]
            #      #the transitions that were located were for each techniques FULL map. this appropriately shifts
            #     #them to the proper cropped location
            #     otrs = ostats['TR_idx']
                otrs = otrs - min(ocrop)
                strs = strs - min(scrop)
                xtrs = xtrs - min(xcrop)

                # Gotta correlate the gradients now. The off diagonals of the correlation matrix is the 
                graddict[key][mix] = {}
                PC[key][mix] = {}
            #     print(xkey)
                for adx,bdx in [(0,1),(0,2),(1,2)]:

                    #loading in the technique pairs Opt:Spec, Opt:XRD, Spec:XRD
                    a = TechGrads[adx]
                    b = TechGrads[bdx]
                    aps = TechPSs[adx]
                    bps = TechPSs[bdx]
                    aname = TechNames[adx]
                    bname = TechNames[bdx]
            #         print(aname,bname)
            #         print(len(a),len(b))

                    #Turning the gradients into  splines for the optimized correlation functions
                    afxn,anormgrad,axgrad,acidx = GradSigNormAndCenter(signal=a,pixelsize=aps,CFon=True,plotting=False)
                    bfxn,bnormgrad,bxgrad,bcidx = GradSigNormAndCenter(signal=b,pixelsize=bps,CFon=True,plotting=False)

                    #storing them into the appropriate dictionary to be called upon later
                    graddict[key][mix][f'{aname} spline fxn'] = afxn
                    graddict[key][mix][f'{bname} spline fxn'] = bfxn


                    # finding the shared space for the two techniques to be compared
                    maxx = min(max(axgrad),max(bxgrad)) 
                    sharedx = np.linspace(0,maxx,501)
                    graddict[key][mix][f'x for {aname} and {bname}'] = sharedx

                    # optimizes the Gradients over the shared space within some distance from eachother
                    xshift, PC0, newx, nafxn, nbfxn = bestPC(sharedx,bfxn,afxn,shift_tol=shiftol,plotting=False)
                    PC[key][mix][f'PC for {aname} and {bname}'] = PC0

        #         print(PC0)


PCCfp = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Retake2/RGB_mixtures.json'
with open(PCCfp,'w') as json_file:
	json.dump(PC,json_file)




