import numpy as np
import timeit
import matplotlib.pyplot as plt
from itertools import permutations
from SARA_cornell_funcs import *
import itertools
import time
import sys

def time_the_function(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print ('Function name:{}\nWith arguments:{}, {})\nTook {} sec'.format(method.__name__, args, kw, te-ts))
        return result
    return timed

def create_cost_matrix(optical,spectro):
    costMatrix = np.ones((len(optical),len(spectro)))
    #Cost matrix is built row by row. In this case, the rows are set A and columns set B
    for i, Ai in enumerate(optical):
        for j, Bj in enumerate(spectro):
            costMatrix[i,j] = np.sqrt((Ai-Bj)**2)
    print("Optical", optical)
    print("Spectro", spectro)
    print("Cost Matrix:")
    print(np.round(costMatrix,decimals=0))
    n_sol = min((len(optical),len(spectro)))
    more_optical_than_spectro = False
    #The cost Matrix is oriented sot that the number of rows <= number of columns
    if costMatrix.shape[1]<costMatrix.shape[0]:
        costMatrix = costMatrix.T
        more_optical_than_spectro = True
        print("Transposed")
    print(np.round(costMatrix, decimals=0))
    return costMatrix, more_optical_than_spectro

def print_info(optical, spectro):
    print("{} optical transformation, {} spectro transformation".format(len(optical), len(spectro)))
    print("Optical \n{} \nSpectro \n{}".format(optical, spectro))
   
def get_lowest_cost_assignment(costMatrix, more_optical_than_spectro):
    lowest_cost = sys.maxsize
    lowest_comb = []
    indicator = ""
    if more_optical_than_spectro:
        indicator = "[Spectrometer, Optics]"
        print("There is more optical peaks.")
    else:
        indicator = "[Optics, Spectrometer]"
        print("There is more spectrometer peaks.")
    for N in range(1,len(costMatrix)+1):
        lowest_cost_for_N_assign = sys.maxsize
        lowest_comb_for_N_assign = []
        comb_list = generate_comb_list(costMatrix, N)
        for comb in comb_list:
            cost, assignment = calculate_lowest_cost(costMatrix, comb)
            if cost<lowest_cost_for_N_assign: 
                lowest_cost_for_N_assign = cost
                lowest_comb_for_N_assign = assignment
        print("lowest for {} assignment: {}".format(N,lowest_cost_for_N_assign))
        print("Assignment: {}={}".format(indicator,lowest_comb_for_N_assign))
        if lowest_cost_for_N_assign < lowest_cost:
            lowest_cost = lowest_cost_for_N_assign
            lowest_comb = lowest_comb_for_N_assign
    return lowest_cost, lowest_comb

#def get_min_dimension_of_2D_matrix(matrix):
#    if len(matrix)==0:
#        return 0
#    if len(matrix)>len(matrix[0]):
#        print("More spectroscopy tranisition found.")
#        return len(matrix[0])
#    elif len(matrix[0])>len(matrix):
#        print("More optical transition found.")
#        return len(matrix)
#    return len(matrix) if len(matrix)<len(matrix[0]) else len(matrix[0])

def calculate_lowest_cost(costMatrix, comb):
    lowest_cost = sys.maxsize
    lowest_comb = []
    cost = 0
    #CHANGED THIS LINE FROM PERMUTATION TO COMBINATIONS
    y = itertools.combinations(range(len(costMatrix[0])), len(comb))
    for c in y:
        for i, j in zip(comb,c):
            cost += costMatrix[i][j]
        if cost < lowest_cost: 
            lowest_cost = cost
            lowest_comb = list(zip(comb, c))
            cost = 0
        else:
            cost = 0
    return lowest_cost, lowest_comb

def generate_comb_list(costMatrix, N):
    return list(itertools.combinations(range(len(costMatrix)), N))

@time_the_function
def assign(optical, spectro):
    more_optical_than_spectro = False
    print_info(optical, spectro)
    cost_matrix, more_optical_than_spectro = create_cost_matrix(optical, spectro)
    return get_lowest_cost_assignment(cost_matrix, more_optical_than_spectro)


#start = timeit.default_timer()
# I have created arrays with all the transitions in the metric form already so you don't have to. attached are two example for you to improve timing with.
#Ox = np.load('IR_950_LaMnOx_OpticalTransition_distances.npy', allow_pickle=True)
#Sx = np.load('IR_950_LaMnOx_SpectroscopyTransition_distances.npy', allow_pickle=True)

## 8x8 data point
#Ox1 = Ox[4]
#Sx1 = Sx[4]
## 9x6 data point
#Ox2 = Ox[21]
#Sx2 = Sx[21]
#
#testnum=617
#for i in range(testnum):
#    assign(Ox[i],Sx[i])
#
#
#stop = timeit.default_timer()
#
#print("Total time: {}s".format(stop-start))
