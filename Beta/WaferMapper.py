#Lists for storing image paths and coordinates from those image paths used to make maps
import glob
import os
import itertools
from SARA_cornell_funcs import *
def WaferMap(imagespath,outputpath,MatSysName,pathtoblank,filetype='.bmp',align_coords=[],Resolution=128):
    """This will generate a spatial wafer map given a directory of images.
    
    imagespath: (path) the directory containing all the .bmp files for a given system
    outputpath: (path) where you want the map to be stored
    MatSysName: (str) appends an identifying label to the output file
    pathtoblank: (path) an image to fill any missing conditions/blank space in a wafer map
    Resolution: (int) should be a typical image size (e.g. 128, 256, 512...)
    
    Future Edits: Needs axes on the overall image to show conditions
    
    Returns: a .png image of the wafer map

    """
    RowsForWaferMap = []
    coords = []
    gbs=pathtoblank
    fnamepaths=[]
    keys=[]
    for file in glob.glob(imagespath+'*'+filetype):
        filename = os.path.basename(file)[:-len(filetype)]
        s = filename.split('_')
        x = int(s[0][1:])
        y = int(s[1])
        tau = str(int(s[2]))
        T = str(int(s[3]))
        coords.append([x,y])
        key = f'tau_{tau}_T_{T}'
        keys.append(key)
        fnamepaths.append(file)

    coords = np.array(coords)    
    rangey = np.unique(coords[:,1])
    rangex = np.unique(coords[:,0])


    dimex = len(rangex)
    dimey = len(rangey)
    # if align_coords==False:
    #     print('we here')
    #     AlCo = np.array([[0,40],[0,-40],[40,0],[-40,0],[32,30],[32,-30],[-32,30],[-32,-30],[0,0]])
    # else:
    if align_coords!=[]:
        AlCo = np.array(align_coords)

        delcor = []
        for acx,acy in AlCo:
            delcor.append(np.where((coords[:,0]==acx)&(coords[:,1]==acy))[0][0])
        # print(len(coords))
        coords = np.delete(coords,delcor,axis=0)
        rmv_fnamepaths = [fnamepaths[idx] for idx in delcor]
        fnamepaths = [path for path in fnamepaths if path not in rmv_fnamepaths]


    test = list(itertools.chain(itertools.product(rangex, rangey)))
    test = sorted(test,key=lambda x: (x[1],x[0])) 
    
    # for t in test: 
    #     print(t)        
    # print('') 
    
    for idx,(x,y) in enumerate(test): 
        pad = np.where((x==coords[:,0]) & (y==coords[:,1]))[0] 
        if len(pad) > 0:
            RowsForWaferMap.append(fnamepaths[np.where((x==coords[:,0]) & (y==coords[:,1]))[0][0]]) 
        else:
            RowsForWaferMap.append(gbs)

    WaferMapImage = make_contact_sheet(RowsForWaferMap,(dimex,dimey),(Resolution,int(Resolution*2.5)),(1,1,1,1),1).transpose(Image.FLIP_LEFT_RIGHT)
    WaferMapImage = WaferMapImage.rotate(180)

    WaferMapImage.save(outputpath+MatSysName+'_WaferMap.png')

    return 'done'