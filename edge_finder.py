import re
import csv
import glob
import copy as cp
import math as mt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy import optimize
from matplotlib import rc
import matplotlib.font_manager as font_manager
from scipy.spatial import distance
from scipy.signal import savgol_filter
from cookb_signalsmooth import *
from scipy.signal import blackmanharris
from numpy.fft import rfft
from numpy import argmax, mean, diff, log
from parabolic import parabolic
import scipy 
from scipy import signal
from scipy import ndimage
from gzopen import gzopen
from scipy.signal import convolve2d
from scipy.signal import find_peaks
#Helper functions
def fft_smoothing(d,param):
#d is a 1D vector, param is the cutoff frequency
    rft = np.fft.rfft(d)
    rft[int(param):] = 0.
    d = np.fft.irfft(rft,len(d))
    return d

def edge_filter(normal_data,fname):
#Filter and edge finder using first derivative approximations
    if fname == "sobel":
        fx = np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
        ims = []
        cx = convolve2d(normal_data, fx, mode="same", boundary="symm")
        im_conv = np.sqrt(cx*cx)# + sy*sy)
        return im_conv
    elif fname == "sobel-feldman":
        fx = np.array([[-3,0,3],[-10,0,10],[-3,0,3]])
        ims = []
        cx = convolve2d(normal_data, fx, mode="same", boundary="symm")
        im_conv = np.sqrt(cx*cx)# + sy*sy)
        return im_conv
    elif fname == "scharr":
        fx = np.array([[-47,0,47],[-162,0,162],[-47,0,47]])
        ims = []
        cx = convolve2d(normal_data, fx, mode="same", boundary="symm")
        im_conv = np.sqrt(cx*cx)# + sy*sy)
        return im_conv
    elif fname == "robert":
        fx = np.array([[0,0,0],[0,1,0],[0,0,-1]])
        ims = []
        cx = convolve2d(normal_data, fx, mode="same", boundary="symm")
        im_conv = np.sqrt(cx*cx)# + sy*sy)
        return im_conv
    elif fname == "prewitt":
        fx = np.array([[1,0,-1],[1,0,-1],[1,0,-1]])
        ims = []
        cx = convolve2d(normal_data, fx, mode="same", boundary="symm")
        im_conv = np.sqrt(cx*cx)# + sy*sy)
        return im_conv
    elif fname == "kirsch":
        fwest= np.array([[5,-3,-3],[5,0,-3],[5,-3,-3]])
        feast= np.array([[-3,-3,5],[-3,0,5],[-3,-3,5]])
        ims = []
        cwest = convolve2d(normal_data, fwest, mode="same", boundary="symm")
        ceast = convolve2d(normal_data, feast, mode="same", boundary="symm")
        im_conv = np.sqrt(cwest*cwest + ceast*ceast)
        return im_conv
    elif fname == "robinson":
        fwest= np.array([[1,0,-1],[2,0,-2],[1,0,-1]])
        feast= np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
        ims = []
        cwest = convolve2d(normal_data, fwest, mode="same", boundary="symm")
        ceast = convolve2d(normal_data, feast, mode="same", boundary="symm")
        im_conv = np.sqrt(cwest*cwest + ceast*ceast)
        return im_conv
    elif fname == "nevatia-babu":
        fx = np.array([[-100,-100,0,100,100],[-100,-100,0,100,100],[-100,-100,0,100,100],[-100,-100,0,100,100],[-100,-100,0,100,100]])
        ims = []
        cx = convolve2d(normal_data, fx, mode="same", boundary="symm")
        im_conv = np.sqrt(cx*cx)# + sy*sy)
        return im_conv
    else:
        print("Filter not implemented")


def edge_finder(im_conv,lheight,lprom,dist):
    im_conv_sum = np.sum(im_conv,axis=0)
    #Parameters for Savitzky-Golay filtering
    window_length = 5
    #Smoothing method
    smooth_method = 'hanning'
    im_conv_sum = smooth(im_conv_sum, window_len=window_length, window=smooth_method)
    prom = np.mean(im_conv_sum)*lprom
    stats = find_peaks(im_conv_sum, height=np.mean(im_conv_sum)*lheight, distance=dist, prominence=prom, width=0.01*len(im_conv_sum))
    peaks = stats[0]
    return peaks, im_conv_sum,stats


def get_edge(normal_data_orig,lheight=0.8,lprom=0.5,numvote=4,s_param=25,plotting=True):
    normal_data = cp.deepcopy(normal_data_orig)
    dist=2

#Smooth out data
#    s_param = 25
    for i in range(normal_data.shape[1]):
        normal_data[:,i] = fft_smoothing(normal_data[:,i],s_param)


#Filter and edge finder
    filters = ["sobel","sobel-feldman","scharr","robert","prewitt","kirsch","robinson","nevatia-babu"]
    peaks = [] ; im_max = [] ; im_conv = [] ; stats = []
#     lheight = 0.8; lprom = 0.5
    for f in filters:
        im_c = edge_filter(normal_data[:,:],f)
        p, i_max, s = edge_finder(im_c,lheight,lprom,dist)
        peaks.append(p)
        im_max.append(i_max)
        im_conv.append(im_c)
        stats.append(s)
#Compute peak position by voting: count how frequently a peak is found for a specific value
#     numvote = 4
    all_peaks = np.array([])
    for p in peaks:
        all_peaks = np.append(all_peaks,p)
    unique, counts = np.unique(all_peaks, return_counts=True)
    v_peaks = unique[np.where( counts > numvote)[0]]
#    print (v_peaks,len(v_peaks))
#Now lower the height until the value is even, if odd, to enforce symmetry
    if len(v_peaks)%2 != 0:
        for l in np.linspace(lheight, 2., num=100):
 #           print (l)
            peaks = [] ; stats = []
            all_peaks = np.array([])
            for im_c in im_conv:
                p, tmp,s = edge_finder(im_c,l,lprom,dist)
                all_peaks = np.append(all_peaks,p)
                peaks.append(p)
                stats.append(s)

            unique, counts = np.unique(all_peaks, return_counts=True)
#            print(counts)
            v_peaks = unique[np.where( counts > numvote)[0]]
            if len(v_peaks)%2 == 0:
                break
#    print (v_peaks,len(v_peaks))
    if len(v_peaks)%2 != 0:
        v_peaks = unique[np.where( counts > numvote+1)[0]]
    if len(v_peaks)%2 != 0:
        v_peaks = unique[np.where( counts > numvote+2)[0]]
    if len(v_peaks)%2 != 0:
        v_peaks = unique[np.where( counts > numvote+3)[0]]
    if len(v_peaks)%2 != 0:
        v_peaks = unique[np.where( counts > numvote+4)[0]]
    if len(v_peaks)%2 != 0:
        v_peaks = unique[np.where( counts > numvote+5)[0]]
        
    if plotting==True:
        fig, ax1 = plt.subplots(nrows=1, constrained_layout = False)
        pos = np.array(range(normal_data.shape[1]))
        x = []
        y = []
        aax = []
        lines = []
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        for p,im,f,i in zip(peaks,im_max,filters,range(len(filters))):
            aax.append(ax1.twinx())
            aax[i].set_yticks([])
            ll, = aax[i].plot(pos,im,color = colors[i],label = f)
            aax[i].plot(pos[p],im[p], "x")
            lines.append(ll)

#         fig.legend(lines, [l.get_label() for l in lines],loc="center right",   # Position of legend
#                   borderaxespad=0.1,    # Small spacing around legend box
#                   title="Parameters"  # Title for the legend
#                   )
        plt.show()
        plt.imshow(normal_data[:,:], aspect='auto')
        plt.show()
    return v_peaks, unique, counts, stats

def get_edge_dist(normal_data_orig,center,nav=3,alpha=200,lheight = 0.5,lprom = 0.2,cutoff = 30,plotting=True):
    normal_data = cp.deepcopy(normal_data_orig)
    f = normal_data
    blurred_f = ndimage.gaussian_filter(f, [2,2])
    filter_blurred_f = ndimage.gaussian_filter(blurred_f, [0,0.8])
    sharpened = blurred_f + alpha * (blurred_f - filter_blurred_f)
    if plotting==True:
        plt.figure(figsize=(12, 12))
        plt.subplot(131)
        plt.imshow(blurred_f, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.subplot(132)
        plt.imshow(blurred_f - filter_blurred_f, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.subplot(133)
        plt.imshow(sharpened, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.tight_layout()
        plt.show()
    normal_data = sharpened
    #To make the shit symmetric, split the search for the peaks by scanning from the center to the left and right
    xdata = np.array(range(normal_data_orig.shape[1]))
    cnorm = np.zeros(len(xdata))
    weights = np.amax(normal_data, axis = 0)
    weights = np.ones(len(weights))
    #Left
    for i in range(center,center-cutoff,-1):
        cnorm[i] = distance.cosine(np.sum(normal_data[:,i:i+nav:1],axis=1)/nav, np.sum(normal_data[:,i:i-nav:-1],axis=1)/nav)*weights[i]
    #Right
    for i in range(center,center+cutoff,1):
        cnorm[i] = distance.cosine(np.sum(normal_data[:,i:i-nav:-1],axis=1)/nav, np.sum(normal_data[:,i:i+nav:1],axis=1)/nav)*weights[i]
    cnorm = np.array(cnorm)/np.max(cnorm)
    if plotting ==True:
        plt.plot(xdata,cnorm)
        plt.show()
    peaks = [] ; im_max = [] ; im_conv = [] 

    dist = 2
    prom = np.mean(cnorm)*lprom
    peaks, _ = find_peaks(cnorm, height=lheight, distance=dist, prominence=prom)

    if len(peaks)%2 != 0:
        for l in np.linspace(0.01*lprom, 10*lprom, num=100):
#             print (l)
            all_peaks=np.array([])
            p, tmp = find_peaks(cnorm,height=lheight,prominence=l,distance=dist)
            all_peaks = np.append(all_peaks,p)
#             print(p)       
            if len(p)%2 == 0:
                peaks = all_peaks.astype(int)
                break

    if plotting ==True:
        plt.plot(xdata,cnorm)
        plt.plot(xdata[peaks],cnorm[peaks],"x")
        plt.show()

    return peaks
    

def sharpen_y(normal_data_orig,alpha=20,bf=[2,2],fbf=[0.8,0.],plotting=False):
    normal_data = cp.deepcopy(normal_data_orig)
    f = normal_data
    blurred_f = ndimage.gaussian_filter(f, bf)
    filter_blurred_f = ndimage.gaussian_filter(blurred_f, fbf)
    sharpened = blurred_f + alpha * (blurred_f - filter_blurred_f)
    if plotting:
        plt.figure(figsize=(12, 12))
        plt.subplot(131)
        plt.imshow(blurred_f, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.subplot(132)
        plt.imshow(blurred_f - filter_blurred_f, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.subplot(133)
        plt.imshow(sharpened, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.tight_layout()
        plt.show()
    normal_data = sharpened
    return sharpened

