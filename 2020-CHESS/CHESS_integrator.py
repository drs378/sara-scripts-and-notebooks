import os
from glob import glob
import numpy as np
import pandas as pd
from imageio import imread
import pyFAI
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
import sys
sys.path.append('/home/vandover/Documents/2020_CHESS_run/')

from xrdCalibFit import PoniFit, automask, ptt
import matplotlib.pyplot as plt



def CHESS_Integrator(tau=3214,Tmax=1000,plotting=False):
	"""
	The integrating function. given a Tau and Tmax, XRD data is collected and returns a 2d array
	"""
	# NOTE: these .poni files were generated on rotated images!
	wd = '/home/vandover/zoo-fs/data/CHESS/2019-12-CHESS-3B/Ga2O3_19AJA17/'
	PONIS = glob(f"{wd}/poni/*.poni")
	DBCSV = f"{wd}/14_Ga2O3_19AJA17_Summary.csv"
	TIFFS = glob(f"/home/vandover/zoo-fs/data/CHESS/2019-12-CHESS-3B-raw/Ga2O3_19AJA17/Stripes/*/*.tiff")
	OUTPUTDIR = f"/home/vandover/Desktop/Test/Integrator/"


	#Dan's conversion uses the db that Mike constructed post experiment. We can use the same db for the conditions and mapping to position for the integration but we'll invert the process
	# tau = 1023 #tau input request
	# Tmax = 1000 #Tmax input request


	#porting in the database
	db = pd.read_csv(DBCSV)

	# CHESS x-ray wavelength in meters; energy = 9.7 keV
	wl = 1.2781875567010312e-10
	def CondToPos(db,tau, Tmax):
	    """
	    Takes the expected Tau and Tmax and returns the x and y positions as well as the stripe
	    index and sub scan x positions

	    
	    """
	    
	    idx = np.where((db['LSAjob:Tpeak']==Tmax)&(db['LSAjob:dwell']==tau))[0]
	    x_center = float(db['Scan:xcenter'][idx])
	    y_center = float(db['Scan:ycenter'][idx])
	    
	    scan_pos = np.linspace(float(db['LasGo:xmin'][idx]),
	                           float(db['LasGo:xmax'][idx]),
	                           int(db['chess_xrd:count'][idx]))
	    stripe_idx = int(db['LasGo:scan_number'][idx])
	    return (x_center,y_center),str(stripe_idx).zfill(3),scan_pos 


	(x,y),sidx,x_sub_arr = CondToPos(db,Tmax=Tmax,tau=tau)
	print(sidx)
	# this establishes the PONI map based on the 9 wafer points. done as a calibration step prior to
	# any active learning
	pf = PoniFit(PONIS)

	# Building the detector dimensions for the integration
	rot300k = pyFAI.detectors.Detector(
	    pixel1=172.0e-6, pixel2=172.0e-6, max_shape=(487, 619)
	)

	#the path to all the 2D detector images for a stripe with the requested conditions. This can be where the raw chess data is input.
	pathstoraw = list(glob(f"/home/vandover/zoo-fs/data/CHESS/2019-12-CHESS-3B-raw/Ga2O3_19AJA17/Stripes/*_{sidx}/*.tiff"))

	#Array for the integrated patterns to fall into
	xmap = []
	transformed_chiQ = []
	#integrating each of the paterns for the given xy conditions
	for tiff in pathstoraw:

		#tracking integration number
	    print(os.path.basename(tiff))

	    #loading the dp
	    img = imread(tiff).T

	    #integrating using pyFAI
	    ai = az(*pf.getpars((x,y)), detector=rot300k, wavelength=wl)
	    
	    #adds a mask that Dan Guevarra wrote
	    ai.mask = automask(img)

	    #returns the Q,I for 1D integration
	    Q1d, I1d = ai.integrate1d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
	    
	    #returns the 2D Q,I,Chi data
	    I2d, Q2d, chi2d = ai.integrate2d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
	    
	    transformed_chiQ.append(I2d)
	    #only storing the 1D data
	    xmap.append(I1d)

	#Converting to an array and transposing so the xaxis is position and the y axis is Q space
	xmap = np.array(xmap).T



	#test plotting
	if plotting:
	    fig,ax = plt.subplots(1,1,dpi=150)
	    ax.imshow(xmap,aspect='auto',extent=[0,100,np.max(Q1d),np.min(Q1d)])
	    ax.set_xlabel('scan index')
	    ax.set_ylabel('Q in nm$^{-1}$')
	    ax.set_title(f'Tau:{tau}\u03BCs Tmax:{Tmax}\u00B0C xpos:{np.round(x,1)}mm ypos:{np.round(y,1)}mm')
	#     fig.colorbar(pcm)
	    plt.show(fig)
	    plt.close(fig)

	return xmap,Q1d,Q2d,chi2d,I2d,transformed_chiQ
	#minor change

xmap,Q,Q2d,chi2d,I2d,tcq = CHESS_Integrator(tau=10000,Tmax=1080,plotting=True)

plt.imshow(tcq[50],extent=[Q2d[0],Q2d[-1],chi2d[-1],chi2d[0]])
plt.show()
plt.close()

print(chi2d)