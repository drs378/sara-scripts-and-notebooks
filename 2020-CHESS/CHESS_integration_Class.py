import os
from glob import glob
import numpy as np
import pandas as pd
from imageio import imread
import pyFAI
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
from xrdCalibFit import PoniFit, automask, ptt
import matplotlib.pyplot as plt


class ChessIntegrator():
	def __int__

# NOTE: these .poni files were generated on rotated images!
wd = '/home/vandover/Documents/2020_CHESS_run/'
PONIS = glob(f"{wd}/input/Au_alignment/*.poni")
DBCSV = f"{wd}/input/14_Ga2O3_19AJA17_Summary.csv"
TIFFS = glob(f"{wd}/input/Stripes/*/*.tiff")
OUTPUTDIR = f"/home/vandover/Desktop/Test/Integrator/"


#Dan's conversion uses the db that Mike constructed post experiment. We can use the same
#db for the conditions and mapping to position for the integration but we'll invert the process
tau = 1023 #tau input request
Tmax = 1000 #Tmax input request


#porting in the database
db = pd.read_csv(DBCSV)

# CHESS x-ray wavelength in meters; energy = 9.7 keV
wl = 1.2781875567010312e-10
def CondToPos(db,tau, Tmax):
    """
    Takes the expected Tau and Tmax and returns the x and y positions as well as the stripe
    index and sub scan x positions

    
    """
    
    idx = np.where((db['LSAjob:Tpeak']==Tmax)&(db['LSAjob:dwell']==tau))[0]
    x_center = float(db['Scan:xcenter'][idx])
    y_center = float(db['Scan:ycenter'][idx])
    
    scan_pos = np.linspace(float(db['LasGo:xmin'][idx]),
                           float(db['LasGo:xmax'][idx]),
                           int(db['chess_xrd:count'][idx]))
    stripe_idx = int(db['LasGo:scan_number'][idx])
    return (x_center,y_center),str(stripe_idx).zfill(3),scan_pos 


(x,y),sidx,x_sub_arr = CondToPos(db,Tmax=Tmax,tau=tau)
print(sidx)
# this establishes the PONI map based on the 9 wafer points. done as a calibration step prior to
# any active learning
pf = PoniFit(PONIS)

# Building the detector dimensions for the integration
rot300k = pyFAI.detectors.Detector(
    pixel1=172.0e-6, pixel2=172.0e-6, max_shape=(487, 619)
)

#the path to all the 2D detector images for a stripe with the requested conditions
pathstoraw = list(glob(f"/home/vandover/zoo-fs/data/CHESS/2019-12-CHESS-3B-raw/Ga2O3_19AJA17/Stripes/*_{sidx}/*.tiff"))

#Array for the integrated patterns to fall into
xmap = []
for tiff in pathstoraw:
    print(os.path.basename(tiff))
    img = imread(tiff).T

    ai = az(*pf.getpars((x,y)), detector=rot300k, wavelength=wl)
    ai.mask = automask(img)
    Q1d, I1d = ai.integrate1d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
    I2d, Q2d, chi2d = ai.integrate2d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
    
    xmap.append(I1d)

    
xmap = np.array(xmap).T


plotting=False
#test plotting
if plotting:
    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(xmap,aspect='auto',extent=[np.min(Q1d),np.max(Q1d),100,0])
    ax.set_xlabel('scan index')
    ax.set_ylabel('Q in nm$^{-1}$')
    ax.set_title(f'Tau:{tau}\u03BCs Tmax:{Tmax}\u00B0C xpos:{np.round(x,1)}mm ypos:{np.round(y,1)}mm')
#     fig.colorbar(pcm)
    plt.show(fig)
    plt.close(fig)
