mport pyFAI
from pyFAI.calibrant import CALIBRANT_FACTORY
from pyFAI.goniometer import SingleGeometry
import h5py as H5
import gzip
from extraCode import ZingerBGone
import sys
import itertools
sys.path.insert(1,"/home/duncan/sara-socket-client/")
import truncation_rod as trr

def get_poni_from_az(azimuthal_integrator=None):
    """
    returns the array of
    wavelength in m,dist in m,poni1,poni2,rot1,rot2,rot3
    """
    ai = azimuthal_integrator
    PONI_arr = np.array([ai._dist, ai._poni1, ai._poni2,
                                  ai._rot1, ai._rot2, ai._rot3, ai._wavelength],
                                 dtype=np.float64)
    return np.array(PONI_arr)

class PONI_fit():
    """
    init_poni        (.poni file)  initial guess for the PONI parameters
    detector         (str or detector) detector name or pyFAI detector object
    calibrant_name   (str)  Elemental name
    tol              (float64,float64) the minimum residual value, the delta between refinements
    """
    def __init__(self, init_poni="file.poni", detector="Eiger1M", calibrant_name="Au",tol=(1e-6,1e-7), maxiter=10):
        try:
            self.detector = pyFAI.detector_factory(detector)
        except:
            self.detector = detector
            
        self.calibrant = CALIBRANT_FACTORY(calibrant_name)
        self.calibrant_name = calibrant_name
        self.calibrant.wavelength = np.genfromtxt(init_poni,skip_header=1)[-1,-1]
        self.init_poni = init_poni
        self.qs = []
        self.Is = []
        self.refined_PONI_params = []
        self.residuals = []
        self.sg = False
        self.tol = tol
        self.maxiter=maxiter
        
    def single_fit(self, frame=None, integrate_frame=False):
        self.sg = SingleGeometry(label=self.calibrant_name, image=frame, geometry=self.init_poni,
                           detector=self.detector, calibrant=self.calibrant)

        self.sg.extract_cp()
        self.ai = self.sg.get_ai()
        self.refined_PONI_params.append(get_poni_from_az(self.ai))
        if integrate_frame:
            q,I = ai.integrate1d(frame,1024, unit="q_nm^-1", method="csr", safe=True)
            self.qs.append(q)
            self.Is.append(I)
        return self.ai
        
    def multi_fit(self, frame=None, integrate_frame=False, fix=["rot1", "rot2", "rot3", "wavelength"]):
        #Need to instantiate some SingleGeometry
        if self.sg == False:
            self.single_fit(frame)
        
        #keep track of residuals, change, and iteration    
        res = 1e0
        dres = 1e0
        i = 1
        
        #fit until convergence
        while (self.tol[0] < res) and (self.tol[1] < dres) and (self.maxiter> i):
            res_n = self.sg.geometry_refinement.refine2(fix=fix)
            dres = res - res_n
            res = res_n
            i += 1
            self.residuals.append(res_n)
            print(res_n,dres,i)
        
            #Make new PONI files from optimized fit
            if self.calibrant_name=="Au":
                self.sg.extract_cp(max_rings=5)
            else:
                self.sg.extract_cp()

        self.ai = self.sg.get_ai()
        
        #integrate if asked for
        if integrate_frame:
            q,I = ai.integrate1d(frame,1024, unit="q_nm^-1", method="csr", safe=True)
            self.qs.append(q)
            self.Is.append(I)
        
        #store the next iteration of PONI params
        self.refined_PONI_params.append(get_poni_from_az(self.ai))
        return self.ai
    
if __name__() == "__main__":
    
    auxzoo =  '/home/duncan/zoo-fs/data/CHESS/2022-04-CHESS-3B/CHESS_AUX/'
    daqzoo =  '/home/duncan/zoo-fs/data/CHESS/2022-04-CHESS-3B/CHESS_RAW/'
    init_poni = '/home/duncan/zoo-fs/data/CHESS/2022-04-CHESS-3B/CHESS_AUX/18_63560_Ta2Pb2O7/PONIs/scan_001.poni'
    fn = '/home/duncan/zoo-fs/data/CHESS/2022-04-CHESS-3B/CHESS_RAW/40_AL_TiCrOx/Calibration/PONIs/v-calib_pad_32_30_001/v-calib_pad_32_30_EIG1_001_data_000240.h5.gz'
    file = gzip.open(fn)
    
    
    frame = H5.File(file,'r')['entry']['data']['data'][0]
    frame = trr.truncation_rod_max(ZingerBGone(test_frame,maxi=1000))[0]
    plt.imshow(np.log10(frame+2),origin='lower')
    plt.show()
    
    
    pf = PONI_fit(init_poni=init_poni,calibrant_name="Au",detector="Eiger1M")
    pf.multi_fit(frame=frame,fix=['rot3','wavelength'])
    pparams = get_poni_from_az(pf.sg.get_ai())
    autofit_ponis.append(pparams)
    σ, conf = pf.sg.geometry_refinement.curve_fit()
    errors = [σ[key] for key in list(σ)]

    σ_autofits.append(σ)
    pf.sg.geometry_refinement.save(f"/home/duncan/Desktop/AutoPoniOut/PONIs/scan_{str(i).zfill(3)}")
    pf.sg.geometry_refinement.save(f"/home/duncan/Desktop/AutoPoniOut/PONIs/scan_{str(i+9).zfill(3)}")
    summary.append([int(xpad)]+[int(ypad)]+[i for b in itertools.zip_longest(pparams, errors) for i in b if i is not None])
    autofit_ponis_constrained = np.array(autofit_ponis_constrained)
    summary = np.array(summary)
    poniheader = 'XX in mm, YY in mm, dist in m, dist_err in m, poni1, poni1_err, poni2, poni2_err, rot1 in rad, rot1_err, rot2 in rad, rot2_err, rot3 in rad, wl in m'
    np.savetxt(f'/home/duncan/Desktop/AutoPoniOut/{platename}_summary.txt',summary,delimiter=',', header=poniheader)

   
