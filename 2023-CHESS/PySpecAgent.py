import numpy as np
from pyspec.client import spec
import sys
import time
# sys.path.insert(1,'/home/duncan/Documents/')
from CondtoPath import condtopath
from pyspec.css_logger import log

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"


class PySpecAgent():
    """
    This class will interact with the spec server on the hutch IDB3 computer and allow SARA pass the appropriate commands

    ConnectToServer()                 Creates the spec connection from sara to the spec server
    SetFilePaths(path,filename2D)     Creates the directory structure and moves to the proper location. sends newfile cmd to spec
    SetFlyScan()                     Sets up flyscan cmd and arms the detector for XRD collection

    """
    def __init__(self, address='id3b.classe.cornell.edu', port='spec'):
        self.address = address
        self.port = port
        self.connected = False        
        self.output = []
        self.last_output = None
    def _update_output(self, value, channel):
        self.output.append(value)
        self.last_output = self.output[-1]
        print(self.last_output)
        

    def _update_channel(self, value, channel):
        print(f"{channel}: {value}")


    def ConnectToServer(self):
        '''
        Connect to the external spec server at connection name conn.
        This should be an object that can be passed to the other functions that talk to a spec server
        '''
        self.conn = f'{self.address}:{self.port}'
        print('')
        print(self.conn)

        self.client = spec(self.conn) #hard coded connection
        # while not self.client.is_connected():
        #     pass

        # if self.client.is_connected():
        #     print(f'established connection to {self.conn}')
        
        #register our previously used channels:
        self.client.register('status/ready',self._update_channel) #this channel can be read as if spec is busy or not??
        self.client.register('output/tty',self._update_output) #this channel returns the output from the cmd line and passes it into the output list
        print('List of registered channels')
        for ch in list(self.client.reg_channels):
            print(ch)
        print("")
        return

    def SetFilePaths(self, path, filename2D):
        '''
        SetFilePaths will create the file system and change to the directory where the current raw data willbe stored
        The commands passed into the spec server are:
            mkdir -p path/to/2d/dir/
            cd path/to/2d/dir/
            newfile 2dfilename
        '''
        #make the directory structure
        self.makedirs_cmd = f'u mkdir -p {path}'
        #change into
        self.cd_cmd = f'cd {path}'
        #set newfile
        self.nf_dummy = f'newfile dummy'
        self.nf_cmd = f'newfile {filename2D}'
        #pass newfile to detector
        self.eig_setdir_cmd = 'eig_setdir'
        
        #execute
        self.client.run_cmd(self.makedirs_cmd, timeout=1800)
        self.client.run_cmd(self.cd_cmd, timeout=1800)
        self.client.run_cmd(self.nf_dummy, timeout=1800)
        self.client.run_cmd(self.nf_cmd, timeout=1800)        
        self.client.run_cmd(self.eig_setdir_cmd, timeout=1800)
        return

    def SetFlyScan(self, nframes=151, det_int_time=0.05):
        '''
        Instantiate the flyscan macro and arm the detector for a XRD measurement
        '''

        self.flyscan_cmd = f'flyscan {nframes} {det_int_time}' 
        #print(nframes*det_int_time*2)
        self.client.run_cmd(self.flyscan_cmd, timeout=1800)
        print('Detector Armed')
        return

    def Get_WO_status(self):
        """Will output to the cmd line the status of the detector"""
        self.queryDetOut_cmd = "p epics_get('EIG1:cam1:DetectorState_RBV')"
        self.client.run_cmd(self.queryDetOut_cmd, timeout=1800)
        self.last_output = self.client.reg_channels['output/tty'].read()
        return self.last_output

    def GetTimeToFill(self):
        """Returns the time in seconds until beam refill at chess"""
        self.queryTTF_cmd = "p epics_get('cesr_run_left')"
        self.client.run_cmd(self.queryTTF_cmd, timeout=1800)
        self.last_output = self.client.reg_channels['output/tty'].read()
        return self.last_output

    def GetIntensity(self):
        self.queryIC_low = "p epics_get('ID3B_CNT04_VLT')"
        self.queryIC_high = "p epics_get('ID3B_CNT04_VLT')"
        self.client.run_cmd(self.queryIC_low, timeout=1800)

        iclow = self.client.reg_channels['output/tty'].read()
        self.client.run_cmd(self.queryIC_high, timeout=1800)
        self.last_output = self.client.reg_channels['output/tty'].read()
        ichigh = self.client.reg_channels['output/tty'].read()
        return iclow

    def cd_spec(self, path):
        """ A generic mv command. Moves the Spec directory to the specified path
        """

        self.mv_cmd = f'cd {path}'
        self.client.run_cmd(self.mv_cmd, timeout=1800)
        return

    def mkdir_spec(self, dirpath):
        """A generic mkdir command. makes the specified directory or set of directories """

        self.mkdir_cmd = f'u mkdir {dirpath}'
        self.client.run_cmd(self.mkdir_cmd, timeout=1800)
        return

    def round_b(self, x, base):
          return base * round(x/base)

    def test_func(self, nframes=151, det_int_time=0.05, path=None, filename2D=None):

        self.makedirs_cmd = f'u mkdir -p {path}'
        self.cd_cmd = f'cd {path}'
        self.nf_cmd = f'newfile {filename2D}'

        self.flyscan_cmd = f'flyscan {nframes} {det_int_time}' 

        #list o commands
        self.client.run_cmd(self.makedirs_cmd)
        self.client.run_cmd(self.cd_cmd)
        self.client.run_cmd(self.nf_cmd)
        self.client.run_cmd(self.flyscan_cmd)

#necessary to send the queue of cmds to the spec server. If this is suppressed, it will not send anything        
        # time.sleep(0.05)
        ## we might want to add a in-situ CO2_1 vs in-situ CO2_2 shift in the table...
    #def table_shift_AI(self,mode="AL/EX")
        #"""
        #mode should be either insitu or ALEX to specify where the table should be w/r/t either laser position 
        #"""
        #self.querry_table_pos = f''
        #self.table_move = f''
        #self.client.run_cmd(self.

if __name__ == "__main__":
    #This is address info for IDB3 hutch computer which is where it will be running the Spec server
    address = 'id3b.classe.cornell.edu'
    port = 'spec'

    #filepath naming convention for the DAQ and 2021 CHESS run

    DAQfs = '/nfs/chess/daq/2021-1/id3b/vandover-test/'    #working directory
    MatSysName = '01_21F7_Bi2O3'                #base MatSysName
    Stripe_iteration = 114                         #iteration number


    #Read the current state of the lasgo agent 
    status = CurrentState()

    #set the parent DAQ directory
    status.directory = DAQfs+MatSysName

    #test the file scheme generation and test stripes
    test_conds = [
        [(-18.6,39.2),     (900,1400)],
        [(42.,-15.),    (1150,900)],
        [(-5.6,0.),        (10000,650)]
    ]
    psa = PySpecAgent()
    psa.ConnectToServer()
    for idx, conds in enumerate(test_conds):
        Sidx = Stripe_iteration + idx
        status.pos = conds[0]
        status.cond = conds[1]



        dirpath, calibpath = condtopath(status)
        print(dirpath)
        print('')
        # print(calibpath)

    # #instantiate the pyspec agent and set the file system and flyscan parameters
    # psa.SetFilePaths(path=dirpath, filename2D='frame')
    # psa.SetFlyScan(nframes=11, det_int_time=0.5)
    ttf = psa.GetTimeToFill()
    print(ttf)
