#!/usr/bin/env python3
import sys
from pathlib import Path
import pathlib
import argparse
import os
cwd = Path.cwd()
sys.path.insert(1,'sara-socket-client/CHESS2022_Spring')
sys.path.insert(1,'sara-socket-client/Scripts')
sys.path.insert(1,'sara-socket-client/')

import yaml
import json
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from xrd_client_chess2021 import *
from PySpecAgent import PySpecAgent
from CondtoPath import condtopath, auxpath, StripeKeyAndFolder
from threading import Thread
from multiprocessing import Process, Queue
from Interpolate2D import *
import imageio as io
import zoocam_client as zcc
from Mappers import *
#from platelvlh5maker_new import H5_builder
from   import CurrentState
def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

# The threader thread pulls a worker from the queue and processes it
def threader(q):
    while True:
        # gets a worker from the queue
        try:
            worker = q.get()
            print("WORKER", worker)
            if worker is None:
                break
            # Run the example job with the avail worker in queue (thread)
            # this call now takes more keyword arguments that depend on the type of scan being collected (scan, point, asdep, insitu, maybe air_scatter...)
            integrate_data(worker[0], worker[1], worker[2], worker[3], worker[4], worker[5], worker[6], worker[7], worker[8])
        except:
            print("worker did not execute correctly")
            break
        #completed with the job
        #q.task_done()
    return True

#class CurrentState():
#    """
#    Contains the current status of the stage.
#    Used to pass parameters between the agents.
#    """
#    def __init__(self):
#        self.image_error = True
#        self.pos = [6., 25.]
#        self.cond = [0., 0.] #<---- Max takes Tmax,tau as the condition inputs
#        self.detector_info = {}
#        self.directory = "/"
        
def integrate_data(xrd_client, pos, cond, offset, dirpath, xmin, xmax, plotpath, plateinputs):
    #Collect and integrate the images from the server
    msg_id = 101
    map_info = xrd_client.get_XRD_GET_MAP_INFO(msg_id)
    map_data, q_data = xrd_client.get_classe(msg_id, pos, cond, offset)
    
    print("")
    print("")
    print('recieved xmap and Q')

    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(np.array(map_data).T, aspect='auto', extent=[xmin,xmax, np.max(q_data), np.min(q_data)])
    ax.set_xlabel('x position')
    ax.set_ylabel('Q in nm$^{-1}$')
    print(dirpath.split("/"))
    os.makedirs(plotpath,exist_ok=True)
    fn_png = dirpath.split("/")[-2] + ".png"
    fn_png = os.path.join(plotpath, fn_png)
    print(fn_png)

    fn_q = dirpath.split("/")[-2] + "_q.npy"
    fn_q = os.path.join(plotpath, fn_q)
    # print(q_data)
    # print(type(q_data))
    print(fn_q)

    fn_map = dirpath.split("/")[-2] + "_map.npy"
    fn_map = os.path.join(plotpath, fn_map)
    print(fn_map)
    # print(map_data)
    # print(type(map_data))

    print("writing map npy")
    np.save(file=fn_map,arr=np.array(map_data))
    print("writing Q npy")
    np.save(file=fn_q,arr=np.array(q_data))
    print("Writing plot to", fn_png)
    
    try:
        plt.savefig(fn_png)
    except:
        fig.savefig(fn_png)
    print("worker is done")

    #h5plate = H5_builder(plateinputs[0],plateinputs[1])
    #h5plate.add_StripeData(x=pos[0], y=pos[1], tau=cond[1], Tmax=cond[0], r=None, q=np.array(q_data), xmap=np.array(map_data), Tprof=None)
    


def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    # parser.add_argument('-d',    '--daqpath',       required=True,  type=str,      help="Absolute path to the DAQ file system where spec writes to")
    # parser.add_argument('-a',    '--auxpath',       required=True,  type=str,      help="Absolute path to the Aux file system where all processed data should be written")
    parser.add_argument('-m',    '--matsysname',    required=True,  type=str,      help="Wafer identifying ID ex: 01_20F7_Bi2O3")
    parser.add_argument('-w',    '--wafermap',      required=True,  type=str,      help="CSV file with the anneal conditions")
    parser.add_argument('-y',    '--yshift',        required=True,  type=str,      help="y-shift calibration file")
    parser.add_argument('-c',    '--comps',         required=False, type=str,      help="CSV compositoin file from Lan")
    # parser.add_argument('-p',    '--plotpath',      required=False, type=str,      help="Directory to write the integrated png files to")
    # parser.add_argument('-s',    '--servername',         required=False, type=str,      help="Integrator host name")
    args = parser.parse_args()
    return args

args = parse()

filename_yshift = args.yshift
points, values = CalibReader_2021(filename_yshift)
print(points, values)


#Read wafermap
filename_wafermap = args.wafermap
wm_data = np.genfromtxt(filename_wafermap,delimiter=',',skip_header=1)
conds = wm_data[:,2:4]
poss = wm_data[:,:2]
# print(conds,poss)
print("CSV file read")


# parse args and establish directories
# daqpath = args.daqpath
# auxpath = args.auxpath
daqpath = "/nfs/chess/raw/2023-1/id3b/vandover-3367-C/"
auxpath = "/nfs/chess/auxiliary/reduced_data/cycles/2023-1/id3b/vandover-3367-C/"
MatSysName = args.matsysname

# print("conditions",conds)
# print("positions:",poss)
print("CSV file read")

wd = os.path.join(os.path.join(daqpath,MatSysName),"")
ad = os.path.join(os.path.join(auxpath,MatSysName),"")
print(f'DAQ dir: {daqpath}')
print(f'AUX dir: {auxpath}')
print(f'MatSysName: {MatSysName}')
print(f'working dir: {wd}')
print(f'working aux: {ad}')

plotpath = os.path.join(os.path.join(ad,"Data"),"")
# plotpath = os.path.join(os.path.join("/home/duncan","Desktop"),"Test")
print(plotpath)
# pathlib.Path(plotpath).mkdir(parents=True, exist_ok=True)

# #Check plotpath
# if not args.plotpath:
#     args.plotpath = "."
# else:
#     if not os.path.exists(args.plotpath):
#         pathlib.Path(args.plotpath).mkdir(parents=True, exist_ok=True)


#Make the plate level h5
#h5plate = H5_builder(auxpath,MatSysName)
  

#Setup the lasgo sockets
address = "CHESS"
socket_list = ["lasgo"] 
socket_clients = sc.socket_clients()
clients = socket_clients.get_clients(address, socket_list = socket_list)
LasGo = clients["lasgo"]

#set up Camera Client
camera_client = zcc.ClientZOOCAMProtocol(connect = True, address = "128.84.183.184")
msg_id = 101
camera_info = camera_client.get_ZOOCAM_GET_CAMERA_INFO(msg_id)
print(camera_info)
# add camera info modifiers here


#Setup xrd socket
if args.servername:
    print("using servername")
    xrd_address = args.servername
else:
    print("using localhost")
    xrd_address = "localhost"
xrd_client = ClientXRDProtocol(connect = True, address = xrd_address)


# Instantiate the laser connection
laser = LSA_Laser.laser()
laser.socket_clients = socket_clients

#connect to the Spec Server
psa = PySpecAgent()
psa.ConnectToServer()

#Setup the SARA client info, and set remote READING directory
status = CurrentState()
status.directory = wd
msg_id = 101
rcv = xrd_client.set_XRD_SET_DIR(msg_id, wd)

#Data collection cycle
#the start and end will now be variable with Max's code
default_offset = [0.0,1.0]      #CO2 and LD offset [0., 0.]       #Pre-LD re-align offset [0.4, 0.] 
#dstart = -1.0
#dend = 1.0
det_int_time = 50 #In msec
#nframes = 201

# Create the queue and threader
q = Queue()

# how many threads are we going to allow for
for x in range(1):
     #t = Thread(target=threader)
     t = Process(target=threader, args=[q])

     # classifying as a daemon, so they will die when the main dies
     t.daemon = True

     # begins, must come after daemon definition
     t.start()

counter = 160 
#Loop over all conditions and positions on the wafer map
for pos, cond in zip(poss[160:], conds[160:]):
    counter += 1
    status.pos = pos.tolist()
    status.cond = [cond[1],cond[0]]
    print(status)

    fpt = pos
    y_offset = interpolate2d(points, values, fpt, method = "linear", scaling = 1.2)
    print(f"y_offsets-------------------------------------------------------------------->{y_offset}")
    offset = cp.deepcopy(default_offset)
    offset[1] += y_offset
    #grab an image at nominal position plus default offsets
    LasGo.moveto_safe([pos[0], pos[1] + offset[1]])
    
    
    #camera client new
     
    img,image_info = camera_client.get_image(101, get_raw=False)
    raw = image_info["img_raw"]

    
    
    os.makedirs(os.path.join(ad,"Images"),exist_ok=True)
    img_savepath = os.path.join(os.path.join(ad,"Images"),f"b{StripeKeyAndFolder(status)[0]}.bmp")
    raw_savepath = os.path.join(os.path.join(ad,"Images"),f"r{StripeKeyAndFolder(status)[0]}.raw")
    print(img_savepath)
    print(raw_savepath)
    io.imsave(img_savepath, img, '.bmp')
    camera_client.write_raw_image(raw_savepath, image_info)

    #Get offset: 
    start = cp.deepcopy(pos)
    start[0] += dstart
    start[0] += default_offset[0]
    start[1] += default_offset[1] + y_offset 
    end = cp.deepcopy(pos)
    end[0] += dend
    end[0] += default_offset[0] 
    end[1] += default_offset[1] + y_offset

    #Set flyscan params
    print(det_int_time,nframes)

    #Spawn thread and look to the registered output for the "Armed" state
    print("Is it armed?", "Armed" in psa.client.reg_channels['output/tty'].read())

    #Check for time to fill. if there is < current det_int_time*nframes + 10s (buffer), wait the durration of the fill + 15s
    #ttf = psa.GetTimeToFill()
    #iclow = psa.GetIntensity()

    #print(f'Time to fill: {ttf} seconds')
    #while (ttf < det_int_time/1000*nframes+3) or (iclow < 1.6): # <--- this is a hard coded parameter that needs to be tuned when x-rays are gucci
    #    print(f'holding for {ttf + 1} seconds')
    #    time.sleep(1)
    #    ttf = psa.GetTimeToFill()
    #    iclow = psa.GetIntensity()


    #Set directory and prime the detector
    dirpath, calibpath = condtopath(status)
    padname = "frame"
    print("dirpath", dirpath)
    psa.SetFilePaths(dirpath, padname)

    time.sleep(1)
    process = Thread(target=psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
    process.start()
    #while "Armed" not in psa.client.reg_channels['output/tty'].read():
    #    print("Calling status", psa.client.reg_channels['output/tty'].read())
    #    print("Waiting for armed")
    #    time.sleep(1)
    #    print("Not ready yet")

    while psa.client.read_channel("EIG_ARMED") != "Armed":
        print("Waiting for armed")
        time.sleep(1)
        print("Not ready yet")
        print("")
        print("")

    #Code that executes the flyscan from lasgo
    laser.execute_flyscan(start, end, det_int_time, nframes)
    print("Calling status", psa.client.reg_channels['status/ready'].read())
    print("Waiting for my thread to finish")
    process.join()
    print("The PySpec thread has finished", counter)
    #time.sleep(1)

    #Check if files are written to DAQ in the appropriate folder:
    detstate = psa.Get_WO_status()
    while "Idle" not in detstate:
        detstate = psa.Get_WO_status()
        time.sleep(0.5)
        pass

    #Put integration into queue
    #Pass filename for h5 not plate...
    print(plotpath)
    plateinputs=(auxpath,MatSysName)
    q.put((xrd_client, status.pos, status.cond, offset, dirpath, start[0], end[0], plotpath, plateinputs))
    print("Submitted worker", dirpath, counter)

    # wait until the thread terminates.
    

q.put(None)
print("Submitted all workers")
# wait until the thread terminates.
t.join()
#q.join()
print("Stopped XRD thread")


#TODO
#- generate tauT map and wafer map
#- execute H5 plate writer if not parallelized
