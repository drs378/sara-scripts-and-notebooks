#!/usr/bin/env python3
import sys
sys.path.insert(1, '/home/duncan/sara-socket-client/gaussianprocesses_v4')
sys.path.insert(1, '/home/duncan/sara-socket-client/Scripts')
sys.path.insert(1, '/home/duncan/sara-socket-client')
sys.path.insert(1, '/home/duncan/sara-scripts-and-notebooks')

import yaml
import json
import os
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
import Wafer as wf
from restart import *
import data_storage as ds
import zone as LSA_Zone
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from pyspec.client import SpecConnection
from PySpecAgent import PySpecAgent, condtopath

def LSAtoAnalysis_Zone(zone, x_start=-0.75, x_end=0.75, y_offset=1.25, n_frames = 151, det_int_time = 0.05, lon_FWHM=0.08):
	"""A function to set up the lasgo analysis zone:
	zone 		= zone.py objct which has the lasgo parameters which set the zone
	x_start 	= position where the analysis will start collecting data w.r.t. the stripe center
	x_end 		= position where the analysis will end 
	y_offset 	= offset in y from the nominal stripe center (steady state location)
	n_frames	= number of frames the detector will take
	det_int_time= time in seconds that the detector integrates for
	lon_FWHM	= FWHM in mm

	scan velocity is set by the dwell time and is calculated by the (total distance)/(n_frames * det_int_time)
	"""
	azone = cp.deepcopy(zone)
	# for key in list(azone):
	# 	print(key)
		
	# print('')
	print('initial zone parameters')
	for key in (['Xmin','Xmax','Ymin','Ymax','Dwell']):
		print(f'{key} : {zone[key]}')
	
	#anneal center
	ac_x = 0.5*(azone['Xmax'] - azone['Xmin']) + azone['Xmin']
	ac_y = 0.5*(azone['Ymax'] - azone['Ymin']) + azone['Ymin']

	#analysis center
	acq_x = ac_x
	acq_y = ac_y + y_offset

	#analysis zone definition
	azone['Xmin'] = acq_x + x_start
	azone['Xmax'] = acq_x + x_end
	azone['Ymin'] = acq_y
	azone['Ymax'] = acq_y

	#set the dwell so all frames collect
	total_d = azone['Xmax'] - azone['Xmin'] # in mm
	total_t = n_frames*det_int_time # in seconds
	velocity = total_d/total_t # in mm/s
	print('')
	print('dist, time, velocity')
	print(total_d,total_t,velocity)
	#from the longitudinal FWHM, we can convert the desired scan velocity to dwell
	azone['Dwell'] = 10**6*lon_FWHM/velocity  #FWHM in mm in mm/s corrected to us 

	print('')
	print('modified zone parameters')
	for key in (['Xmin','Xmax','Ymin','Ymax','Dwell']):
		print(f'{key} : {azone[key]}')
	print('')
	print('')
	print('')
	return azone

if __name__ == "__main__":
	AlignmentPadLocs  = np.array([
	#The vertical scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],

	#The horizontal scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],
	])

	#Left to Right then up to down
	# Can create the job file with the appropriate coordinates and apply no power:
	# There is still a default power in the job file when there shouldn't be anything at all
	Tpeak_list = np.zeros(len(AlignmentPadLocs))

	# this doesn't matter. The job file will need to be modified to account for the two different scan directions 
	# in the search of the alignment pad. 
	dwell_list = np.linspace(1000,1000,len(AlignmentPadLocs))

	#create the jobfile 
	jf = wf.jobfile()
	# print(jf)
	# print(list(jf.CO2))
	print('')
	#Preaparing the zone for annealjf = jobfile()
	waferparams = wf.wafer_sample_100(sample_id = "MySample")
	jf.CO2["WarmupWatts"]                 = 50.0
	jf.CO2["ChangeDelay"]                 = 2.0
	jf.Job["EdgeExclusion"]               = 2.0
	cond_list = []
	for D, T in zip(dwell_list, Tpeak_list):
	    cond_list.append([D, T])
	cond_list = np.array(cond_list)

	wafermap, posr, condr = waferparams.link_conditions_pos(AlignmentPadLocs, cond_list, dump = True, filetype = "csv", power_scaling = 1.0, suffix = "Calibration")
	wafer_job, wz = jf.write_jobfile("NextStripe.job", wafermap, waferparams, sample_id = "Calibration")


	# print(wafer_job)
	for z in wz[0:2]:
		LSAtoAnalysis_Zone(z,lon_FWHM=1)
