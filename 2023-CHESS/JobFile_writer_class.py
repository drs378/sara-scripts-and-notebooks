import numpy as np
import matplotlib.pyplot as plt
parameters = {'axes.labelsize': 14,
              'xtick.labelsize': 12,
              'ytick.labelsize': 12,
              'legend.fontsize': 12,
              'legend.title_fontsize': 14,
              'axes.titlesize': 18}
plt.rcParams.update(parameters)

import sys
import os
sys.path.insert(1,"/home/duncan/sara-socket-client/CHESS2023_Spring/")
# sys.path.insert(1,"/home/duncan/sara-scripts-and-notebooks/")
sys.path.insert(2,"/home/duncan/sara-socket-client/")
from Wafer import *
from composition import *
import argparse

#args in: dir start end
def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-m',   '--matsysname',        type=str,               help="Material System Name and ID")
    #parser.add_argument('-r',   '--job_request_file',  type=str,               help="path/to/job_request.txt")
    #parser.add_argument('-c',    '--composition_gp',   type=str,     action='store_false',          help="path/of/the/composition.sav", nargs='+')
    #parser.add_argument('-pr',  '--pos_r',             type=str,     action='store_false',          help="returned positions if this is not the first job on the wafer")
    #parser.add_argument('-cr',  '--cond_r',            type=str,     action='store_false',          help="returned conditions if not all of the grid had been searched")
    args = parser.parse_args()
    return args

class JobCreator():
    def __init__(self, MatSysName, comp_gp_sav=None, auxpath=None):
        self.auxpath = auxpath
        self.matsysname = MatSysName
        self.my_sample_id = os.path.join(os.path.join(self.auxpath, self.matsysname), self.matsysname)
        print(self.my_sample_id)
        self.s = sampling()
        #if os.path.is_file(self.my_sample_id+'_wafermap.csv'):
        #    all_fns = sorted(glob.glob(self.auxpath + '*.job'))
        #    self.my_sample_id = self.my_sample_id
        self.cl = wafer_sample_100(sample_id = f"{my_sample_id}")
        self.pos = self.cl.get_layout_1()
        
        if comp_gp_sav:
            self.comp_gp_sav = comp_gp_sav
            self.filt = composition(samp=self.s, composition_file = comp_gp_sav, gp = True)
            self.pos_comps = filt.get_pos_comps(pos)
        else:
            self.comp_gp_sav = None 
            self.filt = None
            self.pos_comps = None
	self.jf = jobfile()

        
    def set_sampling(self,taus=[7,350.,10000.], temps=[15,450.,1400.]):
        self.s.nd = taus[0]
        self.s.dmin = taus[1]
        self.s.dmax = taus[2]

        self.s.nt = temps[0]
        self.s.tmin = temps[1]
        self.s.tmax = temps[2]

    def set_layout(self, xpitch=2., ypitch=5., exclusion=2., xmin=-44., xmax=+44, ymin=-40, ymax=40):
        self.cl.length = ypitch
        self.cl.width  = xpitch
        self.cl.exclusion = exclusion
        self.pos = elf.cl.get_layout_1()   
        try:
            self.pos = self.pos[(self.pos[:,0] <= xmax) & (self.pos[:,0] >= xmin) & (self.pos[:,1] <= ymax) & (self.pos[:,1] >= ymin)]

        except:
            pass

    def make_job(self, laser="CO2", unit="WATTS", delay=(True,1000)):
	self.jf.Zone["Laser"] = laser
	self.jf.Zone["Units"] = unit
	self.jf.Zone["Delay"] = delay[0]
	self.jf.Zone["ms_Delay"] = delay[1]
	
	job, zones = jf.write_jobfile(self.my_sample_id + "_wafer.job", self.wafermap, self.cl, sample_id = self.my_sample_id)
	print('updated and wrote job')
    
    

    def	make_compspread_job(self, nbins=None):
	self.nbins = nbins
        self.frac = self.pos_comps[:,2]
        self.delta_comp = np.round((max(self.frac)-min(self.frac))/nbins,2)
        

        ## plot and store the histogram for binaries
        fig,ax = plt.subplots()
        ax.hist(self.frac,bins=self.nbins)
        ax.set_xlabel(f"composition by {self.delta_comp*100}% increment")
        ax.set_ylabel("number of positions")
        ax.set_title("Element composition histogram")
        
        freqs, bins   = np.histogram(frac, bins = nbins)
        
        #plt.show()
        fig.savefig(my_sample_id+'_histogram.png')

        inds = np.digitize(self.frac, self.bins)
        self.pos_comps_ind = np.zeros((self.pos_comps.shape[0], self.pos_comps.shape[1]+1))
        self.pos_comps_ind[:,0:3] = self.pos_comps
        self.pos_comps_ind[:,3] = inds
        
        # Determine max mesh size given a dtau
        ntpeaks = []
        # remaining = []
        ndwell = self.s.nd
        for freq in freqs:
            sq = freq/ndwell
        #    print(sq)
            ntpeaks.append(int(np.floor(sq)))
        # print(ntpeaks)
        ntpeaks = self.s.nt*np.ones(len(freqs),dtype=int)
        
        meshes = []
        
        for ntpeak in ntpeaks:
            #s.nd = ndwell #Spacing in dwell space
            #s.nt = ntpeak
            meshes.append(self.s.shuffle_mesh(self.s.get_mesh()))
        


        ## plotting for verification
        fig = plt.figure(figsize=(6,6),dpi=300)
        ax0 = fig.add_subplot(projection='3d')
        for i, mesh in enumerate(meshes):
            sp = ax0.scatter3D(bins[i]*np.ones(len(mesh)), np.log10(mesh[:,0]),mesh[:,1],
                          c=bins[i]*np.ones(len(mesh)),cmap='rainbow',vmin=0,vmax=1)
        ax0.set(
            xlabel="Bi fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title="targeted exhaustive mesh"
        )
        cb = plt.colorbar(sp,ax=ax0)
        fig.savefig(my_sample_id+'_ideal_comp_distribution_1.png')
        #plt.show()

        fig = plt.figure(figsize=(6,6),dpi=300)
        ax0 = fig.add_subplot(projection='3d')
        ax0.view_init(0,-90)
        for i, mesh in enumerate(meshes):
            sp = ax0.scatter3D(bins[i]*np.ones(len(mesh)), np.log10(mesh[:,0]),mesh[:,1],
                          c=bins[i]*np.ones(len(mesh)),cmap='rainbow',vmin=0,vmax=1)
        ax0.set(
            xlabel="Bi fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title="targeted exhaustive mesh"
        )
        cb = plt.colorbar(sp,ax=ax0)
        fig.savefig(my_sample_id+'_ideal_comp_distribution_2.png')
        #plt.show()
        


        ## the defined grid is applied to each binned composition
        wm_all = []
        comps_all = []
        for ind, cond in zip(np.unique(inds), meshes):
            pos_mesh = sel.fpos_comps_ind[self.pos_comps_ind[:,3]==float(ind), :]
            np.savetxt(self.my_sample_id+"_compositions_bins_"+str(int(ind))+".csv", pos_mesh, delimiter=',')
            pos = pos_mesh[:,:2]
            if cond.shape[0] != pos.shape[0]:
                print(cond.shape[0], pos.shape[0])
                print("this is FINE")
                
            #pos = pos.tolist()
            wafermap, posr, condr = self.cl.link_conditions_pos(pos, cond, dump = True, filetype = "csv", power_scaling = 1.0)
            wm_all.append(wafermap)
            
        #appends all wavermaps together for one file
        wafermap = sum(wm_all,[])
        
        # Sort wafermap according to power
        wafermap = self.cl.sort_wafer_power(wafermap, reverse = False)
        
        #Write conditions as csv tile to disk
        self.cl.dump_wafer_csv(wafermap, self.my_sample_id + "_wafermap.csv")

        #check if I can read the csv file that I just wrote to disk
        print(self.cl.read_wafer_csv(self.my_sample_id + "_wafermap.csv"))

        #make the job files now
        self.make_job()
        
        wafermap = np.genfromtxt(my_sample_id+'_wafermap.csv',delimiter=',',skip_header=1)
        comps_few = []
        for idx, row in enumerate(wafermap):
            comps_few.append(self.pos_comps[(self.pos_comps[:,0]==row[0]) & (self.pos_comps[:,1]==row[1])][0][2])
            
        ## further plotting to validate the binned distribution
        fig = plt.figure(figsize=(6,6),dpi=300)
        ax = plt.axes(projection='3d')
        sp3d = ax.scatter3D(comps_few, np.log10(wafermap[:,2]),wafermap[:,3],c=comps_few,cmap='rainbow')
        ax.set(
            xlabel="cation fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title=f"real distribution")
        plt.colorbar(sp3d,ax=ax,fraction=0.046,pad=0.1)
        fig.savefig(my_sample_id+'_real_distribution_1.png')
        
        fig = plt.figure(figsize=(6,6),dpi=300)
        ax = plt.axes(projection='3d')
        ax.view_init(0,-90)
        sp3d = ax.scatter3D(comps_few, np.log10(wafermap[:,2]),wafermap[:,3],c=comps_few,cmap='rainbow')
        ax.set(
            xlabel="cation fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title=f"real distribution")
        plt.colorbar(sp3d,ax=ax,fraction=0.046,pad=0.1)
        fig.savefig(my_sample_id+'_real_distribution_2.png')








if __name__ == "__main__":
    args = parse()
    print(args)
    #daq = "/nfs/chess/raw/2023-1/id3b/vandover-3367-C/"                                 
    #aux = "/nfs/chess/auxiliary/reduced_data/cycles/2023-1/id3b/vandover-3367-C/"       
    aux = '/home/duncan/Documents/Data/21F61_Bi-Ti-O/'
    MatSysName = args.matsysname  
    ad = os.path.join(aux, MatSysName)
    
    
    ###Workflow using the class (untested)
    
    ##single composition
    #jc = JobCreator(MatSysName+'_unary', comp_gp_sav=None, auxpath=ad)
    #jc.set_sampling(taus=[15,250,10000],Temps=[20,600,1400])
    #jc.set_layout()
    #jc.make_job()

    ##binary composition
    #jc = JobCreator(MatSysName+'_binary', comp_gp_sav=aux + '/62288_21F51_BiTi_composition_Bi_gp_model.sav', auxpath=ad)
    #jc.set_sampling(taus=[3,350,10000],Temps=[9,600,1400])
    #jc.set_layout()
    #jc.make_compspread_job(nbins=9)





    ## Workflow without the class tested, just change the paths to be correct
    my_sample_id = ad + MatSysName
    comp_gp_sav = aux + '/62288_21F51_BiTi_composition_Bi_gp_model.sav' 
    s = sampling()
    
    s.tmin  =     600.          #Minimum value of maximum temperature
    s.tmax  =    1400.          #Maximum value of maximum temperature
    s.nt    =      11           #number of temperatures

    s.dmin  =     350.          #Minimum dwell time
    s.dmax  =   10000.          #Maximum dwell time
    s.nd    =      10           #number of dwells
    oldwmpath = None 
    
    #sets up the wafer
    
    #generates some default coordinates for either CO2 or diode laser annealing
    cl = wafer_sample_100(sample_id = f"{my_sample_id}")
    
    #set the length of the stripe to whatever conditions you want, length and width for pitch 
    #exclusion is the distance in mm from the edge of the wafer
    cl.length = 5 
    cl.width =  2
    cl.exclusion = 4
    
    #this generates the mesh of layout conditions
    pos = cl.get_layout_1()
    cl.plot_pos(pos)

    if comp_gp_sav:
        filt = composition(samp=s, composition_file = comp_gp_sav, gp = True)
        pos_comps = filt.get_pos_comps(pos)
        
    
        
        #composition binning
        nbins = 9 
        frac = pos_comps[:,2]
        delta_comp = np.round((max(frac)-min(frac))/nbins,2)
        
        fig,ax = plt.subplots()
        ax.hist(frac,bins=nbins)
        ax.set_xlabel(f"composition by {delta_comp*100}% increment")
        ax.set_ylabel("number of positions")
        ax.set_title("Element composition histogram")
        freqs, bins   = np.histogram(frac, bins = nbins)
        plt.show()
        fig.savefig(my_sample_id+'_histogram.png')

        inds = np.digitize(frac, bins)
        pos_comps_ind = np.zeros((pos_comps.shape[0], pos_comps.shape[1]+1))
        pos_comps_ind[:,0:3] = pos_comps
        pos_comps_ind[:,3] = inds
        
        # Determine max mesh size given a dtau
        ntpeaks = []
        # remaining = []
        ndwell = s.nd
        for freq in freqs:
            sq = freq/ndwell
        #    print(sq)
            ntpeaks.append(int(np.floor(sq)))
        # print(ntpeaks)
        ntpeaks = s.nt*np.ones(len(freqs),dtype=int)
        
        meshes = []
        
        for ntpeak in ntpeaks:
            #s.nd = ndwell #Spacing in dwell space
            #s.nt = ntpeak
            meshes.append(s.shuffle_mesh(s.get_mesh()))
        
        ## plotting for verification
        fig = plt.figure(figsize=(6,6),dpi=300)
        ax0 = fig.add_subplot(projection='3d')
        for i, mesh in enumerate(meshes):
            sp = ax0.scatter3D(bins[i]*np.ones(len(mesh)), np.log10(mesh[:,0]),mesh[:,1],
                          c=bins[i]*np.ones(len(mesh)),cmap='rainbow',vmin=0,vmax=1)
        ax0.set(
            xlabel="Bi fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title="targeted exhaustive mesh"
        )
        cb = plt.colorbar(sp,ax=ax0)
        fig.savefig(my_sample_id+'_ideal_comp_distribution_1.png')
        plt.show()

        fig = plt.figure(figsize=(6,6),dpi=300)
        ax0 = fig.add_subplot(projection='3d')
        ax0.view_init(0,-90)
        for i, mesh in enumerate(meshes):
            sp = ax0.scatter3D(bins[i]*np.ones(len(mesh)), np.log10(mesh[:,0]),mesh[:,1],
                          c=bins[i]*np.ones(len(mesh)),cmap='rainbow',vmin=0,vmax=1)
        ax0.set(
            xlabel="Bi fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title="targeted exhaustive mesh"
        )
        cb = plt.colorbar(sp,ax=ax0)
        fig.savefig(my_sample_id+'_ideal_comp_distribution_2.png')
        plt.show()
        
        wm_all = []
        comps_all = []
        for ind, cond in zip(np.unique(inds), meshes):
            pos_mesh = pos_comps_ind[pos_comps_ind[:,3]==float(ind), :]
            np.savetxt(my_sample_id+"_compositions_bins_"+str(int(ind))+".csv", pos_mesh, delimiter=',')
            pos = pos_mesh[:,:2]
            if cond.shape[0] != pos.shape[0]:
                print(cond.shape[0], pos.shape[0])
                print("this is FINE")
                
            #pos = pos.tolist()
            wafermap, posr, condr = cl.link_conditions_pos(pos, cond, dump = True, filetype = "csv", power_scaling = 1.0)
            wm_all.append(wafermap)
            
        #appends all wavermaps together for one file
        wafermap = sum(wm_all,[])
        
        # Sort wafermap according to power
        wafermap = cl.sort_wafer_power(wafermap, reverse = False)
        
        #Write conditions as csv tile to disk
        cl.dump_wafer_csv(wafermap, my_sample_id + "_wafermap.csv")

        #check if I can read the csv file that I just wrote to disk
        print(cl.read_wafer_csv(my_sample_id + "_wafermap.csv"))
        jf = jobfile()
        jf.Zone["Laser"] = "CO2"
        jf.Zone["Units"] = "Watts"
        jf.Zone["Delay"] = True
        jf.Zone["ms_Delay"] = 1000
        
        job, zones = jf.write_jobfile(my_sample_id + "_wafer.job", wafermap, cl, sample_id = my_sample_id)
        print(zones)
        
        wafermap = np.genfromtxt(my_sample_id+'_wafermap.csv',delimiter=',',skip_header=1)
        comps_few = []
        for idx, row in enumerate(wafermap):
            comps_few.append(pos_comps[(pos_comps[:,0]==row[0]) & (pos_comps[:,1]==row[1])][0][2])
            
        fig = plt.figure(figsize=(8,8),dpi=300)
        ax = plt.axes(projection='3d')
        sp3d = ax.scatter3D(comps_few, np.log10(wafermap[:,2]),wafermap[:,3],c=comps_few,cmap='rainbow')
        ax.set(
            xlabel="cation fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title=f"real distribution")
        plt.colorbar(sp3d,ax=ax,fraction=0.046,pad=0.1)
        fig.savefig(my_sample_id+'_real_distribution_1.png')
        
        fig = plt.figure(figsize=(8,8),dpi=300)
        ax = plt.axes(projection='3d')
        ax.view_init(0,-90)
        sp3d = ax.scatter3D(comps_few, np.log10(wafermap[:,2]),wafermap[:,3],c=comps_few,cmap='rainbow')
        ax.set(
            xlabel="cation fraction",
            ylabel="log$_{10}$(τ in μs)",
            zlabel="temperature in \u00B0C",
            title=f"real distribution")
        plt.colorbar(sp3d,ax=ax,fraction=0.046,pad=0.1)
        fig.savefig(my_sample_id+'_real_distribution_2.png')

