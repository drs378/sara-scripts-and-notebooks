import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys
import glob
import argparse
import os
# sys.path.insert(1,'/home/duncan/sara-scripts-and-notebooks/')
sys.path.insert(1,'sara-socket-client/CHESS2022/')
sys.path.insert(1,'sara-socket-client/Scripts/')
from center_finder_asym import get_center_asym
from xrdCalibFit_new import PoniFit 
from GP_Interpolate2D import *
#from TemperatureProfile import *
from CondtoPath import CurrentState, StripeKeyAndFolder as condmaker
from ReIntegratorSpring2022 import fft_smoothing, sharpen_y, get_center
import time

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt or .csv file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

def h5_parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-a',    '--auxpath',       type=str,               help="Absolute path to the plate level auxiliary")
    parser.add_argument('-w',    '--wafermap',     type=str,               help="name of wafermap.csv")
    parser.add_argument('-c',    '--composition_gp',       type=str,               help="path/of/the/composition.sav", nargs='+')
    parser.add_argument('-p',    '--ponis',          type=str,               help="path to the poni files used to integrate")
    parser.add_argument('-t', 	 '--transform',	  dest='conversion', action='store_true',    help="Convert power scan velocity to Tmax, tau assumes fwhm of 88 um")
    parser.add_argument('-i',    '--reintegrated', dest='reint',     action='store_true',    help="read from ReIntegratedData")
    parser.set_defaults(conversion=False,reint=False)
    args = parser.parse_args()
    return args

def CO2_powerconverter(scanvelocity, power):
    """
    This takes scan velocity in mm/s and power in watts and converts to a dwell and a Tmax for a given stripe
    """
    p_melt = 52.2 + 0.162*scanvelocity
    Tmax = 1400 * power/p_melt # in deg C
    dwell = 0.088/scanvelocity*10**6 # in dwell
    return dwell, Tmax

def jitter_correction(data, plotting=False):
    mean_data = data.mean()
    mean_column = data.mean(axis=1)
    mean_diff = mean_column - mean_data
    data_corrected = (data.transpose() - mean_diff).transpose()
    if plotting:
        plt.figure(figsize=(6,6))
        plt.subplot(121)
        plt.imshow(data, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.subplot(122)
        plt.imshow(data_corrected, cmap=plt.cm.gray,aspect='auto')
        plt.axis('off')
        plt.tight_layout()
        plt.show()
    return data_corrected


class H5_builder(object):
    def __init__(self, auxpath, MatSysName,ponipath):
        self.auxpath = auxpath
        self.MatSysName = MatSysName
        print(ponipath)
        self.ponis = sorted(glob.glob(ponipath+"*.poni")) #relative path to the poni files
        print(self.ponis)
        # print(os.path.join(os.path.join(auxpath,MatSysName),'PONIs'))
        self.pf = PoniFit(self.ponis)
        self.plate = None
        self.filename = f"{self.MatSysName}_all_1d.h5"

    
        # instantiate the plate (load or make if not already)
        self.check_H5()
        # print(self.plate)

    def check_H5(self):
        """
        Makes the H5 file into the auxiliary file system
        """
        
        self.plate = h5.File(os.path.join(self.auxpath,self.filename),'a')

    def add_StripeData(self, x=None, y=None, tau=None, Tmax=None, cats=None, fracs=None, q=None, xmap=None, Tprof=None, xx=None, xc=None):
        """
        Add the appropriate data for the scan 
        """
        try:
            self.check_H5()

        except:
            print("issue loading path")
            pass

        cond = f'tau_{int(tau)}_T_{int(Tmax)}'
        clvl = f'exp/{cond}'

        try:
            self.plate.create_group(clvl)

        except:
            pass

        #print(self.plate[clvl])
        #print(list(self.plate[clvl].attrs))
        self.plate[clvl].attrs['x_center'] = x
        self.plate[clvl].attrs['y_center'] = y
        self.plate[clvl].attrs['dwell time'] = int(tau)
        self.plate[clvl].attrs['Tmax'] = int(Tmax)
        self.plate[clvl].attrs['poni'] = self.pf.getpars((x,y))
        self.plate[clvl].attrs['xx'] = xx
        self.plate[clvl].attrs['stripe_center'] = xc
        if cats != None:
            self.plate[clvl].attrs['cations'] = cats
            self.plate[clvl].attrs['fracs'] = fracs
        

        # self.plate[clvl].attrs['poni_params'] = self.pf.getpars((float(x),float(y)))
        print(Tprof, type(Tprof))
        if (Tprof is not None):
            print("Tprof here")
            self.plate[clvl].attrs['T_profile'] = Tprof
        
        #breaking down by scan index:
        for s in range(xmap.shape[0]):
            slvl = f'{clvl}/{s}/integrated_1d'
            try:
                self.plate.create_dataset(name=slvl,data=[q,xmap[s]],dtype='float32')
            except:
                data = self.plate[slvl]
                data = [q,xmap[s]]
        self.plate.close()

if __name__ == "__main__":
    args = h5_parse()
    auxpath = args.auxpath
    MatSysName = os.path.basename(auxpath[:-1])
    comp_gps = args.composition_gp
    ponipath = args.ponis
    #print(comp_gps)
    cations = None
    fracs = None
    Tprof = None
    wm_fn = args.wafermap
    conversion = args.conversion
    #print(args.reint)
    #print(args.conversion)
    #print('')
    #print('')
    #print('')
    time.sleep(4)
    if args.reint:
        datapath = os.path.join(auxpath,'ReIntegratedData/')
    else:
        datapath = os.path.join(auxpath,'Data/')

    q_all = sorted(glob.glob(datapath + '/*_q.npy'))
    xmap_all = sorted(glob.glob(datapath + '/*_map.npy'))
    
    #Instantiate H5 file
    all1d = H5_builder(auxpath,MatSysName,ponipath)
    current = None 
    
    for idx, row in enumerate(np.genfromtxt(wm_fn, delimiter=',', skip_header=1)):
        print(idx, row)
        x = row[0]
        y = row[1]
        
        if conversion:
            sv = row[2]
            power = row[3]
    
            status = CurrentState()
            status.cond = [power, sv]
            status.pos = [x, y]
            fn = condmaker(status)
            
            
            q_fn = q_all[q_all.index(os.path.join(datapath, fn[1]) + "_q.npy")]
            xmap_fn = xmap_all[xmap_all.index(os.path.join(datapath, fn[1]) + "_map.npy")]
            #print(xmap_fn)     
            tau, Tmax = CO2_powerconverter(sv, power)
            tau, Tmax = int(tau), int(Tmax) 
            print(x,y,tau,Tmax)
            print('')
        else:
            tau = row[2]
            Tmax = row[3]
            status = CurrentState()
            status.cond = [Tmax,tau]
            status.pos = [x, y]
            fn = condmaker(status)
            #print(fn)
            q_fn = q_all[q_all.index(os.path.join(datapath, fn[1]) + "_q.npy")]
            #print(q_fn)
            xmap_fn = xmap_all[xmap_all.index(os.path.join(datapath, fn[1]) + "_map.npy")] 
            #print(xmap_fn)
    
            try:
                current = row[4]
            except:
                print("current not supplied, cannot provide tprofile")
        print("")


        pos = [x,y]
        if comp_gps != None:
            cations = []
            fracs = []
            for gp_fn in comp_gps:
                
                print(gp_fn.split('_')[-3])
                comp = predict_gp_2d([pos], filename=gp_fn)[0] # composition GP model for single composition systems
                cat = gp_fn.split('_')[-3]
                cations.append(cat)
                fracs.append(comp)

            print(cations,fracs)
            print(sum(np.round(fracs,2)))
            print("")

        else:
            comp = None
        
        
        q = np.load(q_fn)
        #print(min(q), max(q))
        #if np.isnan(np.load(xmap_fn)).any():
        #    print("Fucked!")
        xmap_raw = np.nan_to_num(np.load(xmap_fn), nan=0.5)
        xmap_raw = np.where(xmap_raw > 10000, 0.5, xmap_raw)
        #print(np.min(xmap_raw), np.max(xmap_raw))
        #print(np.min(xmap_raw), np.max(xmap_raw))        
        #xmap = JitterCorrection(xmap_raw.T).T
        xmap = jitter_correction(xmap_raw,plotting=False)
        #xmap = xmap_raw
        #exhaustive data spans 2 mm
        xx = np.linspace(-1000, 1000, xmap.shape[0])
        sharpened_data = sharpen_y(xmap, bf=[0,25])
        #xc = get_center(sharpened_data.T[100:-100,:], plotting=False)[1]
        _, xc = get_center_asym(xmap, plotting=False, ROI_search=False, left_fwhm = 1.4, right_fwhm =  1.0)
        #xc = min(xx) + xc/sharpened_data.shape[0]*(max(xx) - min(xx))
        
    
        print(xc)
        xx = xx - xx[round(xc)]
        
        #xx = xx - xc
        
    
        #Tprofile is here!!!
        if (current != None) & (conversion == False):
            #Tprof = oned_gaussian(xx, tau, current)
            #fig, ax = plt.subplots(2,1, constrained_layout=True)
            #fig.suptitle(fn[0])
            #ax[0].plot(xx, Tprof, c='r')
            #ax[0].set_xlabel("across stripe")
            #ax[0].set_ylabel("Temperature in \u00B0C")
            #ax[0].set_title("Temperature Profile")
            #ax[0].set_xlim((min(xx),max(xx)))

            #ax[1].imshow(xmap.T, aspect="auto", extent=[min(xx),max(xx),max(q),min(q)])                  
            #ax[1].set_xlabel("across stripe")
            #ax[1].set_ylabel("Q in nm$^{-1}$")
            #ax[1].set_title("xrd map")
            #plt.show()
    
            Tprof = None
            all1d.add_StripeData(x=x, y=y, tau=tau, Tmax=Tmax, cats=cations, fracs=fracs, q=q, xmap=xmap, Tprof=Tprof, xx=xx, xc=xc*10)
        else:
            Tprof = None
            all1d.add_StripeData(x=x, y=y, tau=tau, Tmax=Tmax, cats=cations, fracs=fracs, q=q, xmap=xmap, Tprof=Tprof, xx=xx, xc=xc)



