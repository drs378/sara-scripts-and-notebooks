from pathlib import Path
import glob
import os

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from tqdm import tqdm

mpl.rcParams['font.size'] = 12
home = '/nfs/chess/auxiliary/reduced_data/cycles/2022-2/id3b/vandover-3367-A'
matsys = '21_22AJA-D02_SnOx_Al2O3_cap_right'
regex = '*map.npy'
def main():
    fig_ls = glob.glob(f'{home}/{matsys}/Data/{regex}')
    x_set, y_set, dwell_set, tpeak_set = get_cond_set(fig_ls)
    x_ls, y_ls, dwell_ls, tpeak_ls = map(lambda x: sorted(list(x)),
                                          [x_set, y_set, dwell_set, tpeak_set])
    
    if regex.endswith('.npy'):
        dt_data = np.zeros((255*len(tpeak_ls), 255*len(dwell_ls)))
        xy_data = np.zeros((255*len(y_ls), 255*len(x_ls)))
    else:
        dt_data = np.zeros((255*len(tpeak_ls), 255*len(dwell_ls), 3))
        xy_data = np.ones((255*len(y_ls), 255*len(x_ls), 3))*255 # White background
    
    for fig in tqdm(fig_ls):
        x, y, dwell, tpeak = parse_fn(os.path.basename(fig)[1:-4])
        d_index = dwell_ls.index(dwell)
        t_index = tpeak_ls.index(tpeak)
        x_index = x_ls.index(x)
        y_index = y_ls.index(y)

        t = read_n_process_data(fig)
        if regex.endswith('.npy'):
            dt_data[t_index*255:(t_index+1)*255, d_index*255:(d_index+1)*255] = t
            xy_data[y_index*255:(y_index+1)*255, x_index*255:(x_index+1)*255] = t
        else:
            dt_data[t_index*255:(t_index+1)*255, d_index*255:(d_index+1)*255, :] = t
            xy_data[y_index*255:(y_index+1)*255, x_index*255:(x_index+1)*255, :] = t

    dt_data = dt_data.astype(int)
    xy_data = xy_data.astype(int)
    print(tpeak_ls, dwell_ls)
    make_plots(dt_data, xy_data, (np.min(dwell_ls), np.max(dwell_ls)), (np.min(tpeak_ls), np.max(tpeak_ls)), f'{home}/{matsys}', matsys)

def parse_fn(fn):
    _, *conds, _ = fn.split('_') 
    conds = map(int, conds)
    return conds
    
def get_cond_set(fn_ls):
    x_set = set()
    y_set = set()
    dwell_set = set()
    tpeak_set = set()

    for fig in fn_ls:
        x, y, dwell, tpeak = parse_fn(os.path.basename(fig)[:-4])
        x_set.add(x)
        y_set.add(y)
        dwell_set.add(dwell) 
        tpeak_set.add(tpeak)

    return x_set, y_set, dwell_set, tpeak_set

    
def read_n_process_data(fp):
    fn, extension = os.path.splitext(fp)
    if extension == ".npy":
        t = np.load(fp)
    else:
        t = plt.imread(fp)
    t = cv2.resize(t, (255, 255))
    t = np.log10(t).astype(float)
    t *= 255/np.max(t)
    return t.T

def make_plots(dt_data, xy_data, dw_lim, temp_lim, save_path, fn_prefix):
     savepath = Path(save_path)
     
     plt.figure(figsize=(8,6), dpi=150)
     plt.imshow(dt_data)
     plt.ylim(0, dt_data.shape[0])
     plt.xlabel("Dwell")
     plt.ylabel("Tpeak")
     ax = plt.gca()
     ax.set_xticks([0, dt_data.shape[1]])
     ax.set_xticklabels([str(dw_lim[0]), str(dw_lim[1])])
     ax.set_yticks([0, dt_data.shape[0]])
     ax.set_yticklabels([str(temp_lim[0]), str(temp_lim[1])])
     plt.savefig(str(savepath / f"{fn_prefix}_dwell_temp.png"), bbox_inches='tight')

     plt.figure(figsize=(8,6), dpi=150)
     plt.imshow(xy_data, aspect=45/17)
     plt.ylim(0, xy_data.shape[0])
     plt.xticks([])
     plt.yticks([])
     plt.xlabel("x")
     plt.ylabel("y")
     plt.savefig(str(savepath / f"{fn_prefix}_xy.png"), bbox_inches='tight')
     plt.clf()
     plt.close("all")

if __name__ == "__main__":
    main()
