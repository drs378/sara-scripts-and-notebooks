#!/usr/bin/env python3
import sys
from pathlib import Path
import pathlib
import argparse
import os
cwd = Path.cwd()
sys.path.insert(1,'sara-socket-client/CHESS2023_Spring')
sys.path.insert(1,'sara-socket-client/Scripts')
sys.path.insert(1,'sara-socket-client/')

import yaml
import json
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
import zoocam_client as zcc
from xrd_client_chess202 import *
from PySpecAgent import PySpecAgent

from CondtoPath import StripeKeyAndFolder
from CHESS2023_SPRING_Integrator import condtopath
from GetImages import arm_insitu, get_insitu, get_image_at

from threading import Thread
from multiprocessing import Process, Queue
from Interpolate2D import *
import imageio as io
from Mappers import *
from xrd_server_state import CurrentState

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z



def threader(q):
    while True:
        # gets a worker from the queue
        try:
            worker = q.get()
            print("WORKER", worker)
            if worker is None:
                break
            # Run the example job with the avail worker in queue (thread)
            # this call now takes more keyword arguments that depend on the type of scan being collected (scan, point, asdep, insitu, maybe air_scatter...)
            integrate_data(worker[0], worker[1], worker[2], worker[3], worker[4], worker[5], worker[6], worker[7], worker[8])
        except:
            print("worker did not execute correctly")
            break
        #completed with the job
        #q.task_done()
    return True

def integrate_data(xrd_client, pos, cond, offset, dirpath, xmin, xmax, plotpath, plateinputs):
    #Collect and integrate the images from the server
    msg_id = 101
    map_info = xrd_client.get_XRD_GET_MAP_INFO(msg_id)
    map_data, q_data = xrd_client.get_classe(msg_id, pos, cond, offset)
    
    print("")
    print("")
    print('recieved xmap and Q')

    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(np.array(map_data).T, aspect='auto', extent=[xmin,xmax, np.max(q_data), np.min(q_data)])
    ax.set_xlabel('x position')
    ax.set_ylabel('Q in nm$^{-1}$')
    print(dirpath.split("/"))
    os.makedirs(plotpath,exist_ok=True)
    fn_png = dirpath.split("/")[-2] + ".png"
    fn_png = os.path.join(plotpath, fn_png)
    print(fn_png)

    fn_q = dirpath.split("/")[-2] + "_q.npy"
    fn_q = os.path.join(plotpath, fn_q)
    # print(q_data)
    # print(type(q_data))
    print(fn_q)

    fn_map = dirpath.split("/")[-2] + "_map.npy"
    fn_map = os.path.join(plotpath, fn_map)
    print(fn_map)
    # print(map_data)
    # print(type(map_data))

    print("writing map npy")
    np.save(file=fn_map,arr=np.array(map_data))
    print("writing Q npy")
    np.save(file=fn_q,arr=np.array(q_data))
    print("Writing plot to", fn_png)
    
    try:
        plt.savefig(fn_png)
    except:
        fig.savefig(fn_png)
    print("worker is done")

