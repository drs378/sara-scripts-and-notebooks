from scipy.interpolate import CubicSpline
import numpy as np
import copy as cp
from matplotlib import pyplot as plt
#import julia
#from julia import Main
##Main.include("center_finder_asym.jl")
#j = julia.Julia()
#get_center_asym_julia = j.include('center_finder_asym.jl')

def get_center_asym(data,plotting=False,ROI_search=False, left_fwhm = 1., right_fwhm = 1.):
    print("Finding center")
    # Using the correlation method
    im1 = np.array(data[:,:])
    im1 = im1.T
    im = cp.deepcopy(im1)
    center = []
    weights = []
    weights_max = []
    corr = []
    mid = im.shape[1]
    scale = right_fwhm/left_fwhm # < 0
    x = np.array(range(im1.shape[1]) )
    correlation = np.zeros(im1.shape[1])
    # smoothed = fft_smoothing(im1, 15)
    smoothed = im1
    smoothed = smoothed - np.mean(smoothed, axis = -1).reshape(im.shape[0], 1) 
    for i_center in range(int(im1.shape[1]*0.25), int(im1.shape[1]*0.75)):
        left = smoothed[:, :i_center]
        right = smoothed[:, i_center:]
        cs = CubicSpline(x[i_center:], right, axis = -1) # interpolate rows
        #plt.plot(x[i_center:], right[100, :])
        #plt.plot(x[i_center:], cs(x[i_center:])[100,:])
        min_x = np.min(x[i_center:])
        right_scaled = cs(scale * (x[i_center:] -min_x) + min_x)
        #plt.plot(scale * x[i_center:], right_scaled[100,:])
        #plt.show()
        #asds
        #right_scaled = cs(scale * x[i_center:])
        maxlength = min(left.shape[-1], right.shape[-1])
        #print(scale, np.amin(right_scaled), np.amin(left), maxlength)

        left = left[:, i_center-maxlength:i_center]
        #left = (left - np.mean(left)) #/ (np.std(left) + 1.e-8)

        right = right_scaled[:, maxlength-1::-1]
        #right = (right - np.mean(right))#/(np.std(right) + 1.e-8)
        #print(left.shape, right.shape, np.std(left), np.std(right))


        correlation[i_center] += np.sum(left * right) /maxlength
    imaximum_conv = np.argmax(correlation)

#        for i in range(im1.shape[0]):
#            row = im1[i,:]
#            result = np.array(row)-np.mean(np.array(row))
#            smoothed = fft_smoothing(result,15)
#            cs = CubicSpline(x, smoothed)
#
#            result = ndimage.correlate(fft_smoothing(result,15),np.flip(fft_smoothing(result,15),0), mode='wrap')
#            if ROI_search==True:
#                result = fft_smoothing(np.array(result),15.)[int(round(mid-0.5*mid)):int(round(mid+0.5*mid))]
#            else:
#                result = fft_smoothing(np.array(result),15.)
#            im[i,:] = result
#            center.append(np.argmax(result))
#            corr.append(result)
#            weights.append(sum(np.abs(im1[i,:])))
#            weights_max.append(np.amax(result))
#        corr = np.array(corr)
#        smooth = []
#        for i in range(corr.shape[1]):
#            smooth.append(np.mean(corr[:,i]))
#        smd = fft_smoothing(np.array(smooth),15.)
#        center = np.array(center)
#        weights_max = np.power(np.array(weights_max),4)
#        imaximum_conv = (len(result))*0.5 + (np.average(center,weights=weights_max)-(len(result))*0.5)*0.5
    if plotting==True:
        plt.figure()
        plt.plot(correlation)
        plt.show()
        #plt.imshow(corr,aspect='auto')
        #plt.axvline(np.average(center,weights=weights_max),c='gold')
        #plt.show()

        plt.figure()
        plt.imshow(im,aspect='auto')
        plt.axvline(imaximum_conv,c='gold')
        plt.show()
    #print("Center found and returned")
    return 0, imaximum_conv #imaximum excluded because it was throwing error

#@jit(nopython = False)
def fft_smoothing(d,param):
#d is a 1D vector, param is the cutoff frequency
    rft = np.fft.rfft(d)
    rft[:, int(param):] = 0.
    d = np.fft.irfft(rft, d.shape[1])
    return d


if __name__ == "__main__":
    data = np.load("00009b-38_+05_+00369_+0999_data_map.npy")
    #plt.imshow(data)
    #plt.show()
    #plt.imshow(fft_smoothing(data, 15))
    #plt.show()

    #center_julia = get_center_asym_julia(data.T, 1.4, 1.0)
    _, center = get_center_asym(data, plotting=True, ROI_search=False, left_fwhm = 1.4, right_fwhm =  1.0)
    #print(center_julia, center)
    print(center)
