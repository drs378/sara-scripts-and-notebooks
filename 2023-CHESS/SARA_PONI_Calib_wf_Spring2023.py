#!/usr/bin/env python3
import sys
from pathlib import Path
import argparse

sys.path.insert(1,'sara-socket-client/CHESS2021')
sys.path.insert(1,'sara-socket-client/Scripts')
sys.path.insert(1,'sara-socket-client/')
#sys.path.insert(1,'sara-scripts-and-notebooks')
import yaml
import json
import os
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
#matplotlib.use('TkAgg')
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from PySpecAgent import PySpecAgent
from CondtoPath import condtopath, auxpath, CurrentState
from threading import Thread
from Interpolate2D import *
from multiprocessing import Process, Queue
from h5tolinescan_class_2023 import LineScan

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    # parser.add_argument('-d',    '--daqpath',      type=str,                help="Absolute path to the DAQ file system where spec writes to")
    # parser.add_argument('-a',    '--auxpath',      type=str,                help="Absolute path to the Aux file system where all processed data should be written")
    parser.add_argument('-m',    '--matsysname',     type=str,              help="Wafer identifying ID ex: 01_20F7_Bi2O3")
#    parser.add_argument('-y',    '--yshifts',       type=str,               help="txt file containing the yshift coordinates")
    args = parser.parse_args()
    return args




                                                                ###This is updated for 2023 to handle the "low-res" vs. "high-res" scans
                                                                ###There may be some way to automate the pyFAI calls here too...
#def threader(q):
#    while True:
#        # gets a worker from the queue
#        try:
#            worker = q.get()
#            print("WORKER",worker)
#            LS = LineScan(daqpath=worker[0], auxpath=worker[1], MatSysName=worker[2])
#            LS.createOutPaths()
#            if worker is None:
#                break
#            # Run the example job with the avail worker in queue (thread)
#            # returns the coarse center finding
#            shift = LS.lsWorkflow(inputs=worker[3],padname=worker[4])
#            
#            print("worker DID complete----------------------------------------------------------")
#        except:
#            print("worker did not execute correctly")
#            break
#        # completed with the job
#        # q.task_done()
#    return True

# parse args and establish directories
args = parse()
print(args)
# daq = args.daqpath
# aux = args.auxpath
daq = "/nfs/chess/raw/2023-1/id3b/vandover-3367-C/"                                 #To be modified for the current CHESS run
aux = "/nfs/chess/auxiliary/reduced_data/cycles/2023-1/id3b/vandover-3367-C/"       #To be modified for the current CHESS run
MatSysName = args.matsysname                                                        #The one input for the routine

wd = daq+MatSysName+'/'
ad = aux+MatSysName+'/'
print(f'DAQ dir: {daq}')
print(f'AUX dir: {aux}')
print(f'MatSysName: {MatSysName}')
print(f'working dir: {wd}')







##Setup the sockets
#address = "CHESS"
#socket_list = ["lasgo"] 
#socket_clients = sc.socket_clients()
#clients = socket_clients.get_clients(address, socket_list = socket_list)
#
## Instantiate the laser connection
#laser = LSA_Laser.laser()
#laser.socket_clients = socket_clients
#
##connect to the Spec Server
#psa = PySpecAgent()
#psa.ConnectToServer()

# Alignment pad locations: 18 scans in total. 2 at each alignment pad one horizontal 'h-calib' and one vertical 'v-calib'
AlignmentPadLocs  = np.array([
	#The vertical scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],
])

print('')
#Setup the SARA client info
status = CurrentState()
status.directory = wd

det_int_time = 50 #in milliseconds
counter = 0

##Create the linescan object to run
#q = Queue()
#
## how many threads are we going to allow for
#for x in range(1):
#     #t = Thread(target=threader)
#     t = Process(target=threader, args=[q])
#
#     # classifying as a daemon, so they will die when the main dies
#     t.daemon = True
#
#     # begins, must come after daemon definition
#     t.start()





                                                       #This section can be re-worked so that we are only collecting a small amount now
                                                       #There is no need for the entire pad width to be collected
                                                       #x alignment should be within 50 um or less
                                                       #y alignment is more important for calculating the y-shifts and the offset from the optical center.
                                                       #the hard coded laser offset will tell us what the position shift is for the in-situ collection as well:q
yshifts_all = []
summary_shifts_file = []
poni_summary = []

#Data collection cycle
for idx, padloc in enumerate(AlignmentPadLocs):
    counter += 1
    #pass position info to SARA status 
    print('cycle start')
    status.pos = (padloc[0],padloc[1])
    yoffset=0
    print('Status Pos')
    print(status.pos)
    print("")
    print("")
    print("")
    ## can shift this from the default value here
    xshift_lowres    =  0.000
    xshift_highres   =  0.000
    
    yshift_lowres    =  0.000
    yshift_highres   =  0.000
    
    lowres_dx        =  0.100
    lowres_xmin      = -1.000
    lowres_xmax      =  1.000

    lowres_dy        =  0.100
    lowres_ymin      = -1.200
    lowres_ymax      =  3.800
    
    highres_dx       =  0.005
    highres_xmin     = -1.200
    highres_xmax     = -0.900

    highres_dy       =  0.010
    highres_ymin     =  2.300
    highres_ymax     =  2.800
    
    x_beam           =  0.010   #stdev of x-rays along x
    err_x_beam       =  1e-04   #error in stdev of x-rays along x
    
    y_beam           =  0.100   #stdev of x-rays along y 
    err_y_beam       =  1e-04   #error in stdev of x-rays along y

                                                        #Start the low-res/high-res scans    
    for sdx, scan_type in enumerate(['lowres_x','highres_x','lowres_y','highres_y']):
        dirpath,calibpath = condtopath(status)
        print('Calibration daq read/write path')
        print(calibpath)
        calibpath = calibpath + scan_type + '/'
        #Low resolution horizontal alignment scans 2 mm long
        if sdx == 0: 
            padname = f'h-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
            start = (status.pos[0] + lowres_xmin - 0.55, status.pos[1] + 1.3 + yoffset) #hscan start for the pad is -0.55 mm from pad origin and 1 mm before that 
            end =   (status.pos[0] + lowres_xmax - 0.55, status.pos[1] + 1.3 + yoffset) #hscan end for the pad is -0.55 mm from pad origin and 1 mm past that 
            print("low res x-scan")
            print(padname)
            print(start,end)

	        #Set flyscan params
            nframes = int((end - start)/lowres_dx + 1) #int frames
            print(det_int_time,nframes)
            print("")

        elif sdx == 1:
            
            #a high resolution scan on the left edge of the pad to find the x-ray x-shift
            padname = f'h-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
            #hscan start for the high resolution x-scan is on the left edge of the pad
            start = (status.pos[0] + highres_xmin + xshift_lowres, status.pos[1] + 1.3 + yoffset)
            #hscan end for the high resolution x-scan is on the left edge of the pad 
            end =   (status.pos[0] + highres_xmax + xshift_lowres, status.pos[1] + 1.3 + yoffset) 
            print("high res x-scan")
            print("lowres shift: ", xshift_lowres)
            print(padname)
            print(start,end)

	        #Set flyscan params
            nframes =int((highres_xmax - highres_xmin)/highres_dx + 1)  #int frames
            print(det_int_time,nframes)
            print("")

        elif sdx == 2:
	        # Vertical alignment scans 5 mm long
            padname = f'v-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
            start = (status.pos[0] - 0.55, status.pos[1] + lowres_ymin + yoffset) #vscan start for the pad is 1.3 mm from pad origin and 2.5 mm before that 
            end =   (status.pos[0] - 0.55, status.pos[1] + lowres_ymax + yoffset) #vscan end for the pad is 1.3 mm from pad origin and 2.5 mm past that 
            print(padname)
            print(start,end)
	        #Set flyscan params
            nframes = int((end - start)/lowres_dy + 1) #int frames
            print(det_int_time,nframes)
            print("")

        elif sdx == 3:
	        #high resolution vertical scans along the top edge, 500 um long 
            padname = f'v-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
            #vscan start 0.25 mm before the upper edge at 2.55 mm with the coarse shift 
            start = (status.pos[0] - 0.55, status.pos[1] + yshift_lowres + highres_ymin + yoffset)
            #vscan end 0.25 mm past the upper edge at 2.55 mm with the coarse shift
            end =   (status.pos[0] - 0.55, status.pos[1] + yshift_lowres + highres_ymax + yoffset)
            print(padname)
            print(start,end)
	        #Set flyscan params
            nframes = int((highres_ymax - highres_ymin)/highres_dy + 1) #int frames
            print(det_int_time,nframes)
        
            

#        #Check for time to fill. if there is < current det_int_time*nframes + 10s (buffer), wait the durration of the fill + 15s
#        ttf = psa.GetTimeToFill()
#        iclow = psa.GetIntensity()
#
##Checking the intensity of the x-ray beam and ensuring that there is enough intensity for homogeneous data collection 
#        print(f'Time to fill: {ttf} seconds')
#        while (ttf < det_int_time/1000*nframes+3) or (iclow < 1.6):
#            print(f'holding')
#            time.sleep(1)
#            ttf = psa.GetTimeToFill()
#            iclow = psa.GetIntensity()




                                                                            #Execution loop
#        psa.SetFilePaths(calibpath,padname)
#
#                                                
#        #Spawn thread and look to the registered output for the "Armed" state
#        process = Thread(target=psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
#        process.start()
#        while psa.client.read_channel("EIG_ARMED") != "Armed":
#            print("Not ready yet")
#            time.sleep(1)
#
#
#                                                            #Code that executes the flyscan from lasgo
#        laser.execute_flyscan(start, end, det_int_time, nframes)
#        print("Calling status", psa.client.reg_channels['status/ready'].read())
#        print("Waiting for my thread to finish")
#        process.join()
#        print("The PySpec thread has finished")
#
#
#                                            #new code to check if files are written to DAQ in the appropriate folder:
#        detstate = psa.Get_WO_status()
#        while "Idle" not in detstate:
#            detstate = psa.Get_WO_status()
#            time.sleep(0.5)
#            pass
#
        print("calibpath ",calibpath)
        inputs = sorted(glob.glob(calibpath+f'/{padname}*/'+'*.h5'))
        print(inputs[0],inputs[-1])
        
        #New code for determining shifts
        #low resolution 
        if sdx == 0:
            LS = LineScan(daqpath = daq, auxpath = aux, MatSysName = MatSysName, scan_type = 'low_res')
            xshift_lowres, poni_idx = LS.calc_lowres(inputs=inputs,padname=padname)
            xshift_total = xshift_lowres
        
        if sdx == 1:
            LS = LineScan(daqpath = daq, auxpath = aux, MatSysName = MatSysName, scan_type = 'high_res')
            #  or do the following 
            #LS.dx = lowres_dx
            #LS.xmin = highres_xmin               
            #LS.xmax = highres_xmax
            #LS.create_pad_profiles()       
            xshift_highres, (x_beam, err_x_beam) = LS.calc_highres(inputs=inputs,padname=padname)
            xshift_total =  xshift_lowres + xshift_highres

        if sdx == 2:
            LS = LineScan(daqpath = daq, auxpath = aux, MatSysName = MatSysName, scan_type = 'low_res')
            yshift_lowres, poni_idx = LS.calc_lowres(inputs=inputs,padname=padname)
            yshift_total = yshift_lowres
        
        if sdx == 3:
            LS = LineScan(daqpath = daq, auxpath = aux, MatSysName = MatSysName, scan_type = 'high_res')
            #  or do the following
            #LS.dy    =  highres_dy
            #LS.ymin  =  highres_ymin                 
            #LS.ymax  =  highres_ymax
            #LS.create_pad_profiles()     
            yshift_highres, (y_beam, err_y_beam) = LS.calc_highres(inputs=inputs,padname=padname)
            yshift_total = yshift_lowres + yshift_highres
    # xpad, ypad, yshift_total
    yshifts_all.append([padloc[0],padloc[1],yshift_total])
    #xpad, ypad, xshift_lowres, highres, total, x-sigma, err, y-lowres,yhighres, total y-sigma,err
    summary_shifts_file.append([padloc[0],padloc[1],xshift_lowres, xshift_highres,xshift_total,x_beam,err_x_beam,yshift_lowres,yshift_highres,yshift_total,y_beam,err_y_beam])
    
    ## get the frame to do the PONI integration
    frames = []
    for frame in inputs[poni_idx-3, poni_idx+3]:
        frames.append(h5.File(frame,'r')['entry']['data']['data'][0])
    int_frame = np.average(np.array(frames),axis=0)
    print("integrated frame size",int_frame.shape)
    frame = trr.truncation_rod_max(ZingerBGone(int_frame,maxi=1000))[0]
    
    init_poni = "init_poni.poni"
    poni_auto_fit = PoniFit(init_poni = init_poni, calibrant_name='Au', detector = 'Eiger1M')
    poni_auto_fit.multi_fit(frame=frame,fix=['rot3','wavelength'])
    
    pparams = get_poni_from_az(pf.sg.get_ai())
    σ, conf = poni_auto_fit.sg.geometry_refinement.curve_fit()
    errors = [σ[key] for key in list(σ)]

    pf.sg.geometry_refinement.save(f"{ad}/PONIs/scan_{str(counter).zfill(3)}")
    pf.sg.geometry_refinement.save(f"{ad}/PONIs/scan_{str(counter+9).zfill(3)}")
    
    poni_summary.append([int(padloc[0])]+[int(padloc[1])]+[i for b in itertools.zip_longest(pparams, errors) for i in b if i is not None])

 

                                            #to add:
                                            #add PONI workflow as a threaded object??
## save yshifts
np.savetxt(ad+f'_yshifts_2023.txt',np.array(yshifts_all),delimiter=',')

## save shifts and beam profile summary 
summary_header = "xpad (mm), ypad (mm), lr x-shift (mm), hr x-shift, total x-shift, sigma-x, err_sigma-x, lr y-shift, hr y-shift, total y-shift, sigma-y, err_sigma-y"
np.savetxt(ad+'_shifts_and_beamprofile_summary.txt',np.array(summary_shifts_file),delimiter=',',header = summary_header)

## save ponis and poni summary
poni_header = 'XX in mm, YY in mm, dist in m, dist_err in m, poni1, poni1_err, poni2, poni2_err, rot1 in rad, rot1_err, rot2 in rad, rot2_err, rot3 in rad, wl in m'
np.savetxt(ad+'_poni_summary.txt',np.array(poni_summary),delimiter=',', header=poni_header)
                                                                #Obsolete. for faster workflow, serialized but smarter
        #q.put((daq,aux,MatSysName,inputs,padname))
        #print("Submitted worker------------------------------------------------------", counter)

#q.put(None)
#print("Submitted all workers")
## wait until the thread terminates.
#t.join()
#q.join()
#print("Stopped Calibration thread")

print("finished calibration collection")
