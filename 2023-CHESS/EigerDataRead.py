import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import glob
import sys 
import os
sys.path.insert(1,"/home/duncan/sara-scripts-and-notebooks/")
sys.path.insert(1,"/home/duncan/sara-socket-client/Scripts/")
#from SARA_cornell_funcs import ZingerBGone
import argparse

def ZingerBGone(data,mini=0, maxi=9000, fill=1):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',    '--daqpath',   required=True,   type=str,                help="Absolute path to the DAQ file system where spec writes to")
    args = parser.parse_args()
    return args

args = parse()
daqpath = args.daqpath
frames_int = []
for idx, file in enumerate(sorted(glob.glob(daqpath+'*.h5')[:-1])):
    print(idx, os.path.basename(file))
    if 'master' not in file:
        print(os.path.basename(file))
        frame = ZingerBGone(h5.File(file,'r')['entry']['data']['data'][0],fill=2,maxi=20000)
        frames_int.append(np.sum(frame))



plt.figure()
plt.plot(frames_int)
plt.xlabel("frame number")
plt.ylabel("Integrated Intensity (counts)")
plt.title(os.path.basename(daqpath[:-1]))
plt.show()
