import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import glob
import sys
import os 
import gzip
import pathlib
import scipy.signal as signal
from PySpecAgent import PySpecAgent

import argparse
from scipy.optimize import curve_fit
from scipy import special

## proof of concept
def logistic_sigmoid(x,L,x0,k):
    return L/(1 + np.exp(-k*(x-x0)))
def gaussian_fx(x,μ,σ,A,b):
    return A * np.exp(-(x-μ)**2/σ**2) + b
def mod_erf(x,μ,σ,A,b):
    return A/2 * (1 - special.erfc((x - μ) / (np.sqrt(2)*σ))) + b

#args in: dir start end
def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',   '--daqpath',    type=str,   help="Directory in the DAQ where raw alignment pad data is stored")
    # parser.add_argument('-se',      '--se',     type=float,      nargs='+',          help="tuple of positions for start and end in mm")
    parser.add_argument('-a',   '--auxpath',    type=str,   help="Directory in the AUX where the xI linescan data is stored")
    parser.add_argument('-p',   '--plots',      action='store_true',  help='will display the plots so one can see the correlated profiles and the resulting y-shift maps')
    parser.add_argument('-m',   '--MatSysName',    type=str,   help="Material System Name and ID")
    args = parser.parse_args()
    return args


def ZingerBGone(data,mini=0, maxi=9000, fill=1):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

def filtdata(data):
    """returns the data but with a min/max filter"""
    fdata = []
    for idx,d in enumerate(data):
        print(idx)
        fdata.append(ZingerBGone(d))
    return fdata

def linescan_extract(fdir='/path/to/h5s/', outdir='path/to/aux/', start=(1.0,0.0), end=(2.0,0.0), onescan=True, save=True):
    """returns the distance,Intensity array"""
    pathtodir = fdir
    fname = fdir.split('/')[-2]
    print(fname)
    print(start,end)
    # if start[0] == end[0]:
    #     start = start[1]
    #     end = end[1]
    # else:
    #     start = start[0]
    #     end = end [0]


    Is = []
    if onescan:
        files = sorted(glob.glob(fdir+'*.h5'))
        for f in files:
            if 'master' not in f:
                data = h5.File(f,'r')['entry']['data']['data'][0]
                I = np.sum(ZingerBGone(data))
                Is.append(I)

    else:
        files = glob.glob(fdir+'*.h5')
        for f in files:
            if 'master' not in f:
                data = h5.File(f,'r')['entry']['data']['data']
                print(data.shape)
                print(data)
                for frame in range(data.shape[0]):
                    print(frame)
                    I = np.sum(ZingerBGone(data[frame]))
                    Is.append(I)


    Is = np.array(Is)
    coord = np.linspace(start,end,len(Is))
    output = np.vstack((coord,Is)).T

    if save:
        np.save(f'{outdir}{fname}.npy',output)
    return output


class LineScan():
    def __init__(self, daqpath=None, auxpath=None, MatSysName=None, scan_type='low_res'):
        """
        This converts the calibration data pad intensity to linescans for correlation.
        The inputs are:
        daqpath = (path) where to read the x-ray data from
        auxpath = (path) where to write the linescan data to (note that the user needs to define high/low res for scan types
        MatSysName = (str) name of the material system for testing     
        scan_type = (str) can be high_res or low_res to modify the out file path 
        """
        self.MatSysName = MatSysName
        self.daqpath = daqpath
        self.auxpath = auxpath
        self.scan_type = scan_type
        self.outdir = os.path.join(self.auxpath,self.MatSysName)
        self.ls_out_path = None
        self.fname = None
        
        
        self.lowres_xshift = []
        self.lowres_yshift = []
        
        self.highres_xshift = []
        self.highres_yshift = []
        
        self.lowres_vscans = []
        self.lowres_hscans = []
        
        self.highres_vscans = []
        self.highres_hscans = []
        
        self.vpadlocs = []
        self.hpadlocs = []
        
        self.dx   =  0.010             #step size in mm
        self.xmin = -1.550           #x scan start in mm
        self.xmax =  0.450            #x scan end in mm
        
        self.dy   =  0.020             #step size in mm
        self.ymin = -3.500           #y scan start in mm
        self.ymax =  3.500           #y scan end in mm
    #def set_scan_params(self, scan_type="low_res"):
    #    """
    #    sets the nominal low resolution or high resolution scan parameters
    #    """
    #    if scan_type == "low_res":
    #        self.dx   =  0.100             #step size in mm
    #        self.xmin = -1.550           #x scan start in mm
    #        self.xmax =  0.450            #x scan end in mm
    #        
    #        self.dy   =  0.100             #step size in mm
    #        self.ymin = -1.200           #y scan start in mm
    #        self.ymax =  3.800           #y scan end in mm


    #    if scan_type == "high_res":
    #        self.dx   =  0.005              #step size in mm
    #        self.xmin = -1.200            #x scan start in mm
    #        self.xmax = -0.900            #x scan end in mm
    #        
    #        self.dy   =  0.010             #step size in mm
    #        self.ymin =  2.300           #y scan start in mm
    #        self.ymax =  2.800           #y scan end in mm
    #    self.create_padprofile()

    
    ### test function
    def set_scan_params(self, scan_type="low_res"):
        """
        sets the nominal low resolution or high resolution scan parameters
        """
        if scan_type == "low_res":
            self.dx   =  0.010             #step size in mm
            self.xmin = -1.550           #x scan start in mm
            self.xmax =  0.450            #x scan end in mm
            
            self.dy   =  0.020             #step size in mm
            self.ymin = -3.500           #y scan start in mm
            self.ymax =  3.500           #y scan end in mm


        if scan_type == "high_res":
            self.dx   =  0.010              #step size in mm
            self.xmin = -1.550            #x scan start in mm
            self.xmax =  0.450            #x scan end in mm
            
            self.dy   =  0.020             #step size in mm
            self.ymin = -3.500           #y scan start in mm
            self.ymax =  3.500           #y scan end in mm
        self.create_padprofile()

    def create_padprofile(self):
        #2023 alignment pad profiles
        #h-scan span
        self.nframes_x = int((self.xmax-self.xmin)/self.dx+1)
        self.pwfx_I = np.zeros(self.nframes_x)
        self.pwfx = np.linspace(self.xmin,self.xmax,self.nframes_x)

        #h-scan main box shape 
        self.pwfx_I[(-1.05 < self.pwfx) & (self.pwfx < -.05)] = 1
        #h-scan center bar
        self.pwfx_I[(-0.005 < self.pwfx) & (self.pwfx < .005)] = 0.25

        #v-scan span
        self.nframes_y = int((self.ymax-self.ymin)/self.dy+1)
        self.pwfy_I = np.zeros(self.nframes_y)
        self.pwfy = np.linspace(self.ymin, self.ymax, self.nframes_y)

        #v-scan main box shape
        self.pwfy_I[(0.05 < self.pwfy) & (self.pwfy < 2.550)] = 1

        #v-scan crosshair
        self.pwfy_I[(-0.005 < self.pwfy) & (self.pwfy < 0.005)] = .25

        #v-scan PL alignment marks
        self.pwfy_I[(-1.7 <self.pwfy) & (self.pwfy < -1.4)] = 0.25
        self.pwfy_I[(-1.1 <self.pwfy) & (self.pwfy < -0.8)] = 0.25 
        

    def createOutPaths(self, scan_type='low_res'):
        pathlib.Path(self.outdir).mkdir(parents=True, exist_ok=True)
        print("making files ")
        print(" ")
        print(self.outdir)
        self.ls_out_path = os.path.join(self.outdir,'linescans')
        self.ls_out_path = os.path.join(self.ls_out_path, scan_type)
        print(self.ls_out_path)
        pathlib.Path(self.ls_out_path).mkdir(parents=True, exist_ok=True)
        print(os.path.join(self.outdir, 'Data'))
        pathlib.Path(os.path.join(self.outdir, 'Data')).mkdir(parents=True, exist_ok=True)
        pathlib.Path(os.path.join(self.outdir, 'PONIs')).mkdir(parents=True, exist_ok=True)
        print(os.path.join(self.outdir, 'Data'))
        print(os.path.join(self.outdir, 'PONIs'))
        print("made file paths")


    def filtSum(self, frame):
        return np.sum(ZingerBGone(frame))


    def linescan_extract(self, inputs=None, outdir=None, start=(1.0,0.0), end=(2.0,0.0), onescan=True, save=True):
        """
        returns the distance,Intensity array
        inputs is a list of paths
        outdir is a single path
        """
        print("Linescan START and END------------------------------------------------------------------------------------------")
        print(start,end)
        # if start[0] == end[0]:
        #     start = start[1]
        #     end = end[1]
        # else:
        #     start = start[0]
        #     end = end [0]


        Is = []
        if onescan:
            for f in inputs:
                # print(f)
                self.fname = f.split('/')[-2]
                # print(f'fname is {self.fname}')
                # try:
                #     f = gzip.open(f,'rb')
                # except:
                #     pass

                if 'master' not in f:
                    data = h5.File(f,'r')['entry']['data']['data'][0]
                    I = np.sum(ZingerBGone(data))
                    Is.append(I)

        else:
            for f in inputs:
                print(f)
                self.fname = f.split('/')[-2]
                # print(f'fname is {self.fname}') 
                # try:
                #     f = gzip.open(f,'rb')
                # except:
                #     pass

                if 'master' not in f:
                    data = h5.File(f,'r')['entry']['data']['data']
                    # print(data.shape)
                    # print(data)
                    for frame in range(data.shape[0]):
                        print(frame)
                        I = np.sum(ZingerBGone(data[frame]))
                        Is.append(I)


        Is = np.array(Is)
        coord = np.linspace(start,end,len(Is))
        output = np.vstack((coord,Is)).T

        if save:
            np.save(os.path.join(self.ls_out_path,f'{self.fname}.npy'),output)
        return output

    def calc_lowres(self,inputs=None, padname=None):
        if self.scan_type != None:
            self.set_scan_params("low_res")
            self.createOutPaths("low_res")
        
        scanname = padname
        print(f"scanname is {scanname}")
        print(f"inputs {inputs[0]}")
        scandirection = scanname[0]
        xpad = float(scanname.split('_')[2])
        ypad = float(scanname.split('_')[3])
        print(scandirection,xpad,ypad)
        if scandirection == 'h':
            start = xpad - self.xmin
            end = xpad + self.xmax
            # test function
            try:
                output = np.load(inputs)
            except:
                output = self.linescan_extract(inputs=inputs, outdir=self.ls_out_path, start=start, end=end)
            xI = output[:,1]
            xn = output[:,0]
            

            mean = np.mean(xI)
            pcorr = signal.correlate(xI-mean, self.pwfx_I[:len(xI)]-0.5, mode='same')
            lendiff = len(self.pwfx_I) - len(xI)
            corcenter = np.where(pcorr==max(pcorr))[0][0]
            shift_idx = corcenter + lendiff - 0.5*len(xI)
            shift = shift_idx*self.dx
            poni_idx = int(np.round(len(xn)/2 + shift_idx))

            print(f'lowres xshift is {shift*self.dx} mm at ({xpad}, {ypad})')
            print('')
            #plt.plot(xn,(xI-min(xI))/max(xI),label='raw')
            #plt.plot(xn,self.pwfx_I,label='profile')
            #plt.show()
        
            self.lowres_xshift.append(shift)
            self.hpadlocs.append([xpad,ypad])
            self.lowres_hscans.append(output)            

        else:
            start = ypad - self.ymin
            end = ypad + self.ymax
            # test function
            try:
                output = np.load(inputs)
            except:    
                output = self.linescan_extract(inputs=inputs, outdir=self.ls_out_path, start=start, end=end)
            yI = output[:,1]
            yn = output[:,0]


            mean = np.mean(yI)
            pcorr = signal.correlate(yI-mean,self.pwfy_I[:len(yI)]-0.5,mode='same')
            lendiff = len(self.pwfy_I) - len(yI)
            corcenter = np.where(pcorr==max(pcorr))[0][0]
            shift_idx = corcenter + lendiff - 0.5*len(yI)
            shift = shift_idx*self.dy
            poni_idx = int(np.round(len(yn)/2 + shift_idx))
            
            print(f'lowres yshift is {shift*self.dy} mm at ({xpad}, {ypad})')
            print('')
            #plt.plot(yn,(yI-min(yI))/max(yI),label='raw')
            #plt.plot(yn,self.pwfy_I,label='profile')
            #plt.show()
            
            self.lowres_yshift.append(shift)
            self.vpadlocs.append([xpad,ypad])
            self.lowres_vscans.append(output)
        return shift, poni_idx 
    
    def calc_highres(self, inputs=None, padname=None):
        if self.scan_type != None:
            self.set_scan_params("high_res")
            self.createOutPaths("high_res")
        scanname = padname
        print(f"scanname is {scanname}")
        print(f"inputs {inputs[0]}")
        scandirection = scanname[0]
        xpad = float(scanname.split('_')[2])
        ypad = float(scanname.split('_')[3])
        print(scandirection,xpad,ypad)
        if scandirection == 'h':
            start = xpad - self.xmin
            end = xpad + self.xmax
            # test the function
            try:
                output = np.load(inputs)
            except:
                output = self.linescan_extract(inputs=inputs, outdir=self.ls_out_path, start=start, end=end)
            xI = output[:,1]
            xn = output[:,0]
            
            grad_xI = abs(np.gradient(xI))
            try:
                peak = signal.find_peaks(grad_xI, height=(0.5*max(grad_xI),max(grad_xI)))[0][0]
                p0 = [xn[peak], 0.100, max(grad_xI), min(grad_xI)]
            except:
                peak = int(np.round(len(grad_xI)/2))
                p0 = [xpad-1.05, 0.01, max(grad_xI), min(grad_xI)]

            try:
                popt1, pcov1 = curve_fit(f=gaussian_fx,xdata=xn, ydata=grad_xI,p0=p0)
            except:
                popt1 =[xpad - 1.05,0.01, max(grad_xI), min(grad_xI)]

            shift = popt1[0] - xpad + 1.05
            beam_profile = popt1[1] 
            try:
                errs =  np.sqrt(np.diag(pcov1))[1]
            except:
                print('failed to fit')
                errs = -1e2
            plt.plot(xn,xI,label='raw')
            plt.plot(xn,grad_xI,label='grad')
            plt.plot(xn,gaussian_fx(xn,*popt1),label="model")
            plt.show()
            print(f'highres xshift is {shift} mm at ({xpad}, {ypad})')
            print('')
            #plt.plot(xn,xI,label='raw')
            #plt.plot(xn,grad_xI,label='grad')
            #plt.plot(xn,gaussian_fx(xn,*popt1),label='model')
            #plt.legend()
            #plt.show()

            return shift, (beam_profile, errs)
        
        
        
        else:
            start = ypad - self.ymin
            end = ypad + self.ymax
            try:
                output = np.load(inputs)
                yn = output[:,0]
                yI = output[:,1][yn>ypad+1.0]
                yn = yn[yn>ypad+1.0]
            except:
                output = self.linescan_extract(inputs=inputs, outdir=self.ls_out_path, start=start, end=end)
                yI = output[:,1]
                yn = output[:,0]

            grad_yI = abs(np.gradient(yI))
            
            #print([yn[i] for i in signal.find_peaks(grad_yI, height=(0.5*max(grad_yI),max(grad_yI)))[0]])
            #print(signal.find_peaks(grad_yI, height=(0.5*max(grad_yI),max(grad_yI))))
            #plt.plot(yn,yI,label='raw')
            #plt.plot(yn,grad_yI,label='grad')
            #plt.show()
            try:
                print("pass1")
                peak = signal.find_peaks(grad_yI, height=(0.5*max(grad_yI),max(grad_yI)))[0][-1]
                p0 = [yn[peak], 0.100, max(grad_yI), min(grad_yI)]
            except:
                print("fail1")
                peak = int(np.round(len(grad_yI)/2))
                p0 = [ypad+2.55, 0.01, max(grad_yI), min(grad_yI)]
                
            try:
                print("pass2")
                popt1, pcov1 = curve_fit(f=gaussian_fx,xdata=yn,ydata=grad_yI,p0=p0)
            except:
                print("fail2")
                popt1 =[ypad + 2.55,0.07, max(grad_yI), min(grad_yI)]
            
            print(popt1[0], ypad)
            shift = popt1[0] - ypad - 2.55
            beam_profile = popt1[1] 

            try:
                errs =  np.sqrt(np.diag(pcov1))[1]
            except:
                print('failed to fit')
                errs = -1e2
            plt.plot(yn,yI,label='raw')
            plt.plot(yn,grad_yI,label='grad')
            plt.plot(yn,gaussian_fx(yn,*popt1),label="model")
            plt.show()
            print(f'highres yshift is {shift} mm at ({xpad}, {ypad})')
            print('')
            #plt.plot(yn,yI,label='raw')
            #plt.plot(yn,grad_yI,label='grad')
            #plt.plot(yn,gaussian_fx(yn,*popt1),label='model')
            #plt.legend()
            #plt.show()
            return shift, (beam_profile, errs)



if __name__ == "__main__":
    args = parse()
    #daq = args.daqpath
    #aux = args.auxpath
    #MatSysName = args.MatSysName
    #plotting = args.plots
    
    daq = "~/"
    aux = "/nfs/chess/auxiliary/reduced_data/cycles/2021-3/id3b/vandover-2782-H/"
    MatSysName = "30_21F54_Bi-Zr-O"


    xtest = '/nfs/chess/auxiliary/reduced_data/cycles/2021-3/id3b/vandover-2782-H/30_21F54_Bi-Zr-O/linescans/h-calib_pad_-32_-30_001.npy'
    ytest = '/nfs/chess/auxiliary/reduced_data/cycles/2021-3/id3b/vandover-2782-H/30_21F54_Bi-Zr-O/linescans/v-calib_pad_-32_-30_001.npy'
   
    
    LS = LineScan(daqpath=daq, auxpath=aux, MatSysName=MatSysName, scan_type='low_res')
    shift, poni_idx = LS.calc_lowres(inputs=xtest,padname= 'h-calib_pad_-32_-30')
    print(poni_idx, len(np.load(xtest)))
    shift = LS.calc_highres(inputs=xtest,padname= 'h-calib_pad_-32_-30')
    
    LS = LineScan(daqpath=daq, auxpath=aux, MatSysName=MatSysName, scan_type='low_res')
    shift, poni_idx = LS.calc_lowres(inputs=ytest,padname= 'v-calib_pad_-32_-30')
    shift = LS.calc_highres(inputs=ytest,padname= 'v-calib_pad_-32_-30')
    print(shift)



