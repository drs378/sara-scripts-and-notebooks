import numpy as np
import matplotlib.pyplot as plt
import os
import sys


def dwellsort(listofkeys,Temp = True):
    demkeys = listofkeys
    temp = []
    s=[]
    for i in demkeys:

        i = i.split('_')
        i[1] = i[1].zfill(5)
        i[3] = i[3].zfill(5)
        s.append(i)
    s = np.array(s)
#    for i in s[:,1]:
#        i = i.zfill(5)
#        temp.append(i)

#    s=np.insert(s,1,temp,axis=1)
#    temp = []
#    s = np.array(s)
#    s = np.delete(s,2,1)
    if Temp:
        dwellsorted = sorted(s,key=lambda x:(x[3],x[1]))
    else:
        dwellsorted = sorted(s,key=lambda x:(x[1],x[3]))
#    dwellsorted = sorted(dwellsorted,key=lambda x:x[1])


    for i in dwellsorted:
        i[1] = i[1].lstrip('0')
        i[3] = i[3].lstrip('0')
        i = np.hstack((i))
        i = i.tolist()
        b = '_'
        i = b.join(i)
        temp.append(i)

    demkeys = temp
    
    return demkeys


def MissingConditions(keys):
    demkeys = dwellsort(keys)
    grid = []
    blanks = []
    for key in demkeys:
        split = key.split('_')
        tau = split[1]
        T = split[3]
        grid.append([tau,T])
    conds = np.array(grid,dtype=float)
    uniqueTs = np.unique(conds[:,1])
    uniqueDs = np.unique(conds[:,0])
    for T in uniqueTs:
        empty_dwells = np.array(list(set(conds[conds[:,1]==T][:,0]).symmetric_difference(set(uniqueDs))))
        blank_keys=['tau_'+str(int(i))+'_T_'+str(int(T)) for i in empty_dwells]
        for key in blank_keys:
            blanks.append(key)
    return blanks


def GenerateXmap(tau=10000,Tmax=1400,h5file=None):
    data = h5.File(h5file, 'r')['exp']
    key = f'tau_{tau}_T_{Tmax}'
    xmap = []
    for subscan in sorted(list(data[key]), key=int):
        xmap.append(data[key][subscan]['integrated_1d'][1])
        xmap = np.array(xmap)
        Q = data[key][subscan]['integrated_1d'][0]
    return xmap, Q


