# for key in ['tau_700_T_1400','tau_3000_T_1050','tau_700_T_900','tau_900_T_1200','tau_7000_T_1275']:
def PrettyFigureStack(omap,smap,xmap,pxs,WL,Q,wholeimg,resample=False):
    # resample = False
    xpos = xys[key][0]
    ypos = xys[key][1]
    xkey = key

        #inputing the pixel sizes once. Different sets of data were collected at different resolutions
    sps = pxs[1]
    ops = pxs[0]
    xps = pxs[2]
    ops1 = 10
    TechPSs = [ops,sps,xps]
    TechNames = ['Optical','Spectroscopy','XRD']

    # each technique spans a different anneal space. Each technique needs to be tailored accordingly.
    # For this sample, the x-ray maps span 1 mm, whereas the optical and spectroscopy are ~1.2 mm 
    # and 2.01 mm respectively. This will change depending on the material system, the zoom of the
    # optical camera, the space collected by the spectroscopy and that of the XRD. 

    #defining the anneal space span for each technique
    oscale = omap.shape[1]*ops
    sscale = smap.shape[1]*sps
    xscale = xmap.shape[1]*xps

    #locating the smallest one and determining the appropriate crop pixel
    scales = [oscale,sscale,xscale]
    minscale = min(scales)

    oscale = int(np.round(0.5*(oscale-minscale)/ops))
    sscale = int(np.round(0.5*(sscale-minscale)/sps))
    xscale = int(np.round(0.5*(xscale-minscale)/xps))
    scales = [oscale,sscale,xscale]

    #loading the technique maps
    omap = np.array(omap,dtype=float) # the image needs to be in float value for calculations
    smap = smap[300:1300,:] # the spectrometer is only good for the following wavelength values
    xmap = xmap

    #was having issues with some of the transition finding/gradient calculations. Re-did the gradient
    #calculations here
    isum = np.sum(omap,axis=0)
    isum = ndimage.gaussian_filter1d(isum,10)
    isum = isum/max(isum)
    g = np.gradient(isum)
    g = np.sqrt(g*g)
    g = g/max(g)

    h_cutoff = 150

    a_sigma = abs((np.std(g[0:h_cutoff])+np.std(g[-h_cutoff:]))*0.5)
    a_mean = abs((np.mean(g[0:h_cutoff])+np.mean(g[-h_cutoff:]))*0.5)
    h_filt = 3*a_sigma
    xpix = len(g)
    g = g - a_mean

    # Locating the transitions
    ostats = signal.find_peaks(g,prominence=.05,width=[0.001*xpix,0.25*xpix],height=h_filt)[0]
    #     ostats = MSI_Transition_Finder(filepath=omap,mpath=mirror,blur=1,h_thresh=5,Gpromfilt=0,v_cutoff=0,pixel_size=ops,norm=True,ImgIO=True,plotting=False)
    sstats = Spectroscopy_Transiton_Finder(smap,Gpromfilt=.05,h_thresh=3,h_cutoff=50,s_param=.5,cond=key,plotting=False)
    xstats = xrd_transition_finder(xmap,h_thresh=3,h_cutoff=10,s_param=0.5,cond=xkey,plotting=False)

    # redefined maps so that they span approximately the same anneal space. also applying a smoothing function
    omap = ImgDict[key]['whole']
    smap = ndimage.gaussian_filter(ReflDict[key][300:1300,:][:,sscale:-sscale],0.5)
    xmap = ndimage.gaussian_filter(xrdDict[xkey][:,xscale:-xscale],(3,.25))

    # Taking the gradients along the correct scale
    ograd = g
    #     ograd = ostats['Gradient']
    sgrad = sstats['gradient'][sscale:-sscale]
    xgrad = xstats['gradient'][xscale:-xscale]

    for grad in [ograd,sgrad,xgrad]:
        grad[0] = np.mean(grad[:15])
        grad[-1] = np.mean(grad[-15:])


    TechGrads = [ograd,sgrad,xgrad]

    if resample==True:
        y = signal.resample(ograd,len(xgrad))
        ops = 10
        TechPSs = [ops,sps,xps]
        TechGrads = [y,sgrad,xgrad]

    #the transitions that were located were for each techniques FULL map. this appropriately shifts
    #them to the proper cropped location
    #     otrs = ostats['TR_idx']
    print('scales')
    print(oscale,sscale,xscale)
    otrs = ostats-oscale
    strs = sstats['TR_idx']-sscale
    xtrs = xstats['TR_idx']-xscale

    # anneal space definition
    ox = np.linspace(0,ops*(len(ograd)-1),len(ograd))
    sx = np.linspace(0,sps*(len(sgrad)-1),len(sgrad))
    xx = np.linspace(0,xps*(len(xgrad)-1),len(xgrad))

    #Centerfinding
    oc = CenterFinding(ograd,1)
    sc = CenterFinding(sgrad,1)
    xc = CenterFinding(xgrad,1)
    print('centers')
    print(oc,sc,xc)

    ox = (ox-oc*ops)
    sx = (sx-sc*sps)
    xx = (xx-xc*xps)

    fig,ax = plt.subplots(3,1)
    ax[0].plot(ox,ograd,c='r')
    ax[1].plot(sx,sgrad,c='g')
    ax[2].plot(xx,xgrad,c='b')
    ax[0].axvline(0,c='r')
    ax[1].axvline(0,c='g')
    ax[2].axvline(0,c='b')
    plt.show()
    plt.close(fig)

    # minmax
    minx = max(min(ox),min(sx),min(xx))
    maxx = min(max(ox),max(sx),max(xx))
    print(minx,maxx)
    # #same spaces
    omap1 = omap[:,[(ox>=minx)&(ox<=maxx)][0],:]
    smap1 = smap[:,[(sx>=minx)&(sx<=maxx)][0]]    
    xmap1 = xmap[:,[(xx>=minx)&(xx<=maxx)][0]]

    #plotting the maps, transitions in the maps, and the gradients. The header contains
    #The conditions and the location
    
    fig,ax = plt.subplots(3,1,figsize=(6.4,6.4),constrained_layout=True,dpi=150)
    fig.suptitle('3 Technique comparison: Optical, Spectroscopy, XRD')
    ax[0].imshow(omap1,aspect='auto',extent=[0,omap1.shape[1]*ops,0,omap1.shape[0]*ops])
    ax[1].imshow(smap1,aspect='auto',extent=[0,smap1.shape[1]*sps,min(wl),max(wl)])
    ax[2].imshow(ndimage.gaussian_filter(xmap1,1),aspect='auto',
                   extent=[minx,maxx,max(Q),min(Q)])
#     ax[0].axvline(0.5*omap1.shape[1],c='goldenrod')
#     ax[1].axvline(0.5*smap1.shape[1],c='goldenrod')
#     ax[2].axvline(0.5*xmap1.shape[1],c='goldenrod')

    ax[0].set_xticks([])
    ax[1].set_xticks([])
    ax[2].set_xlabel('Anneal Space in \u03bcm')
    ax[0].set_ylabel('Anneal Space in \u03bcm')
    ax[1].set_ylabel('Wavelength in nm')
    ax[2].set_ylabel('Q in nm$^{-1}$')
    # plt.show(fig)
#     pdf.savefig(fig)
    # plt.close(fig)
    return fig