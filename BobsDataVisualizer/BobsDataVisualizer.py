import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import sys 
# sys.path.insert(1,"/home/duncan/sara-scripts-and-notebooks/")
import os 
import glob
import h5py as h5
from SARA_cornell_funcs import *
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
import seaborn as sns
import ipywidgets as ipy
import ipywidgets as ipy
from ipywidgets import  Output as output


class BobsDataLoader(object):
    """
    A class that generates the XRD data from Bob's master file
    """
    def __init__(self,dbcsv=None,basepath=None):
        
        
        self.csvfile = dbcsv
        
        raw = csv.reader(open(self.csvfile), delimiter=',')
        data = np.array([row for row in raw])
        self.master_dict = {}
        for idx,key in enumerate(data[0]):
            self.master_dict[key] = data[1:,idx]
            
        self.basepath = basepath
        self.composition = []
        self.taus = []
        for fn in self.master_dict['/* Filename']:
            self.composition.append(fn.split('/')[0])

        self.comp_keys = np.unique(self.composition)
        
        for fdx,fn in enumerate(self.master_dict['/* Filename']):
            if fn.split('/')[0] == self.comp_keys[0]:
                self.taus.append(self.master_dict['dwell/ms'][fdx])
        self.taus = np.unique(self.taus)[1:]
        
        self.Fe_frac = [0, 0.17, 0.5, 0.66, 0.83, 1]
        self.Mn_frac = [1, 0.83, 0.5, 0.33, 0.17, 0]
        self.dataset = {}
        for kdx, comp_name in enumerate(self.comp_keys):
            comp = f"Fe_{self.Fe_frac[kdx]}_Mn_{self.Mn_frac[kdx]}"
#             print(comp)
#             print(kdx)
            self.dataset[comp] = {}
            for tau in self.taus:
                key = f'tau_{str(int(float(tau)*1000)).zfill(5)}_T_1250'
                self.dataset[comp][key] = {}
                xmap = []
                Is = []
                qs = []
                temperature = [] 
                d = (data[1:])
                dblocs = np.where(
                    (d[:,5] == str(tau)) &
                    np.array([comp_name in f for f in d[:,0]])
                )
                for idx in dblocs[0]: 
        #             print(idx)
                    fn = d[idx,[0]].astype(str)[0]
        #             print(basepath+fn)
                    temperature.append(d[idx,10])
                    file = self.basepath+fn
                    rdat = csv.reader(open(file),delimiter='\t')
                    dat = np.array([row for row in rdat][274:],dtype=float)
                    q = dat[:,0]*10
                    I = dat[:,1]

                    f = interp1d(q, I, kind="cubic")
                    qs.append(q)
                    Is.append(I)

                    self.Q = np.linspace(13.5,41,1501)
                    xmap.append(f(self.Q))

                xmap = np.stack(xmap,axis=0).T
                self.dataset[comp][key]['xmap'] = xmap
                self.dataset[comp][key]['Temperature'] = np.array(temperature,dtype=float)
                self.dataset[comp][key]['Mn mol frac'] = self.Mn_frac[kdx]
                self.dataset[comp][key]['Fe mol frac'] = self.Fe_frac[kdx]
                self.dataset[comp][key]['tau'] = tau
                self.dataset[comp][key]['Tmax'] = 1250
                               
                
            self.tauT_keys =  list(self.dataset[comp])
            self.xmap = np.array(JitterCorrection(xmap))
            self.Tprof = np.array(temperature,dtype=float)
            self.tau = tau
            self.Tmax = 1250
            self.tTkey = key
            self.ckey = comp
            self.xx = np.linspace(0,xmap.shape[1]*12.5,xmap.shape[1])
            self.xc = get_center(xmap[650:-650,:])[0]
            self.xx = self.xx - 12.5*self.xc
            self.xg,_ = Grad(self.xmap,bound_avg=True,norm=True,plotting=False)
            
            self.xsym = Symmetrize(self.xg,self.xc,plotting=False)

            
    def ActiveStripeData(self, tauT_key=None, comp_key=None, exclude=(1,1)):
        nix = exclude
        self.tTkey = tauT_key
        self.ckey = comp_key
        self.xmap = np.array(JitterCorrection(self.dataset[comp_key][tauT_key]['xmap'][:,nix[0]:-nix[1]]))
        self.Tprof = self.dataset[comp_key][tauT_key]['Temperature'][nix[0]:-nix[1]]
        self.tau = self.dataset[comp_key][tauT_key]['tau']
        self.Fe_frac = self.dataset[comp_key][tauT_key]['Fe mol frac']
        self.Mn_frac = self.dataset[comp_key][tauT_key]['Mn mol frac']
        self.Tmax = self.dataset[comp_key][tauT_key]['Tmax']
        
        self.xx = np.linspace(0,self.xmap.shape[1]*12.5,self.xmap.shape[1])
        self.xc = get_center(self.xmap[650:-650,:])[0]
        self.xx = self.xx - 12.5*self.xc
        self.xg,_ = Grad(self.xmap,bound_avg=True,norm=True,plotting=False)
        self.xsym = Symmetrize(self.xg,self.xc,plotting=False)
        
        
        self.xtrs = TransitionFinder(data=self.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        
    def StickPatterns(self, CIFpaths=None):
        self.CIFpaths = CIFpaths
        self.Qmin = self.Q[0]
        self.Qmax = self.Q[-1]

        self.TTmin = QtoTT(self.Qmin)
        self.TTmax = QtoTT(self.Qmax)
        xrdcalc = pmg.XRDCalculator()
        
#         self.StickStructures = {}
        self.PhaseSets = {}
        for idx, (root, dirs, files) in enumerate(os.walk(self.CIFpaths)):
            if idx == 0:
#                 print('Create Dictionary')
                for jdx in np.arange(len(dirs)):
                    self.PhaseSets[f'Phase Set {jdx+1}'] = {}

            if idx > 0:
                for jdx, file in enumerate(files):
#                     print(root+'/'+file)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}'] = {}
                    smeta = os.path.basename(file).split('_')
                    dp = xrdcalc.get_pattern(cif.CifParser(root+'/'+file).get_structures()[0],two_theta_range=(self.TTmin,self.TTmax))
#                     print(smeta)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Formula'] = smeta[0]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['SpaceGroup'] = smeta[1]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['fp'] = file
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['MP Structures'] = cif.CifParser(root+'/'+file).get_structures()[0]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Qs'] = TTtoQ(dp.x)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Is'] = dp.y
        
        
class Bobs_XRD_GUI(object):
    def __init__(self, BDL=None, PMoutputpath=None):
        #Interactables
        #stripe slider
        BDL.ckey = "Fe_0.17_Mn_0.83"
        BDL.tTkey = "tau_10000_T_1250"
        BDL.ActiveStripeData(BDL.tTkey,BDL.ckey)
        self.BDL = BDL
        output = ipy.Output()
        self.PMoutputpath = PMoutputpath
        with output:
            self.fig = plt.figure(figsize=(12,6), tight_layout=True)
        self.fig.suptitle(f"MnTiO3 - FeTiO3  Mn:{BDL.dataset[BDL.ckey][BDL.tTkey]['Mn mol frac']} Fe:{BDL.dataset[BDL.ckey][BDL.tTkey]['Fe mol frac']}")
        self.colors = list(sns.color_palette('tab10',16).as_hex())
        gs = gridspec.GridSpec(4,2)
        TT = QtoTT(BDL.Q)
        
        
        self.init_idx = 35
        self.oned_map_plot = self.fig.add_subplot(gs[:3,0])
        self.oned_map = self.oned_map_plot.imshow(np.log10(BDL.xmap+abs(np.min(BDL.xmap))), aspect='auto', cmap='cividis', extent=[min(BDL.xx),max(BDL.xx),max(BDL.Q),min(BDL.Q)])
        self.oned_map_plot.set_ylabel("q in nm$^{-1}$")
        self.oned_map_plot.set_xlabel("across stripe in \u03BCm")
        self.TT_ax1 = self.oned_map_plot.twinx()
        self.TT_ax1.set_ylim((max(TT),min(TT)))
        self.TT_ax1.set_ylabel("\u00B02\u03B8 at Cu k\u03B1", rotation=270, labelpad=15)

        self.oned_map_plot.set_title(f"{BDL.tTkey} at {int(BDL.xx[self.init_idx])} \u03BCm: {BDL.Tprof[self.init_idx]} \u00B0C")
        self.mapline = self.oned_map_plot.axvline(x=BDL.xx[self.init_idx], c='#5AAA95', linewidth=2, alpha=0.75)

        self.xtrs = TransitionFinder(data=BDL.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        for tdx,tr in enumerate(self.xtrs):
            self.oned_map_plot.axvline(BDL.xx[tr],ymin=0.5,ymax=1,c="#BDE411",linestyle='--')
        #     DD1155

        self.Tprof_plot = self.fig.add_subplot(gs[3,0])
        self.Tprof = self.Tprof_plot.plot(BDL.xx, BDL.Tprof, c='red')
        self.Tprof_plot.set_xlim((min(BDL.xx),max(BDL.xx)))
        self.Tprof_plot.set_xlabel("across stripe in \u03BCm")
        self.Tprof_plot.set_ylabel("Temp. in \u00B0C")
        self.Tline = self.Tprof_plot.axvline(x=BDL.xx[self.init_idx], c="#5AAA95", linewidth=2, alpha=0.75)


        self.DP_bases = PhaseRegions(techmap=BDL.xmap,trs=self.xtrs)
        self.roi_DPs = self.fig.add_subplot(gs[:2,1])
        self.TT_ax2 = self.roi_DPs.twiny()
        self.TT_ax2.set_xlim(min(TT),max(TT))
        self.TT_ax2.set_xlabel("\u00B02\u03B8 at Cu k\u03B1")

        for dpx,dp in enumerate(self.DP_bases[0]):
            self.roi_DPs.plot(BDL.Q,dp+0.5*dpx*(max(self.DP_bases[0][0])-min(self.DP_bases[0][0])))
        self.roi_DPs.set_xlim((min(BDL.Q),max(BDL.Q)))
        self.roi_DPs.set_yticks([])

        self.active_DP_plot = self.fig.add_subplot(gs[2,1])
        self.active_DP = self.active_DP_plot.plot(BDL.Q,BDL.xmap[:,self.init_idx],c="#5AAA95")[0]
        self.active_DP_plot.set_xlim((min(BDL.Q),max(BDL.Q)))
        self.active_DP_plot.set_yticks([])
        self.TT_ax3 = self.active_DP_plot.twiny()
        self.TT_ax3.set_xlim(min(TT),max(TT))

        self.StickPlots = self.fig.add_subplot(gs[3,1])
        self.StickPlots.set_yticks([])
        self.TT_ax4 = self.StickPlots.twiny()
        self.TT_ax4.set_xlim(min(TT),max(TT))


        self.fig.tight_layout()
        self.int_slider = ipy.IntSlider(
            value=self.init_idx,min=0,max=BDL.xmap.shape[1],
            continuous_update=False,
        )
        self.int_slider.readout = False
        self.style = {'description_width':'initial'}

        #loaded phase check boxes for comparison
        self.phase_cboxes = []
        for idx, phaseset in enumerate(list(BDL.PhaseSets)):
            self.phase_cboxes.append([])
            for jdx, phase in enumerate(list(BDL.PhaseSets[phaseset])):
                formula = BDL.PhaseSets[phaseset][phase]['Formula']
                sg = BDL.PhaseSets[phaseset][phase]['SpaceGroup']
                self.phase_cboxes[idx].append(ipy.Checkbox(
                    description=formula+'_'+sg,
                    value=False,

                ))

        # restructuring for multiple phase sets in a system
        self.phaseset_cboxes = []
        for pset in self.phase_cboxes:
            self.phaseset_cboxes.append(ipy.VBox([cbox for cbox in pset]))
        self.phase_cboxes = ipy.HBox([pset for pset in self.phaseset_cboxes])
        
        #button controling the stick pattern replot
        self.replot_sticks_button = ipy.Button(description='Stick DP replot')

        #button creating/or passing labels to phsae diagram file
        self.PhaseMapPassingButton = ipy.Button(description='Pass to Phase Map')
        
        #button to pass write in phases
        self.WriteInPhaseButton = ipy.Button(description='Add write in Phase')

        #morphology and unknown toggles
        self.texturetoggle = []
        self.defaultTexTyp = ['equiaxed','textured','amorphous','nanocrystalline','small grain','large grain'] 
        for texturetype in self.defaultTexTyp:
            self.texturetoggle.append(
                ipy.Checkbox(description=texturetype,value=False)
            )

        #last elemen in texture toggle is the write in function. 
        self.texturetoggle.append(
            ipy.Text(description='write in:',placeholder='unknown phase 1')
            )

        #layout of widgets
        self.texturetoggle = ipy.VBox(self.texturetoggle)
        

        
        # supplying the write in checkboxes with phases that one has already assigned (not in the CIFs)
        self.writeins = []
        self.assignedPLs = []
        for vbox in list(self.phaseset_cboxes):
            for cb in vbox.children:
                self.assignedPLs.append(cb.description)
                
#         print('this is what exits ',self.PMoutputpath)
        if (self.PMoutputpath != None) and (self.PMoutputpath != ''):
            self.PMdict = json.load(open(self.PMoutputpath,'r'))
            for key in list(self.PMdict):
                for pos in list(self.PMdict[key]['PosDepPLs']):
                    for phase in list(self.PMdict[key]['PosDepPLs'][pos]):
                        self.writeins.append(phase)
            self.assigned = np.unique(self.writeins).tolist()
            self.writeins = []
            for known in self.assigned:
                if all(known not in l for l in self.assignedPLs+self.defaultTexTyp):
                    self.writeins.append(ipy.Checkbox(description=known,value=False))
            # self.cor_wi = [obj.children for obj in self.writeins.children]
            self.writeins = ipy.VBox(self.writeins)
        else:
            self.writeins = ipy.VBox([])

        self.position_control = ipy.HBox([ipy.VBox([ipy.Label('stripe position'),self.int_slider]),self.texturetoggle,self.writeins,ipy.VBox([self.PhaseMapPassingButton,self.WriteInPhaseButton])])
        pc_layout = ipy.Layout(
            border = 'solid 3px gray',
            margin = '10px 10px 10px 10px',
            padding = '5px 5px 5px 5px',
            align_items = 'center'
        )
        self.position_control.layout = pc_layout
        self.phase_control = ipy.VBox([self.phase_cboxes,self.replot_sticks_button])
        self.phase_control.layout = pc_layout
        self.phase_control.layout.align_items = 'center'

        #observe the interactables. link to a function that interactable does
        self.int_slider.observe(self.VerticalLineAndDP_Updates,'value')
        self.replot_sticks_button.on_click(self.StickPatternUpdate)
#         self.PhaseMapPassingButton.on_click(self.WritePhasesToFile)
        self.WriteInPhaseButton.on_click(self.AddWriteInPhase)

        controls = ipy.VBox([self.position_control,self.phase_control])

        display(ipy.VBox([controls,output]))
        
    def StripeReInitialize(self):

        BDL = self.BDL
        BDL.xmap = np.array(BDL.xmap)

        self.init_idx = int(np.round(BDL.xmap.shape[1]/2))
        print('reloaded')
        print(BDL.xmap.shape)
        ###manipulables
        #vertical line on XRD map
        self.oned_map_plot.cla()
        self.oned_map = self.oned_map_plot.imshow(BDL.xmap,aspect='auto',
                            extent=[min(BDL.xx),max(BDL.xx),max(BDL.Q),min(BDL.Q)],
                            cmap='cividis')
    
        self.oned_map_plot.set_title(f"{BDL.tTkey} at {int(BDL.xx[self.init_idx])} \u03BCm: {BDL.Tprof[self.init_idx]} \u00B0C")
        self.mapline = self.oned_map_plot.axvline(x=BDL.xx[self.init_idx], c='#5AAA95', linewidth=2, alpha=0.75)
        
        self.oned_map_plot.set_xlabel('across stripe \u03BCm')
        self.oned_map_plot.set_ylabel('Q in nm$^{-1}$')
        
        self.Tprof[0].set_xdata(BDL.xx)
        self.Tprof[0].set_ydata(BDL.Tprof)
        self.Tline.set_xdata(BDL.xx[self.init_idx])

        self.active_DP.set_xdata(BDL.Q)
        self.active_DP.set_ydata(BDL.xmap[:,self.init_idx])
        self.active_DP_plot.set_xlim((min(BDL.Q),max(BDL.Q)))
        self.active_DP_plot.set_ylim(bottom=min(self.active_DP.get_ydata()),
                               top=max(self.active_DP.get_ydata()))
        
        self.roi_DPs.cla()
        self.roi_DPs.set_yticks([])
        self.xtrs = TransitionFinder(data=BDL.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        self.DP_bases = PhaseRegions(techmap=BDL.xmap,trs=self.xtrs)
        for dpx,dp in enumerate(self.DP_bases[0]):
            self.roi_DPs.plot(BDL.Q,dp+0.5*dpx*(max(self.DP_bases[0][0])-min(self.DP_bases[0][0])))

        self.roi_DPs.set_ylabel('Int. (a.u.)')
        self.roi_DPs.set_xlim((min(BDL.Q),max(BDL.Q)))

        for tdx,tr in enumerate(self.xtrs):
            self.oned_map_plot.axvline(BDL.xx[tr],ymin=0.5,ymax=1,c="#BDE411",linestyle='--')
            

        self.fig.suptitle(f"MnTiO3 - FeTiO3  Mn:{BDL.dataset[BDL.ckey][BDL.tTkey]['Mn mol frac']} Fe:{BDL.dataset[BDL.ckey][BDL.tTkey]['Fe mol frac']}")
        self.int_slider.max = BDL.xmap.shape[1]
        self.fig.canvas.draw_idle()    
        
    def StickPatternUpdate(self,change):
        BDL = self.BDL
        self.StickPlots.clear()
        self.current_phases = []
        newLabels, newHandles = [], []
        # print(self.phase_cboxes.children)
        for idx, ps_child in enumerate(self.phase_cboxes.children):
            self.current_phases.append([])
            for phase_child in ps_child.children:
                if phase_child.value:
                    self.current_phases[idx].append(phase_child.description)

        colorcounter = 0
        for idx, pset in enumerate(self.current_phases):
            for jdx, activephase in enumerate(pset):
                
                phaseID = activephase
                formula, sg = phaseID.split('_')
                phaseset = list(BDL.PhaseSets)[idx]
                candidate_phases = list(BDL.PhaseSets[phaseset])
                
                for phase in candidate_phases:
#                     print(phase)
#                     print(list(StripeData.PhaseSets[phaseset][phase]))
#                     print(formula,sg)

                    checkformula = BDL.PhaseSets[phaseset][phase]['Formula']
                    checksg = BDL.PhaseSets[phaseset][phase]['SpaceGroup']
                    if (formula == checkformula) & (sg == checksg):
                        Qs = BDL.PhaseSets[phaseset][phase]['Qs']
                        Is = BDL.PhaseSets[phaseset][phase]['Is']
                        for pdx, (q,i) in enumerate(zip(Qs,Is)):
                            self.StickPlots.plot([q,q],[i+1,1],linewidth=2,c=self.colors[colorcounter],label=phaseID)
                        self.StickPlots.set_xlim((min(BDL.Q),max(BDL.Q)))

                    handles, labels = self.StickPlots.get_legend_handles_labels()
                    newLabels, newHandles = [], []
                    for handle, label in zip(handles, labels):
                        if label not in newLabels:
                            newLabels.append(label)
                            newHandles.append(handle)
                    
                colorcounter += 1

        self.StickPlots.set_xlabel('Q in nm$^{-1}$')
        self.StickPlots.legend(newHandles,newLabels,loc='upper left',bbox_to_anchor=(1,1.5))
        self.StickPlots.set_xlim((min(BDL.Q),max(BDL.Q)))
        self.fig.canvas.draw_idle()
        
        
    
        

    def AddWriteInPhase(self,change):
        writeinphase = list(self.texturetoggle.children)[-1].value
        wip_cbox = ipy.Checkbox(description=writeinphase,value=False)
        self.position_control.children[-2].children = tuple(list(self.position_control.children[-2].children )+ [wip_cbox])

        # list(self.position_control.children)
    #Callback Functions
    def VerticalLineAndDP_Updates(self,change):
        """ Redraws the vertical line and the 2D DP corresponding to the current position on the XRD map"""
        # print(self.StripeData.key)
        BDL = self.BDL
        self.mapline.set_xdata(BDL.xx[change.new])
        self.Tline.set_xdata(BDL.xx[change.new])
        # print(StripeData.xx1[change.new])
        
        self.active_DP.set_ydata(BDL.xmap[:,change.new])
        self.active_DP_plot.set_ylim(bottom=min(self.active_DP.get_ydata()),
                               top=max(self.active_DP.get_ydata()))
#         self.oneD_text.set_text(f'1D XRD at {np.int(self.xx[change.new])} \u03BCm')
        self.fig.canvas.draw_idle()

#     def WritePhasesToFile(self,change):
#         # needs to read a file if it exists... write to a file if it exists... create a file if it does not exist.
#         # file structure should be readable by new class that plots the "labeled" phase map
#         fpath = self.DL.pth2exPM
#         key = self.StripeData.key
#         xpos = self.StripeData.xpos
#         ypos = self.StripeData.ypos
#         tau = self.StripeData.tau
#         Tmax = self.StripeData.Tmax
#         outputpath = self.PMoutputpath
#         # set the exiting file path from the init file if it exists.
#         if os.path.exists(fpath):
#             # print('your file exists')
#             with open(fpath,'r') as fp:
#                 PMdict = json.load(fp)

#                 # print(PMdict)
#         # otherwise create a dictionary and set the fpath to what it should be
#         else:
            
#             fpath = f'{outputpath}{self.DL.MatSysName}_PhaseMap.json'
#             self.DL.pth2exPM = fpath
#             self.DL.SaveInitFile(path2output=os.path.dirname(self.DL.InitFile)+'/')
#             PMdict = {}
#         PMdict[key] = {}
#         PMdict[key]['tau'] = tau
#         PMdict[key]['Tmax'] = Tmax
#         PMdict[key]['xpos'] = xpos
#         PMdict[key]['ypos'] = ypos
#         if self.CompSpread:
#             PMdict[key]['Cation1_frac'] = float(self.DL.XrdDict[key]['Cation1_frac'])
            
#         #need to pass sub stripe position, and phases/morphology. create a sub folder for it
#         PMdict[key]['PosDepPLs'] = {}
#         sub_xpos = self.xx[self.int_slider.value]
#         PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'] = []

#         #refreshes the current phases to what is currently toggled.
#         self.current_phases = []
#         for idx, ps_child in enumerate(self.phase_cboxes.children):
#             self.current_phases.append([])
#             for phase_child in ps_child.children:
#                 if phase_child.value:
#                     self.current_phases[idx].append(phase_child.description)

#         for phaseset in self.current_phases:
#             for active_phase in phaseset:
#                 PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(active_phase)
#         for child in self.texturetoggle.children:
#             if child.value:
#                 PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(child.description)
#         for child in self.writeins.children:
#             if child.value:
#                 PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(child.description)

#         #write out the file to a .json
#         with open(fpath,'w') as fp:
#             json.dump(PMdict,fp)

class BobsProcessingSpace(object):
    """
    This is an interactive processing space plot. will be helpful for manuvering through the data. 
    """
    def __init__(self, BDL, XRD_GUI):
        print("initialized")
        self.BDL = BDL
        self.XRD_GUI = XRD_GUI
#         print(list(self.BDL.dataset["Fe_1_Mn_0"]))
        self.procfig, self.procax = plt.subplots(1,1,constrained_layout=True,figsize=(5,5),dpi=150)
        self.axfe = self.procax.twinx()
        self.procax.set_zorder(self.axfe.get_zorder()+1)
        self.procax.set_xlabel('procesing time (\u03BCs)')
        self.procax.set_ylabel('Mn mol frac.')
        self.procax.set_title(f'Dwell Time vs Composition')
        self.procax.set_xscale('symlog')
        
        self.MnFe = [(0.0,1.0), (0.17,0.83), (0.5,0.5), (0.66,0.33), (0.83,0.17), (1.0,0.0)]
        self.Fe_frac = []
        self.Mn_frac = []
        self.taus = []
        
        self.ckeys = []
        self.tTkeys = []
        for idx, comp in enumerate(sorted(list(self.BDL.dataset))):
            self.Fe_frac.append(self.MnFe[idx][1])
            self.Mn_frac.append(self.MnFe[idx][0])
            self.ckeys.append(comp)
            
        print(self.ckeys)
        self.ckeys = [x for _, x in sorted(zip([1,2,3,4,0,5], self.ckeys))][::-1]
        print(self.ckeys)


        self.axfe.set_ylim((self.Fe_frac[0],self.Fe_frac[-1]))
        self.axfe.set_ylabel("Fe mol frac." ,rotation=270, labelpad=15)
        for idx, tau in enumerate(sorted(list(BDL.dataset[comp]))):
            self.tTkeys.append(tau)
            tau = float(tau.split('_')[1])
            self.taus.append(tau)
        
        self.all_tTkeys, self.all_ckeys = np.meshgrid(self.tTkeys, self.ckeys)
        self.all_tTkeys = self.all_tTkeys.ravel()
        self.all_ckeys = self.all_ckeys.ravel()
        
        print(self.tTkeys)
        print(self.ckeys)

        
        
        self.procfig.canvas.mpl_connect('pick_event', self.ProcSpacePicker)
        self.XRD_GUI.fig.canvas.mpl_connect('pick_event', self.ProcSpacePicker)

    def Plotter(self):
        tt, cc = np.meshgrid(self.taus, self.Mn_frac)
        self.all_taus = tt.ravel()
        self.all_comps = cc.ravel()
        self.procspace = self.procax.scatter(tt.ravel(), cc.ravel(), c='b', picker=True)
        
    def ProcSpacePicker(self,event):
        artist = event.artist
        print(artist)
        if artist in self.procspace.findobj():
            print("you found it")
            idx = event.ind[0]
            tTkey = self.all_tTkeys[idx]
            ckey = self.all_ckeys[idx]
            print(tTkey,ckey)
            self.XRD_GUI.BDL.ActiveStripeData(tauT_key=tTkey,comp_key=ckey)
            self.XRD_GUI.StripeReInitialize()