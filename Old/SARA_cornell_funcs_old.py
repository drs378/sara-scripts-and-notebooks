import numpy as np
import tables
import matplotlib.pyplot as plt
import h5py  as h5
import sys
import os
import glob
from matplotlib.backends.backend_pdf import PdfPages
import csv
import copy as cp

import pandas as pd
import matplotlib.patches as patches
from scipy import optimize, signal
from skimage.measure import block_reduce
import subprocess

from cookb_signalsmooth import *
from edge_finder import *

import PIL.Image as Image
import math as mt
import matplotlib.cm as cm
from matplotlib import rc
import matplotlib.font_manager as font_manager
from scipy.spatial import distance
from scipy.signal import savgol_filter
from scipy.signal import blackmanharris
from numpy.fft import rfft
from numpy import argmax, mean, diff, log
from scipy import ndimage

def AnnealStripeCenter(img,window =15,xray=False):
#This function will sample an image from bottom to top and find the symmetric point within it
#given a percentage of the image for the window.

#The returns are the index of the image where the symmetric point lies and the resulting images used for analysis



#Generates the starting index for the window based on the % input. The counter is to iterate until the window reaches the
# edge of the image if it is the optical data OR to the specified endpoint for the x-ray data
    if xray == False:
        lead_idx_start = int(np.ceil(window/100*img.shape[0])-1)
        counter = np.arange(0,(img.shape[0]-lead_idx_start),1)
    else:
        Brightest= int(np.where(img == np.max(img))[0])
        lead_idx_start = Brightest-30
        counter = np.arange(0,60,1)
#         print(lead_idx_start,min(counter),max(counter))

    Diff_img = []
    leftside_img = []
    rightside_img = []
    ImageWindow = []


    if xray == False:
        for i in counter:
            lead_idx = lead_idx_start+i
#             print(lead_idx)
            if (lead_idx_start) % 2 == 0:

                sub_img = img[i:(lead_idx),:]
                leftside = sub_img[0:int(lead_idx_start*0.5)-1,:] 
                rightside = sub_img[(int(lead_idx_start*0.5)+1):,:]
    #             print('it went even')
            else:
                sub_img = img[i:lead_idx,:]
                leftside = sub_img[0:int(np.ceil(lead_idx_start*0.5))-1,:]
                rightside = sub_img[int(np.ceil(lead_idx_start*0.5)):,:]       
    #             print('it went odd')
    

            if i+lead_idx<img.shape[0]:
                Diff_img.append((rightside-np.flip(leftside,axis=0))) 
                leftside_img.append(np.flip(leftside,axis=0))
                rightside_img.append(rightside)
                ImageWindow.append(sub_img)
                
                
    else:
        for i in counter:
            lead_idx = lead_idx_start+i
            sub_img_start = lead_idx-40
            sub_img_end = lead_idx+40

            sub_img = img[sub_img_start:sub_img_end,:]
            leftside = sub_img[0:39,:] 
            rightside = sub_img[41:,:]
#             print(sub_img_start,sub_img_end,len(sub_img))
#             print(lead_idx,img.shape[0])
            if sub_img_end<img.shape[0]:
                Diff_img.append((rightside-np.flip(leftside,axis=0))) 
                leftside_img.append(np.flip(leftside,axis=0))
                rightside_img.append(rightside)
                ImageWindow.append(sub_img)
            

    
    lin_Diff_img = []
    Dif_min = []

    for i in np.arange(len(Diff_img)):
        lin_Diff_img.append(Diff_img[i].sum(axis=0))
        Dif_min.append(np.max(abs(lin_Diff_img[i][:-100])))
    
#     plt.figure()
#     plt.plot(Dif_min)
    minval = np.min(Dif_min)
    
    
    
    Diff_img = np.array(Diff_img)
    leftside_img = np.array(leftside_img)
    rightside_img = np.array(rightside_img)
    ImageWindow = np.array(ImageWindow)
    
    if xray == True:
        center_idx = int(Dif_min.index(minval)+lead_idx_start)
        
    else:
                        
        center_idx = int(Dif_min.index(minval)+np.ceil(lead_idx_start*0.5))
    return center_idx,leftside_img,rightside_img,Diff_img,ImageWindow



#Helper functions
def fft_smoothing(d,param):
#d is a 1D vector, param is the cutoff frequency
    rft = np.fft.rfft(d)
    rft[int(param):] = 0.   
    d = np.fft.irfft(rft,len(d))
    return d

#Find LSA center
def get_center(data,plotting=True):
#Using the cosine method
#     maximum = 1.
#     for i in range(int(round((data.shape[1])*0.35)),int(round((data.shape[1])*0.65))):
#         delta = min(i,data.shape[1]-i)
#         dl = data[:,i-delta:i]
#         dr = data[:,i:i+delta]
#         norm = 0.
#         for j in range(dl.shape[1]):
#             norm += distance.cosine(dl[:,-j] , dr[:,j])
#         norm = norm/dl.shape[1]
#         if norm < maximum:
#             maximum = norm
#             imaximum = i
#Using the correlation method
    im1 = np.array(data[:,:])
    im = cp.deepcopy(im1)
    center = []
    weights = []
    weights_max = []
    corr = []
    for i in range(im1.shape[0]):
        row = im1[i,:]
        result = np.array(row)-np.mean(np.array(row))
        result = ndimage.correlate(fft_smoothing(result,15),np.flip(fft_smoothing(result,15),0), mode='wrap')
        result = fft_smoothing(np.array(result),15.)
        im[i,:] = result
        center.append(np.argmax(result))
        corr.append(result)
        weights.append(sum(np.abs(im1[i,:])))
        weights_max.append(np.amax(result))
    corr = np.array(corr)
    smooth = []
    for i in range(corr.shape[1]):
        smooth.append(np.mean(corr[:,i]))
#         print(np.mean(corr[:,i]))
#         for j in range(corr.shape[0]):
#             print(corr[j,i], end=" ")
#         print(" ")
    smd = fft_smoothing(np.array(smooth),15.)
#     fig, ax = plt.subplots()
#     for i in corr:
#         ax.plot(i)
#     plt.show()
    #for i in range(corr.shape[1]-1):
    #    print(smooth[i], smd[i])
    center = np.array(center)
    weights_max = np.power(np.array(weights_max),4)
    #imaximum_conv = (len(result)-1.)*0.5 + (np.average(center,weights=weights)-(len(result)-1.)*0.5)*0.5
    #imaximum_conv = (len(result)-1.)*0.5 + (np.average(center)-(len(result)-1.)*0.5)*0.5
    imaximum_conv = (len(result))*0.5 + (np.average(center,weights=weights_max)-(len(result))*0.5)*0.5
    if plotting==True:
        plt.figure()
        plt.imshow(corr,aspect='auto')
        plt.show()

        plt.figure()
        plt.imshow(smd,aspect='auto')
        plt.show()
        


    return int(np.round(imaximum_conv)), imaximum_conv #imaximum excluded because it was throwing error

#function to round to nearest known composition
def myround(x, base=5):
    return int(base * round(float(x)/base))

def Qcorr_LaMnOx(Q,xpos,ypos):
    #Equation of plane
    # z = (-ax-by+d)/c
    Scalar_params = np.array([9.306469032556051e-05, 5.5205308570035094e-05, 1.0, -1.0417393949965705])
    Shift_params = np.array([0.0012388597559986768, -0.0020479440264392235, 1.0, -1.0547679259819134])
    scale = (-Scalar_params[0]*xpos-Scalar_params[1]*ypos-Scalar_params[3])
    shift = (-Shift_params[0]*xpos-Shift_params[1]*ypos-Shift_params[3])
    Qcorrected =aected

#Maxamillian's Code-a-doodle-dandy
#Get data from file, filter it, and normalize it, original data is still
#eturned in data
def get_spects(fn_data, fn_mirror, fn_blank):
    s_param = 15
    newversion = False
    #Parse header and metadata to see if it is the new or old file format
    meta = {}
    with gzopen(fn_data) as f:
        cnt = 0
        for line in f:
            if line.startswith('/'):
                if "correction" in line:
                    newversion = True
            cnt += 1
    f.close()
    if newversion:
#         print ("Reading new file format")
        with gzopen(fn_data) as f:
            cnt = 0
            for line in f:
                if line.startswith('/'):
                    if "Center" in line:
                        meta['scan center'] = [float(i) for i in line.strip().split()[-1].replace("(", "").replace(")", "").split(',')]
                    if "Range" in line:
                        l = line.strip().split()[-2:]
                        l = [elem.replace("(","").replace(")","") for elem in l]
                        meta['delta start'] = [float(i) for i in l[0].split(',')]
                        meta['delta end']   = [float(i) for i in l[1].split(',')]
                    if "Scan lines" in line:
                        meta['points']      = int(line.strip().split()[-1])
                cnt += 1
        f.close()
        data = np.genfromtxt(fn_data, dtype=float, delimiter=',', skip_header=17)
        if "avg" in fn_mirror:
            mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',', skip_header=1) #Mike calls it ref
        else:
            mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',', skip_header=13) #Mike calls it ref
        if "avg" in fn_blank:
            blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',', skip_header=1)
        else:
            blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',', skip_header=13)

    #Parse information from the filename
        fn_meta = fn_data.split("_")
    #The last part is the temperature in C
        meta["temp"] = float(fn_meta[-1].split(".")[0])
    #The second last part is the temperature in dwell time in microsec
        meta["dwell"] = float(fn_meta[-2])
    #We can check the coordinates with the ones already read from the comment block
        dy = float(fn_meta[-3])
        dx = float(re.sub('[^0-9,+,-]','',fn_meta[-4].split("/")[-1]))
        if np.linalg.norm(np.array([dx,dy])-np.array(meta['scan center'])) > 0.0001:
            print ("The coordinate from the metadata and the filename disagree")
            sys.exit(1)
    #eThe new data is already normalized... maybe?
    #    for i in range(data.shape[1]-1):
    #        data[:,i+1] = fft_smoothing(data[:,i+1],s_param)
    #    normal_data = data
    else:
#Old format of files for reading
#         print ("Reading old file format")
        #Parse header and metadata
        meta = {}
        with gzopen(fn_data) as f:
            first_line = f.readline().strip().split(' ')
            first_line = [elem.replace("(","").replace(")","") for elem in first_line]
            meta['scan center'] = [float(i) for i in first_line[3].split(',')]
            meta['delta start'] = [float(i) for i in first_line[7].split(',')]
            meta['delta end']   = [float(i) for i in first_line[11].split(',')]
            meta['points']      = int(first_line[14])
        #print("reading ", fn_data)
        data = np.genfromtxt(fn_data, dtype=float, delimiter=',', skip_header=1)
        mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',', skip_header=8) #Mike calls it ref
        blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',', skip_header=8)

    #Getting rid of a dead pixel
    mirror[1388,1] = (mirror[1387,1]+mirror[1389,1])*0.5
    blank[1388,1]  = (blank[1387,1]+blank[1389,1])*0.5
    data[1388,1:]  = (data[1387,1:]+data[1389,1:])*0.5

    #Smooth data
    mirror[:,1] = fft_smoothing(mirror[:,1],s_param)
    blank[:,1] = fft_smoothing(blank[:,1],s_param)
    for i in range(data.shape[1]-1):
        data[:,i+1] = fft_smoothing(data[:,i+1],s_param)

    #Normalization
    norm = np.maximum(1.,mirror[:,1]-blank[:,1])
    normal_data = cp.deepcopy(data)
    normal_data[:,1:] = (np.maximum(data[:,1:],1.)-blank[:,1].reshape(-1,1))/norm.reshape(-1,1)

    #Wavelengths
    wl = normal_data[:,0]

    #The resolution is always 10 microns by 10 microns
    meta['scan res']    = [10., 10.]


    return wl, data[:,1:], normal_data[:,1:], meta


def get_spects_LaMnOx(fn_data, fn_mirror, fn_blank):
    mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',',skip_header=8) #Mike calls it ref
    blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',',skip_header=8)
#     print("reading ", fn_data)
    data = np.genfromtxt(fn_data, dtype=float, delimiter=',',skip_header=1)

    #Parse header and metadata
    meta = {}
    with open(fn_data) as f:
        first_line = f.readline().strip().split(' ')
        first_line = [elem.replace("(","").replace(")","") for elem in first_line]
        meta['scan center'] = [float(i) for i in first_line[3].split(',')]
        meta['delta start'] = [float(i) for i in first_line[7].split(',')]
        meta['delta start'] = [float(i) for i in first_line[7].split(',')]
        meta['delta end']   = [float(i) for i in first_line[11].split(',')]
        meta['points']      = int(first_line[14])

    #The resolution is always 10 microns by 10 microns
    meta['scan res']    = [10., 10.]

    #Getting rid of a dead pixel
    mirror[1388,1] = (mirror[1387,1]+mirror[1389,1])*0.5
    blank[1388,1]  = (blank[1387,1]+blank[1389,1])*0.5
    data[1388,1:]  = (data[1387,1:]+data[1389,1:])*0.5

    #Smooth data
    s_param = 10
    mirror[:,1] = fft_smoothing(mirror[:,1],s_param)
    blank[:,1] = fft_smoothing(blank[:,1],s_param)
    for i in range(data.shape[1]-1):
        data[:,i+1] = fft_smoothing(data[:,i+1],s_param)

    #Normalization
    norm = np.maximum(1.,mirror[:,1]-blank[:,1])
    normal_data = cp.deepcopy(data)
    normal_data[:,1:] = (data[:,1:]-blank[:,1].reshape(-1,1))/norm.reshape(-1,1)

    #Wavelengths
    wl = normal_data[:,0]

    return wl, data[:,1:], normal_data[:,1:], meta

import sympy
def gaussian(x,background,height,position,FWHM) :
    std = FWHM/2*np.sqrt(2*np.log(2))

    return background+height*np.exp(-1*(x-position)**2/(2*std**2))

#This code is for the CO2 laser on Silicon. 

FWHM = 660 # in um defined by Bob Bells thesis results for the power distribution

# A laser power is determined based on the desired peak temperature and dwell from the following polynomial relationships
# We need this beacause the power of the laser is not readily accessible in the meta data of the experiment.

def LaserPower(tau, Temp):
    #this is what the peak temperature depends on
    root_tau = np.sqrt(tau)

    
    #these coefficients are tabulated and don't need to be changed    
    a0= np.array([-10.60597,      2.627598,      -0.04569063,     0.0002334397])
    a1= np.array([15.00736,     -0.4516194,      0.008374273,   -4.164741E-005])
    a2= np.array([-0.6096512,     0.05861906,    -0.0005016423,   8.149019E-007])
    a3= np.array([0.01521283,   -0.001040471,   -5.582083E-006,  1.073043E-007])
    a4= np.array([-0.0001454438,  8.095709E-006,  2.572534E-007, -2.423004E-009])
    
    a_coeffs = np.vstack((a0,a1,a2,a3,a4))
    
    b = [np.polyval(i,root_tau) for i in np.fliplr(a_coeffs)]

    power = sympy.symbols('power',real=True)
    peakpower = np.array(sympy.solve(b[0] + b[1]*power + b[2]*power**2 + b[3]*power**3 + b[4]*power**4-Temp,power),dtype=float)
    peakpower = peakpower[np.where(peakpower<120)]
    peakpower = peakpower[np.where(peakpower>0)]
    
    return peakpower #this is 4 roots, the second one (index 1) is the correct wattage

def LaserFWHM(T, tau):
    
    """This Function outputs a FWHM of the laser at a given peak temperature and dwell
    
    T must be in degrees Celcius and dwell must be in milliseconds
    output is a FWHM in (in \u03bcm)

    """
    
    kern = ((T-665.3)/128.04)
    FWHM = 559.5 - 77.93*scipy.special.erfc(kern) + 130.87*np.log10(tau)
    
    return FWHM

#This function takes the tau and power and returns the corresponding Temperature
def LSA_Temperature(tau,power):
    root_tau = np.sqrt(tau)
    a0= np.array([-10.60597,      2.627598,      -0.04569063,     0.0002334397])
    a1= np.array([15.00736,     -0.4516194,      0.008374273,   -4.164741*10**-5])
    a2= np.array([-0.6096512,     0.05861906,    -0.0005016423,   8.149019E-007])
    a3= np.array([0.01521283,   -0.001040471,   -5.582083E-006,  1.073043E-007])
    a4= np.array([-0.0001454438,  8.095709E-006,  2.572534E-007, -2.423004E-009])
    
    a_coeffs = np.vstack((a0,a1,a2,a3,a4))
    
    
    b = [np.polyval(i,root_tau) for i in np.fliplr(a_coeffs)]
    
    Temperature = b[0] + b[1]*power + b[2]*power**2 + b[3]*power**3 + b[4]*power**4
    
    return Temperature #this is one temperature


#example of how I have implemented the code
#given the supposed peak temperature for the laser anneal scan, I calculate the peak power. The power distribution
#is gaussian with a DEFINED FWHM of 660 um for the CO2 laser on silicon. The spatially resolved power can easily
#be determined then using T(power,dwell), one can calculate tbe temperature profile across the stripe. 

#Temperature profile
#     PeakPower = LaserPower(tau=Dwell,Temp=Temp)[1]
#     Power_Profile = gaussian(annealspace_Vector,background = 0, height =PeakPower,
#                               center=peaktempcenter,FWHM = 660)
#     Temperature_Profile = np.array([LSA_Temperature(tau=Dwell,power=j) for j in Power_Profile])

#Note: any and all spatial components need to be in microns

#Bug to be addressed: The FWHM of the laser will have a correction term to it for the purpose of getting all profiles consistent
#across the experimental conditions probed


def dwellsort(listofkeys,Temp = True):
    demkeys = listofkeys
    temp = []
    s=[]
    for i in demkeys:

        i = i.split('_')
        i[1] = i[1].zfill(5)
        i[3] = i[3].zfill(5)
        s.append(i)
    s = np.array(s)
#    for i in s[:,1]:
#        i = i.zfill(5)
#        temp.append(i)

#    s=np.insert(s,1,temp,axis=1)
#    temp = []
#    s = np.array(s)
#    s = np.delete(s,2,1)
    if Temp:
        dwellsorted = sorted(s,key=lambda x:(x[3],x[1]))
    else:
        dwellsorted = sorted(s,key=lambda x:(x[1],x[3]))
#    dwellsorted = sorted(dwellsorted,key=lambda x:x[1])


    for i in dwellsorted:
        i[1] = i[1].lstrip('0')
        i[3] = i[3].lstrip('0')
        i = np.hstack((i))
        i = i.tolist()
        b = '_'
        i = b.join(i)
        temp.append(i)

    demkeys = temp
    
    return demkeys

def DamageControl(data,lheight=0.01,lprom=0.01,FiltRange=(300,1200),smooth_param = 5,plotting=True):
    diff = []
    for opt in np.arange(data.shape[1]):
        diff.append(abs(max(data[FiltRange[0]:FiltRange[1],opt])-min(data[FiltRange[0]:FiltRange[1],opt])))
    diff = smooth(np.array(diff),5)
    der = abs(np.gradient(diff))
    
    
    
    peaks = signal.find_peaks(der,height=lheight,prominence=lprom)
    peak_arr=(peaks[1]['peak_heights'],peaks[0])
    peak_arr = np.vstack((peak_arr[0],peak_arr[1]))
    peak_arr = np.transpose(peak_arr)
    peak_arr = np.transpose(sorted(peak_arr,key=lambda x:x[0],reverse=True))
   
    if len(peak_arr[1])>0:
        RegionMarkers=[int(i) for i in peak_arr[1]][0:2]

        ROI = data[:,min(RegionMarkers):(max(RegionMarkers)+1)]    
        SmoothedROI = np.array([smooth(ROI[i,:],int(np.round(ROI.shape[1]/smooth_param,decimals=0))) for i in np.arange(len(ROI))])
        error = np.zeros(SmoothedROI.shape,dtype=float)
        Smoothed_Refl = np.array(data)
        Smoothed_Refl[:,min(RegionMarkers):(max(RegionMarkers)+1)] = SmoothedROI

        for x in np.arange(error.shape[0]):
            error[x] = np.array(SmoothedROI-ROI)[x]
        Apo_error = np.array(error[:,1:])
        x = np.abs(np.sum(Apo_error**8,axis=0))
        DamageMarkers = signal.find_peaks(x=x,prominence=(max(x)-min(x))/20,height=(max(x)-min(x))/20)[0]+1
    else:
        RegionMarkers = 'DNE'
        ROI = 'DNE'
        SmoothedROI = 'DNE'
        error = 'DNE'
        DamageMarkers = 'DNE'
        Apo_error = 'DNE'
        Smoothed_Refl = data


    #Plotting
    if plotting == True:
        fig2 = plt.figure()
        plt.plot(diff,'b')
        plt.plot(smooth(np.array(der),3),'r')
        plt.title('reflectance min vector')
        
        
        fig1, (ax1,ax2) = plt.subplots(1,2,constrained_layout=True)
        ax1.imshow(data,aspect='auto')
        for opt in [25,40,45,50,55,60,75]:
            ax2.plot(test[:,opt])
            
        for j in RegionMarkers:
            ax1.plot(j*np.ones(data.shape[0]),np.arange(data.shape[0]),'r')
#         fig1.suptitle('Anneal conditions ')
        
        fig3 = plt.figure()
        plt.imshow(ROI,aspect='auto')
    
    
    return RegionMarkers, ROI, SmoothedROI, error, DamageMarkers, Apo_error, Smoothed_Refl

def make_contact_sheet(montage_imgs,rowscols,photo_dimensions,margins,padding):
    """\
    Make a contact sheet from a group of filenames:

    fnames       A list of names of the image files
    
    ncols        Number of columns in the contact sheet
    nrows        Number of rows in the contact sheet
    photow       The width of the photo thumbs in pixels
    photoh       The height of the photo thumbs in pixels

    marl         The left margin in pixels
    mart         The top margin in pixels
    marr         The right margin in pixels
    marl         The left margin in pixels

    padding      The padding between images in pixels

    returns a PIL image object.
    """
    fnames = montage_imgs
    ncols,nrows = [i for i in rowscols]
    photow,photoh = [i for i in photo_dimensions]
    marl,mart,marr,marb = [i for i in margins]
    # Read in all images and resize appropriately
    imgs = [Image.open(fn).resize((photow,photoh)) for fn in fnames]

    # Calculate the size of the output image, based on the
    #  photo thumb sizes, margins, and padding
    marw = marl+marr
    marh = mart+ marb

    padw = (ncols-1)*padding
    padh = (nrows-1)*padding
    isize = (ncols*photow+marw+padw,nrows*photoh+marh+padh)

    # Create the new image. The background doesn't have to be white
    white = (255,255,255)
    inew = Image.new('RGB',isize,white)

    # Insert each thumb:
    for irow in range(nrows):
        for icol in range(ncols):
            left = marl + icol*(photow+padding)
            right = left + photow
            upper = mart + irow*(photoh+padding)
            lower = upper + photoh
            bbox = (left,upper,right,lower)
            try:
                img = imgs.pop(0)
            except:
                break
            inew.paste(img,bbox)
    return inew


def CropA(w, h, angle):
    import math
    """
    Given a rectangle of size wxh that has been rotated by 'angle' (in
    radians), computes the width and height of the largest possible
    axis-aligned rectangle (maximal area) within the rotated rectangle.
    """
    if w <= 0 or h <= 0:
        return 0,0

    width_is_longer = w >= h
    side_long, side_short = (w,h) if width_is_longer else (h,w)

    # since the solutions for angle, -angle and 180-angle are all the same,
    # if suffices to look at the first quadrant and the absolute values of sin,cos:
    sin_a, cos_a = abs(math.sin(angle)), abs(math.cos(angle))
    if side_short <= 2.*sin_a*cos_a*side_long or abs(sin_a-cos_a) < 1e-10:
        # half constrained case: two crop corners touch the longer side,
        #   the other two corners are on the mid-line parallel to the longer line
        x = 0.5*side_short
        wr,hr = (x/sin_a,x/cos_a) if width_is_longer else (x/cos_a,x/sin_a)
    else:
        # fully constrained case: crop touches all 4 sides
        cos_2a = cos_a*cos_a - sin_a*sin_a
        wr,hr = (w*cos_a - h*sin_a)/cos_2a, (h*cos_a - w*sin_a)/cos_2a

    return wr,hr

def MissingConditions(keys):
    demkeys = dwellsort(keys)
    grid = []
    blanks = []
    for key in demkeys:
        split = key.split('_')
        tau = split[1]
        T = split[3]
        grid.append([tau,T])
    conds = np.array(grid,dtype=float)
    uniqueTs = np.unique(conds[:,1])
    uniqueDs = np.unique(conds[:,0])
    for T in uniqueTs:
        empty_dwells = np.array(list(set(conds[conds[:,1]==T][:,0]).symmetric_difference(set(uniqueDs))))
        blank_keys=['tau_'+str(int(i))+'_T_'+str(int(T)) for i in empty_dwells]
        for key in blank_keys:
            blanks.append(key)
    return blanks

def OpticalImageTransitions(filepath,blur=5,contrast_threshold=0.075,plotting=True):
    TransitionStats = {}
    outputfile = []
    
    file = glob.glob(filepath)[0]
    filename = os.path.basename(file)[:-4]

    cond = '_'
    s = filename.split('_')[2:]
    s[0] = str(int(s[0]))
    s[1] = str(int(s[1]))
    s.insert(0,'tau')
    s.insert(2,'T')
    cond = cond.join(s) 

    if 'align' not in filename:
        print(cond)
        
        img = Image.open(file)
        
        xpix = np.array(img.getchannel(0)).shape[0]
        ypix = np.array(img.getchannel(0)).shape[1]
        
        fp = blur/100
        
        #calling on a partiular channel
        redline = smooth(np.sum(ndimage.gaussian_filter(img.getchannel(0),(6,6)),axis=0),window_len=np.round(fp*img.size[0]).astype(int))
        greenline = smooth(np.sum(ndimage.gaussian_filter(img.getchannel(1),(6,6)),axis=0),window_len=np.round(fp*img.size[0]).astype(int))
        blueline = smooth(np.sum(ndimage.gaussian_filter(img.getchannel(2),(6,6)),axis=0),window_len=np.round(fp*img.size[0]).astype(int))
        
        meanr = np.mean(redline)
        meang = np.mean(greenline)
        meanb = np.mean(blueline)
  
        stdr = np.std(redline)
        stdg = np.std(greenline)
        stdb = np.std(blueline)
        
        rcontrast = stdr/meanr
        gcontrast = stdg/meang
        bcontrast = stdb/meanb
        
        r_grad = np.gradient(redline)
        r_grad = np.sqrt(r_grad*r_grad)
        g_grad = np.gradient(greenline)
        g_grad = np.sqrt(g_grad*g_grad)
        b_grad = np.gradient(blueline)
        b_grad = np.sqrt(b_grad*b_grad)
        
        
        #where I am filtering the signals
        pfp = 15*0.01
        LargestG = np.max([r_grad,g_grad,b_grad])
        promfilt = LargestG*pfp 

        #peak finding in the gradient I used an all encompasing sest so I could get the fit values to output
        #same with the height
        r_trans = signal.find_peaks(r_grad,prominence=promfilt,width=[0.005*xpix,0.15*xpix],height=promfilt*0.5)
        g_trans = signal.find_peaks(g_grad,prominence=promfilt,width=[0.005*xpix,0.15*xpix],height=promfilt*0.5)
        b_trans = signal.find_peaks(b_grad,prominence=promfilt,width=[0.005*xpix,0.15*xpix],height=promfilt*0.5)
        transes = [r_trans,g_trans,b_trans]
        TransitionStats[cond] = {}
        c=['r','g','b']
        
        for idx, chan_contrast in enumerate([rcontrast,gcontrast,bcontrast]):
            if chan_contrast>contrast_threshold:
                TransitionStats[cond][c[idx]+'_trans'] = transes[idx]
            else:
                TransitionStats[cond][c[idx]+'_trans'] = 'none'
        
        yayornay = [TransitionStats[cond]['r_trans'],TransitionStats[cond]['g_trans'],TransitionStats[cond]['b_trans']]

        r_con = ndimage.correlate(redline,redline[::-1],mode='wrap')
        g_con = ndimage.correlate(greenline,greenline[::-1],mode='wrap')
        b_con = ndimage.correlate(blueline,blueline[::-1],mode='wrap')

        r_center = (len(r_con))*0.5 + (np.argmax(r_con)-(len(r_con))*0.5)*0.5
        g_center = (len(g_con))*0.5 + (np.argmax(g_con)-(len(g_con))*0.5)*0.5
        b_center = (len(b_con))*0.5 + (np.argmax(b_con)-(len(b_con))*0.5)*0.5
        

        contrastsum = (rcontrast+gcontrast+bcontrast)
        contra_weights = np.array([rcontrast/contrastsum,gcontrast/contrastsum,bcontrast/contrastsum])
        
        
        TransitionStats[cond]['r_center'] = r_center
        TransitionStats[cond]['g_center'] = g_center
        TransitionStats[cond]['b_center'] = b_center
        TransitionStats[cond]['avgcenter'] = (r_center+g_center+b_center)/3
        TransitionStats[cond]['r_mean'] = meanr
        TransitionStats[cond]['g_mean'] = meang
        TransitionStats[cond]['b_mean'] = meanb
        TransitionStats[cond]['r_std'] = stdr
        TransitionStats[cond]['g_std'] = stdg
        TransitionStats[cond]['b_std'] = stdb
        TransitionStats[cond]['rcontrast'] = rcontrast
        TransitionStats[cond]['gcontrast'] = gcontrast
        TransitionStats[cond]['bcontrast'] = bcontrast
        TransitionStats[cond]['r_S/N'] = meanr/stdr
        TransitionStats[cond]['g_S/N'] = meang/stdg        
        TransitionStats[cond]['b_S/N'] = meanb/stdb
        
        center = np.average(a=np.array([r_center,g_center,b_center]),weights=contra_weights) 
        
        if all(chan=='none' for chan in yayornay[idx])==False:

            AllTR_pos = []
            AllTR_prom = []
            AllTR_channel = []
            AllTR_SN = []
            AllTR_widths = []
            AllTR_peak_heights = []
            
            for idx in [0,1,2]:
                if yayornay[idx]!='none':
                    for tr in TransitionStats[cond][c[idx]+'_trans'][1]['widths']:
                        AllTR_widths.append(tr)
                    for tr in TransitionStats[cond][c[idx]+'_trans'][1]['peak_heights']:
                        AllTR_peak_heights.append(tr)
                        AllTR_SN.append(tr/TransitionStats[cond][c[idx]+'_std'])
                    for tr in  TransitionStats[cond][c[idx]+'_trans'][1]['prominences']:
                        AllTR_prom.append(tr)
                    for tr in TransitionStats[cond][c[idx]+'_trans'][0]:
                        AllTR_pos.append(tr)
                        AllTR_channel.append(c[idx])
            

            print('info order is:')
            print('transition in image index[0],prominence[1],peak height[2],peak width[3],signal to noise ratio[4],image center index[5],channel it came from[6],anneal condition[7]')
            AllTR = np.vstack((np.array(AllTR_pos),np.array(AllTR_prom),np.array(AllTR_peak_heights),
                               np.array(AllTR_widths),np.array(AllTR_SN),
                               np.array([center for i in np.arange(len(AllTR_prom))]),
                               np.array(AllTR_channel),np.array([cond for i in np.arange(len(AllTR_prom))])
                               )).T
    
            AllTR_sorted  = sorted(np.array(AllTR[:,:2],dtype=float),key=lambda x: x[1],reverse=True)

        else:
            print('no transitions found')
        
        outputfile.append(AllTR)
        initialized = outputfile[0]
        for i in np.arange(len(outputfile)-1):
            initialized = np.vstack((initialized,outputfile[i+1]))
        
        
        
        
        
## Plotting        
        
        
        if plotting==True:              
            
            plt.figure()
            plt.title('Anneal conditions '+cond)
            plt.plot(r_grad,'r')
            plt.plot(g_grad,'g')
            plt.plot(b_grad,'b')
            
            for idx in [0,1,2]:
                if yayornay[idx]!='none':
                    for tr in TransitionStats[cond][c[idx]+'_trans'][0]:
                        print(tr)
                        plt.axvline(x=tr, ymax=LargestG,ymin=0,c=c[idx],linestyle='-.',alpha=0.5)
                
            plt.axvline(x=(r_center+g_center+b_center)/3,ymax=max(r_grad),ymin=0,c='orchid')
            plt.axvline(x=center,ymax=np.max((r_grad,g_grad,b_grad)),ymin=0,linestyle='-.',c='green')
            plt.show()
            
            fig = plt.figure(figsize=(6,6))

            gs = gridspec.GridSpec(2,3,height_ratios=[1,2],figure=fig)
            ax1 = plt.subplot(gs[0,0])
            ax2 = plt.subplot(gs[0,1])
            ax3 = plt.subplot(gs[0,2])
            ax4 = plt.subplot(gs[1,:])

            ax4.imshow(img)
            ax4.set_title('RGB Image '+cond)
            ax4.axis('off')
            ax4.set_xticklabels([])

            for idx, tr in enumerate(np.array(AllTR_sorted)):
                if (tr[1])/max(np.array(AllTR_sorted)[:,1])>.1:
                    plt.plot(float(tr[0])*np.ones(np.array(img).shape[0]),np.arange(np.array(img).shape[0]),linestyle='-.',c='gold')
            ax4.plot(center*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),linestyle=':',c='w')
            
        
            ax1.imshow(np.array(img.getchannel(0)),cmap="Reds")
            ax1.plot(r_center*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),c='orchid')
            ax1.set_title('Red channel')
            ax1.axis('off')
            ax1.set_xticklabels([])
            
            
            ax2.imshow(np.array(img.getchannel(1)),cmap='Greens')
            ax2.set_title('Green channel')
            ax2.axis('off')
            ax2.set_xticklabels([])
            ax2.plot(g_center*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),c='orchid')

            
            ax3.imshow(np.array(img.getchannel(2)),cmap='Blues')
            ax3.set_title('Blue channel')
            ax3.axis('off')
            ax3.set_xticklabels([])
            ax3.plot(b_center*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),c='orchid')            
            
            
            
            if TransitionStats[cond]['r_trans']!='none':
                for tr in TransitionStats[cond]['r_trans'][0]:
                    ax1.plot(tr*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),'gold')                
            

            if TransitionStats[cond]['g_trans']!='none':
                for tr in TransitionStats[cond]['g_trans'][0]:
                    ax2.plot(tr*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),'gold')

                    
            if TransitionStats[cond]['b_trans']!='none':
                for tr in TransitionStats[cond]['b_trans'][0]:
                    ax3.plot(tr*np.ones(int(img.size[1]*0.5)),np.arange(int(img.size[1]*0.5)),'gold')
            
            
            plt.show()
        
    
        
    return outputfile
