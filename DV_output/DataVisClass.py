# %matplotlib widget
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import os
import sys
import glob
from SARA_cornell_funcs import *
import pandas as pd
import imageio
from pymatgen.io import cif as cif
from pymatgen.analysis.diffraction import xrd as pmg
from IPython.display import display, clear_output
import ipywidgets as ipy
from itertools import compress
import json
import seaborn as sns

class DataLoader():
    def __init__(self, InitFile=None):
        self.InitFile = InitFile
        if self.InitFile:
            with open(self.InitFile,'r') as infile:
                self.LIIF = json.load(infile)
            self.MatSysName = self.LIIF['DataPaths']['MatSysName']
            self.ptim =  self.LIIF['DataPaths']['Images Data']
            self.ptim_m = self.LIIF['DataPaths']['Image mirror']
            self.O_pxs = self.LIIF['DataPaths']['Optical Pxs']
            self.S_pxs = self.LIIF['DataPaths']['Spectroscopy Pxs']
            self.X_pxs = self.LIIF['DataPaths']['XRD Pxs']
                    
            self.ptsp_raw = self.LIIF['DataPaths']['Spec raw']
            self.ptsp_b = self.LIIF['DataPaths']['Spec blank']
            self.ptsp_m = self.LIIF['DataPaths']['Spec mirror']

            self.pth5 = self.LIIF['DataPaths']['H5 path']
            self.pth2csvdb = self.LIIF['DataPaths']['CHESS CSVDB']
            self.ptcomp = self.LIIF['DataPaths']['XRF CSV']
            self.pth2rawXRD = self.LIIF['DataPaths']['Raw 2D location']
            self.CIFpaths = self.LIIF['DataPaths']['Path to CIFs']
            self.pth2exPM = self.LIIF['DataPaths']['Phase Info']

        else:
            self.MatSysName = None
            
            self.ptim = None
            self.ptim_m = None
            self.ptsp_raw = None
            self.ptsp_m = None
            self.ptsp_b = None
            
            self.pth5 = None
            self.pth2rawXRD = None
            self.pth2csvdb = None
            
            self.ptcomp = None
            self.CIFpaths = None
            self.pth2exPM = None
            
            self.O_pxs = None
            self.S_pxs = None
            self.X_pxs = None
        
        self.output = ipy.Output()
        #define widgets
        self.style = {'description_width':'initial'}
        self.MatSysName_Box = ipy.Text(
            value = self.MatSysName,
            placeholder = 'La-Mn-Ox',
            description = 'Material System',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptim_Box = ipy.Text(
            value = self.ptim,
            placeholder = 'path/to/images/',
            description = 'Images Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptim_mirror_Box = ipy.Text(
            value = self.ptim_m,
            placeholder = '/path/to/RGBmirror.bmp',
            description = 'Image Mirror .bmp',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptsp_raw_Box = ipy.Text(
            value = self.ptsp_raw,
            placeholder = '/path/to/raw/spectroscopy/',
            description = 'Raw Spectroscopy Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptsp_b_Box =  ipy.Text(
            value = self.ptsp_b,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Take6/Spectroscopy/Blank/blank_00.csv',
            description = 'Spectroscopy Blank Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Ptsp_m_Box =  ipy.Text(
            value = self.ptsp_m,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/Take6/Spectroscopy/Mirror/mirror_00.csv',
            description = 'Spectroscopy Mirror Data',
            continuous_update = False,
            style=self.style
        )
        
        self.Pth5_Box = ipy.Text(
            value = self.pth5,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/LaMnOx_18CIT49586_rerun_all_oned.h5',
            description = '1D XRD H5',
            continuous_update = False,
            style=self.style
        )
        
        self.Pth2rawXRD_box = ipy.Text(
            value = self.pth2rawXRD,
            placeholder = '/home/vandover/zoo-fs/data/CHESS/2019-12-CHESS-3B-raw/LaMnOx_18CIT49586_rerun/Stripes/',
            description = 'Raw 2D XRD',
            continuous_update = False,
            style=self.style
        )
        
        self.P2CSVDB_Box = ipy.Text(
            value = self.pth2csvdb,
            placeholder = '/home/vandover/Documents/Data/18CIT49586_LaMnOx_retake/17_LaMnO_CIT49586_SummaryDB.csv',
            description = 'CSV DataBaseFile',
            continuous_update = False,
            style=self.style
        )
        
        self.PtComp_Box = ipy.Text(
            value = self.ptcomp,
            placeholder = 'N/A',
            description = 'XRF Composition File',
            continuous_update = False,
            style=self.style
        )
            
        self.PtCIFs_Box = ipy.Text(
            value = self.CIFpaths,
            placeholder = '/home/vandover/Documents/GoogleDrive/KnownLaMnOxPhases/SeparatedCIFS/',
            description = 'CIFs Path',
            continuous_update = False,
            style=self.style
        )

        self.ExistingPhaseMap = ipy.Text(
            value = self.pth2exPM,
            placeholder = 'N/A',
            description = 'Existing Phase Map',
            continuous_update = False,
            style = self.style
        )
        
        self.Ops_Box = ipy.FloatText(
            value = self.O_pxs,
            placeholder = 0.808,
            description = 'Image PxSize (\u03BCm)',
            continuous_update = False,
            style=self.style
        ) 
        self.Sps_Box = ipy.FloatText(
            value = self.S_pxs,
            placeholder = 10.,
            description = 'Spectroscopy Pitch (\u03BCm)',
            continuous_update = False,
            style=self.style
        )
        self.Xps_Box = ipy.FloatText(
            value = self.X_pxs,
            placeholder = 10.,
            description = 'XRD Pitch (\u03BCm)',
            continuous_update = False,
            style=self.style
        )
        
        self.group1 = ipy.VBox([
            self.MatSysName_Box,
            self.Ptim_Box,
            self.Ptim_mirror_Box,
            self.Ptsp_raw_Box,
            self.Ptsp_b_Box,
            self.Ptsp_m_Box
        ])

        self.group2 = ipy.VBox([
            self.Pth5_Box,
            self.Pth2rawXRD_box,
            self.PtCIFs_Box,
            self.P2CSVDB_Box,
            self.ExistingPhaseMap
        ])
        
        self.group3 = ipy.VBox([
            self.PtComp_Box,
            self.Ops_Box,
            self.Sps_Box,
            self.Xps_Box
        ])
        
        self.FpBoxs = ipy.VBox([
            ipy.HBox([self.group1,self.group2]),
            self.group3
        ])
        self.LoadButton = ipy.Button(description = 'Load It!')
        self.LoadPorts = ipy.VBox([self.FpBoxs,self.LoadButton])
        display(self.LoadPorts)
        self.LoadButton.on_click(self.LoadIt)
            
    def LoadIt(self,event):
        #Dictionary structure:
        # Dictionary:
        #     condtion key
        #         [tau,T,xpos,ypos,fp]
        self.MatSysName = self.MatSysName_Box.value
        self.O_pxs = self.Ops_Box.value
        self.S_pxs = self.Sps_Box.value
        self.X_pxs = self.Xps_Box.value
        self.ptim = self.Ptim_Box.value
        self.ptim_m = self.Ptim_mirror_Box.value
        self.ptsp_raw = self.Ptsp_raw_Box.value
        self.ptsp_b = self.Ptsp_b_Box.value
        self.ptsp_m = self.Ptsp_m_Box.value
        self.pth5 = self.Pth5_Box.value
        self.pth2rawXRD = self.Pth2rawXRD_box.value
        self.CIFpaths = self.PtCIFs_Box.value
        self.pth2exPM = self.ExistingPhaseMap.value

        
        self.pth2csvdb = self.P2CSVDB_Box.value
        self.ptcomp = self.PtComp_Box.value
        
        self.DictGen()
        
    def DictGen(self):
        # For the a-Si work, T is maximum power for a stripe
        if self.ptim:
            self.OptDict = FpDict(pathtofiles=self.ptim,ftype='.bmp')
        if self.ptsp_raw:
            self.SpecDict = FpDict(self.ptsp_raw,ftype='.csv')
        if self.pth5:
            self.XrdDict = FpDict(self.pth5,Xrays=True,json_friendly=True)
        if self.pth2csvdb:
            # This DB is Mike's LasGO generated file from the CHESS run
            self.db = pd.read_csv(self.pth2csvdb)
        if self.ptcomp:
            self.comp_raw = np.genfromtxt(self.ptcomp,dtype=float,delimiter=',',skip_header=3)
            #tau,T,x_mm,y_mm,Cation_frac
            for idx in np.arange(len(self.comp_raw)):
                taus = int(self.comp_raw[idx,3])
                Ts = int(self.comp_raw[idx,4])
                Cat1_frac = self.comp_raw[idx,7]
                xpos = self.comp_raw[idx,5]
                ypos = self.comp_raw[idx,6]

                key = f'tau_{taus}_T_{Ts}'
                self.XrdDict[key]['Cation1_frac'] = str(Cat1_frac)
                self.OptDict[key]['Cation1_frac'] = str(Cat1_frac)
                self.SpecDict[key]['Cation1_frac'] = str(Cat1_frac)
            
    # def LoadInitFile(self,path2input='/Some/path/to/input/json.json'):
    #     with open(path2input,'r') as infile:
    #         self.LIIF = json.load(infile)
    #     # print(self.LIIF)

    #     self.MatSysName = self.LIIF['DataPaths']['MatSysName']
    #     self.O_pxs = self.LIIF['DataPaths']['Optical Pxs']
    #     self.S_pxs = self.LIIF['DataPaths']['Spectroscopy Pxs']
    #     self.X_pxs = self.LIIF['DataPaths']['XRD Pxs']
    #     self.ptim =  self.LIIF['DataPaths']['Images Data']
    #     self.ptim_m = self.LIIF['DataPaths']['Image mirror']
    #     self.ptsp_raw = self.LIIF['DataPaths']['Spec raw']
    #     self.ptsp_b = self.LIIF['DataPaths']['Spec blank']
    #     self.ptsp_m = self.LIIF['DataPaths']['Spec mirror']
    #     self.pth5 = self.LIIF['DataPaths']['H5 path']
    #     self.pth2csvdb = self.LIIF['DataPaths']['CHESS CSVDB']
    #     self.ptcomp = self.LIIF['DataPaths']['XRF CSV']
    #     self.pth2rawXRD = self.LIIF['DataPaths']['Raw 2D location']
    #     self.CIFpaths = self.LIIF['DataPaths']['Path to CIFs']
    #     self.pth2exPM = self.LIIF['DataPaths']['Phase Info']
        
    #     self.DictGen()
        
    def SaveInitFile(self,path2output='/Some/path/to/save/File/'):
        self.LID = {}
        self.LID['DataPaths'] = {}
        self.LID['DataPaths']['MatSysName'] = self.MatSysName
        self.LID['DataPaths']['Optical Pxs'] = self.O_pxs
        self.LID['DataPaths']['Spectroscopy Pxs'] = self.S_pxs
        self.LID['DataPaths']['XRD Pxs'] = self.X_pxs
        self.LID['DataPaths']['Images Data'] = self.ptim
        self.LID['DataPaths']['Image mirror'] =self.ptim_m
        self.LID['DataPaths']['Spec raw'] = self.ptsp_raw
        self.LID['DataPaths']['Spec blank'] = self.ptsp_b
        self.LID['DataPaths']['Spec mirror'] = self.ptsp_m
        self.LID['DataPaths']['H5 path'] = self.pth5
        self.LID['DataPaths']['CHESS CSVDB'] = self.pth2csvdb
        self.LID['DataPaths']['XRF CSV'] = self.ptcomp
        self.LID['DataPaths']['Raw 2D location'] = self.pth2rawXRD
        self.LID['DataPaths']['Path to CIFs'] = self.CIFpaths
        self.LID['DataPaths']['Phase Info'] = self.pth2exPM

        with open(f'{path2output}{self.MatSysName}_init.json','w') as outfile:
            print(outfile)
            outfile.write(json.dumps(self.LID))





class ConditionDataPreProcessor():
    """
    A class that generates stripe data for all 3 techniques.    
    """
    def __init__(self,DL,CompSpread=False):
        self.DL = DL
        self.key = list(self.DL.XrdDict)[0]
        self.CompSpread = CompSpread
        print(self.key)
        self.tau = self.DL.XrdDict[self.key]['tau']
        self.Tmax = self.DL.XrdDict[self.key]['T']
        self.stripe = TechStripe()
        self.xpos = None
        self.ypos = None
        #the filepaths to the appropriate stripels and technique normalization data are called on here
        self.stripe.fpb = self.DL.ptsp_b
        self.stripe.fpm = self.DL.ptsp_m
        self.stripe.h5file = self.DL.pth5

        #Each technique esesentailly produces a 'map' of data. 
        #The Y and intensity coordinates all vary, but they share a common spatial extent.
        #It is important to make sure that the effective pixel size or technique resolution is recorded
        #  - The pitch of most reflectance spectroscopy data are 10 um, this example uses 5 um
        #  - the image pixel size depends on the magnification and needs the PSC. recent data are 0.808 um/pixel
        self.stripe.O_pxs = self.DL.O_pxs
        self.stripe.S_pxs = self.DL.S_pxs
        self.stripe.X_pxs = self.DL.X_pxs
        self.raw2DXRD = []
        self.stripe_idx = None
        self.colors = list(sns.color_palette('tab10',16).as_hex())
        self.comp = None
        self.TechNames = []
        
    def TechMapCreation(self,wlmin=450,wlmax=800):
        self.tau = int(self.DL.XrdDict[self.key]['tau'])
        self.Tmax = int(self.DL.XrdDict[self.key]['T'])
        self.xpos = int(float(self.DL.XrdDict[self.key]['xpos']))
        self.ypos = int(float(self.DL.XrdDict[self.key]['ypos']))
        self.TechNames = []
        #create optical and spectroscopy maps with the techstripe fucntions Optmap and Specmap
        if self.DL.ptim:
            self.stripe.fpo = self.DL.OptDict[self.key]['fp']
            self.omap = self.stripe.Optmap(okey=self.key,weights=[0,0,1],grayscale=False,Norm=False,Average=True,crop=(True,[250,750]))
            self.TechNames.append('opt')
        if self.DL.ptsp_raw:
            self.stripe.fprs = self.DL.SpecDict[self.key]['fp']
            self.smap = self.stripe.Specmap(self.key,wlmax=wlmax,wlmin=wlmin,s_param=5)[2]
            self.wl = self.stripe.wl
            self.TechNames.append('spec')
        if self.DL.pth5:
            self.xmap = self.stripe.Xraymap(self.key,dpath=self.stripe.h5file)
            self.xmap = sharpen_y(self.xmap,alpha=0.25,bf=[1,1],fbf=[0.0,0.8],plotting=False)[:-24,:]
            self.Q = self.stripe.Q[:-24] #remove the last 24 values because it's noisy
            self.TechNames.append('xrd')
        if self.CompSpread:
            self.comp = self.DL.XrdDict[self.key]['Cation1_frac']

        print(self.TechNames)
        
    def TwoD_DP_Loader(self):
        self.raw2DXRD = []

        _,self.stripe_idx,_,_,_ = CondToPos(self.DL.db,tau=self.tau,Tmax=self.Tmax)
        for pth in list(glob.glob(self.DL.pth2rawXRD+f'scan_{self.stripe_idx}/*.tiff')):
#             print(pth)
            self.raw2DXRD.append(pth)
            
    def StickPatterns(self):
        self.Qmin = self.Q[0]
        self.Qmax = self.Q[-1]

        self.TTmin = QtoTT(self.Qmin)
        self.TTmax = QtoTT(self.Qmax)
        xrdcalc = pmg.XRDCalculator()
        
#         self.StickStructures = {}
        self.PhaseSets = {}
        for idx, (root, dirs, files) in enumerate(os.walk(self.DL.CIFpaths)):
            if idx == 0:
#                 print('Create Dictionary')
                for jdx in np.arange(len(dirs)):
                    self.PhaseSets[f'Phase Set {jdx+1}'] = {}

            if idx > 0:
                for jdx, file in enumerate(files):
#                     print(root+'/'+file)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}'] = {}
                    smeta = os.path.basename(file).split('_')
                    dp = xrdcalc.get_pattern(cif.CifParser(root+'/'+file).get_structures()[0],two_theta_range=(self.TTmin,self.TTmax))
#                     print(smeta)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Formula'] = smeta[0]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['SpaceGroup'] = smeta[1]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['fp'] = file
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['MP Structures'] = cif.CifParser(root+'/'+file).get_structures()[0]
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Qs'] = TTtoQ(dp.x)
                    self.PhaseSets[f'Phase Set {idx}'][f'Phase {jdx}']['Is'] = dp.y
        
    def TechExtentMatch(self,minx=-400,maxx=400):
        #output the spatial extent for each technique given
        self.TechDict = {}
        maps = [self.omap,self.smap,self.xmap]
        print(len(maps))
        pxs = [self.DL.O_pxs,self.DL.S_pxs,self.DL.X_pxs]
        xtents = []
        for tdx, tech in enumerate(self.TechNames):#This will need some more intellegent thought. currently there are 3 possible techniques available
            print(tech)
            self.TechDict[tech] ={}
            tmap = maps[tdx]
            tpxs = pxs[tdx]
            if tech == 'opt':
                self.omap = ndimage.gaussian_filter(self.omap,(5/self.stripe.O_pxs))
                self.g = np.gradient(np.sum(self.omap,axis=0))
                self.g = np.sqrt(self.g*self.g)
                self.og = self.g/max(self.g)
                self.TechDict[tech]['fullmap'] = self.omap
                self.TechDict[tech]['grad'] = self.og
            else:
                self.TechDict[tech]['fullmap'] = tmap
                self.TechDict[tech]['grad'],_ = Grad(tmap,bound_avg=True,norm=True,plotting=False)
            # grad = self.TechDict[tech]['grad']

            if tech == 'xrd':
                self.TechDict[tech]['center'] = get_center(tmap[:500,:])[0]
            elif tech == 'opt':
                mid = int(np.round(tmap.shape[0]))
                self.TechDict[tech]['center'] = get_center(tmap[(mid-50):(mid+50)])[0]
            else:
                self.TechDict[tech]['center'] = get_center(tmap)[0]
            center = self.TechDict[tech]['center']

            x = np.linspace(0,tmap.shape[1]*tpxs,tmap.shape[1])
            x = x - center*tpxs
            print(tech, center)
            xtents.append(x)
            self.TechDict[tech]['fullxtent'] = x

        #common extent
        self.minx = np.max([np.min(x) for x in xtents])
        self.maxx = np.min([np.max(x) for x in xtents])
        print(self.minx,self.maxx)
        for tdx, tech in enumerate(self.TechNames):
            x = self.TechDict[tech]['fullxtent']
            # print(min(x),max(x))
            grad = self.TechDict[tech]['grad']
            center = self.TechDict[tech]['center']
            tmap = self.TechDict[tech]['fullmap']


            self.TechDict[tech]['comextent'] = x[(x>=self.minx)&(x<=self.maxx)]
            self.TechDict[tech]['symgrad'] = Symmetrize(grad,center,plotting=False)[(x>=self.minx)&(x<=self.maxx)]
            self.TechDict[tech]['commap'] = tmap[:,(x>=self.minx)&(x<=self.maxx)]

            if (self.raw2DXRD != '') and (tech == 'xrd'):
                self.raw2DXRD_f = list(compress(self.raw2DXRD,[(x>=self.minx)&(x<=self.maxx)][0]))
            print(f'finsihed {tech}')
        #Defining the spatial extent

        # self.ox = np.linspace(0,self.omap.shape[1]*self.stripe.O_pxs,self.omap.shape[1])
        # self.sx = np.linspace(0,self.smap.shape[1]*self.stripe.S_pxs,self.smap.shape[1])
        # self.xx = np.linspace(0,self.xmap.shape[1]*self.stripe.X_pxs,self.xmap.shape[1])

        # #The spectroscopy and diffraction gradient
        # self.sg,_ = Grad(self.smap,bound_avg=True,norm=True,plotting=False)
        # self.xg,_ = Grad(self.xmap,bound_avg=True,norm=True,plotting=False)

        #determining the center of each stripe through the gradient signals and setting the center to 0
        # self.ocenter = np.round(CenterFinding(self.og,sparam=2,plotting=False))
        # self.scenter = np.round(CenterFinding(self.sg,sparam=2,plotting=False))
        # self.xcenter = np.round(CenterFinding(self.xg,sparam=2,plotting=False))

        # self.ocenter = get_center(self.omap)[0]
        # self.scenter = get_center(self.smap)[0]
        # self.xcenter = get_center(self.xmap[:500,:])[0] #this allows for the center finding to avoid the silicon peak

        # self.ox = self.ox - self.ocenter*self.stripe.O_pxs
        # self.sx = self.sx - self.scenter*self.stripe.S_pxs
        # self.xx = self.xx - self.xcenter*self.stripe.X_pxs


        #Matching the spatial extents
        #Identifying the common spatial extent for each technique 
        # self.maxx = np.min([np.max(self.ox),np.max(self.sx),np.max(self.xx)])
        # self.minx = np.max([np.min(self.ox),np.min(self.sx),np.min(self.xx)])

        # #can also hard code it
        # self.minx,self.maxx = [minx,maxx]

        # self.ox1 = self.ox[(self.ox>=self.minx)&(self.ox<=self.maxx)]
        # self.sx1 = self.sx[(self.sx>=self.minx)&(self.sx<=self.maxx)]
        # self.xx1 = self.xx[(self.xx>=self.minx)&(self.xx<=self.maxx)]

        # self.osym = Symmetrize(self.og,self.ocenter,plotting=False)[(self.ox>=self.minx)&(self.ox<=self.maxx)]
        # self.ssym = Symmetrize(self.sg,self.scenter,plotting=False)[(self.sx>=self.minx)&(self.sx<=self.maxx)]
        # self.xsym = Symmetrize(self.xg,self.xcenter,plotting=False)[(self.xx>=self.minx)&(self.xx<=self.maxx)]

        # self.omap1 = self.omap[:,[(self.ox>=self.minx)&(self.ox<=self.maxx)][0]]
        # self.smap1 = self.smap[:,[(self.sx>=self.minx)&(self.sx<=self.maxx)][0]]
        # self.xmap1 = self.xmap[:,[(self.xx>=self.minx)&(self.xx<=self.maxx)][0]]

        
        # self.raw2DXRD_f = list(compress(self.raw2DXRD,[(self.xx>=self.minx)&(self.xx<=self.maxx)][0]))
        
    # def TechPlotter(self):
    #     self.TPfig,self.TPax = plt.subplots(3,1,dpi=150,figsize=(6,6),sharex=True)
    #     if self.CompSpread:
    #         self.TPfig.suptitle(f'{self.DL.MatSysName} {self.key} Cat. 1 frac: {np.round(a=float(self.comp),decimals=2)}')
    #     else:
    #         self.TPfig.suptitle(f'{self.DL.MatSysName} {self.key}')
    #     self.TPax[0].set_title('Optical Micrograph')
    #     self.TPax[1].set_title('Reflectance Map')
    #     self.TPax[2].set_title('XRD Map')
    #     self.TPax[0].imshow(self.omap1,aspect='auto',extent=[self.minx,self.maxx,0,self.omap1.shape[0]*self.stripe.O_pxs])
    #     self.TPax[1].imshow(self.smap1,aspect='auto',extent=[self.minx,self.maxx,self.wl[0],self.wl[-1]])
    #     self.TPax[2].imshow(self.xmap1,aspect='auto',extent=[self.minx,self.maxx,self.Q[-1],self.Q[0]])

    #     # ax[0].set_xlabel('across stripe in \u03BCm')
    #     self.TPax[2].set_xlabel('across stripe in \u03BCm')
    #     self.TPax[0].set_ylabel('along stripe in \u03BCm')
    #     self.TPax[1].set_ylabel('wavelengthin  nm')
    #     self.TPax[2].set_ylabel('Q in nm$^{-1}$')

    #     plt.show(self.TPfig)






class WaferAndTauT_GUIs():
    def __init__(self, DL, StripeData, XRD_GUI, CompSpread=False, path2pm=None):
        self.StripeData = StripeData
        self.XRD_GUI = XRD_GUI
        self.DL = DL
        self.xpos_all = []
        self.ypos_all = []
        self.taus_all = []
        self.logtaus_all = None
        self.Tmaxs_all = []
        self.Comp_all = []
        self.CationFrac = None
        self.CompSpread = CompSpread
        self.key = StripeData.key

        self.xpos_done = []
        self.ypos_done = []
        self.taus_done = []
        self.logtaus_done = []
        self.Tmaxs_done = []
        self.Comps_done = []


        self.width = 0.75
        self.height = 4.5
        self.wafer_r = 50

        self.ws_start = (self.StripeData.xpos,self.StripeData.ypos)
        self.taut_start = (self.StripeData.tau,self.StripeData.Tmax)

        if self.CompSpread:
            self.logtaus_all = []
            self.tTfig = plt.figure(constrained_layout=True,figsize=(5,5),dpi=150)
            self.tTax = self.tTfig.add_subplot(111,projection='3d')
            self.tTax.set_xlabel('processing time log10(\u03BCs)')
            self.tTax.set_ylabel('Cation fraction')
            self.tTax.set_zlabel('Tmax (\u00b0C)')
            self.tTax.set_title(f'{self.DL.MatSysName} Tau vs T')

        else:
            self.tTfig, self.tTax = plt.subplots(1,1,constrained_layout=True,figsize=(5,5),dpi=150)
            self.tTax.set_xlabel('procesing time (\u03BCs)')
            self.tTax.set_ylabel('Tmax (\u00b0C)')
            self.tTax.set_title(f'{self.DL.MatSysName} Tau vs T')
        
        self.Waffig, self.Wafax = plt.subplots(1,1,constrained_layout=True,figsize=(5,5),dpi=150)
        self.Wafax.set_xlabel('x position (mm)')
        self.Wafax.set_ylabel('y position (mm)')
        self.Wafax.set_title(f'{self.DL.MatSysName} Wafer Map')

        
        self.Waffig.canvas.mpl_connect('pick_event', self.WaferPosAndTauT_picker)
        self.tTfig.canvas.mpl_connect('pick_event', self.WaferPosAndTauT_picker)
        self.XRD_GUI.XRDfig.canvas.mpl_connect('pick_event', self.WaferPosAndTauT_picker)
    
        if path2pm != None:
            with open(path2pm,'r') as fp:
                self.PMdict = json.load(fp)
        else:
            self.PMdict = {}

        # print(list(self.PMdict))


    def Plotter(self,CompSpread=False):

        for cond in dwellsort(list(self.DL.XrdDict)):
            self.xpos_all.append(int(self.DL.XrdDict[cond]['xpos']))
            self.ypos_all.append(int(self.DL.XrdDict[cond]['ypos']))
            self.taus_all.append(int(self.DL.XrdDict[cond]['tau']))
            self.Tmaxs_all.append(int(self.DL.XrdDict[cond]['T']))

            if cond in list(self.PMdict):
                self.xpos_done.append(int(self.DL.XrdDict[cond]['xpos']))
                self.ypos_done.append(int(self.DL.XrdDict[cond]['ypos']))
                self.taus_done.append(int(self.DL.XrdDict[cond]['tau']))
                self.Tmaxs_done.append(int(self.DL.XrdDict[cond]['T']))


            if self.CompSpread:
                self.logtaus_all.append(np.log10(int(self.DL.XrdDict[cond]['tau'])))
                self.Comp_all.append(float(self.DL.XrdDict[cond]['Cation1_frac']))
                if cond in list(self.PMdict):
                    self.logtaus_done.append(np.log10(int(self.DL.XrdDict[cond]['tau'])))
                    self.Comps_done.append(float(self.DL.XrdDict[cond]['Cation1_frac']))

        self.wafer_space = self.Wafax.scatter(self.xpos_all,self.ypos_all,s=1,c='b')
        self.wafer_edge = self.Wafax.add_patch(Circle(xy=(0,0),radius=self.wafer_r,
                                            color='k',fill=False,linewidth=2))
        self.Wafax.set_xlim([-(self.wafer_r+2),self.wafer_r+2])
        self.Wafax.set_ylim([-(self.wafer_r+2),self.wafer_r+2])
        self.Wafax.axis('equal')

        for x,y in zip(self.xpos_all,self.ypos_all):
            self.Wafax.add_patch(Rectangle(xy=(x-self.width/2, y-self.height/2),
                                      width=self.width, height=self.height,
                                      color='b',fill=True,picker=True))

        for x,y in zip(self.xpos_done,self.ypos_done):
            self.Wafax.add_patch(Rectangle(xy=(x-self.width/2, y-self.height/2),
                                      width=self.width, height=self.height,
                                      color='lime',fill=True))

        self.wafer_select = self.Wafax.add_patch(Rectangle(xy=(self.ws_start[0]-self.width/2,self.ws_start[1]-self.height/2),
                                                 width=self.width,height=self.height,
                                                 color='r',fill=True))



        if self.CompSpread:
            self.CationFrac = float(self.DL.XrdDict[self.key]['Cation1_frac'])
            self.tau_t_space = self.tTax.scatter(xs=self.logtaus_all,ys=self.Comp_all,zs=self.Tmaxs_all,
                                                 s=12,color=['b' for i in range(len(self.Tmaxs_all))],picker=True)

            self.tau_t_processed = self.tTax.scatter(xs=self.logtaus_done,ys=self.Comps_done,zs=self.Tmaxs_done,
                                                 s=12,c='lime',edgecolors='face')

            self.taut_select = self.tTax.scatter(xs=np.log10(self.taut_start[0]),ys=self.CationFrac,zs=self.taut_start[1],
                                                 s=20,facecolors='none',
                                                 edgecolors='r',linewidth=2)
                
    
        else:
            self.tau_t_space = self.tTax.scatter(x=self.taus_all,y=self.Tmaxs_all,s=12,facecolors='b',picker=True)

            self.taut_select = self.tTax.scatter(self.taut_start[0],self.taut_start[1],s=20,
                                        facecolors='none',edgecolors='r',linewidth=2)
            
            self.tau_t_processed = self.tTax.scatter(x=self.taus_done,y=self.Tmaxs_done,s=12,c='lime')

            self.tTax.set_xscale('symlog')


        # print(self.tau_t_space.get_facecolors())
        


    def WaferPosAndTauT_picker(self,event):
        # print('you clicked!')
        artist = event.artist
        if artist in self.tau_t_space.findobj():
            print('tau_t space')
            taut_idx = event.ind[0]
            self.tau = self.taus_all[taut_idx]
            self.Tmax = self.Tmaxs_all[taut_idx]
            
            self.key = f'tau_{self.tau}_T_{self.Tmax}'
            self.xpos = int(self.DL.XrdDict[self.key]['xpos'])
            self.ypos = int(self.DL.XrdDict[self.key]['ypos'])
            
            if self.CompSpread:
                self.CationFrac = self.Comp_all[taut_idx]
                self.taut_select._offsets3d = (np.array([self.logtaus_all[taut_idx]]),
                                              np.array([self.CationFrac]),
                                              np.array([self.Tmax]))
                self.tau_t_space._facecolor3d
            else:
                self.taut_select.set_offsets([self.tau,self.Tmax])
                
            self.tTax.figure.canvas.draw()
            self.wafer_select.set_xy((self.xpos-0.5*self.width,self.ypos-0.5*self.height))
            self.Waffig.canvas.draw()
            
        else:
            print('wafer space')
            self.xpos,self.ypos = artist.xy
            #shifted origin to lot position
            self.xpos = self.xpos+0.5*self.width
            self.ypos = self.ypos+0.5*self.height
            _,_,_,self.tau,self.Tmax = CondToPos(self.DL.db,xpos=self.xpos,ypos=self.ypos,cond2pos=False)
            
            self.key = f'tau_{self.tau}_T_{self.Tmax}'
            if self.CompSpread:
                self.CationFrac = float(self.DL.XrdDict[self.key]['Cation1_frac'])
                self.taut_select._offsets3d = (np.array([np.log10(self.tau)]),
                                              np.array([self.CationFrac]),
                                              np.array([self.Tmax]))
            else:
                self.taut_select.set_offsets([self.tau,self.Tmax])
                
            self.tTfig.canvas.draw_idle()  
            self.wafer_select.set_xy((self.xpos-0.5*self.width,self.ypos-0.5*self.height))
            self.Waffig.canvas.draw_idle()

        print('point selected')
        self.StripeData.key = self.key
        self.StripeData.xpos = self.xpos
        self.StripeData.ypos = self.ypos
        self.StripeData.tau = self.tau
        self.StripeData.Tmax = self.Tmax

        self.StripeData.TechMapCreation()
        if len(self.StripeData.raw2DXRD_f)>0:
            self.StripeData.TwoD_DP_Loader()
        self.StripeData.TechExtentMatch()
        self.XRD_GUI.StripeReInitialize()
        print('reinitialized')




class ManualXRD_GUI():
    def __init__(self, DL, StripeData, CompSpread=False, PMoutputpath=None):
        self.StripeData = StripeData
#         self.WTplot = WTplot
        self.init_idx = np.int(np.round(self.StripeData.TechDict['xrd']['center']))
        output = ipy.Output()
        self.tau = self.StripeData.tau
        self.Tmax = self.StripeData.Tmax
        self.xpos = self.StripeData.xpos
        self.ypos = self.StripeData.ypos
        self.PMoutputpath = PMoutputpath
        self.DL = DL
        self.CompSpread = CompSpread
        self.current_phases = []

        # pull the relevant info from StripeData for now
        self.xx = self.StripeData.TechDict['xrd']['comextent']
        self.xmap1 = self.StripeData.TechDict['xrd']['commap']
        self.xsym = self.StripeData.TechDict['xrd']['symgrad']
        self.xcenter = self.StripeData.TechDict['xrd']['center']



        with output:
            self.XRDfig = plt.figure(figsize=(10,6))
            
        self.gs = self.XRDfig.add_gridspec(4, 5)
        self.OneDMapPlot = self.XRDfig.add_subplot(self.gs[:2,:2])
        self.OneDMap = self.OneDMapPlot.imshow(self.xmap1,aspect='auto',
                            extent=[self.StripeData.minx,self.StripeData.maxx,max(self.StripeData.Q),min(self.StripeData.Q)],
                            cmap='cividis')
        self.vertline = self.OneDMapPlot.axvline(x=self.xx[self.init_idx],ymin=0,ymax=1,c='orchid',alpha=0.7)
        
        if self.CompSpread:
            catfrac = np.round(float(self.DL.XrdDict[self.StripeData.key]['Cation1_frac']),2)
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key} at comp {catfrac}')
        else:
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key}')

        self.OneDMapPlot.set_xlabel('across stripe \u03BCm')
        self.OneDMapPlot.set_ylabel('Q in nm$^{-1}$')

        self.OneDPlot = self.XRDfig.add_subplot(self.gs[2,2:])
        self.StickPlots = self.XRDfig.add_subplot(self.gs[3,2:])
        self.PhaseRegions = self.XRDfig.add_subplot(self.gs[:2,2:])
        self.StickPlots.set_title(f'Reference patterns')
        self.StickPlots.set_xlabel('Q in nm$^{-1}$')
        self.StickPlots.set_ylabel('Rel. Int. (a.u.)')
        
        self.OneDPlot.set_ylabel('Int. (a.u.)')
        self.PhaseRegions.set_ylabel('Int. (a.u.)')
        self.OneDPlot.set_yticks([])
        self.PhaseRegions.set_yticks([])

        self.oneD_DP = self.OneDPlot.plot(self.StripeData.Q,self.xmap1[:,self.init_idx],c='orchid')[0]
        self.OneDPlot.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        self.oneD_text = self.OneDPlot.set_title(f'1D XRD at {np.int(self.xx[self.init_idx])} \u03BCm')
        
        #Using the Phase Region code to help with signal to noise and averaging over the entire bounded region
        self.xtrs = TransitionFinder(data=self.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        self.DP_bases = PhaseRegions(techmap=self.xmap1,trs=self.xtrs)

        for dpx,dp in enumerate(self.DP_bases[0]):
            self.PhaseRegions.plot(self.StripeData.Q,dp+0.2*dpx)

        for tdx,tr in enumerate(self.xtrs):
            self.OneDMapPlot.axvline(self.xx[tr],ymin=0.5,ymax=1,c='turquoise',linestyle='--')
        

        self.TwoDPlot = self.XRDfig.add_subplot(self.gs[2:,:2])
        self.TwoDPlot.set_xlabel('pixel x')
        self.TwoDPlot.set_ylabel('pixel y')
        # print(len(self.StripeData.raw2DXRD_f))
        if len(self.StripeData.raw2DXRD_f)>0:
            self.twoDDP_img = imageio.imread(self.StripeData.raw2DXRD_f[self.init_idx])
            self.twoDDP_img = np.log10(self.twoDDP_img+abs(np.min(self.twoDDP_img))+1)
            self.twoD_DP = self.TwoDPlot.imshow(self.twoDDP_img,cmap='cividis')
            self.twoD_text = self.TwoDPlot.set_title(f'raw 2D data at {np.int(self.xx[self.init_idx])} \u03BCm')
        
        self.XRDfig.tight_layout()


        #Interactables
        #stripe slider
        self.int_slider = ipy.IntSlider(
            value=self.init_idx,min=0,max=self.xmap1.shape[1],
            continuous_update=False,
        )
        self.int_slider.readout = False
        self.style = {'description_width':'initial'}

        #loaded phase check boxes for comparison
        self.phase_cboxes = []
        for idx, phaseset in enumerate(list(self.StripeData.PhaseSets)):
            self.phase_cboxes.append([])
            for jdx, phase in enumerate(list(self.StripeData.PhaseSets[phaseset])):
                formula = self.StripeData.PhaseSets[phaseset][phase]['Formula']
                sg = self.StripeData.PhaseSets[phaseset][phase]['SpaceGroup']
                self.phase_cboxes[idx].append(ipy.Checkbox(
                    description=formula+'_'+sg,
                    value=False,

                ))

        # restructuring for multiple phase sets in a system
        self.phaseset_cboxes = []
        for pset in self.phase_cboxes:
            self.phaseset_cboxes.append(ipy.VBox([cbox for cbox in pset]))
        self.phase_cboxes = ipy.HBox([pset for pset in self.phaseset_cboxes])
        
        #button controling the stick pattern replot
        self.replot_sticks_button = ipy.Button(description='Stick DP replot')

        #button creating/or passing labels to phsae diagram file
        self.PhaseMapPassingButton = ipy.Button(description='Pass to Phase Map')
        
        #button to pass write in phases
        self.WriteInPhaseButton = ipy.Button(description='Add write in Phase')

        #morphology and unknown toggles
        self.texturetoggle = []
        self.defaultTexTyp = ['equiaxed','textured','amorphous','nanocrystalline','small grain','large grain'] 
        for texturetype in self.defaultTexTyp:
            self.texturetoggle.append(
                ipy.Checkbox(description=texturetype,value=False)
            )

        #last elemen in texture toggle is the write in function. 
        self.texturetoggle.append(
            ipy.Text(description='write in:',placeholder='unknown phase 1')
            )

        #layout of widgets
        self.texturetoggle = ipy.VBox(self.texturetoggle)
        

        
        # supplying the write in checkboxes with phases that one has already assigned (not in the CIFs)
        self.writeins = []
        self.assignedPLs = []
        for vbox in list(self.phaseset_cboxes):
            for cb in vbox.children:
                self.assignedPLs.append(cb.description)

        if self.DL.pth2exPM != None:
            self.PMdict = json.load(open(self.DL.pth2exPM,'r'))
            for key in list(self.PMdict):
                for pos in list(self.PMdict[key]['PosDepPLs']):
                    for phase in list(self.PMdict[key]['PosDepPLs'][pos]):
                        self.writeins.append(phase)
            self.assigned = np.unique(self.writeins).tolist()
            self.writeins = []
            for known in self.assigned:
                if all(known not in l for l in self.assignedPLs+self.defaultTexTyp):
                    self.writeins.append(ipy.Checkbox(description=known,value=False))
            # self.cor_wi = [obj.children for obj in self.writeins.children]
            self.writeins = ipy.VBox(self.writeins)
        else:
            self.writeins = ipy.VBox([])

        self.position_control = ipy.HBox([ipy.VBox([ipy.Label('stripe position'),self.int_slider]),self.texturetoggle,self.writeins,ipy.VBox([self.PhaseMapPassingButton,self.WriteInPhaseButton])])
        pc_layout = ipy.Layout(
            border = 'solid 3px gray',
            margin = '10px 10px 10px 10px',
            padding = '5px 5px 5px 5px',
            align_items = 'center'
        )
        self.position_control.layout = pc_layout
        self.phase_control = ipy.VBox([self.phase_cboxes,self.replot_sticks_button])
        self.phase_control.layout = pc_layout
        self.phase_control.layout.align_items = 'center'

        #observe the interactables. link to a function that interactable does
        self.int_slider.observe(self.VerticalLineAndDP_Updates,'value')
        self.replot_sticks_button.on_click(self.StickPatternUpdate)
        self.PhaseMapPassingButton.on_click(self.WritePhasesToFile)
        self.WriteInPhaseButton.on_click(self.AddWriteInPhase)

        controls = ipy.VBox([self.position_control,self.phase_control])

        display(ipy.VBox([controls,output]))
        
    def StripeReInitialize(self):        
        print(self.StripeData.key)

        self.xx = self.StripeData.TechDict['xrd']['comextent']
        self.xmap1 = self.StripeData.TechDict['xrd']['commap']
        self.xsym = self.StripeData.TechDict['xrd']['symgrad']
        self.xcenter = self.StripeData.TechDict['xrd']['center']
        print('reloaded')
        
        ###manipulables
        #vertical line on XRD map
        self.init_idx = int(np.round(0.5*(self.xmap1.shape[1])))
        self.OneDMapPlot.cla()
        self.OneDMap = self.OneDMapPlot.imshow(self.xmap1,aspect='auto',
                            extent=[self.StripeData.minx,self.StripeData.maxx,max(self.StripeData.Q),min(self.StripeData.Q)],
                            cmap='cividis')
        self.OneDMap.set_extent([self.StripeData.minx,self.StripeData.maxx,max(self.StripeData.Q),min(self.StripeData.Q)])
        self.OneDMap.set_clim(np.min(self.xmap1),np.max(self.xmap1))
        self.OneDMapText.set_text(f'XRD map {self.StripeData.key}')
        self.vertline = self.OneDMapPlot.axvline(x=self.xx[self.init_idx],ymin=0,ymax=1,c='orchid',alpha=0.7)
        if self.CompSpread:
            catfrac = np.round(float(self.DL.XrdDict[self.StripeData.key]['Cation1_frac']),2)
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key} at comp {catfrac}')
        else:
            self.OneDMapText =  self.OneDMapPlot.set_title(f'XRD map {self.StripeData.key}')
        self.OneDMapPlot.set_xlabel('across stripe \u03BCm')
        self.OneDMapPlot.set_ylabel('Q in nm$^{-1}$')
        

        self.oneD_DP.set_xdata(self.StripeData.Q)
        self.oneD_DP.set_ydata(self.xmap1[:,self.init_idx])
        self.OneDPlot.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        self.OneDPlot.set_ylim(bottom=min(self.oneD_DP.get_ydata())-0.01,
                               top=max(self.oneD_DP.get_ydata())+0.01)
        self.oneD_text.set_text(f'1D XRD at {np.int(self.xx[self.init_idx])} \u03BCm')
        

        self.PhaseRegions.cla()
        self.PhaseRegions.set_yticks([])
        self.PhaseRegions.set_ylabel('Int. (a.u.)')

        self.xtrs = TransitionFinder(data=self.xsym,bkgd_ROI=[10,-10],prom_filt=0,sig=3)[0]
        self.DP_bases = PhaseRegions(techmap=self.xmap1,trs=self.xtrs)
        self.PhaseRegions.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        for dpx,dp in enumerate(self.DP_bases[0]):
            self.PhaseRegions.plot(self.StripeData.Q,dp+0.2*dpx)
        for tdx,tr in enumerate(self.xtrs):
            self.OneDMapPlot.axvline(self.xx[tr],ymin=0.5,ymax=1,c='turquoise',linestyle='--')

        #2D DP corresponding to the XRD map with location in um
        if len(self.StripeData.raw2DXRD_f)>0:
            self.twoDDP_img = imageio.imread(self.StripeData.raw2DXRD_f[self.init_idx])
            self.twoDDP_img = np.log10(self.twoDDP_img+abs(np.min(self.twoDDP_img))+1)
            self.twoD_DP.set_array(self.twoDDP_img)
            self.twoD_text.set_text(f'raw 2D data at {np.int(self.xx[self.init_idx])} \u03BCm')
        self.int_slider.max = self.xmap1.shape[1]
        self.XRDfig.canvas.draw_idle()
        

    def AddWriteInPhase(self,change):
        writeinphase = list(self.texturetoggle.children)[-1].value
        wip_cbox = ipy.Checkbox(description=writeinphase,value=False)
        self.position_control.children[-2].children = tuple(list(self.position_control.children[-2].children )+ [wip_cbox])

        list(self.position_control.children)
    #Callback Functions
    def VerticalLineAndDP_Updates(self,change):
        """ Redraws the vertical line and the 2D DP corresponding to the current position on the XRD map"""
        
        self.vertline.set_xdata(self.xx[change.new])
#         print(StripeData.xx1[change.new])
        if len(self.StripeData.raw2DXRD_f)>0:
            self.twoDDP_img = imageio.imread(self.StripeData.raw2DXRD_f[change.new])
            self.twoDDP_img = np.log10(self.twoDDP_img+abs(np.min(self.twoDDP_img))+1)
            self.twoD_DP.set_array(self.twoDDP_img)
            self.twoD_text.set_text(f'raw 2D data at {np.int(self.xx[change.new])} \u03BCm')
        self.oneD_DP.set_ydata(self.xmap1[:,change.new])
        self.OneDPlot.set_ylim(bottom=min(self.oneD_DP.get_ydata())-0.01,
                               top=max(self.oneD_DP.get_ydata())+0.01)
        self.oneD_text.set_text(f'1D XRD at {np.int(self.xx[change.new])} \u03BCm')
        self.XRDfig.canvas.draw_idle()

    def WritePhasesToFile(self,change):
        # needs to read a file if it exists... write to a file if it exists... create a file if it does not exist.
        # file structure should be readable by new class that plots the "labeled" phase map
        fpath = self.DL.pth2exPM
        key = self.StripeData.key
        xpos = self.StripeData.xpos
        ypos = self.StripeData.ypos
        tau = self.StripeData.tau
        Tmax = self.StripeData.Tmax
        outputpath = self.PMoutputpath
        # set the exiting file path from the init file if it exists.
        if os.path.exists(fpath):
            # print('your file exists')
            with open(fpath,'r') as fp:
                PMdict = json.load(fp)

                # print(PMdict)
        # otherwise create a dictionary and set the fpath to what it should be
        else:
            
            fpath = f'{outputpath}{self.DL.MatSysName}_PhaseMap.json'
            self.DL.pth2exPM = fpath
            self.DL.SaveInitFile(path2output=os.path.dirname(self.DL.InitFile)+'/')
            PMdict = {}
        PMdict[key] = {}
        PMdict[key]['tau'] = tau
        PMdict[key]['Tmax'] = Tmax
        PMdict[key]['xpos'] = xpos
        PMdict[key]['ypos'] = ypos
        if self.CompSpread:
            PMdict[key]['Cation1_frac'] = float(self.DL.XrdDict[key]['Cation1_frac'])
            
        #need to pass sub stripe position, and phases/morphology. create a sub folder for it
        PMdict[key]['PosDepPLs'] = {}
        sub_xpos = self.xx[self.int_slider.value]
        PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'] = []

        #refreshes the current phases to what is currently toggled.
        self.current_phases = []
        for idx, ps_child in enumerate(self.phase_cboxes.children):
            self.current_phases.append([])
            for phase_child in ps_child.children:
                if phase_child.value:
                    self.current_phases[idx].append(phase_child.description)

        for phaseset in self.current_phases:
            for active_phase in phaseset:
                PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(active_phase)
        for child in self.texturetoggle.children:
            if child.value:
                PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(child.description)
        for child in self.writeins.children:
            if child.value:
                PMdict[key]['PosDepPLs'][f'{np.round(sub_xpos,2)} um'].append(child.description)


        
#         #need to updtate WaferandTauTplot:
#         if self.CompSpread:
#             print(np.log10(tau),PMdict[key]['Cation1_frac'],Tmax)
#             self.WTplot.tTax.scatter(xs=np.log10(tau),ys=PMdict[key]['Cation1_frac'],zs=Tmax,c='g')
#         else:
#             self.WTplot.tTax.scatter(xs=tau,ys=Tmax,c='g')
#         self.WTplot.tTfig.canvas.draw_idle()
        
        #write out the file to a .json
        with open(fpath,'w') as fp:
            json.dump(PMdict,fp)
            
    def StickPatternUpdate(self,change):
        self.StickPlots.clear()
        self.current_phases = []
        newLabels, newHandles = [], []
        # print(self.phase_cboxes.children)
        for idx, ps_child in enumerate(self.phase_cboxes.children):
            self.current_phases.append([])
            for phase_child in ps_child.children:
                if phase_child.value:
                    self.current_phases[idx].append(phase_child.description)

        colorcounter = 0
        for idx, pset in enumerate(self.current_phases):
            for jdx, activephase in enumerate(pset):
                
                phaseID = activephase
                formula, sg = phaseID.split('_')
                phaseset = list(self.StripeData.PhaseSets)[idx]
                candidate_phases = list(self.StripeData.PhaseSets[phaseset])
                
                for phase in candidate_phases:
#                     print(phase)
#                     print(list(StripeData.PhaseSets[phaseset][phase]))
#                     print(formula,sg)

                    checkformula = self.StripeData.PhaseSets[phaseset][phase]['Formula']
                    checksg = self.StripeData.PhaseSets[phaseset][phase]['SpaceGroup']
                    if (formula == checkformula) & (sg == checksg):
                        Qs = self.StripeData.PhaseSets[phaseset][phase]['Qs']
                        Is = self.StripeData.PhaseSets[phaseset][phase]['Is']
                        for pdx, (q,i) in enumerate(zip(Qs,Is)):
                            self.StickPlots.plot([q,q],[i+1,1],linewidth=2,c=self.StripeData.colors[colorcounter],label=phaseID)
                        self.StickPlots.set_xlim(self.StripeData.Qmin,self.StripeData.Qmax)

                    handles, labels = self.StickPlots.get_legend_handles_labels()
                    newLabels, newHandles = [], []
                    for handle, label in zip(handles, labels):
                        if label not in newLabels:
                            newLabels.append(label)
                            newHandles.append(handle)
                    
                colorcounter += 1
        self.StickPlots.set_xlabel('Q in nm$^{-1}$')
        self.StickPlots.legend(newHandles,newLabels,loc='upper left',bbox_to_anchor=(0,1.5))
        self.StickPlots.set_xlim((self.StripeData.Qmin,self.StripeData.Qmax))
        self.XRDfig.canvas.draw_idle()


class ManuelPhaseMap():
    def __init__(self, DL, StripeData, CompSpread=False):

        self.DL = DL
        self.xpos_all = []
        self.ypos_all = []
        self.taus_all = []
        self.logtaus_all = None
        self.Tmaxs_all = []
        self.Comp_all = []
        self.phaselabels_all = []
        self.CationFrac = None
        self.CompSpread = CompSpread
        self.colors = list(sns.color_palette("tab10",16).as_hex())
        
        output = ipy.Output()
        with output:
            self.PMapFig = plt.figure(constrained_layout=True,figsize=(7,5))
        if self.CompSpread:
            self.logtaus_all = []
            self.PMapax = self.PMapFig.add_subplot(111,projection='3d')
            self.PMapax.set_xlabel('processing time log10(\u03BCs)')
            self.PMapax.set_ylabel('Cation fraction')
            self.PMapax.set_zlabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
        else:
            self.PMapax = self.PMapFig.add_subplot(111)
            self.PMapax.set_xlabel('procesing time (\u03BCs)')
            self.PMapax.set_ylabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
            self.PMapax.set_xscale('symlog')
#         self.PMapax.axis('equal')
        with open(self.DL.pth2exPM) as fp:
            self.PMdict = json.load(fp)
        
#         print(list(self.PMdict))
        for key in list(self.PMdict):
            # print(self.PMdict[key])
            self.taus_all.append(self.PMdict[key]['tau'])
            self.Tmaxs_all.append(self.PMdict[key]['Tmax'])
            self.xpos_all.append(self.PMdict[key]['xpos'])
            self.ypos_all.append(self.PMdict[key]['ypos'])
            for pos in list(self.PMdict[key]['PosDepPLs']):
                self.phaselabels_all.append(self.PMdict[key]['PosDepPLs'][pos])
            if self.CompSpread:
                self.Comp_all.append(float(self.PMdict[key]['Cation1_frac']))
                self.logtaus_all.append(np.log10(self.PMdict[key]['tau']))
                
                
        #get the unique Phase labels and generate checkboxes for them
        self.PhaseLabels = set(x for l in self.phaselabels_all for x in l)
        self.PhaseViewerBoxes = []
        for pl in self.PhaseLabels:
            self.PhaseViewerBoxes.append(
                ipy.Checkbox(value=False,
                             description=pl                    
                )
            )
        rows = int(np.ceil(np.sqrt(len(self.PhaseLabels))))
        cols = int(np.ceil(len(self.PhaseLabels)/rows))
        controls = ipy.HBox([ipy.VBox(self.PhaseViewerBoxes[cols*i : cols*(i+1)]) for i in range(rows)])
        
        self.ReplotButton = ipy.Button(description='Replot Phases')
        self.ReplotButton.on_click(self.ReplotPhases)
        
        controls = ipy.VBox([controls,self.ReplotButton])
        display(ipy.VBox([controls,output]))
        
    def ReplotPhases(self,change):
        self.PMapax.clear()
        current_phases = [act_ph.description for act_ph in self.PhaseViewerBoxes if act_ph.value]
        for idx, phase in enumerate(current_phases):
            voro_hull = []
            color = self.colors[idx]
            for pdx in range(len(self.Tmaxs_all)):
                if phase in list(self.phaselabels_all)[pdx]:
                    if self.CompSpread:
                        tau = self.logtaus_all[pdx]
                        Tmax = self.Tmaxs_all[pdx]
                        comp = self.Comp_all[pdx]
                        self.PMapax.scatter(tau,comp,Tmax,
                                            c=color,alpha=0.3,label=phase)
                        voro_hull.append([tau,Tmax,comp])
                    else:
                        tau = self.taus_all[pdx]
                        Tmax = self.Tmaxs_all[pdx]
                        self.PMapax.scatter(x=tau,y=Tmax,
                                           c=color,alpha=0.3,label=phase)
                        voro_hull.append([tau,Tmax])
                        
#             #calculate the voronoi hull for the labeled data
#             cvhull = scipy.spatial.ConvexHull(voro_hull)
#             cv_surf = []
#             for smplx in cvhull.simplices:
#                 if self.CompSpread:
#                     cv_surf.append([self.logtaus_all[smplx[0]],self.Comp_all[smplx[1]],self.Tmaxs_all[smplx[2]]])
#                 else:
#                     cv_surf.append([self.taus_all[smplx[0]],self.Tmaxs_all[smplx[1]]])
#                     self.PMapax.plot_surface(cv_surf[:,0],cv_surf[:,1],c=color,alpha=0.2)
            
#             cv_surf = np.array(cv_surf)
#             if self.CompSpread:
#                 self.PMapax.plot_surface(cv_surf[:,0],cv_surf[:,1],cv_surf[:,2],c=color,alpha=0.2)
#             else:
#                 self.PMapax.plot_surface(cv_surf[:,0],cv_surf[:,1],c=color,alpha=0.2)
            
        handles, labels = self.PMapax.get_legend_handles_labels()
        newLabels, newHandles = [], []
        for handle, label in zip(handles, labels):
            if label not in newLabels:
                newLabels.append(label)
                newHandles.append(handle)
        self.PMapax.legend(newHandles,newLabels,loc='upper left',bbox_to_anchor=(1,1.04))
        if self.CompSpread:
            self.PMapax.set_xlabel('processing time log10(\u03BCs)')
            self.PMapax.set_ylabel('Cation fraction')
            self.PMapax.set_zlabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
        else:
            self.PMapax.set_xlabel('procesing time (\u03BCs)')
            self.PMapax.set_ylabel('Tmax (\u00b0C)')
            self.PMapax.set_title(f'{self.DL.MatSysName} Phase Map')
            self.PMapax.set_xscale('symlog')