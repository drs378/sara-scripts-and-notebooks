import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys
import glob
import os

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-a',    '--auxpath',       type=str,               help="Absolute path to the plate level auxiliary")
    parser.add_argument('-fp',    '--filepath',     type=str,               help="path/of/the/h5_file.h5")
    parser.add_argument('-c',    '--composition',       type=str,               help="path/of/the/composition.csv")
    parser.add_argument('-r',    '--reflectance map',  type=str,            help="path/of/the/reflectancedata.txt") 
    args = parser.parse_args()
    return args



if __name__ == "__main__":
    args = parse()
    datapath = args.auxpath
    path2npys = sorted(glob.glob(f'{datapath}Stripes/*_map.npy'))
    path2Qs = sorted(glob.glob(f'{datapath}Stripes/*_q.npy'))
    path2shifts = sorted(glob.glob(f'{datapath}Stripes/*_offsets.npy'))

    plate = h5.File(args.filepath,'w')
    for idx in range(len(path2npys)):
        #data stored as .npys
        mf = path2npys[idx]
        qf = path2Qs[idx]
        sf = path2shifts[idx]
        
        xmap = np.load(mf,'r')
        q = np.load(qf,'r')
        
        bs = np.load(sf,'r')
        print(os.path.basename(mf))
        x, y, Tmax, tau = os.path.basename(mf).split('_')[1:-1]
        
        #plate level attributes
        cond = f'tau_{int(tau)}_T_{int(Tmax)}'
        clvl = f'exp/{cond}'
        plate.create_group(clvl)
        
        plate[clvl].attrs['x_center'] = x
        plate[clvl].attrs['y_center'] = y
        plate[clvl].attrs['dwell time'] = int(tau)
        plate[clvl].attrs['Tmax'] = int(Tmax)
        plate[clvl].attrs['xrd_beamshift'] = 0.5*(bs[2,1]-bs[1,1])
        
        #breaking down by scan index:
        for s in range(xmap.shape[0]):
            slvl = f'{clvl}/{s}/integrated_1d'
            plate.create_dataset(name=slvl,data=[q,xmap[s]],dtype='float32')
        
    plate.close()