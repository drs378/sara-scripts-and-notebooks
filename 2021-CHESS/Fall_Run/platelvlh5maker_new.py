import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys
import glob
import argparse
import os
# sys.path.insert(1,'/home/duncan/sara-scripts-and-notebooks/')
sys.path.insert(1,'/home/duncan/sara-socket-client/Scripts/')
from xrdCalibFit_new import PoniFit 
from Interpolate2D import *

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt or .csv file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-a',    '--auxpath',       type=str,               help="Absolute path to the plate level auxiliary")
    parser.add_argument('-fn',   '--filename',     type=str,               help="name of h5_file.h5")
    parser.add_argument('-c',    '--composition',       type=str,               help="path/of/the/composition.csv")
    parser.add_argument('-r',    '--reflectance_map',  type=str,            help="path/of/the/reflectancedata.txt")
    parser.add_argument('-p',    '--ponis',          type=str,               help="path to the poni files used to integrate")
    args = parser.parse_args()
    return args


class H5_builder(object):
    def __init__(self,auxpath,MatSysName):
        self.auxpath = auxpath
        self.MatSysName = MatSysName
        # self.ponis = sorted(glob.glob(os.path.join(os.path.join(os.path.join(auxpath,MatSysName),'PONIs'),"")+"*.poni")) #relative path to the poni files
        # print(self.ponis)
        # print(os.path.join(os.path.join(auxpath,MatSysName),'PONIs'))
        # self.pf = PoniFit(self.ponis)
        self.plate = None
        self.filename = f"{self.MatSysName}_all_1d"

    
        # instantiate the plate (load or make if not already)
        self.check_H5()
        # print(self.plate)

    def check_H5(self):
        """
        Makes the H5 file into the auxiliary file system
        """
        
        self.plate = h5.File(os.path.join(self.auxpath,self.MatSysName)+"/"+self.filename+'.h5','a')

    def add_StripeData(self, x=None, y=None, tau=None, Tmax=None, r=None, comp=None, q=None, xmap=None, Tprof=None):
        """
        Add the appropriate data for the scan 
        """
        try:
            self.check_H5()

        except:
            print("issue loading path")
            pass

        cond = f'tau_{int(tau)}_T_{int(Tmax)}'
        clvl = f'exp/{cond}'

        try:
            self.plate.create_group(clvl)

        except:
            pass

        print(self.plate[clvl])
        print(list(self.plate[clvl].attrs))
        self.plate[clvl].attrs['x_center'] = x
        self.plate[clvl].attrs['y_center'] = y
        self.plate[clvl].attrs['dwell time'] = int(tau)
        self.plate[clvl].attrs['Tmax'] = int(Tmax)
        self.plate[clvl].attrs['reflectance'] = r

        if comp:
            self.plate[clvl].attrs['composition'] = comp


        # self.plate[clvl].attrs['poni_params'] = self.pf.getpars((float(x),float(y)))
        self.plate[clvl].attrs['T_profile'] = Tprof
        
        #breaking down by scan index:
        for s in range(xmap.shape[0]):
            slvl = f'{clvl}/{s}/integrated_1d'
            try:
                self.plate.create_dataset(name=slvl,data=[q,xmap[s]],dtype='float32')
            except:
                data = self.plate[slvl]
                data = [q,xmap[s]]
        self.plate.close()

if __name__ == "__main__":
    # args = parse()
    # print(args)
    # daqpath = args.daqpath
    # auxpath = args.auxpath
    auxpath = '/home/duncan/zoo-fs/data/CHESS/2021-02-CHESS-3B/CHESS_AUX/'
    MatSysName = '15_MJF_Ti-Sn-O_N2'
    comps = np.genfromtxt(composition, delimiter=',', skip_header=1)
    r_map = '/home/duncan/Downloads/reflectance_map_use.csv'



    test_WM = "/home/duncan/Desktop/SARA_Analyzer_GUI/TestWaferMap.csv"


    #Instantiate H5 file
    all1d = H5_builder(auxpath,MatSysName)
    points, rs = CalibReader_2021(r_map)

    for row in np.genfromtxt(test_WM,delimiter=',',skip_header=1):
        print(row)
        x = row[0]
        y = row[1]
        tau = row[2]
        Tmax = row[3]
        pos = [x,y]

        r = interpolate2d(points, rs, pos, method = "linear", scaling = 1.2)
        comp = comps[:,2][(x==comps[:,0]) & (y==comps[:,1])]

        q = 0
        xmap = np.zeros((201,1))

        Tprof = np.zeros((1,201))

        all1d.add_StripeData(x=x, y=y, tau=tau, Tmax=Tmax, r=r, q=q, xmap=xmap, Tprof=Tprof)



    # args = parse()
    # print(args)
    # datapath = args.auxpath
    # path2npys = sorted(glob.glob(f'{datapath}/*_map.npy'))
    # path2Qs = sorted(glob.glob(f'{datapath}/*_q.npy'))
    # ponis = glob.glob(f'{args.ponis}/*.poni')
    # # path2shifts = sorted(glob.glob(f'{datapath}/*_offsets.npy'))
    # pf = PoniFit(ponis)

    # plate = h5.File(datapath+args.filename+'.h5','w')
    # for idx in range(len(path2npys)):
    #     #data stored as .npys
    #     mf = path2npys[idx]
    #     qf = path2Qs[idx]
    #     # sf = path2shifts[idx]
        
    #     xmap = np.load(mf,'r')
    #     q = np.load(qf,'r')
        
    #     # bs = np.load(sf,'r')
    #     print(os.path.basename(mf))
    #     x, y, Tmax, tau = os.path.basename(mf).split('_')[1:-1]
        
    #     #plate level attributes
    #     cond = f'tau_{int(tau)}_T_{int(Tmax)}'
    #     clvl = f'exp/{cond}'
    #     plate.create_group(clvl)
        
    #     plate[clvl].attrs['x_center'] = x
    #     plate[clvl].attrs['y_center'] = y
    #     plate[clvl].attrs['dwell time'] = int(tau)
    #     plate[clvl].attrs['Tmax'] = int(Tmax)
    #     # plate[clvl].attrs['xrd_beamshift'] = 0.5*(bs[2,1]-bs[1,1])
    #     plate[clvl].attrs['poni_params'] = pf.getpars((float(x),float(y)))
        
        
    #     #breaking down by scan index:
    #     for s in range(xmap.shape[0]):
    #         slvl = f'{clvl}/{s}/integrated_1d'
    #         plate.create_dataset(name=slvl,data=[q,xmap[s]],dtype='float32')
        
    # plate.close()
