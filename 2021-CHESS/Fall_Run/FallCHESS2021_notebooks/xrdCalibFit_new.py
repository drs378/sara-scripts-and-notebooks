import os
import numpy as np
from imageio import imread
from scipy.optimize import curve_fit
import pyFAI


def deltay(x: float, y: float):
    return (
        0.8744
        + 0.03109 * x
        - 0.02728 * y
        + 3.390e-4 * (x ** 2)
        + 5.196e-4 * (y ** 2)
        + 5.474e-4 * x * y
    )


def biquad(x, a, b, c, d, e, f):
    return a * x[0] ** 2 + b * x[0] + c * x[1] ** 2 + d * x[1] + e * x[0] * x[1] + f


def ptt(stripe_i, dbdf):
    dbi = [
        i
        for i, v in enumerate(dbdf["chess_xrd:directory"])
        if int(os.path.basename(v).split("_")[-1]) == stripe_i
    ][0]
    px = dbdf.iloc[[dbi]]["LSAjob:xcenter"].values[0]
    py = dbdf.iloc[[dbi]]["LSAjob:ycenter"].values[0]
    tau = dbdf.iloc[[dbi]]["LSAjob:dwell"].values[0]
    Tpeak = dbdf.iloc[[dbi]]["LSAjob:Tpeak"].values[0]
    return ((px, py), tau, Tpeak)


def automask(frame, complicated=False, thresh_scale=100):
    mask = np.zeros(frame.shape)
    if complicated:
        for i in range(2, frame.shape[0] - 2):
            for j in range(2, frame.shape[1] - 2):
                locmean = np.mean(frame[i - 2 : i + 2, j - 2 : j + 2])
                locstd = np.std(frame[i - 2 : i + 2, j - 2 : j + 2])
                if frame[i, j] - locmean > 3 * locstd:
                    mask[i, j] = 1
    else:
        a, b = np.where(frame > np.median(frame) * thresh_scale)
        for i, j in zip(a, b):
            mask[i, j] = 1
    return mask

def ZingerBGone(data,mini=0, maxi=9000, fill=0):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

def Mask(data, mini=0, maxi=9000, fill=1):
    """returns a mask for which pyFAI can use to integrate the data"""
    mask = np.zeros(data.shape)
    mask = np.where((mini<=data) & (data<maxi),mask,fill)
    return mask

xys = [
    (0, 0),
    (0, 40),
    (32, 30),
    (40, 0),
    (32, -30),
    (0, -40),
    (-32, -30),
    (-40, 0),
    (-32, 30),
] * 2

# Au pad location dict keyed by scan_{i}
xyd = {i + 1: v for i, v in enumerate(xys)}


class PoniFit():

    def __init__(self, ponis, xydict=xyd):
        self.geomd = {}
        for fn in ponis:
            geo = pyFAI.load(filename=fn)
            ponid = geo.get_config()
            self.geomd[xydict[int(os.path.basename(fn).split(".")[0].split("_")[-1])]] = ponid
        self.poptd = {}
        self.pars = ["dist", "poni1", "poni2", "rot1", "rot2", "rot3"]
        self.xys = list(self.geomd.keys())
        for p in self.pars:
            pvals = [self.geomd[xy][p] for xy in self.xys]
            xyarr = np.array(self.xys).T
            popt, pcov = curve_fit(biquad, xyarr, np.array(pvals))
            self.poptd[p] = popt

        deltays = []
        poni1s = []
        rot1s = []
        for k, d in self.geomd.items():
            deltays.append(deltay(*k))
            poni1s.append(d["poni1"])
            rot1s.append(d["rot1"])

        alpha = np.pi * 2 / 180
        d0, p10, p20, r10, r20, r30 = self.getpars((0, 0))
        beta = np.pi * (90 - 180 * (-1 * r20 / np.pi)) / 180
        deltad = lambda x, y: deltay(x, y) * np.sin(beta) / np.cos(alpha)
        getd = lambda x, y: (d0 * 1000 + deltad(x, y)) / 1000
        deltap2 = lambda x, y: deltay(x, y) * np.cos(beta) * np.cos(alpha)
        getp2 = lambda x, y: (p20 * 1000 + deltap2(x, y)) / 1000

        lf = lambda x, a, b: b + a * x
        p1opt, _ = curve_fit(lf, deltays, poni1s)
        r1opt, _ = curve_fit(lf, deltays, rot1s)
        getrot1 = lambda x, y: lf(deltay(x, y), *r1opt)
        getp1 = lambda x, y: lf(deltay(x, y), *p1opt)
        getpars2 = lambda xy: (getd(x, y), getp1(x, y), getp2(x, y), getrot1(x, y), r20, r30)
        
    def getpars(self, xy):
            return list([biquad(xy, *self.poptd[p]) for p in self.pars])

        
