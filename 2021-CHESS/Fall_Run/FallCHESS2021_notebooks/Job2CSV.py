import numpy as np
import os
import csv
def jf2csv(jobfile = '/path/to/file.job', output = None):
    file = open(jobfile, 'r')
    raw = ('').join([row for row in file])
    strbreak = raw.split('New_Zone')
    zones = strbreak[1:]
    fname = os.path.basename(jobfile)[:-4]


    outfile = open(f"{output}{fname}_test.csv", 'w')
    outfile = csv.writer(outfile, delimiter=',')
    outfile.writerow("x/mm y/mm tau/us Tmax/C Power/W")
    lines = []
    for z in zones:
        print("new Zone")
        xpos = None
        ypos = None
        tau = None
        Tmax = None
        power = None
        for row in z.split('\n'):
            # print(row)
            if "Zone.ID " in row:
                tau, Tmax = (row.split('=')[1]).split('_')
                tau = float(tau[2:])
                Tmax = float(Tmax[:-2])
                # print(tau)
                # print(Tmax)
            elif "Zone.Power " in row:
                power = float(row.split('=')[1])
                # print(power)
            elif "Zone.Xmin" in row:
                xmin = float(row.split('=')[1])
                # print(xmin)
            elif "Zone.Xmax" in row:
                xmax = float(row.split('=')[1])
                # print(xmax)
            elif "Zone.Ymin" in row:
                ymin = float(row.split('=')[1])
                # print(ymin)
            elif "Zone.Ymax" in row:
                ymax = float(row.split('=')[1])
                # print(ymax)

        xpos = 0.5*(xmax - xmin) + xmin
        ypos = 0.5*(ymax - ymin) + ymin


        newrow = [xpos,ypos,tau,Tmax,power]
        lines.append(newrow)
        
        print(newrow)
        # print('')


    outfile.writerows(lines)
if __name__ == '__main__':
    jf2csv("Fluorescent_Samples_JF_wafer.job","/home/dunca  n/Desktop/JobFiles/")