import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import csv
import signal
import glob
from cookb_signalsmooth import smooth
from scipy.optimize import curve_fit
import copy as cp
import cv2
from SARA_cornell_funcs import *

def jobfilereader(file):
    """ This function returns an array containing all the information 
    in the csv file of the wafer map
    
    output structure is usually
    [xpos(mm), ypos(mm), tau(us), Tpeak(*C), Power(W)]
    
    """
    f = open(file)
    raw = csv.reader(f,delimiter = ',')
#     for row in raw:
#         print(row)
    data = np.array([row for row in raw][1:],dtype=float)
    xpos = []
    ypos = []
    for x in data[:,0]:
        if x>=0:
            xpos.append('+'+str(int(x)).zfill(2))
        else:
            xpos.append(str(int(x)).zfill(3))
    for y in data[:,1]:
        if y>=0:
            ypos.append('+'+str(int(y)).zfill(2))
        else:
            ypos.append(str(int(y)).zfill(3))
            
    basenames = ['b'+xpos[i]+'_'+ypos[i]+'_+'+str(int(data[i,2])).zfill(5)+'_+'+str(int(data[i,3])).zfill(4)+'.bmp' for i in np.arange(len(data))]
    return data,basenames




def Gauss(x,A,sigxp,sigxn,mux):
    f = np.ones(x.shape)
    f[x>mux] = gp(x[x>mux],A,sigxp,mux)
    f[x<=mux] = gn(x[x<=mux],A,sigxn,mux)
    return f

def gp(x,A,sigxp,mux):
    z = A*np.exp(-0.5*((x-mux)/sigxp)**2)
    return z

def gn(x,A,sigxn,mux):
    z = A*np.exp(-0.5*((x-mux)/sigxn)**2)
    return z

def Gauss2D(x,y,A,sigxn,sigxp,sigy,mux,muy):
    f = cp.deepcopy(x)
    
    
    f[x>mux] = g2p(x[x>mux],y[x>mux],A,sigxp,sigy,mux,muy)
    f[x<=mux] = g2n(x[x<=mux],y[x<=mux],A,sigxn,sigy,mux,muy)
#     print('call')
    return f

def g2n(x,y,A,sigxn,sigy,mux,muy):
    z = A*np.exp(-0.5*(((x-mux)/sigxn)**2+((y-muy)/sigy)**2))
    return z

def g2p(x,y,A,sigxp,sigy,mux,muy):
    z = A*np.exp(-0.5*(((x-mux)/sigxp)**2+((y-muy)/sigy)**2))
    return z
    
#this just shows how it is asymmetric
def BiGaussian(X,Y,mu_x,mu_y,sig_x1,sig_x2,sig_y,A):
    x1 = X[X<mu_x]
    x2 = X[X>=mu_x]
    Gauss_x1 = Gaussian(x1,1,sig_x1,mu_x)
    Gauss_x2 = Gaussian(x2,1,sig_x2,mu_x)
    Gauss_x = np.concatenate((Gauss_x1,Gauss_x2))
    
    plt.plot(x1,Gauss_x1)
    plt.plot(x2,Gauss_x2)
    plt.plot(x,Gauss_x+1)
    plt.show()
    
    Gauss_y = Gaussian(Y,1,sig_y,mu_y)
    plt.plot(Y,Gauss_y)
    plt.show()
    return Gauss_x,Gauss_y

#this functions inputs are the 1:1 XX,YY mappings
def _G2D(M,*args):
    x, y = M
    arr = Gauss2D(x,y,*args)
    return arr

def RotoGauss(x,y,A,sigxn,sigxp,sigy,theta,mux,muy):
    f = cp.deepcopy(x)
    
    
    f[x>mux] = Rotog2p(x[x>mux],y[x>mux],A,sigxp,sigy,theta,mux,muy)
    f[x<=mux] = Rotog2n(x[x<=mux],y[x<=mux],A,sigxn,sigy,theta,mux,muy)

    return f

def Rotog2n(x,y,A,sigxn,sigy,theta,mux,muy):
    a = np.cos(theta)**2/(2*sigxn**2) + np.sin(theta)**2/(2*sigy**2)
    b = np.sin(2*theta)/(2*sigxn**2) - np.sin(2*theta)/(2*sigy**2)
    c = np.sin(theta)**2/(2*sigxn**2) + np.cos(theta)**2/(2*sigy**2)
    
    z = A*np.exp(-(a*(x-mux)**2+b*(x-mux)*(y-muy)+c*(y-muy)**2))
    return z

def Rotog2p(x,y,A,sigxp,sigy,theta,mux,muy):
    a = np.cos(theta)**2/(2*sigxp**2) + np.sin(theta)**2/(2*sigy**2)
    b = np.sin(2*theta)/(2*sigxp**2) - np.sin(2*theta)/(2*sigy**2)
    c = np.sin(theta)**2/(2*sigxp**2) + np.cos(theta)**2/(2*sigy**2)
    
    z = A*np.exp(-(a*(x-mux)**2+b*(x-mux)*(y-muy)+c*(y-muy)**2))
    return z

def _RotoG2D(M,*args):
    x, y = M
    arr = RotoGauss(x,y,*args)
    return arr


if __name__ == "__main__":
    filepath = '/home/vandover/Documents/LSA-BeamProfiles/2019-12-14/'
    Calib = {}
    Fitz = {}

    for file in glob.glob(filepath+'*.dat'):
        fn = os.path.basename(file)[:-4].split('_')[1]
        ypos = float(fn) 
        key = str(ypos*1000)
        f = open(file)
        raw = np.array([row.split() for row in f],dtype=float)

        I = smooth(raw[:,3])
        Calib[key] = I

    time = raw[:,0].astype(float)

    #the time vector is converted to distance in mm with the scan speed (100 mm/s)
    y = time*5-0.5*max(time*5)

    #The keys from the scan information contain the y position in mm.
    #This sorts and stores the x position array 
    keys = np.sort(np.array(list(Calib),dtype=float)).astype(str)
    x = keys.astype(float)/1000

    #The power distribution is the accumulation of the intensity profiles as a 2D array
    PD = []
    for key in keys:
        PD.append(Calib[key])
    PD = np.rot90(PD)

    #for whatever reason, the intensity is negative when recorded. 
    PowerDist = np.abs(PD)

    #Defining the mesh space over which the 2D gaussian is plotted
    XX,YY = np.meshgrid(x,y)

    Fitz['x'] = x
    Fitz['y'] = y
    Fitz['XX'] = XX
    Fitz['YY'] = YY

    #a good initial guess to the center of the gaussian/the mux and muy
    cx,cy= np.where(PowerDist==np.max(PowerDist))
    mu_x = x[cy[0]]
    mu_y = y[cx[0]]

    #raveling the 2D data into 1D vectors. Each pixels x and y location are stored
    Xdata = np.vstack((XX.ravel(), YY.ravel()))
    Power1D = PD.ravel()

    #initial guesses for optimization
    p0 = [np.min(PD),0.3,0.15,0.01,0,mu_x,mu_y]
    print(p0)
    #optimization over the parameters of the 2D gaussian
    popt, pcov = curve_fit(_RotoG2D,Xdata,Power1D,p0)

    print('popt contains:')
    print('Amplitude: sigx1:   sigx2:   sigy:   theta:   mux:   muy:')
    print(np.round(popt,decimals=4))
    Fitz['params'] = popt

    #Plugging the fit parameters into the fxn and getting the noramlized rms of the residuals
    PP_fit = RotoGauss(XX,YY,*popt)
    NRMSE = np.sqrt(np.mean((PD - PP_fit)**2))/(np.max(PD)-np.min(PD))
    print('NRMSE =', NRMSE)
    print('beam rotated by ',np.round(popt[4]*180/np.pi,2),' degrees')

    Fitz['NRMSE'] = NRMSE

    fig,ax = plt.subplots(2,1,constrained_layout=True,figsize=(4.6,7))
    fig.suptitle('CHESS 2019 Power Profile')
    ax[0].pcolormesh(XX,YY,PD,cmap='rainbow')
    ax[0].set_xlabel('x pos in mm')
    ax[0].set_ylabel('y pos in mm')

    ax[1].pcolormesh(XX,YY,PP_fit,cmap='rainbow')
    ax[1].set_xlabel('x pos in mm')
    ax[1].set_ylabel('y pos in mm')
    plt.show()
    plt.close(fig)

    PDsum_exp = np.sum(PowerDist,axis=0)
    PDsum_model = np.sum(PP_fit,axis=0)
    PDsum_exp = PDsum_exp-np.min(PDsum_exp)

    fit_psum,fitcov = curve_fit(Gauss,x,PDsum_exp,p0=[np.max(PDsum_exp),popt[1],popt[2],popt[5]])

    refit = Gauss(x,*fit_psum)

    print('sigp ',abs(fit_psum[1])*1000,' \u03bcm')
    print('sign ',abs(fit_psum[2])*1000,' \u03bcm')
    FWHM = 2*np.sqrt(2*np.log(2))*0.5*(abs(fit_psum[1])+abs(fit_psum[2]))


    plt.scatter(x,PDsum_exp,c='gold')
    plt.plot(x,PDsum_model,c='rebeccapurple')
    plt.plot(x,refit)
    plt.xlabel('Lateral Profile in mm ')
    plt.ylabel('Signal Intensity (a.u.)')
    plt.legend(['Modeled sum','refit','Measured sum'])
    plt.title('CHESS Lateral 2019 Gradient Profile')
    print('FWHM is ',FWHM*1000,' \u03bcm')
    plt.show()
    plt.close()