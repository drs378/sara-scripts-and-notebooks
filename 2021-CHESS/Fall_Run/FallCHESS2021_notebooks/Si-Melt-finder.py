import numpy as np
import matplotlilb.pyplot as plt 
import cv2 
import cook_signalsmooth
import glob
import os
import argparse

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-m',    '--mirror',       required=False,  type=str,      help="Absolute path to the mirror for normalization")
    parser.add_argument('-i',    '--imagespath', required=True, type=str,           help="Path to the images folder")
    # parser.add_argument('-o',    '--outpath',       required=True, type=str,        help="Path to store the gradient data")
    args = parser.parse_args()
    return args

args = parse()

m = np.sum(cv2.imread(args.mirror),axis=2)
m = m/np.max(m)
pxs = 1.46 #um/pixel
# imdict = {}
pos = []
grads = []
centers = []
widths = []
taus = []
Ps = []
for idx,file in enumerate(sorted(glob.glob(f'{args.i}/*.bmp'))):
    print(idx)
    fn = os.path.basename(file)[1:-4]
#     print(fn)
    xpos = int(fn.split('_')[0])
    ypos = int(fn.split('_')[1])
    tau = int(fn.split('_')[2])
    P = int(fn.split('_')[3])
    print(xpos,ypos,tau,P)
    pos.append([xpos,ypos])
    im = np.sum(cv2.imread(file),axis=2)
    im = im/np.max(im)
    im1 = im/m
#     im1 = ndimage.gaussian_filter(im/m,sigma=(15,15))
#     im2, _, _ = DustCollector(im1,binsize=2,plotting=False)

    s = cookb_signalsmooth.smooth(np.average(im1,axis=0),window_len=25)
    g = cookb_signalsmooth.smooth(np.abs(np.gradient(s,2)),window_len=50)
    g = g/np.max(g)
    grads.append(g)
#     print(f"gradient mu: {np.round(np.mean(g),2)} sig: {np.round(np.std(g),2)} var: {np.round(np.var(g),2)}")

    c1 = CenterFinding(g,sparam=1,plotting=False)
    centers.append(c1)    
    
    #attempt to deal with no xtallization yet
    try:
#         g_mu = np.mean(g)
#         g_std = np.std(g)
        peaks,_ = signal.find_peaks(g,height=(0.4))
#         print(peaks[:2])
        if len(peaks) == 2:
            d = pxs*(peaks[1]-peaks[0])
        elif (len(peaks) > 2) or (len(peaks) < 2):
            d = 0
    except:
        d = 0
        
    widths.append(d)
# np.save('grads.npy', np.array(grads))
# np.save('centers.npy', np.array(centers))
# np.save('pos.npy', np.array(pos))
# np.save('widths.npy', np.array(widths))
#     plt.figure()
#     plt.plot(s)
#     plt.plot(g)
#     plt.plot(Symmetrize(g,center=c1))
#     plt.axvline(x=c1,ymin=0.5,ymax=1,c='goldenrod')
#     for p in peaks:
#         plt.axvline(x=p,ymin=0,ymax=1,c='k',linestyle='--')
        
#     plt.title(f'width: {np.round(d,3)}')
#     plt.show()  
    