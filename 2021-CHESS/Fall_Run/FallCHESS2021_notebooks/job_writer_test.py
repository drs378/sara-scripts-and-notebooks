import numpy as np
import matplotlib.pyplot as plt
import sys
import os
sys.path.insert(1,"/home/duncan/sara-socket-client/Scripts/")
sys.path.insert(2,"/home/duncan/sara-socket-client/")
from Wafer import *


s = sampling()
s.tmin = 800.   #Minimum value of maximum temperature
s.tmax = 1440.  #Maximum value of maximum temperature
s.dmin = 250.   #Minimum dwell time
s.dmax = 10000. #Maximum dwell time
s.nd = 15 #Spacing in dwell space
s.nt = 15 #Spacing in temperature space

#Generate equally sampled mesh
cond = s.get_mesh()

#If we want to read a list of conditions from file
#cond = s.read_cond_csv("MyConditions.csv")
cond = s.shuffle_mesh(cond)

my_sample_id = "job2csv"

cl = wafer_sample_100(sample_id = f"{my_sample_id}")
#cl.missing = np.zeros((2,1))
#cl.missing[:, 0] = [  1111.,   1111.]
# pos = cl.get_layout_flattop()
pos = cl.get_layout_1()
# cl.plot_pos(pos)

pos = cl.shuffle_mesh(pos)

# power_cutoff = 76./2.5
# cond_wb, cond_nb = cl.split_based_on_power(cond, power_cutoff)
# print(np.sort(cl.cond_to_power(cond_wb)))
# print(np.sort(cl.cond_to_power(cond_nb)))

#First fill the wafer with all possible wide beam conditions
#wafermap_wb, posr_wb, condr_wb = cl.link_conditions_pos(pos,     cond_wb, dump = True, filetype = "json", power_scaling = 2.5, suffix = "WB")
#Now fill the wafer with all possible narrow beam conditions
# wafermap_nb, posr_nb, condr_nb = cl.link_conditions_pos(posr_wb, cond_nb, dump = True, filetype = "json", power_scaling = 1.0, suffix = "NB")
wafermap, posr, condr = cl.link_conditions_pos(pos, cond, dump = True, filetype = "json", power_scaling = 1.0)

#Sort wafermap according to power
wafermap_sorted = cl.sort_wafer_power(wafermap, reverse = False)

#Write conditions as csv tile to disk
cl.dump_wafer_csv(wafermap_sorted, my_sample_id + "_wafermap.csv")

#Check if I can read the csv file that I just wrote to disk
print(cl.read_wafer_csv(my_sample_id + "_wafermap.csv"))

jf = jobfile()
job, zones = jf.write_jobfile(my_sample_id + "_wafer.job", wafermap_sorted, cl, sample_id = my_sample_id)
