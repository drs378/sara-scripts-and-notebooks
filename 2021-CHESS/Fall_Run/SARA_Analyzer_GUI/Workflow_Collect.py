from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QRunnable, QObject, pyqtSlot, pyqtSignal
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from pathlib import Path
import os
import sys
sys.path.insert(1,'/home/duncan/sara-scripts-and-notebooks/')
sys.path.insert(1,'SA_scripts/')
sys.path.insert(1,'/home/duncan/sara-socket-client/CHESS2021')
sys.path.insert(1,'/home/duncan/sara-socket-client/Scripts/')
sys.path.insert(1,'/home/duncan/sara-socket-client/')
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import time
import logging
import logging.config
import socket_clients as sc
from xrd_client_chess2021 import *
from PySpecAgent import PySpecAgent
from LSAtoAnalysisZone import LSAtoAnalysis_Zone as lsa2xrd
from CondtoPath import condtopath, auxpath
#from queue import Queue
from multiprocessing import Process, Queue
from Interpolate2D import *
import pathlib
import glob
import gzip
from platelvlh5maker_new import H5Builder as H5B
from Mappers import TauT, WaferMap


QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

# The threader thread pulls a worker from the queue and processes it
def threader(q):
    while True:
        # gets a worker from the queue
        try:
            worker = q.get()
            print("WORKER", worker)
            if worker is None:
                break
            # Run the example job with the avail worker in queue (thread)
            integrate_data(worker[0], worker[1], worker[2], worker[3], worker[4], worker[5], worker[6], worker[7])
        except:
            break
        #completed with the job
        #q.task_done()
    return True

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"

def integrate_data(xrd_client, pos, cond, comp, offset, dirpath, xmin, xmax, plotpath, H5):
    #Collect and integrate the images from the server
    msg_id = 101
    map_info = xrd_client.get_XRD_GET_MAP_INFO(msg_id)

    ## send the request to the compute farm to integrate the data
    map_data, q_data = xrd_client.get_classe(msg_id, pos, cond, offset)

    ## Store the data in the building H5 file
    H5.add_StripeData(x=pos[0], y=pos[1], tau=cond[0], Tmax=cond[1], r=None, comp=comp, q=q_data, xmap=map_data, Tprof=None)

    ## Plotting for jaguar. Can store in the AUX too. 
    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(np.array(map_data).T, aspect='auto', extent=[xmin,xmax, np.max(q_data), np.min(q_data)])
    ax.set_xlabel('x position')
    ax.set_ylabel('Q in nm$^{-1}$')
#    plt.show(fig)
    print(dirpath.split("/"))
    fn_png = dirpath.split("/")[-2] + ".png"
    fn_png = os.path.join(plotpath, fn_png)
    print("Writing plot to", fn_png)
    plt.savefig(fn_png)






class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('WFC.ui', self) # Load the .ui file
        self.show() # Load the .ui file
        self.setWindowTitle('Workflow Collect')

        #Run Info Loading
        self.MatSysNameLineEdit = self.findChild(QtWidgets.QLineEdit,'MatSysNameLineEdit')
        self.MatSysNameLineEdit.editingFinished.connect(self.OnReturnMatSysName)
        self.MatSysNameLineEdit.setText('MatSysName')
        self.oldMatSysName = self.MatSysNameLineEdit.text()

        self.ActiveAction = self.findChild(QtWidgets.QTextBrowser,'ActiveAction')        
        self.DAQPathLineEdit = self.findChild(QtWidgets.QLineEdit,'DAQPathLineEdit')
        self.LoadDAQPathButton = self.findChild(QtWidgets.QPushButton,'LoadDAQPathButton')
        self.LoadDAQPathButton.clicked.connect(self.onClickedOpenFileNameDialogue_DAQ)

        self.AUXPathLineEdit = self.findChild(QtWidgets.QLineEdit,'AUXPathLineEdit')
        self.LoadAUXPathButton = self.findChild(QtWidgets.QPushButton,'LoadAUXPathButton')
        self.LoadAUXPathButton.clicked.connect(self.onClickedOpenFileNameDialogue_AUX)

        self.CompPathLineEdit = self.findChild(QtWidgets.QLineEdit,'CompPathLineEdit')
        self.LoadCompPathButton = self.findChild(QtWidgets.QPushButton,'LoadCompPathButton')
        self.LoadCompPathButton.clicked.connect(self.onClickedOpenFileNameDialogue_Composition)

        self.CollectImagesCheckBox = self.findChild(QtWidgets.QCheckBox,'CollectOpticalImagesCheckBox')

        #Measurement Offsets
        self.xOffsetDoubleSpinBox = self.findChild(QtWidgets.QDoubleSpinBox,'xOffsetSpinBox')
        self.xOffsetDoubleSpinBox.valueChanged.connect(self.onValueChangeUpdateXOffset)

        self.yOffsetDoubleSpinBox = self.findChild(QtWidgets.QDoubleSpinBox,'yOffsetSpinBox')
        self.yOffsetDoubleSpinBox.valueChanged.connect(self.onValueChangeUpdateYOffset)

        #Flyscan parameters
        self.FrameNumberSpinBox = self.findChild(QtWidgets.QSpinBox,'FrameNumberSpinBox')
        self.FrameNumberSpinBox.valueChanged.connect(self.onValueChangeUpdateFlyscanFrameNumber)

        self.IntegrationTimeSpinBox = self.findChild(QtWidgets.QSpinBox,'IntegrationTimeSpinBox')
        self.IntegrationTimeSpinBox.valueChanged.connect(self.onValueChangeUpdateFlyscanIntegrationTime)

        #Exhaustive data collection and LD Annealing inputs
        self.ReflectanceMapTxtLineEdit = self.findChild(QtWidgets.QLineEdit,'ReflectanceMapTxtLineEdit')
        self.LoadReflectanceMapTxtButton = self.findChild(QtWidgets.QPushButton,'LoadReflectanceMapTxtButton')
        self.LoadReflectanceMapTxtButton.clicked.connect(self.onClickedOpenFileNameDialogue_ReflMapTxt)

        self.JobFileLineEdit = self.findChild(QtWidgets.QLineEdit,'JobFileLineEdit')
        self.LoadJobFileButtonLoadJobFileButton = self.findChild(QtWidgets.QPushButton,'LoadJobFileButton')
        self.LoadJobFileButton.clicked.connect(self.onClickedOpenFileNameDialogue_JobFile)

        self.CSVWaferMapLineEdit = self.findChild(QtWidgets.QLineEdit,'CSVWaferMapLineEdit')
        self.LoadCSVWaferMapButton = self.findChild(QtWidgets.QPushButton,'LoadCSVWaferMapButton')
        self.LoadCSVWaferMapButton.clicked.connect(self.onClickedOpenFileNameDialogue_CSVWaferMap)


        #Visualization Options
        self.CurrentTauTMapButton = self.findChild(QtWidgets.QPushButton,'CurrentTauTMapButton')
        self.CurrentTauTMapButton.clicked.connect(self.run_tauT_thread)

        self.CurrentWaferMapButton = self.findChild(QtWidgets.QPushButton,'CurrentWaferMapButton')
        self.CurrentWaferMapButton.clicked.connect(self.run_WM_thread)

        #checklist
        self.OpticalAlignmentCheckBox = self.findChild(QtWidgets.QCheckBox,'OpticalAlignmentCheckBox')
        # self.VacuumOnCheckBox = self.findChild(QtWidgets.QCheckBox,'VacuumOnCheckBox')
        self.XRayAlignPadCheckBox = self.findChild(QtWidgets.QCheckBox,'XRayAlignPadCheckBox')
        self.XRayServerCheckBox = self.findChild(QtWidgets.QCheckBox,'XRayServerCheckBox')
        self.PONIsCheckBox = self.findChild(QtWidgets.QCheckBox,'PONIsCheckBox')
        self.YShiftsCheckBox = self.findChild(QtWidgets.QCheckBox,'YShiftsCheckBox')
        self.checklist = [
            # self.VacuumOnCheckBox,
            self.OpticalAlignmentCheckBox,
            self.XRayAlignPadCheckBox,
            self.XRayServerCheckBox,
            self.PONIsCheckBox,
            self.YShiftsCheckBox
        ]

        for cbox in self.checklist:
            print(cbox)
            cbox.clicked.connect(self.onClickCheckChecklist)

        self.threadpool = QtCore.QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())

        self.StartWorkflowButton = self.findChild(QtWidgets.QPushButton,'StartWorkflowButton')
        self.StartWorkflowButton.clicked.connect(self.run_main_thread)

        self.scan_data = {}
        self.scan_data['xpos'] = 0.0
        self.scan_data['ypos'] = 0.0
        self.scan_data['x_off'] = self.xOffsetSpinBox.value()
        self.scan_data['y_off'] = self.yOffsetSpinBox.value()
        self.scan_data['FlyscanFrameNumber'] = 201
        self.scan_data['FlyscanIntegrationTime'] = 50
        self.scan_data['MatSysName'] = self.MatSysNameLineEdit.text()
        self.scan_data['AUXPath'] = self.AUXPathLineEdit.text()
        self.scan_data['DAQPath'] = self.DAQPathLineEdit.text()
        self.scan_data['ReflMapFilePath'] = self.ReflectanceMapTxtLineEdit.text()
        self.scan_data['CompFilePath'] = self.CompPathLineEdit.text()
        self.scan_data['JobFilePath'] = self.JobFileLineEdit.text()
        self.scan_data['WaferMapPath'] = self.CSVWaferMapLineEdit.text()



    #Setting up functionaliy and Run info updates
    def OnReturnMatSysName(self): 
        self.ActiveAction.append(f'{self.get_time()}:\nnew Material System {self.MatSysNameLineEdit.text()}')
        if self.MatSysNameLineEdit.text() == self.oldMatSysName:
            self.StartWorkflowButton.setEnabled(False)
            self.ActiveAction.append('Please fill in new Mat Sys Name')
        self.onClickCheckChecklist()

    def onClickedOpenFileNameDialogue_DAQ(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        dirName = QtWidgets.QFileDialog.getExistingDirectory()
        if dirName != '':
            self.DAQPathLineEdit.setText(dirName)
            self.ActiveAction.append(f'{self.get_time()}:\nnew DAQ path {dirName}')
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nno change in path!')

    def onClickedOpenFileNameDialogue_AUX(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        dirName = QtWidgets.QFileDialog.getExistingDirectory()
        print(dirName)
        if dirName != '':
            self.AUXPathLineEdit.setText(dirName)
            self.ActiveAction.append(f'{self.get_time()}:\nnew auxiliary path {dirName}')
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nno change in path!')

    def onClickedOpenFileNameDialogue_Composition(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        # print(filename)
        if filename != '':
            self.CompPathLineEdit.setText(filename)
            self.ActiveAction.append(f'{self.get_time()}:\nnew composition file {filename}')
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nno change in file!')

    def onClickedOpenFileNameDialogue_ReflMapTxt(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        # print(filename)
        if filename != '':
            self.ReflectanceMapTxtLineEdit.setText(filename)
            self.ActiveAction.append(f'{self.get_time()}:\nnew Reflectance Map .txt file {filename}')
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nno change in file!')

    def onClickedOpenFileNameDialogue_JobFile(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        # print(filename)
        if filename != '':
            self.JobFileLineEdit.setText(filename)
            self.ActiveAction.append(f'{self.get_time()}:\nnew Laser Diode Job file {filename}')
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nno change in file!')

    def onClickedOpenFileNameDialogue_CSVWaferMap(self):
        """
        Opens a file selection dialog
        """
        options = QtWidgets.QFileDialog.Options()
        options = QtWidgets.QFileDialog.DontUseNativeDialog
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        # print(filename)
        if filename != '':
            self.CSVWaferMapLineEdit.setText(filename)
            self.ActiveAction.append(f'{self.get_time()}:\nnew WaferMap CSV {filename}')
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nno change in file!')

    def onValueChangeUpdateXOffset(self):
        """
        Updates the pre-requisite offsets for starting a data collection run or LD execution
        """
        xoff_new = self.xOffsetDoubleSpinBox.value()
        self.scan_data['x_off'] = xoff_new
        self.ActiveAction.append(f'{self.get_time()}:\nnew x offset: {xoff_new}')

    def onValueChangeUpdateYOffset(self):
        """
        Updates the pre-requisite offsets for starting a data collection run or LD execution
        """
        yoff_new = self.yOffsetDoubleSpinBox.value()
        self.scan_data['y_off'] = yoff_new
        self.ActiveAction.append(f'{self.get_time()}:\nnew y offset: {yoff_new}')

    def onValueChangeUpdateFlyscanFrameNumber(self):
        """
        Updates the Flyscan frame number. must type INT
        """
        frame_num = self.FrameNumberSpinBox.value()
        self.scan_data['FlyscanFrameNumber'] = frame_num
        self.ActiveAction.append(f'{self.get_time()}:\nFlyscan has {frame_num} number of frames')

    def onValueChangeUpdateFlyscanIntegrationTime(self):
        """
        Updates the Flyscan integration time in ms. must type INT in ms
        """
        int_time = self.IntegrationTimeSpinBox.value()
        self.scan_data['FlyscanIntegrationTime'] = int_time
        self.ActiveAction.append(f'{self.get_time()}:\nFlyscan has integration time of {int_time} ms')


    #Visualization functions
    def run_WM_thread(self):
        """
        Plots a wafer coordinate image of the x-ray data and the optical images
        """
        xwafmap = WaferMap()


    def run_tauT_thread(self):
        """
        If the data is on an even mesh, it will plot the x-ray data and optical images in log(tau) and increasing Tmax
        """



    #Safeguards to prevent data overwrite
    def onClickCheckChecklist(self):
        """
        perform checklist check. enable process button when ready
        """
        checklist_check = []
        for box in self.checklist:
            checklist_check.append(box.isChecked())

        if (not all(checklist_check)) or (self.MatSysNameLineEdit.text() == self.oldMatSysName):
            self.ActiveAction.append('check yourself before you wreck yourself')
            self.CollectPadsButton.setEnabled(False)
        else:
            self.ActiveAction.append(f'{self.get_time()}:\nrun enabled')
            self.CollectPadsButton.setEnabled(True)

    #general functions
    def get_time(self):
        return time.asctime(time.localtime(time.time()))



#---------------------------------------------------------------------------------------------------------------------------------------
#This brick of code is the executable and progress functions
#---------------------------------------------------------------------------------------------------------------------------------------
    #Workflow execution function
    def startWorkflow(self):
        """
        This starts the exhaustive workflow
        a LOT goes here
        """


        self.ActiveAction.append(f'{self.get_time()}:\nFinished Collecting Data {self.AUXPathLineEdit.text()}')
        for box in self.checklist:
            box.setChecked(False)
        self.StartWorkflowButton.setEnabled(False)
        self.oldMatSysName = self.MatSysNameLineEdit.text()

    def progress_fn(self, n):
        """
        progress callbacks go here
        """
        print('Done')

    def run_main_thread(self):
        """
        Executes the worker in a new thread
        """
        #Connect clients if not yet connected
        if self.model.PR.socket_clients is None:
            self.PushButtonConnect.click()

        # self.plot_window.show()
        worker = Worker(self.startWorkflow) # Any other args, kwargs are passed to the run function
        # worker.signals.result.connect(self.print_output)
        # worker.signals.finished.connect(self.thread_complete)
        worker.signals.progress.connect(self.progress_fn)
        # worker.signals.action.connect(self.startWorkflow)

        # Execute
        self.threadpool.start(worker)

    def print_output(self, s):
        """
        Prints final result returned by the worker thread
        """
        print(s)

    def thread_complete(self):
        """
        Triggered upon completion of the worker thread        
        """
        print("THREAD COMPLETE!")
        # self.groupBoxStatus.setTitle("Status: paused")



class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        `tuple` (exctype, value, traceback.format_exc() )

    result
        `object` data returned from processing, anything

    progress
        `int` indicating progress for inner/outer loops

    action
        `str` current status of the worker as a string
    '''
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)
    action = pyqtSignal(str)

class Worker(QRunnable):
    '''
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and 
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()    

        # Add the callback to our kwargs
        self.kwargs['progress_callback'] = self.signals.progress        
        self.kwargs['action_callback'] = self.signals.action     

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with passed args, kwargs.
        '''
        
        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
    window = Ui() # Create an instance of our class
    window.show()
    sys.exit(app.exec())
