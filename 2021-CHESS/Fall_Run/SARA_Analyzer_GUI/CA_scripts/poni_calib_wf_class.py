import sys
from pathlib import Path
import argparse
home = str(Path.home())
sys.path.insert(1, f'{home}/sara-socket-client/CHESS2021')
sys.path.insert(1, f'{home}/sara-socket-client/Scripts')
sys.path.insert(1, f'{home}/sara-socket-client')
sys.path.insert(1, f'{home}/sara-scripts-and-notebooks')
import yaml
import json
import os
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Qt5Agg')
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from PySpecAgent import PySpecAgent
from CondtoPath import condtopath, auxpath
from threading import Thread
import multiprocessing
from Interpolate2D import *
from h5tolinescan_class import ZingerBGone, LineScan 
import h5py as h5

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

# def threader(q):
#     while True:
#         # gets a worker from the queue
#         try:
#             worker = q.get()
#             print("WORKER", worker)
#             if worker is None:
#                 break
#             # Run the example job with the avail worker in queue (thread)
#             createLinescan(*worker)
#         except:
#             break
#         #completed with the job
#         #q.task_done()
#     return True

# def createLinescan(daqpath=None, auxpath=None, MatSysName=None):
#     I = []
#     LS = LineScan(daqpath,auxpath,MatSysName)
#     for frame in sorted(glob.glob(auxpath)):
#     I.append(LS.filtSum(frame))
#     LS.createOutPaths()
#     return I


class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"


class PadCollectionWf():
    def __init__(self,daqpath=None, auxpath=None, MatSysName=None, yshifts=None):
        self.daq = daqpath
        self.aux = auxpath
        self.MatSysName = MatSysName
        self.yshifts = yshifts

        self.wd = daq+MatSysName+'/'
        self.ad = aux+MatSysName+'/'

        # Alignment pad locations: 18 scans in total. 2 at each alignment pad one horizontal 'h-calib' and one vertical 'v-calib'
        self.AlignmentPadLocs  = np.array([
            #The vertical scans
            [+00., +00.],
            [+00., +40.],
            [+32., +30.],
            [+40., +00.],
            [+32., -30.],
            [+00., -40.],
            [-32., -30.],
            [-40., +00.],
            [-32., +30.],

            #The horizontal scans
            [+00., +00.],
            [+00., +40.],
            [+32., +30.],
            [+40., +00.],
            [+32., -30.],
            [+00., -40.],
            [-32., -30.],
            [-40., +00.],
            [-32., +30.],
        ])

        #Setup the SARA client info
        self.status = CurrentState()
        self.status.directory = wd

        #test the new yshift correction:
        if self.yshifts:
            self.points, self.values = CalibReader_2021(yshifts)

        # Create the queue and threader
        self.q = Queue()

        # how many threads are we going to allow for
        for x in range(1):
             #t = Thread(target=threader)
             self.t = Process(target=threader, args=[q])

             # classifying as a daemon, so they will die when the main dies
             self.t.daemon = True

             # begins, must come after daemon definition
             self.t.start()
    

        def connectToHardware(self):
            """
            Sets up the sockets for hardware and spec communication
            """
            self.address = "CHESS"
            self.socket_list = ["lasgo"] 
            self.socket_clients = sc.socket_clients()
            self.clients = self.socket_clients.get_clients(address, socket_list = socket_list)

            # Instantiate the laser connection
            self.laser = LSA_Laser.laser()
            self.laser.socket_clients = self.socket_clients

            #connect to the Spec Server
            self.psa = PySpecAgent()
            self.psa.ConnectToServer()

        def collectPads_and_integrate(self):
            """
            Alignment Pad data collection sequence
            """
            #iterate through each pad location
            for idx, padloc in enumerate(self.AlignmentPadLocs):
                #pass position info to SARA status 
                # print('cycle start')
                self.status.pos = (padloc[0],padloc[1])
                if self.yshifts:
                    self.yoffset = interpolate2d(points,values,self.status.pos,method='linear',scaling=1.2)
                else:
                    self.yoffset=0
                # print('Status Pos')
                # print(status.pos)
               
                _,calibpath = condtopath(self.status)
                # print('Calibration daq read/write path')
                # print(calibpath)
                _,auxcalib = auxpath(self.status,auxprefix=ad)
                # print('Calibration aux read/write path')
                # print(auxcalib)
                #iterate through the horizontal and vertical scans at each location and set the SARA status, flyscan coords, naming convention, and integration params
                if idx<9:
                #Horizontal alignment scans 2 mm long
                    padname = f'h-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
                    start = (self.status.pos[0] - 1.0 - 0.55, self.status.pos[1] + 1.3 + self.yoffset) #hscan start for the pad is -0.55 mm from pad origin and 1 mm before that 
                    end =   (self.status.pos[0] + 1.0 - 0.55, self.status.pos[1] + 1.3 + self.yoffset) #hscan end for the pad is -0.55 mm from pad origin and 1 mm past that 
                    print(padname)
                    print(start,end)
                #Set flyscan params
                    det_int_time = 50 #in milliseconds
                    nframes = 201 #int frames

                else:
                # Vertical alignment scans 7 mm long
                    padname = f'v-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
                    start = (self.status.pos[0] - 0.55, self.status.pos[1] - 3.5 + 1.3 + self.yoffset) #vscan start for the pad is 1.3 mm from pad origin and 2.5 mm before that 
                    end =   (self.status.pos[0] - 0.55, self.status.pos[1] + 3.5 + 1.3 + self.yoffset) #vscan end for the pad is 1.3 mm from pad origin and 2.5 mm past that 
                    print(padname)
                    print(start,end)
                #Set flyscan params
                    det_int_time = 50 #in milliseconds
                    nframes = 351 #int frames
                    print(det_int_time,nframes)
                
                    

                #Check for time to fill. if there is < current det_int_time*nframes + 10s (buffer), wait the durration of the fill + 15s
                ttf = self.psa.GetTimeToFill()
                iclow = self.psa.GetIntensity()

                # print(f'Time to fill: {ttf} seconds')
                while (ttf < det_int_time/1000*nframes+3) or (iclow < 1.6):
                    print(f'holding')
                    time.sleep(1)
                    ttf = self.psa.GetTimeToFill()
                    iclow = self.psa.GetIntensity()




                #Execution loop
                self.psa.SetFilePaths(calibpath,padname)

                #Spawn thread and look to the registered output for the "Armed" state
                print("Is it armed?", "Armed" in self.psa.Spec.reg_channels['output/tty'].read())
                time.sleep(4)
                process = Thread(target=self.psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
                process.start()
                while "Armed" not in self.psa.Spec.reg_channels['output/tty'].read():
                    print("Calling status", self.psa.Spec.reg_channels['output/tty'].read())
                    print("Not ready yet")


                #Code that executes the flyscan from lasgo
                self.laser.execute_flyscan(start, end, det_int_time, nframes)
                print("Calling status", self.psa.Spec.reg_channels['status/ready'].read())
                print("Waiting for my thread to finish")
                process.join()
                print("The PySpec thread has finished")


                #new code to check if files are written to DAQ in the appropriate folder:
                detstate = self.psa.Get_WO_status()
                while "Idle" not in detstate:
                    detstate = self.psa.Get_WO_status()
                    time.sleep(0.5)
                    pass

                #Rapid integerate/correlate (all functionality of h5 to linescan)
                pool = multiprocessing.Pool()
                inputs = sorted(glob.glob())
                output = pool.map_async()
