import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import glob
import sys
import os 
import gzip
import pathlib
import scipy.signal as signal
from PySpecAgent import PySpecAgent

#args in: dir start end
def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',   '--daqpath',    type=str,   help="Directory in the DAQ where raw alignment pad data is stored")
    # parser.add_argument('-se',      '--se',     type=float,      nargs='+',          help="tuple of positions for start and end in mm")
    parser.add_argument('-a',   '--auxpath',    type=str,   help="Directory in the AUX where the xI linescan data is stored")
    parser.add_argument('-p',   '--plots',      action='store_true',  help='will display the plots so one can see the correlated profiles and the resulting y-shift maps')
    parser.add_argument('-m',   '--MatSysName',    type=str,   help="Material System Name and ID")
    args = parser.parse_args()
    return args


def ZingerBGone(data,mini=0, maxi=9000, fill=1):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

def filtdata(data):
    """returns the data but with a min/max filter"""
    fdata = []
    for idx,d in enumerate(data):
        print(idx)
        fdata.append(ZingerBGone(d))
    return fdata

def linescan_extract(fdir='/path/to/h5s/', outdir='path/to/aux/', start=(1.0,0.0), end=(2.0,0.0), onescan=True, save=True):
    """returns the distance,Intensity array"""
    pathtodir = fdir
    fname = fdir.split('/')[-2]
    print(fname)
    print(start,end)
    # if start[0] == end[0]:
    #     start = start[1]
    #     end = end[1]
    # else:
    #     start = start[0]
    #     end = end [0]


    Is = []
    if onescan:
        files = sorted(glob.glob(fdir+'*.h5'))
        for f in files:
            if 'master' not in f:
                data = h5.File(f,'r')['entry']['data']['data'][0]
                I = np.sum(ZingerBGone(data))
                Is.append(I)

    else:
        files = glob.glob(fdir+'*.h5')
        for f in files:
            if 'master' not in f:
                data = h5.File(f,'r')['entry']['data']['data']
                print(data.shape)
                print(data)
                for frame in range(data.shape[0]):
                    print(frame)
                    I = np.sum(ZingerBGone(data[frame]))
                    Is.append(I)


    Is = np.array(Is)
    coord = np.linspace(start,end,len(Is))
    output = np.vstack((coord,Is)).T

    if save:
        np.save(f'{outdir}{fname}.npy',output)
    return output


class LineScan():
    def __init__(self, daqpath=None, auxpath=None, MatSysName=None,SpecClient=None):
        self.MatSysName = MatSysName
        self.daqpath = daqpath
        self.auxpath = auxpath
        self.spec = SpecClient
        self.xshift = []
        self.yshift = []
        self.outdir = os.path.join(self.auxpath,self.MatSysName)
        self.ls_out_path = None
        self.fname = None
        self.vscans = []
        self.hscans = []
        self.vpadlocs = []
        self.hpadlocs = []
        
        
        #2021 alignment pad profiles
        #h-scan span
        self.dx = 0.010
        self.pwfx_I = np.zeros(201)
        self.pwfx = np.linspace(-1.55,0.45,201)

        #h-scan main box shape 
        self.pwfx_I[(-1.05 < self.pwfx) & (self.pwfx < -.05)] = 1
        #h-scan center bar
        self.pwfx_I[(-0.005 < self.pwfx) & (self.pwfx < .005)] = 0.25

        #v-scan span
        self.dy = 0.02
        self.pwfy_I = np.zeros(351)
        self.pwfy = np.linspace(-3.5,3.5,351)

        #v-scan main box shape
        self.pwfy_I[(0.05 < self.pwfy) & (self.pwfy < 2.550)] = 1

        #v-scan crosshair
        self.pwfy_I[(-0.005 < self.pwfy) & (self.pwfy < 0.005)] = .25

        #v-scan PL alignment marks
        self.pwfy_I[(-1.7 <self.pwfy) & (self.pwfy < -1.4)] = 0.25
        self.pwfy_I[(-1.1 <self.pwfy) & (self.pwfy < -0.8)] = 0.25


    def createOutPaths(self):
        self.spec.mkdir_spec(self.outdir)
        # pathlib.Path(self.outdir).mkdir(parents=True, exist_ok=True)
        print(self.outdir)
        self.ls_out_path = os.path.join(self.outdir,'linescans')
        print(self.ls_out_path)
        # pathlib.Path(self.ls_out_path).mkdir(parents=True, exist_ok=True)
        self.spec.mkdir_spec(self.ls_out_path)


    def filtSum(self, frame):
        return np.sum(ZingerBGone(frame))


    def linescan_extract(self, inputs=None, outdir=None, start=(1.0,0.0), end=(2.0,0.0), onescan=True, save=True):
        """
        returns the distance,Intensity array
        inputs is a list of paths
        outdir is a single path
        """

        print(start,end)
        # if start[0] == end[0]:
        #     start = start[1]
        #     end = end[1]
        # else:
        #     start = start[0]
        #     end = end [0]


        Is = []
        if onescan:
            for f in inputs:
                print(f)
                self.fname = f.split('/')[-2]
                print(f'fname is {self.fname}')
                try:
                    f = gzip.open(f,'rb')
                except:
                    pass

                if 'master' not in f:
                    data = h5.File(f,'r')['entry']['data']['data'][0]
                    I = np.sum(ZingerBGone(data))
                    Is.append(I)

        else:
            for f in inputs:
                print(f)
                self.fname = f.split('/')[-2]
                print(f'fname is {self.fname}')
                try:
                    f = gzip.open(f,'rb')
                except:
                    pass

                if 'master' not in f:
                    data = h5.File(f,'r')['entry']['data']['data']
                    print(data.shape)
                    print(data)
                    for frame in range(data.shape[0]):
                        print(frame)
                        I = np.sum(ZingerBGone(data[frame]))
                        Is.append(I)


        Is = np.array(Is)
        coord = np.linspace(start,end,len(Is))
        output = np.vstack((coord,Is)).T

        if save:
            np.save(os.path.join(self.ls_out_path,f'{self.fname}.npy'),output)
        return output

    def lsWorkflow(self,inputs=None, padname=None):
        scanname = padname
        print(scanname)
        # scanname = self.fname
        scantype = scanname[0]
        xpad = float(scanname.split('_')[2])
        ypad = float(scanname.split('_')[3])
        print(scantype,xpad,ypad)
        if scantype == 'h':
            start = xpad - 1.55
            end = xpad + 0.45
            output = self.linescan_extract(inputs=inputs, outdir=self.ls_out_path, start=start, end=end)
            xI = output[:,1]
            xn = output[:,0]


            mean = np.mean(xI)
            pcorr = signal.correlate(xI-mean,self.pwfx_I[:len(xI)]-0.5,mode='same')
            corcenter = np.where(pcorr==max(pcorr))[0][0]
            shift = corcenter - 0.5*len(xI)

            print(f'xshift is {shift*self.dx} mm at ({xpad}, {ypad})')
            print('')
            self.xshift.append(shift*self.dx)
            self.hpadlocs.append([xpad,ypad])
            self.hscans.append(output)

        else:
            start = ypad - 3.5
            end = ypad + 3.5
            output = self.linescan_extract(inputs=inputs, outdir=self.ls_out_path, start=start, end=end)
            yI = output[:,1]
            yn = output[:,0]


            mean = np.mean(yI)
            pcorr = signal.correlate(yI-mean,self.pwfy_I[:len(yI)]-0.5,mode='same')
            corcenter = np.where(pcorr==max(pcorr))[0][0]
            shift = corcenter - 0.5*len(yI)

            print(f'xshift is {shift*self.dy} mm at ({xpad}, {ypad})')
            print('')
            self.yshift.append(shift*self.dy)

            self.vpadlocs.append([xpad,ypad])
            self.vscans.append(output)

        file = open(os.path.join(self.outdir,f'{self.MatSysName}_yshifts.txt'),'a')
        for idx in range(len(self.vscans)):
           file.write(f"{self.vpadlocs[idx][0]},{self.vpadlocs[idx][1]},{self.yshift[idx]}\n")


if __name__ == "__main__":
    args = parse()
    daq = args.daqpath
    aux = args.auxpath
    MatSysName = args.MatSysName
    plotting = args.plots

    outdir = f'{aux}{MatSysName}/linescans/'
    print(outdir)
    pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)
    if pathlib.Path(outdir).exists():
        print('made it!')
        # print('permissions set to all')
        # pathlib.Path(outdir).chmod(stat.S_IROTH | stat.S_IWOTH | stat.S_IXOTH)

    #construct the ideal h and v linescan profiles
    xshift = []
    yshift = []
    #h-scan span
    dx = 0.010
    pwfx_I = np.zeros(201)
    pwfx = np.linspace(-1.55,0.45,201)

    #h-scan main box shape 
    pwfx_I[(-1.05 < pwfx) & (pwfx < -.05)] = 1
    #h-scan center bar
    pwfx_I[(-0.005 < pwfx) & (pwfx < .005)] = 0.25

    #v-scan span
    dy = 0.02
    pwfy_I = np.zeros(351)
    pwfy = np.linspace(-3.5,3.5,351)

    #v-scan main box shape
    pwfy_I[(0.05 < pwfy) & (pwfy < 2.550)] = 1

    #v-scan crosshair
    pwfy_I[(-0.005 < pwfy) & (pwfy < 0.005)] = .25

    #v-scan PL alignment marks
    pwfy_I[(-1.7 <pwfy) & (pwfy < -1.4)] = 0.25
    pwfy_I[(-1.1 <pwfy) & (pwfy < -0.8)] = 0.25

    #data dumps
    vscans = []
    hscans = []
    vpadlocs = []
    hpadlocs = []
    # Basic workflow to write out linescans to the aux file
    for path in glob.glob(daq+'*/'):
        # print(path)
        h5s = sorted(glob.glob(path+'*.h5'))
        print(f'there are {len(h5s)} h5s in {os.path.basename(path[:-1])}')
        scanname = os.path.basename(path[:-1])
        scantype = scanname[0]
        xpad = float(scanname.split('_')[2])
        ypad = float(scanname.split('_')[3])

        if scantype == 'h':
            start = xpad - 1.55
            end = xpad + 0.45
            output = linescan_extract(fdir=path, outdir=outdir, start=start, end=end)
            xI = output[:,1]
            xn = output[:,0]


            mean = np.mean(xI)
            pcorr = signal.correlate(xI-mean,pwfx_I[:len(xI)]-0.5,mode='same')
            corcenter = np.where(pcorr==max(pcorr))[0][0]
            shift = corcenter - 0.5*len(xI)

            print(f'xshift is {shift*dx} mm at ({xpad}, {ypad})')
            print('')
            xshift.append(shift*dx)
            hpadlocs.append([xpad,ypad])
            hscans.append(output)

        else:
            start = ypad - 3.5
            end = ypad + 3.5
            output = linescan_extract(fdir=path, outdir=outdir, start=start, end=end)
            yI = output[:,1]
            yn = output[:,0]


            mean = np.mean(yI)
            pcorr = signal.correlate(yI-mean,pwfy_I[:len(yI)]-0.5,mode='same')
            corcenter = np.where(pcorr==max(pcorr))[0][0]
            shift = corcenter - 0.5*len(yI)

            print(f'xshift is {shift*dy} mm at ({xpad}, {ypad})')
            print('')
            yshift.append(shift*dy)

            vpadlocs.append([xpad,ypad])
            vscans.append(output)

        file = open(f'{aux}{MatSysName}/{MatSysName}_yshifts.txt','w')
        for idx in range(len(vscans)):
           file.write(f"{vpadlocs[idx][0]},{vpadlocs[idx][1]},{yshift[idx]}\n")