#!/usr/bin/env python3
import sys
from pathlib import Path
import argparse
home = str(Path.home())
sys.path.insert(1, f'{home}/sara-socket-client/CHESS2021')
sys.path.insert(1, f'{home}/sara-socket-client/Scripts')
sys.path.insert(1, f'{home}/sara-socket-client')
sys.path.insert(1, f'{home}/sara-scripts-and-notebooks')
import yaml
import json
import os
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from PySpecAgent import PySpecAgent
from CondtoPath import condtopath, auxpath
from threading import Thread
from Interpolate2D import *

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',    '--daqpath',      type=str,                help="Absolute path to the DAQ file system where spec writes to")
    parser.add_argument('-a',    '--auxpath',      type=str,                help="Absolute path to the Aux file system where all processed data should be written")
    parser.add_argument('-m',    '--matsysname',     type=str,              help="Wafer identifying ID ex: 01_20F7_Bi2O3")
    parser.add_argument('-y',    '--yshifts',       type=str,               help="txt file containing the yshift coordinates")
    args = parser.parse_args()
    return args

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"


# parse args and establish directories
args = parse()
print(args)
daq = args.daqpath
aux = args.auxpath
MatSysName = args.matsysname

wd = daq+MatSysName+'/'
ad = aux+MatSysName+'/'
print(f'DAQ dir: {daq}')
print(f'AUX dir: {aux}')
print(f'MatSysName: {MatSysName}')
print(f'working dir: {wd}')


#Setup the sockets
address = "CHESS"
socket_list = ["lasgo"] 
socket_clients = sc.socket_clients()
clients = socket_clients.get_clients(address, socket_list = socket_list)

# Instantiate the laser connection
laser = LSA_Laser.laser()
laser.socket_clients = socket_clients

#connect to the Spec Server
psa = PySpecAgent()
psa.ConnectToServer()

# Alignment pad locations: 18 scans in total. 2 at each alignment pad one horizontal 'h-calib' and one vertical 'v-calib'
AlignmentPadLocs  = np.array([
	#The vertical scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],

	#The horizontal scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],
])

print('')
#Setup the SARA client info
status = CurrentState()
status.directory = wd

#test the new yshift correction:
if args.yshifts:
    points, values = CalibReader_2021(args.yshifts)


# #Data collection cycle
for idx, padloc in enumerate(AlignmentPadLocs):
    #pass position info to SARA status 
    print('cycle start')
    status.pos = (padloc[0],padloc[1])
    if args.yshifts:
        yoffset = interpolate2d(points,values,status.pos,method='linear',scaling=1.2)
    else:
        yoffset=0
    print('Status Pos')
    print(status.pos)
   
    _,calibpath = condtopath(status)
    print('Calibration daq read/write path')
    print(calibpath)
    _,auxcalib = auxpath(status,auxprefix=ad)
    print('Calibration aux read/write path')
    print(auxcalib)
    #iterate through the horizontal and vertical scans at each location and set the SARA status, flyscan coords, naming convention, and integration params
    if idx<9:
	#Horizontal alignment scans 2 mm long
        padname = f'h-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
        start = (status.pos[0] - 1.0 - 0.55, status.pos[1] + 1.3 + yoffset) #hscan start for the pad is -0.55 mm from pad origin and 1 mm before that 
        end =   (status.pos[0] + 1.0 - 0.55, status.pos[1] + 1.3 + yoffset) #hscan end for the pad is -0.55 mm from pad origin and 1 mm past that 
        print(padname)
        print(start,end)
	#Set flyscan params
        det_int_time = 50 #in milliseconds
        nframes = 201 #int frames
        print(det_int_time,nframes)

        

    else:
	# Vertical alignment scans 7 mm long
        padname = f'v-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
        start = (status.pos[0] - 0.55, status.pos[1] - 3.5 + 1.3 + yoffset) #vscan start for the pad is 1.3 mm from pad origin and 2.5 mm before that 
        end =   (status.pos[0] - 0.55, status.pos[1] + 3.5 + 1.3 + yoffset) #vscan end for the pad is 1.3 mm from pad origin and 2.5 mm past that 
        print(padname)
        print(start,end)
	#Set flyscan params
        det_int_time = 50 #in milliseconds
        nframes = 351 #int frames
        print(det_int_time,nframes)
    
        

    #Check for time to fill. if there is < current det_int_time*nframes + 10s (buffer), wait the durration of the fill + 15s
    ttf = psa.GetTimeToFill()
    iclow = psa.GetIntensity()

    # print(f'Time to fill: {ttf} seconds')
    while (ttf < det_int_time/1000*nframes+3) or (iclow < 1.6):
        print(f'holding')
        time.sleep(1)
        ttf = psa.GetTimeToFill()
        iclow = psa.GetIntensity()




    #Execution loop
    psa.SetFilePaths(calibpath,padname)

    #Spawn thread and look to the registered output for the "Armed" state
    print("Is it armed?", "Armed" in psa.Spec.reg_channels['output/tty'].read())
    time.sleep(4)
    process = Thread(target=psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
    process.start()
    while "Armed" not in psa.Spec.reg_channels['output/tty'].read():
        print("Calling status", psa.Spec.reg_channels['output/tty'].read())
        print("Not ready yet")


#Code that executes the flyscan from lasgo
    laser.execute_flyscan(start, end, det_int_time, nframes)
    print("Calling status", psa.Spec.reg_channels['status/ready'].read())
    print("Waiting for my thread to finish")
    process.join()
    print("The PySpec thread has finished")


    #new code to check if files are written to DAQ in the appropriate folder:
    detstate = psa.Get_WO_status()
    while "Idle" not in detstate:
        detstate = psa.Get_WO_status()
        time.sleep(0.5)
        pass




# Duncan's linescan Code
#     #run the linescan integration
#     psa.mkdir_spec(ad)
#     print(f'auxdir is {ad}')
#     h5tols.linescan_extract(fdir=calibpath+padname+'_001', outdir=ad, start=start, end=end, onescan=True, save=False)
#     print('')
# print('Out of 2d-scans')
# print('')
# sys.path.insert(1,f'{home}/xrd-calibration/')
# from util import read_image
# from xrd_calibration import get_shift_using_curve_fit
# #Mings Code here to find centers and fit offset map
# directions = ['h','v']


# calib = {}
# for file in glob.glob(ad+'*/'):
#     print(file)
    #     images = read_image()
    #     shift = get_shift_using_curve_fit(images, plot=False)

    #     if direction == 'h':
    #         shift_arr[X] = shift*DISTANCE_PER_FRAME
    #     if direction == 'v':
    #         shift_arr[Y] = shift*DISTANCE_PER_FRAME
    # calib[(x,y)] = tuple(shift_arr)  # Could use named tuple if necessary
