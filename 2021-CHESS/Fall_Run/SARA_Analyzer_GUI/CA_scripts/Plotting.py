import numpy as np
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QRunnable, QObject, pyqtSlot, pyqtSignal
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import threading
from pathlib import Path
import os
import sys
import glob

class waferPlot(QtWidgets.QGraphicsView):
    def __init__(self):
        self.xspan = 100
        self.yspan = 100
        self.waferedge = None

    def outerRing(self):
        

class lineScanPlot(QtWidgets.QGraphicsView):
    def __init__(self):
        self.dx = 0.010
        self.pwfx_I = np.zeros(201)
        self.pwfx = np.linspace(-1.55,0.45,201)

        #h-scan main box shape 
        self.pwfx_I[(-1.05 < self.pwfx) & (self.pwfx < -.05)] = 1
        #h-scan center bar
        self.pwfx_I[(-0.005 < self.pwfx) & (self.pwfx < .005)] = 0.25

        #v-scan span
        self.dy = 0.02
        self.pwfy_I = np.zeros(351)
        self.pwfy = np.linspace(-3.5,3.5,351)

        #v-scan main box shape
        self.pwfy_I[(0.05 < self.pwfy) & (self.pwfy < 2.550)] = 1

        #v-scan crosshair
        self.pwfy_I[(-0.005 < self.pwfy) & (self.pwfy < 0.005)] = .25

        #v-scan PL alignment marks
        self.pwfy_I[(-1.7 <self.pwfy) & (self.pwfy < -1.4)] = 0.25
        self.pwfy_I[(-1.1 <self.pwfy) & (self.pwfy < -0.8)] = 0.25