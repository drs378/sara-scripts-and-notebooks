import os
from glob import glob
import numpy as np
import pandas as pd
from imageio import imread
import pyFAI
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
import sys

from xrdCalibFit import PoniFit, automask, ptt
import matplotlib.pyplot as plt

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"

class CHESS_XRD:
    """
    Class for XRD integration at CHESS

    while running on the XRD server, the wd should be the DAQ and the file structure that we choose to use for the run
    
    The nominal working directory should be:
    /nfs/chess/daq/2021-1/id3b/vandover/ 

    the compute farm can read directly from this and store the integrated data there too... Would like to add a 
    get_XRD_WRITE_TO_DAQ() function so SARA can save the already integrated 1D data
    """

    def __init__(self,wd,ponipath=None):
        """
        Initialize class with base sample directory
        """
        #This working directory will be used to maneuver to the correct calibration paths
        self.wd = wd

        # There wil be no dbscb, there will be onlny raw data and the alignment pad calibration data.
        if ponipath == None:
            self.ponis = glob(f"{self.wd}/Calibration/PONIs/*.poni")
        else:
            self.ponis = glob(f"{ponipath}*.poni")

        # this establishes the PONI map based on the 9 wafer points. done as a calibration step prior to
        # any active learning
        self.pf = PoniFit(self.ponis)
        # CHESS x-ray wavelength in meters; energy = 9.7 keV
        self.wl = 1.2781875567010312e-10
        # Building the detector dimensions for the integration
        self.eiger1M = self.build_detector()

    def build_detector(self, pixel1=75.0e-6, pixel2=75.0e-6, max_shape=(1028, 1062)):
        # Building the detector dimensions for the integration
        eiger1M = pyFAI.detectors.Detector(
            pixel1=pixel1, pixel2=pixel2, max_shape=max_shape
        )
        return eiger1M

    def get_q1d(self,pathstoraw):
        """
        The path to raw is just one locaiton where the raw 2D data is.
        """

        #Just returns the Q values, picked from the very first entry in the DB
        sidx = str(1).zfill(3)
        xycond_name = 
        #the path to all the 2D detector images for a stripe with the requested conditions. This can be where the raw chess data is input.
        images = self.get_tiffs(pathstoraw)
        Q1d, I1d, I2d, Q2d, chi2d, xmap = self.integrate_images(images, 0., 0.)
        return Q1d

    def integrate_all_images(self, images, x, y):
        """
        Integrates an entire set of images to build a 1D diffraction map. x and y are locations used to update the 
        pyFAI integrator
        """


        #Array for the integrated patterns to fall into
        xmap = []

        #build one azimuthal integrator per stripe. Has to be re-instantiated with each (x,y) position to make sure params are up to date
        ai = az(*self.pf.getpars((x,y)), detector=self.eiger1M, wavelength=self.wl)
        #integrating each of the paterns for the given xy conditions
        for img in images:
            print(img)
            print("Processing img")
        
            #adds a mask that Dan Guevarra wrote
            ai.mask = automask(img)
            print("Processing img 2")
        
            #returns the Q,I for 1D integration
            Q1d, I1d = ai.integrate1d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
            
            #returns the 2D Q,I,Chi data
            I2d, Q2d, chi2d = ai.integrate2d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
        
            #only storing the 1D data
            xmap.append(I1d)
        return Q1d, I1d, I2d, Q2d, chi2d, xmap 

    def condtopath(self, status, iteration=None):
        '''returns a path for the xrd data to be stored. Formatted based on SARA status object.
        ''' 
        #read in the status
        xpos, ypos  = status.pos
        tau, Tmax   = status.cond
        prefix      = self.wd

        #modify xpos, ypos, to be signed integers
        if xpos >= 0.:
            xpos = f'+{str(int(xpos)).zfill(2)}'
        else:
            xpos = f'-{str(int(abs(xpos))).zfill(2)}'

        if ypos >= 0.:
            ypos = f'+{str(int(ypos)).zfill(2)}'
        else:
            ypos = f'-{str(int(abs(ypos))).zfill(2)}'

        tau = f'+{str(int(tau)).zfill(5)}'
        Tmax = f'+{str(int(Tmax)).zfill(4)}'


        if iteration == None:
            scan_data = ['chess',xpos,ypos,tau,Tmax]
        else:
            scan_data = [str(iteration).zfill(3),xpos,ypos,tau,Tmax]

        StripeFolder = '_'.join(scan_data)

        dirpath = f'{prefix}/Stripes/{StripeFolder}/'
        calibpath = f'{prefix}/Calibration/PONIs/'
        
        return dirpath, calibpath

    def get_tiffs(self, pathstoraw):
        #integrating each of the paterns for the given xy conditions
        images = []
        for tiff in pathstoraw:
        
            #tracking integration number
            print("Reading", tiff)
        
            #loading the dp
            img = imread(tiff).T
            images.append(img)
        return images

    def get_map(self,   x_in = status.pos[0],
                        y_in  = status.pos[1],
                        x_offset = status.x_offset[0],
                        y_offset = status.y_offset[1],
                        tpeak = status.cond[0],
                        tau = status.cond[1],
                        complete_stripe=True,scan_coordinates=False,images=None):
        """
        Integrates an entire set of images to build a 1D diffraction map. x and y are locations used to update the 
        pyFAI integrator
        """

        #build file structure that the PySpecAgent wrote to
        if x_in >= 0.:
            x_in = f'+{str(int(x_in)).zfill(2)}'
        else:
            x_in = f'-{str(int(abs(x_in))).zfill(2)}'

        if y_in >= 0.:
            y_in = f'+{str(int(y_in)).zfill(2)}'
        else:
            y_in = f'-{str(int(abs(y_in))).zfill(2)}'

        tau = f'+{str(int(tau)).zfill(5)}'
        tpeak = f'+{str(int(tpeak)).zfill(4)}'

        StripeFolder = '_'.join(['chess',x_in,y_in,tau,tpeak])

        #path to the raw 2D images
        dirpath = f'{self.wd}/Stripes/{StripeFolder}/'
        tiffnames = glob.glob(dirpath+'*.tiff')

        #batch integration
        imagest = self.get_tiffs(tiffnames)
        x = x_in + x_offset
        y = y_in + y_offset

        xmap = []

        #build one azimuthal integrator per stripe. Has to be re-instantiated with each (x,y) position to make sure params are up to date
        ai = az(*self.pf.getpars((x,y)), detector=self.eiger1M, wavelength=self.wl)
        #integrating each of the paterns for the given xy conditions
        for img in images:
            print(img)
            print("Processing img")

            #adds a mask that Dan Guevarra wrote
            ai.mask = automask(img)
            print("Processing img 2")

            #returns the Q,I for 1D integration
            Q1d, I1d = ai.integrate1d(img, 1024, unit="q_nm^-1", method="csr", safe=True)
            
            # #returns the 2D Q,I,Chi data
            # I2d, Q2d, chi2d = ai.integrate2d(img, 1024, unit="q_nm^-1", method="csr", safe=True)

            #only storing the 1D data
            xmap.append(I1d)
        self.Q1d = Q1d
        return np.array(xmap),Q1d 






if __name__ == "__main__":

    DAQfs = '/nfs/chess/daq/2021-1/vandover/'    #working directory
    MatSysName = '01_21F7_Bi2O3'                #base MatSysName
    Stripe_iteration = 114                      #iteration number


    #The state is read 
    status = CurrentState()

    status.directory = DAQfs+MatSysName

    x, y = status.pos
    x_off, y_off = status.offset

    dirpath, calibpath = condtopath(status,iteration=Stripe_iteration)

    MyChess = CHESS_XRD(status.directory)
    Q = MyChess.get_q1d()

    #batch integration
    images = MyChess.get_tiffs(dirpath)
    xmap, Q = MyChess.integrate_images(images, x, y)


    #Converting to an array and transposing so the xaxis is position and the y axis is Q space
    xmap_plot = np.array(xmap).T
    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(xmap_plot,aspect='auto',extent=[0,xmap_plot.shape[0],np.max(Q),np.min(Q)])
    ax.set_xlabel('scan index')
    ax.set_ylabel('Q in nm$^{-1}$')
    plt.show(fig)
    plt.close(fig)
    #xmap, Q = MyChess.integrator(x_in = 6., y_in = 25., plotting = True)
    
    


status.xrd, status.Q = self.chess_int.get_map(x_in = status.pos[0],
                           y_in = status.pos[1],
                           x_offset = status.offset[0],
                           y_offset = status.offset[1],
                           tpeak = status.cond[0],
                           tau = status.cond[1],
                           complete_stripe = True,
                           scan_coordinates = True)


