import numpy as np
import matplotlib.pyplot as plt
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
import pyFAI
import sys
from pathlib import Path
home = Path.home()
sys.path.insert(1,f'{home}/sara-scripts-and-notebooks/')
from xrdCalibFit import PoniFit, automask, ptt
import h5py as h5
import glob
import argparse
import os
import time


def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',   '--daq',    type=str,   help="Directory in the DAQ where raw alignment pad data is stored")
    parser.add_argument('-p',   '--ponis',      type=str,  help='Director in the Aux where the PONIs are')
    parser.add_argument('-a',   '--aux',        type=str,   help="Directory where the integrated .npy arrays will be stored")
    args = parser.parse_args()
    return args


def Mask(data, mini=0, maxi=9000, fill=1):
    """returns a mask for which pyFAI can use to integrate the data"""
    mask = np.zeros(data.shape)
    mask = np.where((mini<=data) & (data<maxi),mask,fill)
    return mask

def ZingerBGone(data,mini=0, maxi=9000, fill=1):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data


if __name__ == "__main__":
    #parse paths
    args = parse()
    ponipath = args.ponis
    stripespath = args.daq
    auxpath = args.aux
    print(ponipath)
    print(stripespath)
    print(auxpath)
    print('')

    #create PONI map
    ponis = glob.glob(f'{ponipath}*.poni')
    pf = PoniFit(ponis)

    #instantiate the detector
    eiger1M = pyFAI.detectors.Detector(
            pixel1=75.0e-6, pixel2=75.0e-6, max_shape=(1065,1030)
        )
    
    stripe_list = list(Path(stripespath).iterdir())
    all_xrd = []
    all_q = []
    Path(auxpath).mkdir(exist_ok=True)
    #looped structure for sorting through the stripes
    for sdx, stripe in enumerate(stripe_list):
        # print(stripe)
        name = os.path.basename(stripe)
        print(f'{sdx}/{len(stripe_list)}')
        print(f'name is {name}')
        files = []
        xmap = []
        latestframe = sorted(list(Path(stripe).iterdir()))[-1]
        print(f'latest frame is {latestframe}')

        twodfiles = sorted(latestframe.glob('*.h5'))
        x, y, Tmax, tau = str(name).split('_')[1:]
        ai = az(*pf.getpars((float(x),float(y))), detector=eiger1M, wavelength=1.2781875567010312e-10)
        t1 = time.time()
        for idx, file in enumerate(twodfiles):
            files.append(file)
            if 'master' not in str(file):
                im = h5.File(file,'r')['entry']['data']['data'][0]

                good_im = h5.File(file,'r')['entry']['data']['data'][0]
                # good_im = ZingerBGone(good_im)
                ai.mask = Mask(good_im)
                Q, I1d = ai.integrate1d(good_im, 1024, unit="q_nm^-1", method="csr", safe=True)
                xmap.append(I1d)
        
        xmap = np.array(xmap)
        np.save(str(Path(auxpath))+'/'+name+'_map.npy',xmap)
        np.save(str(Path(auxpath))+'/'+name+'_q.npy',Q)
        t2 = time.time()

        print(f'Total integration and save time {t2-t1}')
        print('')

    # fig, ax = plt.subplots(len(stripe_list),1)
    # for i in range(len(stripe_list)):
    #     ax[i].imshow(all_xrd[i],aspect='auto',extent=[-1,1,max(all_q[i]),min(all_q[i])])
    
        