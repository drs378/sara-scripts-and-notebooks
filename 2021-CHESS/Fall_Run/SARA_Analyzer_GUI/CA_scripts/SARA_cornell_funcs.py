import numpy as np
import tables
import matplotlib.pyplot as plt
import h5py  as h5
import sys
import os
import glob
from matplotlib.backends.backend_pdf import PdfPages
import csv
import copy as cp

import pandas as pd
import matplotlib.patches as patches
from scipy import optimize, signal
from skimage.measure import block_reduce
import subprocess
from scipy.special import wofz

from cookb_signalsmooth import *
from edge_finder import *

import cv2
import PIL.Image as Image
import math as mt
import matplotlib.cm as cm
from matplotlib import rc
import matplotlib.font_manager as font_manager
from scipy.spatial import distance
from scipy.signal import savgol_filter
from scipy.signal import blackmanharris
from scipy.interpolate import interp1d
from numpy.fft import rfft
from numpy import argmax, mean, diff, log
from scipy import ndimage


def AnnealStripeCenter(img,window =15,xray=False):
#This function will sample an image from bottom to top and find the symmetric point within it
#given a percentage of the image for the window.

#The returns are the index of the image where the symmetric point lies and the resulting images used for analysis



#Generates the starting index for the window based on the % input. The counter is to iterate until the window reaches the
# edge of the image if it is the optical data OR to the specified endpoint for the x-ray data
    if xray == False:
        lead_idx_start = int(np.ceil(window/100*img.shape[0])-1)
        counter = np.arange(0,(img.shape[0]-lead_idx_start),1)
    else:
        Brightest= int(np.where(img == np.max(img))[0])
        lead_idx_start = Brightest-30
        counter = np.arange(0,60,1)
#         print(lead_idx_start,min(counter),max(counter))

    Diff_img = []
    leftside_img = []
    rightside_img = []
    ImageWindow = []


    if xray == False:
        for i in counter:
            lead_idx = lead_idx_start+i
#             print(lead_idx)
            if (lead_idx_start) % 2 == 0:

                sub_img = img[i:(lead_idx),:]
                leftside = sub_img[0:int(lead_idx_start*0.5)-1,:] 
                rightside = sub_img[(int(lead_idx_start*0.5)+1):,:]
    #             print('it went even')
            else:
                sub_img = img[i:lead_idx,:]
                leftside = sub_img[0:int(np.ceil(lead_idx_start*0.5))-1,:]
                rightside = sub_img[int(np.ceil(lead_idx_start*0.5)):,:]       
    #             print('it went odd')
    

            if i+lead_idx<img.shape[0]:
                Diff_img.append((rightside-np.flip(leftside,axis=0))) 
                leftside_img.append(np.flip(leftside,axis=0))
                rightside_img.append(rightside)
                ImageWindow.append(sub_img)
                
                
    else:
        for i in counter:
            lead_idx = lead_idx_start+i
            sub_img_start = lead_idx-40
            sub_img_end = lead_idx+40

            sub_img = img[sub_img_start:sub_img_end,:]
            leftside = sub_img[0:39,:] 
            rightside = sub_img[41:,:]
#             print(sub_img_start,sub_img_end,len(sub_img))
#             print(lead_idx,img.shape[0])
            if sub_img_end<img.shape[0]:
                Diff_img.append((rightside-np.flip(leftside,axis=0))) 
                leftside_img.append(np.flip(leftside,axis=0))
                rightside_img.append(rightside)
                ImageWindow.append(sub_img)
            

    
    lin_Diff_img = []
    Dif_min = []

    for i in np.arange(len(Diff_img)):
        lin_Diff_img.append(Diff_img[i].sum(axis=0))
        Dif_min.append(np.max(abs(lin_Diff_img[i][:-100])))
    
#     plt.figure()
#     plt.plot(Dif_min)
    minval = np.min(Dif_min)
    
    
    
    Diff_img = np.array(Diff_img)
    leftside_img = np.array(leftside_img)
    rightside_img = np.array(rightside_img)
    ImageWindow = np.array(ImageWindow)
    
    if xray == True:
        center_idx = int(Dif_min.index(minval)+lead_idx_start)
        
    else:
                        
        center_idx = int(Dif_min.index(minval)+np.ceil(lead_idx_start*0.5))
    return center_idx,leftside_img,rightside_img,Diff_img,ImageWindow



#Helper functions
def fft_smoothing(d,param):
#d is a 1D vector, param is the cutoff frequency
    rft = np.fft.rfft(d)
    rft[int(param):] = 0.   
    d = np.fft.irfft(rft,len(d))
    return d

#Find LSA center
def get_center(data,plotting=False,ROI_search=False):
#Using the cosine method
#     maximum = 1.
#     for i in range(int(round((data.shape[1])*0.35)),int(round((data.shape[1])*0.65))):
#         delta = min(i,data.shape[1]-i)
#         dl = data[:,i-delta:i]
#         dr = data[:,i:i+delta]
#         norm = 0.
#         for j in range(dl.shape[1]):
#             norm += distance.cosine(dl[:,-j] , dr[:,j])
#         norm = norm/dl.shape[1]
#         if norm < maximum:
#             maximum = norm
#             imaximum = i
#Using the correlation method
    im1 = np.array(data[:,:])
    im = cp.deepcopy(im1)
    center = []
    weights = []
    weights_max = []
    corr = []
    mid = im.shape[1]

    for i in range(im1.shape[0]):
        row = im1[i,:]
        result = np.array(row)-np.mean(np.array(row))
        result = ndimage.correlate(fft_smoothing(result,15),np.flip(fft_smoothing(result,15),0), mode='wrap')
        if ROI_search==True:
            result = fft_smoothing(np.array(result),15.)[int(round(mid-0.5*mid)):int(round(mid+0.5*mid))]
        else:
            result = fft_smoothing(np.array(result),15.)
        im[i,:] = result
        center.append(np.argmax(result))
        corr.append(result)
        weights.append(sum(np.abs(im1[i,:])))
        weights_max.append(np.amax(result))
    corr = np.array(corr)
    smooth = []
    for i in range(corr.shape[1]):
        smooth.append(np.mean(corr[:,i]))
#         print(np.mean(corr[:,i]))
#         for j in range(corr.shape[0]):
#             print(corr[j,i], end=" ")
#         print(" ")
    smd = fft_smoothing(np.array(smooth),15.)
#     fig, ax = plt.subplots()
#     for i in corr:
#         ax.plot(i)
#     plt.show()
    #for i in range(corr.shape[1]-1):
    #    print(smooth[i], smd[i])
    center = np.array(center)
    weights_max = np.power(np.array(weights_max),8)
    # imaximum_conv = (len(result)-1.)*0.5 + (np.average(center,weights=weights)-(len(result)-1.)*0.5)*0.5
    #imaximum_conv = (len(result)-1.)*0.5 + (np.average(center)-(len(result)-1.)*0.5)*0.5
    imaximum_conv = (len(result))*0.5 + (np.average(center,weights=weights_max)-(len(result))*0.5)*0.5
    if plotting==True:
        plt.figure()
        plt.imshow(corr,aspect='auto')
        plt.axvline(np.average(center,weights=weights_max),c='gold')
        plt.show()

        plt.figure()
        plt.plot(smd)
        plt.axvline(np.average(center,weights=weights_max),c='gold')
        plt.show()



    return int(np.round(imaximum_conv)), imaximum_conv #imaximum excluded because it was throwing error

#function to round to nearest known composition
def myround(x, base=5):
    return int(base * round(float(x)/base))


#Maxamillian's Code-a-doodle-dandy
#Get data from file, filter it, and normalize it, original data is still
#eturned in data
def get_spects(fn_data, fn_mirror, fn_blank,s_param=15):
    # s_param = 15
    newversion = False
    #Parse header and metadata to see if it is the new or old file format
    meta = {}
    with gzopen(fn_data) as f:
        cnt = 0
        for line in f:
            if line.startswith('/'):
                if "correction" in line:
                    newversion = True
            cnt += 1
    f.close()
    if newversion:
#         print ("Reading new file format")
        with gzopen(fn_data) as f:
            cnt = 0
            for line in f:
                if line.startswith('/'):
                    if "Center" in line:
                        meta['scan center'] = [float(i) for i in line.strip().split()[-1].replace("(", "").replace(")", "").split(',')]
                    if "Range" in line:
                        l = line.strip().split()[-2:]
                        l = [elem.replace("(","").replace(")","") for elem in l]
                        meta['delta start'] = [float(i) for i in l[0].split(',')]
                        meta['delta end']   = [float(i) for i in l[1].split(',')]
                    if "Scan lines" in line:
                        meta['points']      = int(line.strip().split()[-1])
                cnt += 1
        f.close()
        data = np.genfromtxt(fn_data, dtype=float, delimiter=',', skip_header=17)
        if "avg" in fn_mirror:
            mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',', skip_header=1) #Mike calls it ref
        else:
            mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',', skip_header=13) #Mike calls it ref
        if "avg" in fn_blank:
            blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',', skip_header=1)
        else:
            blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',', skip_header=13)

    #Parse information from the filename
        fn_meta = fn_data.split("_")
    #The last part is the temperature in C
        meta["temp"] = float(fn_meta[-1].split(".")[0])
    #The second last part is the temperature in dwell time in microsec
        meta["dwell"] = float(fn_meta[-2])
    #We can check the coordinates with the ones already read from the comment block
        dy = float(fn_meta[-3])
        dx = float(re.sub('[^0-9,+,-]','',fn_meta[-4].split("/")[-1]))
        # if np.linalg.norm(np.array([dx,dy])-np.array(meta['scan center'])) > 0.0001:
        #     print ("The coordinate from the metadata and the filename disagree")
            # sys.exit(1)
    #eThe new data is already normalized... maybe?
    #    for i in range(data.shape[1]-1):
    #        data[:,i+1] = fft_smoothing(data[:,i+1],s_param)
    #    normal_data = data
    else:
#Old format of files for reading
#         print ("Reading old file format")
        #Parse header and metadata
        meta = {}
        with gzopen(fn_data) as f:
            first_line = f.readline().strip().split(' ')
            first_line = [elem.replace("(","").replace(")","") for elem in first_line]
            meta['scan center'] = [float(i) for i in first_line[3].split(',')]
            meta['delta start'] = [float(i) for i in first_line[7].split(',')]
            meta['delta end']   = [float(i) for i in first_line[11].split(',')]
            meta['points']      = int(first_line[14])
        #print("reading ", fn_data)
        data = np.genfromtxt(fn_data, dtype=float, delimiter=',', skip_header=1)
        mirror = np.genfromtxt(fn_mirror, dtype=float, delimiter=',', skip_header=8) #Mike calls it ref
        blank = np.genfromtxt(fn_blank, dtype=float, delimiter=',', skip_header=8)

    #Getting rid of a dead pixel
    mirror[1388,1] = (mirror[1387,1]+mirror[1389,1])*0.5
    blank[1388,1]  = (blank[1387,1]+blank[1389,1])*0.5
    data[1388,1:]  = (data[1387,1:]+data[1389,1:])*0.5

    #Smooth data
    mirror[:,1] = fft_smoothing(mirror[:,1],s_param)
    blank[:,1] = fft_smoothing(blank[:,1],s_param)
    for i in range(data.shape[1]-1):
        data[:,i+1] = fft_smoothing(data[:,i+1],s_param)

    #Normalization
    norm = np.maximum(1.,mirror[:,1]-blank[:,1])
    normal_data = cp.deepcopy(data)
    normal_data[:,1:] = (np.maximum(data[:,1:],1.)-blank[:,1].reshape(-1,1))/norm.reshape(-1,1)

    #Wavelengths
    wl = normal_data[:,0]

    #The resolution is always 10 microns by 10 microns
    meta['scan res']    = [10., 10.]


    return wl, data[:,1:], normal_data[:,1:], meta


import sympy
def Gaussian(x,background,height,position,FWHM) :
    std = FWHM/2*np.sqrt(2*np.log(2))

    return background+height*np.exp(-1*(x-position)**2/(2*std**2))

#This code is for the CO2 laser on Silicon. 

FWHM = 300 # in um defined by Bob Bells thesis results for the power distribution

# A laser power is determined based on the desired peak temperature and dwell from the following polynomial relationships
# We need this beacause the power of the laser is not readily accessible in the meta data of the experiment.

def LaserPower(tau, Temp):
    #this is what the peak temperature depends on
    root_tau = np.sqrt(tau)

    
    #these coefficients are tabulated and don't need to be changed    
    a0= np.array([-10.60597,      2.627598,      -0.04569063,     0.0002334397])
    a1= np.array([15.00736,     -0.4516194,      0.008374273,   -4.164741E-005])
    a2= np.array([-0.6096512,     0.05861906,    -0.0005016423,   8.149019E-007])
    a3= np.array([0.01521283,   -0.001040471,   -5.582083E-006,  1.073043E-007])
    a4= np.array([-0.0001454438,  8.095709E-006,  2.572534E-007, -2.423004E-009])
    
    a_coeffs = np.vstack((a0,a1,a2,a3,a4))
    
    b = [np.polyval(i,root_tau) for i in np.fliplr(a_coeffs)]

    power = sympy.symbols('power',real=True)
    peakpower = np.array(sympy.solve(b[0] + b[1]*power + b[2]*power**2 + b[3]*power**3 + b[4]*power**4-Temp,power),dtype=float)
    peakpower = peakpower[np.where(peakpower<120)]
    peakpower = peakpower[np.where(peakpower>0)]
    
    return peakpower #this is 4 roots, the second one (index 1) is the correct wattage

def LaserFWHM(T, tau):
    
    """This Function outputs a FWHM of the laser at a given peak temperature and dwell
    
    T must be in degrees Celcius and dwell must be in milliseconds
    output is a FWHM in (in \u03bcm)

    """
    
    kern = ((T-665.3)/128.04)
    FWHM = 559.5 - 77.93*scipy.special.erfc(kern) + 130.87*np.log10(tau)
    
    return FWHM

#This function takes the tau and power and returns the corresponding Temperature
def LSA_Temperature(tau,power):
    root_tau = np.sqrt(tau)
    a0= np.array([-10.60597,      2.627598,      -0.04569063,     0.0002334397])
    a1= np.array([15.00736,     -0.4516194,      0.008374273,   -4.164741*10**-5])
    a2= np.array([-0.6096512,     0.05861906,    -0.0005016423,   8.149019E-007])
    a3= np.array([0.01521283,   -0.001040471,   -5.582083E-006,  1.073043E-007])
    a4= np.array([-0.0001454438,  8.095709E-006,  2.572534E-007, -2.423004E-009])
    
    a_coeffs = np.vstack((a0,a1,a2,a3,a4))
    
    
    b = [np.polyval(i,root_tau) for i in np.fliplr(a_coeffs)]
    
    Temperature = b[0] + b[1]*power + b[2]*power**2 + b[3]*power**3 + b[4]*power**4
    
    return Temperature #this is one temperature


#example of how I have implemented the code
#given the supposed peak temperature for the laser anneal scan, I calculate the peak power. The power distribution
#is gaussian with a DEFINED FWHM of 660 um for the CO2 laser on silicon. The spatially resolved power can easily
#be determined then using T(power,dwell), one can calculate tbe temperature profile across the stripe. 

#Temperature profile
#     PeakPower = LaserPower(tau=Dwell,Temp=Temp)[1]
#     Power_Profile = gaussian(annealspace_Vector,background = 0, height =PeakPower,
#                               center=peaktempcenter,FWHM = 660)
#     Temperature_Profile = np.array([LSA_Temperature(tau=Dwell,power=j) for j in Power_Profile])

#Note: any and all spatial components need to be in microns

#Bug to be addressed: The FWHM of the laser will have a correction term to it for the purpose of getting all profiles consistent
#across the experimental conditions probed


def dwellsort(listofkeys,Temp = True):
    demkeys = listofkeys
    temp = []
    s=[]
    for i in demkeys:

        i = i.split('_')
        i[1] = i[1].zfill(5)
        i[3] = i[3].zfill(5)
        s.append(i)
    s = np.array(s)
#    for i in s[:,1]:
#        i = i.zfill(5)
#        temp.append(i)

#    s=np.insert(s,1,temp,axis=1)
#    temp = []
#    s = np.array(s)
#    s = np.delete(s,2,1)
    if Temp:
        dwellsorted = sorted(s,key=lambda x:(x[3],x[1]))
    else:
        dwellsorted = sorted(s,key=lambda x:(x[1],x[3]))
#    dwellsorted = sorted(dwellsorted,key=lambda x:x[1])


    for i in dwellsorted:
        i[1] = i[1].lstrip('0')
        i[3] = i[3].lstrip('0')
        i = np.hstack((i))
        i = i.tolist()
        b = '_'
        i = b.join(i)
        temp.append(i)

    demkeys = temp
    
    return demkeys

def DamageControl(data,lheight=0.01,lprom=0.01,FiltRange=(300,1200),smooth_param = 5,plotting=True):
    diff = []
    for opt in np.arange(data.shape[1]):
        diff.append(abs(max(data[FiltRange[0]:FiltRange[1],opt])-min(data[FiltRange[0]:FiltRange[1],opt])))
    diff = smooth(np.array(diff),5)
    der = abs(np.gradient(diff))
    
    
    
    peaks = signal.find_peaks(der,height=lheight,prominence=lprom)
    peak_arr=(peaks[1]['peak_heights'],peaks[0])
    peak_arr = np.vstack((peak_arr[0],peak_arr[1]))
    peak_arr = np.transpose(peak_arr)
    peak_arr = np.transpose(sorted(peak_arr,key=lambda x:x[0],reverse=True))
   
    if len(peak_arr[1])>0:
        RegionMarkers=[int(i) for i in peak_arr[1]][0:2]

        ROI = data[:,min(RegionMarkers):(max(RegionMarkers)+1)]    
        SmoothedROI = np.array([smooth(ROI[i,:],int(np.round(ROI.shape[1]/smooth_param,decimals=0))) for i in np.arange(len(ROI))])
        error = np.zeros(SmoothedROI.shape,dtype=float)
        Smoothed_Refl = np.array(data)
        Smoothed_Refl[:,min(RegionMarkers):(max(RegionMarkers)+1)] = SmoothedROI

        for x in np.arange(error.shape[0]):
            error[x] = np.array(SmoothedROI-ROI)[x]
        Apo_error = np.array(error[:,1:])
        x = np.abs(np.sum(Apo_error**8,axis=0))
        DamageMarkers = signal.find_peaks(x=x,prominence=(max(x)-min(x))/20,height=(max(x)-min(x))/20)[0]+1
    else:
        RegionMarkers = 'DNE'
        ROI = 'DNE'
        SmoothedROI = 'DNE'
        error = 'DNE'
        DamageMarkers = 'DNE'
        Apo_error = 'DNE'
        Smoothed_Refl = data


    #Plotting
    if plotting == True:
        fig2 = plt.figure()
        plt.plot(diff,'b')
        plt.plot(smooth(np.array(der),3),'r')
        plt.title('reflectance min vector')
        
        
        fig1, (ax1,ax2) = plt.subplots(1,2,constrained_layout=True)
        ax1.imshow(data,aspect='auto')
        for opt in [25,40,45,50,55,60,75]:
            ax2.plot(test[:,opt])
            
        for j in RegionMarkers:
            ax1.plot(j*np.ones(data.shape[0]),np.arange(data.shape[0]),'r')
#         fig1.suptitle('Anneal conditions ')
        
        fig3 = plt.figure()
        plt.imshow(ROI,aspect='auto')
    
    
    return RegionMarkers, ROI, SmoothedROI, error, DamageMarkers, Apo_error, Smoothed_Refl

def make_contact_sheet(montage_imgs,rowscols,photo_dimensions,margins,padding):
    """\
    Make a contact sheet from a group of filenames:

    fnames       A list of names of the image files
    
    ncols        Number of columns in the contact sheet
    nrows        Number of rows in the contact sheet
    photow       The width of the photo thumbs in pixels
    photoh       The height of the photo thumbs in pixels

    marl         The left margin in pixels
    mart         The top margin in pixels
    marr         The right margin in pixels
    marl         The left margin in pixels

    padding      The padding between images in pixels

    returns a PIL image object.
    """
    fnames = montage_imgs
    ncols,nrows = [i for i in rowscols]
    photow,photoh = [i for i in photo_dimensions]
    marl,mart,marr,marb = [i for i in margins]
    # Read in all images and resize appropriately
    imgs = [Image.open(fn).resize((photow,photoh)) for fn in fnames]

    # Calculate the size of the output image, based on the
    #  photo thumb sizes, margins, and padding
    marw = marl+marr
    marh = mart+ marb

    padw = (ncols-1)*padding
    padh = (nrows-1)*padding
    isize = (ncols*photow+marw+padw,nrows*photoh+marh+padh)

    # Create the new image. The background doesn't have to be white
    white = (255,255,255)
    inew = Image.new('RGB',isize,white)

    # Insert each thumb:
    for irow in range(nrows):
        for icol in range(ncols):
            left = marl + icol*(photow+padding)
            right = left + photow
            upper = mart + irow*(photoh+padding)
            lower = upper + photoh
            bbox = (left,upper,right,lower)
            try:
                img = imgs.pop(0)
            except:
                break
            inew.paste(img,bbox)
    return inew


def CropA(w, h, angle):
    import math
    """
    Given a rectangle of size wxh that has been rotated by 'angle' (in
    radians), computes the width and height of the largest possible
    axis-aligned rectangle (maximal area) within the rotated rectangle.
    """
    if w <= 0 or h <= 0:
        return 0,0

    width_is_longer = w >= h
    side_long, side_short = (w,h) if width_is_longer else (h,w)

    # since the solutions for angle, -angle and 180-angle are all the same,
    # if suffices to look at the first quadrant and the absolute values of sin,cos:
    sin_a, cos_a = abs(math.sin(angle)), abs(math.cos(angle))
    if side_short <= 2.*sin_a*cos_a*side_long or abs(sin_a-cos_a) < 1e-10:
        # half constrained case: two crop corners touch the longer side,
        #   the other two corners are on the mid-line parallel to the longer line
        x = 0.5*side_short
        wr,hr = (x/sin_a,x/cos_a) if width_is_longer else (x/cos_a,x/sin_a)
    else:
        # fully constrained case: crop touches all 4 sides
        cos_2a = cos_a*cos_a - sin_a*sin_a
        wr,hr = (w*cos_a - h*sin_a)/cos_2a, (h*cos_a - w*sin_a)/cos_2a

    return wr,hr

def MissingConditions(keys):
    demkeys = dwellsort(keys)
    grid = []
    blanks = []
    for key in demkeys:
        split = key.split('_')
        tau = split[1]
        T = split[3]
        grid.append([tau,T])
    conds = np.array(grid,dtype=float)
    uniqueTs = np.unique(conds[:,1])
    uniqueDs = np.unique(conds[:,0])
    for T in uniqueTs:
        empty_dwells = np.array(list(set(conds[conds[:,1]==T][:,0]).symmetric_difference(set(uniqueDs))))
        blank_keys=['tau_'+str(int(i))+'_T_'+str(int(T)) for i in empty_dwells]
        for key in blank_keys:
            blanks.append(key)
    return blanks

#defined Functions
def CenterFinding(signal,sparam,window_len=0,filt=False,Failsafe=0.35,plotting=False):
    """Locate the center of a symmetric signal by using auto correlation.
    
    signal = (array) 1D array consisting of the signal
    sparam = (int) smoothing parameter for the signal to help locate the center
    filt = (bool) will apply a window around the center to help with local maxima
    window_len = (int) the filter window in pixels
    Failsafe = (float) a percentage of error allowed for the found center about the middle of the signal
    Plotting = (bool) Troubleshooting center finding
    
    """
    ssig = ndimage.gaussian_filter1d(signal,sparam)
    mid = int(0.5*len(ssig))
    if filt == True:
        corr = ndimage.correlate(ssig,ssig[::-1],mode='wrap')[(mid-window_len):(mid+window_len)]
    else:
        corr = ndimage.correlate(ssig,ssig[::-1],mode='wrap')
    center = (len(ssig))*0.5 + (np.argmax(corr)-(len(corr))*0.5)*0.5
    if (center > (0.5+Failsafe)*len(ssig)) or (center < Failsafe*len(ssig)):
        center = int(np.round(0.5*len(ssig)))
    
    if plotting == True:
        plt.figure()
        plt.plot(signal,c='dodgerblue')
        plt.plot(ssig,c='goldenrod')
        plt.axvline(center,c='darkorchid')
        plt.show()
    return center    

def GradSigNormAndCenter(signal,pixelsize,sparam=0.5,CFon=True,plotting=False):
    pxs = pixelsize
    grad = signal
    if CFon==True:
        center_idx = CenterFinding(grad,sparam)
    else:
        center_idx = int(len(signal)*0.5)
    Cgrad = center_idx*pxs
    xgrad = np.linspace(0,pxs*len(grad),len(grad))

    SplinedFunction = interp1d(xgrad,grad,kind='cubic')
    if plotting == True:
        plt.plot(xgrad,grad)
        plt.axvline(Cgrad,c='goldenrod')
        plt.show()
        plt.close()
    return SplinedFunction,grad,xgrad,center_idx

def bestPC(x,fopt,fspec,shift_tol=20,plotting=False):
    #remove DC component
    opt = fopt(x)
    spec = fspec(x)
    optmod = opt-np.mean(opt) 
    specmod = spec-np.mean(spec)
    dx = x[1]-x[0]
    mid = int(np.round(0.5*(len(x))))
    
    PCs=[]
    search_window = np.linspace(-shift_tol,shift_tol,2*shift_tol+1)
    for mc in search_window:
    #correct allowed dimension
        xmod = x-mc*dx
        minx = max(min(x),min(xmod))
        maxx = min(max(x),max(xmod))
    #     print(minx,maxx)
        newx = np.round(np.linspace(minx,maxx,501),decimals=1)
    #     print(newx)

        nfopt = interp1d(newx,fopt(newx),kind='cubic')
        nfspec = interp1d(newx,spec,kind='cubic')

        PC=np.round(scipy.stats.pearsonr(nfopt(newx),nfspec(newx))[0],decimals=3)
        PCs.append(PC)
        
    mc = search_window[PCs==max(PCs)][0]
    
    xmod = x-mc*dx
    minx = max(min(x),min(xmod))
    maxx = min(max(x),max(xmod))
#     print(minx,maxx)
    newx = np.round(np.linspace(minx,maxx,501),decimals=1)
#     print(newx)

    nfopt = interp1d(newx,fopt(newx),kind='cubic')
    nfspec = interp1d(newx,spec,kind='cubic')
    PC = np.round(scipy.stats.pearsonr(nfopt(newx),nfspec(newx))[0],decimals=3)
    xshift=mc*dx
    
    # plotting to troubleshoot
    if plotting == True:
        fig,ax = plt.subplots(3,1,constrained_layout=True)
        ax[0].plot(x,optmod)
        ax[0].plot(x,specmod)
#         ax[1].plot(cor,'dodgerblue')
        ax[1].plot(search_window,PCs)
        ax[2].plot(newx,nfopt(newx))
        ax[2].plot(newx,nfspec(newx))
        
        plt.show()
        plt.close()
    return xshift, PC, newx, nfopt, nfspec

def bestSC(x,fopt,fspec,shift_tol=20,plotting=False):
    #remove DC component
    opt = fopt(x)
    spec = fspec(x)
    optmod = opt-np.mean(opt) 
    specmod = spec-np.mean(spec)
    dx = x[1]-x[0]
    mid = int(np.round(0.5*(len(x))))
    
    SCs=[]
    search_window = np.linspace(-shift_tol,shift_tol,2*shift_tol+1)
    for mc in search_window:
    #correct allowed dimension
        xmod = x-mc*dx
        minx = max(min(x),min(xmod))
        maxx = min(max(x),max(xmod))
    #     print(minx,maxx)
        newx = np.round(np.linspace(minx,maxx,501),decimals=1)
    #     print(newx)

        nfopt = interp1d(newx,fopt(newx),kind='cubic')
        nfspec = interp1d(newx,spec,kind='cubic')

        SC = np.round(scipy.stats.spearmanr(nfopt(newx),nfspec(newx))[0],decimals=3)
        SCs.append(SC)
        
    mc = search_window[SCs==max(SCs)][0]
    
    xmod = x-mc*dx
    minx = max(min(x),min(xmod))
    maxx = min(max(x),max(xmod))
#     print(minx,maxx)
    newx = np.round(np.linspace(minx,maxx,501),decimals=1)
#     print(newx)

    nfopt = interp1d(newx,fopt(newx),kind='cubic')
    nfspec = interp1d(newx,spec,kind='cubic')
    SC = np.round(scipy.stats.spearmanr(nfopt(newx),nfspec(newx))[0],decimals=3)
    xshift=mc*dx
    
    # plotting to troubleshoot
    if plotting == True:
        fig,ax = plt.subplots(3,1,constrained_layout=True)
        ax[0].plot(x,optmod)
        ax[0].plot(x,specmod)
#         ax[1].plot(cor,'dodgerblue')
        ax[1].plot(search_window,SCs)
        ax[2].plot(newx,nfopt(newx))
        ax[2].plot(newx,nfspec(newx))
        
        plt.show()
        plt.close()
    return xshift, SC, newx, nfopt, nfspec

def get_N_HexCol(N=5):
    HSV_tuples = [(x * 1.0 / N, 0.5, 0.5) for x in range(N)]
    hex_out = []
    for rgb in HSV_tuples:
        rgb = map(lambda x: int(x * 255), colorsys.hsv_to_rgb(*rgb))
        hex_out.append('#%02x%02x%02x' % tuple(rgb))
    return hex_out



def Spectroscopy_Transiton_Finder(spec_map,cond='string',Gpromfilt=0,h_thresh=10,h_cutoff=12,pix_siz=10,s_param=2,plotting=True):
    TransitionStats = {}
    
    im = spec_map
    grad_im = []
    xpix = im.shape[1]
    for idx in np.arange(im.shape[0]):
        x = np.gradient(im[idx,:])
        x = np.sqrt(x*x)
        grad_im.append(x)
        
    grad_im = np.array(grad_im)
    grad = ndimage.gaussian_filter1d(np.sum(grad_im,axis=0),s_param)
    grad = grad/max(grad)
    
    correlation = ndimage.correlate(grad,grad[::-1],mode='wrap')
    spec_center = (len(correlation))*0.5 + (np.argmax(correlation)-(len(correlation))*0.5)*0.5
    
    a_sigma = abs((np.std(grad[0:h_cutoff])+np.std(grad[-h_cutoff:]))*0.5)
    a_mu = abs((np.mean(grad[0:h_cutoff])+np.mean(grad[-h_cutoff:]))*0.5)
    grad = grad-a_mu
    
    h_filt = h_thresh*a_sigma
#     print(a_mu,a_sigma,h_filt)
    
    spec_trans = signal.find_peaks(grad,prominence=Gpromfilt,width=[0.001*xpix,0.25*xpix],height=h_filt)
    
    TransitionStats['center'] = spec_center
    TransitionStats['TR_idx'] = spec_trans[0]
    TransitionStats['transitions'] = {}
    TransitionStats['transitions']['widths'] = spec_trans[1]['widths']
    TransitionStats['transitions']['FWHMs'] = signal.peak_widths(grad,spec_trans[0],rel_height=0.5)[0]
    TransitionStats['transitions']['heights'] = spec_trans[1]['peak_heights']
    TransitionStats['transitions']['prominences'] = spec_trans[1]['prominences']
    TransitionStats['transitions']['distance_from_center'] = (spec_trans[0]-spec_center)*pix_siz 
    TransitionStats['StripeCenterDistFromImageCenter'] = (spec_trans[0]-spec_center)*pix_siz-(int(0.5*im.shape[1])-spec_center)*pix_siz
    TransitionStats['horizontal FOV']= xpix*pix_siz
    TransitionStats['LSA_condition']= cond
    TransitionStats['gradient'] = grad
    if plotting==True:
        fig,ax = plt.subplots(2,1,constrained_layout=True)
        
        fig.suptitle(cond)
        ax[0].imshow(spec_map,aspect='auto')
        ax[0].axvline(spec_center,c='orchid')
        for tr in spec_trans[0]:
            ax[0].axvline(tr,c='goldenrod')
        ax[1].plot(grad)
        ax[1].axvline(spec_center,c='orchid')
        for tr in spec_trans[0]:
            ax[1].axvline(tr,c='goldenrod')
        
        plt.show()
        plt.close(fig)
    return TransitionStats

def xrd_transition_finder(xrd_map,cond='string',Gpromfilt=0,h_thresh=10,h_cutoff=12,pix_siz=10,s_param=2,plotting=True):
    TransitionStats = {}
    
    im = sharpen_y(xrd_map)
    grad_im = []
    xpix = im.shape[1]
    for idx in np.arange(im.shape[0]):
        x = np.gradient(im[idx,:])
        x = np.sqrt(x*x)
        grad_im.append(x)
        
    grad_im = np.array(grad_im)
    grad = ndimage.gaussian_filter1d(np.sum(grad_im,axis=0),s_param)
    grad = grad/max(grad)    
    
    correlation = ndimage.correlate(grad,grad[::-1],mode='wrap')
    xrd_center = (len(correlation))*0.5 + (np.argmax(correlation)-(len(correlation))*0.5)*0.5
    
    a_sigma = abs((np.std(grad[0:h_cutoff])+np.std(grad[-h_cutoff:]))*0.5)
    a_mu = abs((np.mean(grad[0:h_cutoff])+np.mean(grad[-h_cutoff:]))*0.5)
    grad = grad-a_mu
    
    h_filt = h_thresh*a_sigma
    
    
    xrd_trans = signal.find_peaks(grad,prominence=Gpromfilt,width=[0.001*xpix,0.25*xpix],height=h_filt)
    
    TransitionStats['center'] = xrd_center
    TransitionStats['TR_idx'] = xrd_trans[0]+1
    TransitionStats['transitions'] = {}
    TransitionStats['transitions']['widths'] = xrd_trans[1]['widths']
    TransitionStats['transitions']['FWHMs'] = signal.peak_widths(grad,xrd_trans[0],rel_height=0.5)[0]
    TransitionStats['transitions']['heights'] = xrd_trans[1]['peak_heights']
    TransitionStats['transitions']['prominences'] = xrd_trans[1]['prominences']
    TransitionStats['transitions']['distance_from_center'] = (xrd_trans[0]+1-xrd_center)*pix_siz 
    TransitionStats['StripeCenterDistFromImageCenter'] = (xrd_trans[0]+1-xrd_center)*pix_siz-(int(0.5*im.shape[1])-xrd_center)*pix_siz
    TransitionStats['horizontal FOV'] = xpix*pix_siz
    TransitionStats['LSA_condition'] = cond
    TransitionStats['gradient'] = grad
    if plotting==True:
        fig,ax = plt.subplots(2,1,constrained_layout=True)
        
        fig.suptitle(cond)
        ax[0].imshow(xrd_map,aspect='auto')
        ax[0].axvline(xrd_center,c='orchid')
        for tr in xrd_trans[0]:
            ax[0].axvline(tr,c='goldenrod')
        ax[1].plot(grad)
        ax[1].axvline(xrd_center,c='orchid')
        for tr in xrd_trans[0]:
            ax[1].axvline(tr,c='goldenrod')
        
        plt.show()
        plt.close(fig)
    return TransitionStats

def FpDict(pathtofiles,ftype='.csv',Xrays=False,json_friendly=False):
    """Returns a dictionary with condition key indexing

    The dictionary includes tau, T, x, y, the fp location and can be added into relatively easily.
    
    ToDo: add alpha numeric coordinate based on (x,y) location.
    """
    fpDict = {}
    if Xrays == False:
        for file in glob.glob(pathtofiles+'*'+ftype):
            if json_friendly:
                s = os.path.basename(file)[:-len(ftype)].split('_')
                key = f'tau_{int(s[2])}_T_{int(s[3])}'

                fpDict[key] = {}
                fpDict[key]['fp'] = file 
                fpDict[key]['tau'] = s[2]
                fpDict[key]['T'] = s[3]
                fpDict[key]['xpos'] = s[0][1:]
                fpDict[key]['ypos'] = s[1]

            else:
                s = os.path.basename(file)[:-len(ftype)].split('_')
                key = f'tau_{int(s[2])}_T_{int(s[3])}'

                fpDict[key] = {}
                fpDict[key]['fp'] = file 
                fpDict[key]['tau'] = int(s[2])
                fpDict[key]['T'] = int(s[3])
                fpDict[key]['xpos'] = int(s[0][1:])
                fpDict[key]['ypos'] = int(s[1])
    else:
        h5file = h5.File(pathtofiles,'r')
        xrd_exp_files = h5file['exp']

        for idx, key in enumerate(xrd_exp_files):
            if json_friendly:
                meta = xrd_exp_files[key]
                
                xpos,ypos = (meta.attrs['xw'],meta.attrs['yw'])
                
                fpDict[key] = {}
                fpDict[key]['fp'] = ['exp',key]
                fpDict[key]['tau'] = key.split('_')[1]
                fpDict[key]['T'] = key.split('_')[3]
                fpDict[key]['xpos'] = str(xpos)
                fpDict[key]['ypos'] = str(ypos)

            else:
                meta = xrd_exp_files[key]
                
                xpos,ypos = (meta.attrs['xw'],meta.attrs['yw'])
                
                fpDict[key] = {}
                fpDict[key]['fp'] = ['exp',key]
                fpDict[key]['tau'] = key.split('_')[1]
                fpDict[key]['T'] = key.split('_')[3]
                fpDict[key]['xpos'] = xpos
                fpDict[key]['ypos'] = ypos

    

    return fpDict

def DustCollector(im,binsize=20,exc_z=1,filt_crit=3,plotting=True):
    """DustCollector filters the image"""
    nbins = int(np.floor(im.shape[0]/binsize))
    emeans = []
    estds = []
    ROIs = []
    for i in np.linspace(exc_z,nbins-exc_z,nbins-2*exc_z,dtype=int):
    #     print(i)
        ROI_lower = i*binsize
        ROI_upper = (i+1)*binsize
        ROI_avg = np.average(im[ROI_lower:ROI_upper],axis=0)
        ROIs.append(ROI_avg)

        ensemble_mean = 0.5*(np.mean(im[:ROI_lower],axis=0) + np.mean(im[ROI_upper:],axis=0))
        ensemble_std = 0.5*(np.std(im[:ROI_lower],axis=0) + np.std(im[ROI_upper:],axis=0))
        emeans.append(ensemble_mean)
        estds.append(ensemble_std)

    ROIs = np.array(ROIs,dtype=float)
    emeans = np.array(emeans,dtype=float)
    estds = np.array(estds,dtype=float)
    diffs = []
    good = []
    bad = []
    for idx in np.arange(len(ROIs)):
        diff = abs(ROIs[idx]-emeans[idx])
        diffs.append(diff)
        if np.any(diff>filt_crit*estds[idx]):
            bad.append(ROIs[idx])
        else:
            good.append(ROIs[idx])
    good = np.array(good)
    bad = np.array(bad)
    diffs = np.array(diffs)
    if plotting:
        fig,ax = plt.subplots(4,1,figsize = (4,12))
        ax[0].imshow(im)
        ax[1].imshow(diffs,aspect='auto',label='all')
        if len(good)>0:
            ax[2].imshow(good,aspect='auto',label='good')
        if len(bad)>0:
            ax[3].imshow(bad,aspect='auto',label='bad')
        plt.show()
        plt.close(fig)

        fig,ax = plt.subplots(3,1)
        ax[0].plot(np.average(ROIs,axis=0))
        if len(good)>0:
            ax[1].plot(np.average(good,axis=0))
        if len(bad)>0:
            ax[2].plot(np.average(bad,axis=0))
        plt.show()
        plt.close(fig)
        
    return good,bad,diffs

def JitterCorrection(xmap):
    """This will remove some of the jitter in an XRD map due to the detector binning incorrectly
    The mean of each column (1D XRD scan) is collected into an array. the mean of the means is
    determined and a uniform shift is applied based on the differnece between the mean of a column
    and the overall mean so that they are all the same.
    
    Basically, the average intensity of each scan should be the same
    """
    temp = []
    newmap=[]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]
        temp.append(np.mean(col))
    meanofmeans = np.mean(temp)
    diffs = [mean-meanofmeans for mean in temp]
    for idx in np.arange(xmap.shape[1]):
        col = xmap[:,idx]-diffs[idx]
        newmap.append(col)
        
    return np.array(newmap).T


class TechStripe:
    def __init__(self):
        
        #Technique Yspans
        self.opt_yrange = (0,70)
        self.wl = []
        self.Q = []
        self.spm = []
        self.spb = []

        #Technique Maps
        self.omap = []
        self.smap = []
        self.xmap = []
        
        #Gradients of techniques
        self.ograd = []
        self.sgrad = []
        self.xgrad = []
        
        #Distane Across Stripes
        self.ospan = ()
        self.sspan = ()
        self.xspan = ()
        
        #Pixel Sizes
        self.O_pxs = 0.943
        self.S_pxs = 10
        self.X_pxs = 10
    
        #paths to raw data
        self.fpo = 'path/to/img'
        self.fprs = 'path/to/rawSpec'
        self.fpb = 'path/to/blank'
        self.fpm = 'path/to/mirror'
        self.fpImM = 'path/to/image/mirror'
    h5file = 'path/to/.h5'
        
    def Optmap(self,okey,weights=[0.33,0.33,0.33],grayscale=False,Norm=False,crop=(False,[300,500]),Average=True):
        """Loads the image of the stripe, converts to grayscale if desired with 
        a specified weighting [r,g,b]"""
        omap = cv2.imread(self.fpo)

        if grayscale==False:
            if Norm:
                mirror = cv2.imread(self.fpImM)
                mirror = np.transpose(np.array([[smooth(row,window_len=25) for row in mirror[:,:,j]] for j in np.arange(mirror.shape[2])]),axes=(1,2,0))
                omap = omap/mirror
            # print(omap.shape)
            if Average:
                omap = np.average(omap,axis=2,weights=weights)
                if crop[0]:
                    omap = np.array(omap[crop[1][0]:crop[1][1],:])

            else:
                if crop[0]:
                    omap = np.array(omap[crop[1][0]:crop[1][1],:,:])

            
        else:
            if Norm:
                mirror = cv2.imread(self.fpImM)
                omap = omap/mirror
            if crop[0]:
                    omap = np.array(omap[crop[1][0]:crop[1][1],:])

        self.opt_yrange = self.O_pxs*np.array([0,omap.shape[0]],dtype=float)
        self.omap = omap
        return omap
        
    
#     def plot(Tmap,span):
    def Specmap(self,okey,wlmin=450,wlmax=800,s_param=5):
        """The data is pretty much only good from 400 nm to 800 nm so that is 
        where the filter is appied ot the self.smap, the complete data can be called 
        upon with the output of the function"""
        wl, unfsmap, smap,meta = get_spects(self.fprs,self.fpm,self.fpb,s_param=s_param)
        # print([(wl>=wlmin) & (wl<= wlmax)][0])
        self.smap = smap[[(wl>=wlmin) & (wl<= wlmax)][0],:]
        self.wl = wl[(wl>=wlmin) & (wl<= wlmax)]
        spmfp = open(self.fpm)
        spbfp = open(self.fpb)

        self.spm  = np.genfromtxt(spmfp, dtype=float, delimiter=',', skip_header=13)
        self.spb  = np.genfromtxt(spbfp, dtype=float, delimiter=',', skip_header=13)
        return wl,unfsmap,smap[[(wl>=wlmin) & (wl<= wlmax)][0],:],meta
        

    def Xraymap(self,xkey,dpath=h5file,logscale=True):
        """This function generates the xrd map from the location in the .h5 file with the 
        appropriate key corresponding to the condition desired.
               
        if logscale is True(False), the data will be scaled appropriately.
        
        """
        AllXRD = h5.File(dpath,'r')
        substripescans = list(AllXRD['exp'][xkey].keys())
        substripescans.sort(key=int)
        data=[]
        for jdx, scan_num in enumerate(substripescans):
            Q,I = AllXRD['exp'][xkey][scan_num]['integrated_1d']

            if scan_num == '0':
                data = np.append(data, I, axis=0)
            else:
                data = np.vstack((data,I))
        
        data = data.T
        if logscale==True:
            if np.any(data<=1):
                data = data + abs(np.min(data)) + 1
            data = np.log10(data)
        
        data = JitterCorrection(data)
        self.xmap = data
        self.Q = Q


        return data
    
    def Grad(TechMap,bwind=15,bound_avg=False,norm=False):
        gradTechMap = np.array([GenGrad(signal=row,bwind=bwind,exc_z=3,bound_avg=bound_avg) for row in TechMap])
        grad1D = np.average(gradTechMap,axis=0)
        if norm:
            grad1D = grad1D/max(grad1D) 
        return grad1D,gradTechMap
#end of obj



def Grad(TechMap,bwind=15,exc_z=3,bound_avg=False,norm=False,plotting=False):
#     print(bound_avg)
    gradTechMap = np.array([GenGrad(signal=row,bkgd_sub=False,bwind=bwind,exc_z=exc_z,bound_avg=bound_avg) for row in TechMap])
    grad1D = np.sum(gradTechMap,axis=0)
    if norm:
        grad1D = grad1D-np.min(grad1D)
        grad1D = grad1D/max(grad1D)
    
    if plotting:
        fig,ax = plt.subplots(2,1,constrained_layout=True)
        ax[0].plot(grad1D_pn)
        ax[1].plot(grad1D)
        plt.show()
        plt.close()
    return grad1D,gradTechMap

def GenGrad(signal,bwind=15,exc_z=3,bound_avg=True,norm=False,bkgd_sub=True,plotting=False):
    if bkgd_sub:
        signal = LinearBS(signal)
    grad = np.gradient(signal,2)
    grad = abs(grad)
    grad1 = np.copy(grad)
    if bound_avg:
        Lfilt = np.array(grad[exc_z:bwind]) 
        Rfilt = np.array(grad[-bwind:-exc_z])
        mean = np.average(Lfilt)
        std = np.std(Lfilt)
        grad[:exc_z] = np.random.normal(loc=mean,scale=std,size=exc_z)
        
        mean = np.average(Rfilt)
        std = np.std(Rfilt)
        grad[-exc_z:] = np.random.normal(loc=mean,scale=std,size=exc_z)
        
    if norm:
        grad = grad/max(grad)
    
    if plotting:
        fig,ax = plt.subplots(3,1)
        ax[0].plot(signal)
        ax[1].plot(grad1)
        ax[2].plot(grad)
        plt.show()
        plt.close()
    return grad

def LinearBS(s):
    slope = (s[-1]-s[0])/len(s)
    bkg = np.arange(len(s))*slope
    news = s-bkg
    return news

def TransitionFinder(data,bkgd_ROI=[15,-15],sig=5,prom_filt=0.1,plotting=False):
    """Returns the indicies of the peaks in the signal

    data = (1D array) signal you want the peaks of
    bkgd_ROI = (int) the number of points on either end of the signal used to calculate the backgroudn
    sig = (int) the number of standard deviations above the background noise used to define a peak
    prom_filt = (float) the prominence filtering of the peak
    plotting = (bool) Troubleshooting plot
    """
    bkgd_mu = 0.5 * (np.mean(data[:bkgd_ROI[0]]) + np.mean(data[bkgd_ROI[1]:]))
    bkgd_std = 0.5 * (np.std(data[:bkgd_ROI[0]]) + np.std(data[bkgd_ROI[1]:]))
    hfilt = sig * bkgd_std
    wfilt = [0.001*len(data),0.25*len(data)]
    data = data - bkgd_mu
    
    
    peak_idx,stats = signal.find_peaks(data,height=hfilt,prominence=prom_filt,width=wfilt)
    
    if plotting:
        fig,ax = plt.subplots(1,1)
        ax.plot(data,label= 'data')
        for p in peak_idx:
            ax.axvline(p,c='goldenrod')
        ax.set_xlabel("signal index")
        ax.set_ylabel("signal intensity")
        ax.axvspan(0, bkgd_ROI[0], color='darkorchid', alpha=0.5, lw=1,label='left noise floor')
        ax.axvspan(len(data)+bkgd_ROI[1], len(data), color='darkorchid', alpha=0.5, lw=1,label='right noise floor')
#         ax.legend()
        
        
        plt.show()
        plt.close()
    
    return peak_idx, stats

def Symmetrize(data,center,plotting=False):
    """Return the symmetryized signal given the data and the center index
    data = (1D array) the data to be symmetrized
    center = (int) the index of the center value of the signal
    plotting = (bool) Troubleshooting plot
    """
    x = np.arange(len(data))
    x_l = x[x<center]
    x_r = x[x>center]
    
    d_l = data[x<center]
    d_r = data[x>center]
    
    #find which side is larger
    short = min(len(x_l),len(x_r))
    # print(abs(len(x_l)-len(x_r)))
    d_avg = 0.5*(d_l[-short:]+d_r[:short][::-1])
    
    center = int(round(center))
    sym = np.hstack((d_avg,data[center],d_avg[::-1]))
    if len(x_l)>len(x_r):
        extra = d_l[:abs(len(x_l)-len(x_r))]
        sym_data = np.hstack((extra,sym))
    elif len(x_l)<len(x_r):
        extra = d_r[-abs(len(x_l)-len(x_r)):]
        sym_data = np.hstack((sym,extra))
    elif len(x_l)==len(x_r):
        sym_data = sym
    
    sym_data = sym_data/max(sym_data)
    # print('shape check',len(sym_data),len(data))
    if plotting:
        fig,ax = plt.subplots(4,1,constrained_layout=True)
        #original data
        ax[0].plot(data)
        ax[0].axvline(center,c='goldenrod')
        
        ax[1].plot(x_l,d_l,c='b')
        ax[1].plot(x_r,d_r,c='r')
        
#         ax[2].plot(d_avg)
        ax[2].plot(sym,c='darkorchid')
        
        ax[3].plot(sym_data,c='k')
        plt.show()
        plt.close()
    
    return sym_data

def PhaseRegions(techmap,trs,plotting=False):
    """Returns the average signal of each measurement bounded by the transitions identified in each map
    techmap = (ndarray) the heatmap of the data
    trs = (list) a list of intergers corresponding to the indicies of the transformations
    plotting = (bool) Troubleshooting plot
    
    """
    avg_ROIs = []
    ROI_bounds = []
    
    bounded_trs = np.hstack((0,trs,techmap.shape[1]))
#     print(bounded_trs)
    for idx in np.arange(len(bounded_trs)-1):
#         print(bounded_trs[idx],bounded_trs[idx+1])
        ROI = techmap[:,bounded_trs[idx]:bounded_trs[idx+1]]
        avg_ROI = np.average(ROI,axis=1)
        ROI_bounds.append([bounded_trs[idx],bounded_trs[idx+1]])
        if plotting:
            fig,ax = plt.subplots(2,1)
            ax[0].imshow(ROI,aspect='auto')
            ax[1].plot(avg_ROI)
            plt.show()
            plt.close()
        avg_ROIs.append(avg_ROI)
    
    oneside = []
    
    for idx in np.arange((len(avg_ROIs)-1)/2,dtype=int):
#         print(idx)
        equal_ROI = 0.5*(avg_ROIs[idx]+avg_ROIs[-idx])
        oneside.append(equal_ROI)
#     print(int((len(avg_ROIs)-1)/2+1))
    oneside.append(avg_ROIs[int((len(avg_ROIs)-1)/2)])
    
    return oneside,avg_ROIs,ROI_bounds

def spectral_reconstruct(Q,peakpos,heights,widths,HWHM=0.01,norm=True,plotting=False):
    """
    A pseudo spectra of summed Voigt functions for the diffraction data. 
    
    Q : (arr) of the spanning Q values for the spectra
    peakpos: (arr) indicies of the peaks (not Q)
    heights: (arr) same shape as peakpos, but containing the heights
    widths: (arr) same shape as peakpos, but contains the widths of the peaks substituted into the FWHM argument
    
    returns an array of intensities of shape Q
    """
    pseudo_specs =[]
    for p,h,w in zip(peakpos,heights,widths):
#         print(p,h,w)
        w = w*(Q[1]-Q[0])
#         peakfunction = Voigt(x=Q,background=0,height=h,position=Q[p],alpha=w,gamma=HWHM)
        peakfunction = Gaussian(x=Q,background=0,height=h,position=Q[p],FWHM=w)
        pseudo_specs.append(peakfunction)
#         pseudo_specs.append(Lorentz(x=Q,background=0,height=h,position=Q[p],HWHM=0.5))
    singlespec = np.sum(pseudo_specs,axis=0)
    if plotting:
        fig,ax = plt.subplots(2,1,dpi=150)
        for spec in pseudo_specs:
            ax[0].plot(Q,spec)
        ax[1].set_xlabel('Q in nm$^{-1}$')
        ax[0].set_ylabel('Intensity (a.u.)')
        ax[1].plot(singlespec)
        plt.show()
        plt.close()
    return singlespec
def Voigt(x,background,height,position,alpha,gamma):
    """
    Return the Voigt line shape at x with Lorentzian component HWHM gamma
    and Gaussian component HWHM alpha.

    """
    sigma = alpha/np.sqrt(2*np.log(2))

    return background+height*np.real(wofz(((x-position)+1j*gamma)/sigma/np.sqrt(2)))/sigma/np.sqrt(2*np.pi)

def Lorentz(x,background,height,position,HWHM):
    """
    Returns a Lorenztian with the background, the height, the position and HWHM of the peaks
    """
    return background + height*(HWHM/np.pi/((x-position)**2+HWHM**2))

def Alphanumeric(x,y,xpitch=2,ypitch=5,origin_shift=(23,9)):
    """Given an (x,y) location on the wafer, return an alphanumeric grid mark
    
    Can shift the origin and pitches if necessary
    
    """
    number_list = np.linspace(1,45,45,dtype=int)
    letter_list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    number = number_list[int((origin_shift[0]*xpitch+x)/xpitch)-1]
    letter = letter_list[int((origin_shift[1]*ypitch-y)/ypitch)-1]
    return f'{letter}{number}'

def QtoTT(Q,wl=0.15418):
    """ Returns the two theta value given a scattering vector, Q, and an x-ray wavelength in nm"""
    return 2*np.rad2deg(np.arcsin(Q/(4*np.pi)*wl))

def TTtoQ(TT,wl=0.15418):
    """ Returns the scattering vector magnitued given a two theta value, TT, and an x-ray wavelength in nm"""
    return 4*np.pi*np.sin(np.deg2rad(TT/2))/wl

def CondToPos(db,tau=1000,Tmax=1000,xpos=0,ypos=0,cond2pos=True):
    """
    Takes the expected Tau and Tmax and returns the x and y positions or inverted
    also returns the stripe index and sub scan x positions


    """

    if cond2pos:
        idx = np.where((db['LSAjob:Tpeak']==Tmax)&(db['LSAjob:dwell']==tau))[0]
        # print(idx)
    else:
        idx = np.where((db['LSAjob:xcenter']==xpos)&(db['LSAjob:ycenter']==ypos))[0]
        # print(idx)
    xpos = float(db['Scan:xcenter'][idx])
    ypos = float(db['Scan:ycenter'][idx])
    tau = int(db['LSAjob:dwell'][idx])
    Tmax = int(db['LSAjob:Tpeak'][idx])

    scan_pos = np.linspace(float(db['LasGo:xmin'][idx]),
                           float(db['LasGo:xmax'][idx]),
                           int(db['chess_xrd:count'][idx]))
    stripe_idx = db['chess_xrd:directory'][idx].array[0][-3:].zfill(3)


    return (xpos,ypos),stripe_idx,scan_pos,tau,Tmax