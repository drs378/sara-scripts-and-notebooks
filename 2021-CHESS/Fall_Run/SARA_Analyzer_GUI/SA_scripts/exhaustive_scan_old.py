#!/usr/bin/env python3
import sys
from pathlib import Path
import argparse
home = str(Path.home())
sys.path.insert(1, '../Scripts/')
sys.path.insert(1, '../')
sys.path.insert(1, f'{home}/Homefolder/PDC/sara-scripts-and-notebooks/')
sys.path.insert(1, f'{home}/Homefolder/PDC/sara-scripts-and-notebooks/2021CHESS/')
import yaml
import json
import os
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
import csv
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from xrd_client_chess2021 import *
from PySpecAgent import PySpecAgent
from LSAtoAnalysisZone import LSAtoAnalysis_Zone as lsa2xrd
from CondtoPath import condtopath, auxpath
from threading import Thread
#from queue import Queue
from multiprocessing import Process, Queue
from Interpolate2D import *
import pathlib

def CalibReader_2021(path):
    """Extracts the (x,y) and z data from a .txt file"""
    raw = np.genfromtxt(path,delimiter=',',dtype=float)
    x = raw[:,0]
    y = raw[:,1]
    z = raw[:,2]
    points = []
    for xx, yy in zip(x, y):
        points.append([xx, yy])
    return points, z

# The threader thread pulls a worker from the queue and processes it
def threader(q):
    while True:
        # gets a worker from the queue
        try:
            worker = q.get()
            print("WORKER", worker)
            if worker is None:
                break
            # Run the example job with the avail worker in queue (thread)
            integrate_data(worker[0], worker[1], worker[2], worker[3], worker[4], worker[5], worker[6], worker[7])
        except:
            break
        #completed with the job
        #q.task_done()
    return True

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',    '--daqpath',   required=True,   type=str,                help="Absolute path to the DAQ file system where spec writes to")
#    parser.add_argument('-a',    '--auxpath',      type=str,                help="Absolute path to the Aux file system where all processed data should be written")
    parser.add_argument('-m',    '--matsysname', required=True,  type=str,              help="Wafer identifying ID ex: 01_20F7_Bi2O3")
    parser.add_argument('-w',    '--wafermap',   required=True,  type=str,              help="CSV file with the anneal conditions")
    parser.add_argument('-y',    '--yshift',     required=True,  type=str,              help="y-shift calibration file")
    parser.add_argument('-p',    '--plotpath',    type=str,              help="Directory to write the integrated png files to")
    args = parser.parse_args()
    return args

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"

def integrate_data(xrd_client, pos, cond, offset, dirpath, xmin, xmax, plotpath):
    #Collect and integrate the images from the server
    msg_id = 101
    map_info = xrd_client.get_XRD_GET_MAP_INFO(msg_id)
    map_data, q_data = xrd_client.get_classe(msg_id, pos, cond, offset)
    fig,ax = plt.subplots(1,1,dpi=150)
    ax.imshow(np.array(map_data).T, aspect='auto', extent=[xmin,xmax, np.max(q_data), np.min(q_data)])
    ax.set_xlabel('x position')
    ax.set_ylabel('Q in nm$^{-1}$')
#    plt.show(fig)
    print(dirpath.split("/"))
    fn_png = dirpath.split("/")[-2] + ".png"
    fn_png = os.path.join(plotpath, fn_png)
    print("Writing plot to", fn_png)
    plt.savefig(fn_png)


args = parse()
print(args)
#Get interpolated data
#filename_yshift = "FindTheBeam_yshifts.txt"
filename_yshift = args.yshift
points, values = CalibReader_2021(filename_yshift)
print(points, values)

#Check plotpath
if not args.plotpath:
    args.plotpath = "."
else:
    if not os.path.exists(args.plotpath):
        pathlib.Path(args.plotpath).mkdir(parents=True, exist_ok=True)

#Read wafermap
#filename_wafermap = "MnOx_CAL_wafermap.csv"
filename_wafermap = args.wafermap
line = 0
conds = []
poss = []
with open(filename_wafermap, newline='') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        if line > 0:
            poss.append([float(c) for c in row[0:2]])
            conds.append([float(c) for c in row[2:4]])
        line += 1

print("CSV file read")

# parse args and establish directories
daq = args.daqpath
#aux = args.auxpath
MatSysName = args.matsysname

wd = daq+MatSysName+'/'
#ad = aux+MatSysName+'/'
print(f'DAQ dir: {daq}')
#print(f'AUX dir: {aux}')
print(f'MatSysName: {MatSysName}')
print(f'working dir: {wd}')


#Setup the lasgo sockets
address = "CHESS"
socket_list = ["lasgo"] 
socket_clients = sc.socket_clients()
clients = socket_clients.get_clients(address, socket_list = socket_list)


#Setup xrd socket
xrd_address = "localhost"
xrd_client = ClientXRDProtocol(connect = True, address = xrd_address)


# Instantiate the laser connection
laser = LSA_Laser.laser()
laser.socket_clients = socket_clients

#connect to the Spec Server
psa = PySpecAgent()
psa.ConnectToServer()

#Setup the SARA client info, and set remote READING directory
status = CurrentState()
status.directory = wd
msg_id = 101
rcv = xrd_client.set_XRD_SET_DIR(msg_id, wd)

#Data collection cycle
default_offset = [0., 1.5] 
dstart = -1.0
dend = 1.0
det_int_time = 50 #In msec
nframes = 201

# Create the queue and threader
q = Queue()

# how many threads are we going to allow for
for x in range(1):
     #t = Thread(target=threader)
     t = Process(target=threader, args=[q])

     # classifying as a daemon, so they will die when the main dies
     t.daemon = True

     # begins, must come after daemon definition
     t.start()

counter = 0
#Loop over all conditions and positions on the wafer map
for pos, cond in zip(poss[:1], conds[:1]):

    counter += 1
    status.pos = pos
    status.cond = cond
    #Get offset:
    #y_offset = interpolate2d(pos)
    fpt = pos
    y_offset = interpolate2d(points, values, fpt, method = "linear", scaling = 1.2)
    offset = cp.deepcopy(default_offset)
    offset[1] += y_offset 
    start = cp.deepcopy(pos)
    start[0] += dstart
    start[0] += default_offset[0]
    start[1] += default_offset[1] + y_offset 
    end = cp.deepcopy(pos)
    end[0] += dend
    end[0] += default_offset[0] 
    end[1] += default_offset[1] + y_offset

    #Set flyscan params
    print(det_int_time,nframes)

    #Spawn thread and look to the registered output for the "Armed" state
    print("Is it armed?", "Armed" in psa.Spec.reg_channels['output/tty'].read())

    #Check for time to fill. if there is < current det_int_time*nframes + 10s (buffer), wait the durration of the fill + 15s
    ttf = psa.GetTimeToFill()
    iclow = psa.GetIntensity()

    print(f'Time to fill: {ttf} seconds')
    while (ttf < det_int_time/1000*nframes+3) or (iclow < 1.6):
        print(f'holding for {ttf + 1} seconds')
        time.sleep(1)
        ttf = psa.GetTimeToFill()
        iclow = psa.GetIntensity()

    #Set directory
    dirpath, calibpath = condtopath(status)
    padname = "frame"
    print("dirpath", dirpath)
    psa.SetFilePaths(dirpath, padname)

    time.sleep(1)
    process = Thread(target=psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
    process.start()
    while "Armed" not in psa.Spec.reg_channels['output/tty'].read():
        print("Calling status", psa.Spec.reg_channels['output/tty'].read())
        print("Waiting for armed")
        time.sleep(1)
        print("Not ready yet")

    #Code that executes the flyscan from lasgo
    laser.execute_flyscan(start, end, det_int_time, nframes)
    print("Calling status", psa.Spec.reg_channels['status/ready'].read())
    print("Waiting for my thread to finish")
    process.join()
    print("The PySpec thread has finished", counter)
    #time.sleep(1)

    #Check if files are written to DAQ in the appropriate folder:
    detstate = psa.Get_WO_status()
    while "Idle" not in detstate:
        detstate = psa.Get_WO_status()
        time.sleep(0.5)
        pass

    #Put integration into queue
    q.put((xrd_client, pos, cond, offset, dirpath, start[0], end[0], args.plotpath))
    print("Submitted worker", dirpath, counter)

    # wait until the thread terminates.

q.put(None)
print("Submitted all workers")
# wait until the thread terminates.
t.join()
#q.join()
print("Stopped XRD thread")

