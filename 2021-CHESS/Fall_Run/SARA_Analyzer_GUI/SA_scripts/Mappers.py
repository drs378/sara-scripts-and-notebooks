#Lists for storing image paths and coordinates from those image paths used to make maps
import glob
import numpy as np
import matplotlib.pyplot as plt
import os
import itertools
import sys
sys.path.insert(1,'/home/duncan/sara-scripts-and-notebooks/')
from SARA_cornell_funcs import dwellsort, MissingConditions, GenerateXmap, get_center, CenterFinding
import imageio as io
import h5py as h5
import multiprocessing
import PIL.Image as Image


def optWaferMap(imagespath,MatSysName,pathtoblank,filetype='.bmp',align_coords=[],Resolution=128):
    """This will generate a spatial wafer map given a directory of images.
    
    imagespath: (path) the directory containing all the .bmp files for a given system
    outputpath: (path) where you want the map to be stored
    MatSysName: (str) appends an identifying label to the output file
    pathtoblank: (path) an image to fill any missing conditions/blank space in a wafer map
    Resolution: (int) should be a typical image size (e.g. 128, 256, 512...)
    
    Future Edits: Needs axes on the overall image to show conditions
    
    Returns: a .png image of the wafer map

    """
    RowsForWaferMap = []
    coords = []
    gbs=pathtoblank
    fnamepaths=[]
    keys=[]
    for file in glob.glob(imagespath+'*'+filetype):
        filename = os.path.basename(file)[:-len(filetype)]
        s = filename.split('_')

        x = int(s[0][1:])
        y = int(s[1])
        tau = str(int(s[2]))
        T = str(int(s[3]))
        coords.append([x,y])
        key = f'tau_{tau}_T_{T}'
        keys.append(key)
        fnamepaths.append(file)

    coords = np.array(coords)    
    rangey = np.unique(coords[:,1])
    rangex = np.unique(coords[:,0])


    dimex = len(rangex)
    dimey = len(rangey)
    # if align_coords==False:
    #     print('we here')
    #     AlCo = np.array([[0,40],[0,-40],[40,0],[-40,0],[32,30],[32,-30],[-32,30],[-32,-30],[0,0]])
    # else:
    if align_coords!=[]:
        AlCo = np.array(align_coords)

        delcor = []
        for acx,acy in AlCo:
            delcor.append(np.where((coords[:,0]==acx)&(coords[:,1]==acy))[0][0])
        # print(len(coords))
        coords = np.delete(coords,delcor,axis=0)
        rmv_fnamepaths = [fnamepaths[idx] for idx in delcor]
        fnamepaths = [path for path in fnamepaths if path not in rmv_fnamepaths]


    test = list(itertools.chain(itertools.product(rangex, rangey)))
    test = sorted(test,key=lambda x: (x[1],x[0])) 
    
    # for t in test: 
    #     print(t)        
    # print('') 
    
    for idx,(x,y) in enumerate(test): 
        pad = np.where((x==coords[:,0]) & (y==coords[:,1]))[0] 
        if len(pad) > 0:
            RowsForWaferMap.append(fnamepaths[np.where((x==coords[:,0]) & (y==coords[:,1]))[0][0]]) 
        else:
            RowsForWaferMap.append(gbs)

    WaferMapImage = make_contact_sheet(RowsForWaferMap,(dimex,dimey),(Resolution,int(Resolution*2.5)),(1,1,1,1),1).transpose(Image.FLIP_LEFT_RIGHT)
    WaferMapImage = WaferMapImage.rotate(180)

    # WaferMapImage.save(outputpath+MatSysName+'_WaferMap.png')
    plt.figure()
    plt.imshow(WaferMapImage)
    plt.show()
    return WaferMapImage

def optTauT(sourcepath=None,outputpath=None,MatSysName=None,pathtoblank=None,filetype='.bmp',Resolution=128):
    """This will generate a dwell vs. Temperature map given a directory of images.
    
    sourcepath: (path) the directory containing all the .bmp files for a given system or the h5 file
    outputpath: (path) where you want the map to be stored
    MatSysName: (str) appends an identifying label to the output file
    pathtoblank: (path) an image to fill any missing conditions/blank space in a wafer map
    Resolution: (int) should be a typical image size (e.g. 128, 256, 512...)
    
    
    Future Edits: Needs axes on the overall image to show conditions
    
    Returns: a .png image of the tau vs. T map

    """
    #if not 625 stripes, need to find where 8 are missing in condition paths
    keys = []
    fnamepaths = []
    sortedFPs = []
    x=0
    for file in glob.glob(sourcepath+'*'+filetype):
        if 'background' not in file:
            filename = os.path.basename(file)[:-4]
#             print(filename)
            key = '_'
            s = filename.split('_')[2:]
            s[0] = str(int(s[0]))
            s[1] = str(int(s[1]))
            s.insert(0,'tau')
            s.insert(2,'T')
            key = key.join(s) 
            keys.append(key)
            fnamepaths.append(file)

    allkeys = dwellsort(keys+MissingConditions(keys))
    coords = []
    
    tau=[]
    T=[]

    for i in allkeys:
        conds = '_'
        s = i.split('_')
        tau.append(s[1])
        T.append(s[3])
#     print(allkeys)
    dimex=len(np.unique(tau))
    dimey=len(np.unique(T))

    for i in allkeys:
        dw = i.split('_')[1].zfill(5)
        T = i.split('_')[3].zfill(4)

        if i in keys:
            file = glob.glob([j for j in fnamepaths if (dw in j and T+filetype in j)][0])[0]
        else:
            file = glob.glob(pathtoblank)[0]

        filename = os.path.basename(file)[:-4]
        sortedFPs.append(file)
    grid = np.reshape(sortedFPs,(dimex,dimey))
    sortedFPs = np.flipud(grid).ravel()
#     print(len(sortedFPs))
    inew = make_contact_sheet(sortedFPs,(dimex,dimey),(Resolution,Resolution),(0,0,0,0),0)
    plt.figure(figsize=(12,10))
    plt.imshow(inew)
    plt.show()
    # inew.save(outputpath+MatSysName+'_tauvsT.png')
    return inew

def make_contact_sheet(sourcepath=None,rowscols=(0,0),photo_dimensions=(128,128),margins=(0,0,0,0),padding=2,H5type=False):
    """
    Make a contact sheet from a group of filenames:

    sourcepath   A list of names of the image files or the h5 data structure
    
    ncols        Number of columns in the contact sheet
    nrows        Number of rows in the contact sheet
    photow       The width of the photo thumbs in pixels
    photoh       The height of the photo thumbs in pixels

    marl         The left margin in pixels
    mart         The top margin in pixels
    marr         The right margin in pixels
    marl         The left margin in pixels

    padding      The padding between images in pixels

    returns a PIL image object.
    """
    ncols,nrows = [i for i in rowscols]
    photow,photoh = [i for i in photo_dimensions]
    marl,mart,marr,marb = [i for i in margins]
    # Read in all images and resize appropriately

    if H5type:
        imgs = []
        data = h5.File(sourcepath,'r')
        conds = list(data['exp'])
        for cond in conds:
            xmap = []
            for i in sorted(list(data['exp'][cond]),key=int):
                xmap.append(data['exp'][cond][i]['integrated_1d'][1])
            xmap = np.array(xmap)
            xmap = xmap/np.max(xmap)*255
            # print(np.min(xmap),np.max(xmap))
            # 
            imgs.append(Image.fromarray(np.uint8(xmap).T).resize((photow,photoh)))   
    else:
        imgs = [Image.open(fn).resize((photow,photoh)) for fn in sourcepath]
    # print(imgs)
    # Calculate the size of the output image, based on the
    #  photo thumb sizes, margins, and padding
    marw = marl+marr
    marh = mart+ marb

    padw = (ncols-1)*padding
    padh = (nrows-1)*padding
    isize = (ncols*photow+marw+padw,nrows*photoh+marh+padh)

    # Create the new image. The background doesn't have to be white
    if H5type:
        white = (225,255,255)
        inew = Image.new('RGB',isize,white)
    else:
        white = (255,255,255)
        inew = Image.new('RGB',isize,white)

    # Insert each thumb:
    for irow in range(nrows):
        for icol in range(ncols):
            left = marl + icol*(photow+padding)
            right = left + photow
            upper = mart + irow*(photoh+padding)
            lower = upper + photoh
            bbox = (left,upper,right,lower)
            try:
                img = imgs.pop(0)
            except:
                break
            # print(img,bbox)
            # print(inew)
            inew.paste(img,bbox)
    return inew

def xrdTauT(sourcepath=None,outputpath=None,MatSysName=None,pathtoblank=None,Resolution=128):
    """Generates a tau vs T plot of XRD data
    
    sourcepath = path to the h5 file 
    outpath = where the file will get written to 
    MatSysName = save name of the tau t map
    pathtoblank = filler image for missing conditions
    resolution = downbinned pixels per axis
    """

    data = h5.File(sourcepath, 'r')['exp']
    keys = dwellsort(list(data))
    allkeys = dwellsort(keys+MissingConditions(keys))

    coords = []
    taus=[]
    Tmaxs=[]
    for i in allkeys:
        s = i.split('_')
        taus.append(s[1])
        Tmaxs.append(s[3])
    dimex = len(np.unique(taus))
    dimey = len(np.unique(Tmaxs))

    inew = make_contact_sheet(sourcepath=sourcepath,rowscols=(dimex,dimey),photo_dimensions=(Resolution,Resolution),margins=(0,0,0,0),padding=2,H5type=True)
    plt.figure(figsize=(12,10))
    plt.imshow(inew)
    plt.show()
    return inew

def xrdWaferMap(sourcepath=None,MatSysName=None,pathtoblank=None,align_coords=[],Resolution=128):
    """This will generate a spatial wafer map given a directory of images.
    
    sourcepath: (path) the h5 file containing all the data for a given system
    MatSysName: (str) appends an identifying label to the output file
    pathtoblank: (path) an image to fill any missing conditions/blank space in a wafer map
    align_coords: (list of tuples) where the alignment pads are
    Resolution: (int) should be a typical image size (e.g. 128, 256, 512...)
    
    Future Edits: Needs axes on the overall image to show conditions
    
    Returns: a .png image of the wafer map

    """
    data = h5.File(sourcepath, 'r')
    keys = dwellsort(list(data['exp']))
    # allkeys = dwellsort(keys+MissingConditions(keys))
    keysort=[]
    xs = []
    ys = []
    pos = []
    for i in keys:
        xs.append(int(data['exp'][i].attrs['x_center']))
        ys.append(int(data['exp'][i].attrs['y_center']))
        pos.append((int(data['exp'][i].attrs['x_center']),int(data['exp'][i].attrs['y_center'])))
        keysort.append(i)
    for i in range(len(pos)):
        print(pos[i])
        print(keysort[i])
        print('')
    dimex = len(np.unique(xs))
    dimey = len(np.unique(ys))

    ncols,nrows = dimex,dimey
    photow,photoh = Resolution,Resolution
    marl,mart,marr,marb = [1,1,1,1]
    marw = marl+marr
    marh = mart+ marb

    padw = (ncols-1)*2
    padh = (nrows-1)*2
    isize = (ncols*photow+marw+padw,nrows*photoh+marh+padh)
    white = (255,255,255)


    rangex = np.unique(xs)
    rangey = np.unique(ys)

    xysort = sorted(list(itertools.chain(itertools.product(rangex, rangey))),key=lambda x: (x[1],x[0])) 

    a_sort=[]
    for idx,(x,y) in enumerate(xysort): 
        if (x,y) in pos:
            xmap = []

            ta = np.where((xs == x) & (ys == y))[0][0]
            cond = keysort[ta]

            for i in sorted(list(data['exp'][cond]),key=int):
                xmap.append(data['exp'][cond][i]['integrated_1d'][1])
            xmap = np.array(xmap)
            xmap = xmap/np.max(xmap)*255

            a_sort.append(Image.fromarray(np.uint8(xmap).T).resize((photow,photoh)))
        else:
            a_sort.append(Image.open(pathtoblank).resize((photow,photoh)))

    
    inew = Image.new('RGB',isize,white)

    # Insert each thumb:
    for irow in range(nrows):
        for icol in range(ncols):
            left = marl + icol*(photow+2)
            right = left + photow
            upper = mart + irow*(photoh+2)
            lower = upper + photoh
            bbox = (left,upper,right,lower)
            try:
                img = a_sort.pop(0)
            except:
                break
            # print(img,bbox)
            # print(inew)
            inew.paste(img,bbox)
    
    # WaferMapImage = inew.transpose(Image.FLIP_LEFT_RIGHT)
        
    # WaferMapImage = WaferMapImage.rotate(180)
    plt.figure()
    plt.imshow(inew)
    plt.show()

    # return WaferMapImage


if __name__ == "__main__":

    pth5 = '/home/duncan/Documents/CHESS2021_H5s/17_20F68_Bi-Ta-O.h5'
    ptims = '/home/duncan/Documents/Data/19F73_Bi-Ti-O/Images/IR/'
    ptfiller = '/home/duncan/Desktop/filler_img.png'
    
    q = multiprocessing.Queue()

    P1 = multiprocessing.Process(target=xrdTauT,args=[pth5,None,None,ptfiller,128])
    P2 = multiprocessing.Process(target=optTauT,args=[ptims,None,'test',ptfiller,'.bmp',128])
    P3 = multiprocessing.Process(target=optWaferMap,args=[ptims,None,ptfiller,'.bmp',[],128])
    P4 = multiprocessing.Process(target=xrdWaferMap,args=[pth5,None,ptfiller,[],128])

    P1.start()
    P2.start()
    P3.start()
    P4.start()
    