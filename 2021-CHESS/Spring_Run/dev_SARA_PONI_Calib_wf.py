#!/usr/bin/env python3
import sys
from pathlib import Path
import argparse
home = str(Path.home())
sys.path.insert(1, f'{home}/sara-socket-client/CHESS2021')
sys.path.insert(1, f'{home}/sara-socket-client/Scripts')
sys.path.insert(1, f'{home}/sara-socket-client')
sys.path.insert(1, f'{home}/sara-scripts-and-notebooks')
import yaml
import json
import os
import copy as cp
import time
import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
import laser as LSA_Laser
import logging
import logging.config
import socket_clients as sc
from PySpecAgent import PySpecAgent
from LSAtoAnalysisZone import LSAtoAnalysis_Zone as lsa2xrd
from CondtoPath import condtopath, auxpath
from threading import Thread
import h5tolinescan as h5tols

def parse():
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-d',    '--daqpath',      type=str,                help="Absolute path to the DAQ file system where spec writes to")
    parser.add_argument('-a',    '--auxpath',      type=str,                help="Absolute path to the Aux file system where all processed data should be written")
    parser.add_argument('-m',    '--matsysname',     type=str,              help="Wafer identifying ID ex: 01_20F7_Bi2O3")
    args = parser.parse_args()
    return args

class CurrentState():
    """
    Contains the current status of the stage.
    Used to pass parameters between the agents.
    """
    def __init__(self):
        self.image_error = True
        self.pos = [6., 25.]
        self.cond = [0., 0.]
        self.detector_info = {}
        self.directory = "/"


# parse args and establish directories
args = parse()
print(args)
daq = args.daqpath
aux = args.auxpath
MatSysName = args.matsysname

wd = daq+MatSysName+'/'
ad = aux+MatSysName+'/'
print(f'DAQ dir: {daq}')
print(f'AUX dir: {aux}')
print(f'MatSysName: {MatSysName}')
print(f'working dir: {wd}')

print('')
#Setup the SARA client info
status = CurrentState()
status.directory = wd

_,calibpath = condtopath(status)
print('Calibration daq read/write path')
print(calibpath)
_,auxcalib = auxpath(status,auxprefix=ad)
print('')
print('Calibration aux read/write path')
print(auxcalib)

# Setup the sockets
# address = "CHESS"
# socket_list = ["lasgo"] 
# socket_clients = sc.socket_clients()
# clients = socket_clients.get_clients(address, socket_list = socket_list)

# # Instantiate the laser connection
# laser = LSA_Laser.laser()
# laser.socket_clients = socket_clients

# #connect to the Spec Server
# psa = PySpecAgent()
# psa.ConnectToServer()

# Alignment pad locations: 18 scans in total. 2 at each alignment pad one horizontal 'h-calib' and one vertical 'v-calib'
AlignmentPadLocs  = np.array([
	#The vertical scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],

	#The horizontal scans
	[+00., +00.],
	[+00., +40.],
	[+32., +30.],
	[+40., +00.],
	[+32., -30.],
	[+00., -40.],
	[-32., -30.],
	[-40., +00.],
	[-32., +30.],
])


#Data collection cycle
for idx, padloc in enumerate(AlignmentPadLocs):
    #pass position info to SARA status
    status.pos = (padloc[0],padloc[1])
    print('')
    print(status.pos)
    print('')
    #iterate through the horizontal and vertical scans at each location and set the SARA status, flyscan coords, naming convention, and integration params
    if idx<9:
	#Horizontal alignment scans 2 mm long
        padname = f'h-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
        start = (status.pos[0] - 1.0 - 0.55, status.pos[1] + 1.3) #hscan start for the pad is -0.55 mm from pad origin and 1 mm before that 
        end = (status.pos[0] + 1.0 - 0.55, status.pos[1] + 1.3)   #hscan end for the pad is -0.55 mm from pad origin and 1 mm past that 
        print(padname)
        print(start,end)
        print('')
	#Set flyscan params
        det_int_time = 50 #in milliseconds
        nframes = 201 #int frames


        

    else:
	# Vertical alignment scans 5 mm long
        padname = f'v-calib_pad_{int(padloc[0])}_{int(padloc[1])}'
        start = (status.pos[0] - 0.55, status.pos[1]- 2.0 + 1.3) #vscan start for the pad is 1.3 mm from pad origin and 2.5 mm before that 
        end = (status.pos[0] - 0.55, status.pos[1] + 2.0 + 1.3)   #hscan end for the pad is 1.3 mm from pad origin and 2.5 mm past that 
        print(padname)
        print(start,end)
		
        print('')
	#Set flyscan params
        det_int_time = 50 #in seconds
        nframes = 501 #int frames
   
    #Execution loop
    psa.SetFilePaths(calibpath,padname)


    #Spawn thread and look to the registered output for the "Armed" state
    print("Is it armed?", "Armed" in psa.Spec.reg_channels['output/tty'].read())
    time.sleep(4)
    process = Thread(target=psa.SetFlyScan, args=[nframes, det_int_time/1000]) #spec takes an input of seconds for integration time, Mike likes milliseconds
    process.start()
    while "Armed" not in psa.Spec.reg_channels['output/tty'].read():
        print("Calling status", psa.Spec.reg_channels['output/tty'].read())
        print("Not ready yet")


#Code that executes the flyscan from lasgo
    laser.execute_flyscan(start, end, det_int_time, nframes)
    print("Calling status", psa.Spec.reg_channels['status/ready'].read())
    print("Waiting for my thread to finish")
    process.join()
    print("The PySpec thread has finished")

    #Code that executes the flyscan from lasgo 
    laser.execute_flyscan(start, end, det_int_time, nframes)


    # #run the linescan integration
    # psa.mkdir_spec(auxcalib)
    # h5tols.linescan_extract(fdir=calibpath+padname+'_001', outdir=auxcalib, start=start, end=end, onescan=True, save=True)

#Mings Code here to find centers and fit offset map