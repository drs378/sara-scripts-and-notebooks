from MCBL import MCBL_UGM 
import json
import numpy as np

with open('../analysis-codes/json/MoO_19F97.json') as f:
    d = np.array(json.load(f))

mcbl = MCBL_UGM(d[0],d, 4, 3)
print(d.shape)
